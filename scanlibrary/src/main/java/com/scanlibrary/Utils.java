package com.scanlibrary;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Utils {

    private static final String TAG = "Utils";

    private Utils() {

    }


    // convert bitmap to some shareable file and return its URI TODO: make this a file provider instead sharing a direct path so the scanner can be external app later
    public static Uri getUri(Context context, Bitmap bitmap, int jpgQuality) throws Exception{
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "scan" + timeStamp + ".jpg";
        File storageDir = new File(context.getCacheDir(), "scans");
        storageDir.mkdirs();
        File image = new File(storageDir, imageFileName);
        FileOutputStream fos = new FileOutputStream(image);
        //ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, jpgQuality, fos);
        fos.close();
        Uri intentURI =Uri.fromFile(image); //FileProvider.getUriForFile(context, "com.crayonic.sigma.scan.provider", image);
        //String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
        return intentURI;// Uri.parse(image.getAbsolutePath());
    }

    public static Bitmap getBitmap(Context context, Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        return bitmap;
    }

    public static void deleteFromGalleryIndex(Context context,String path ){
        // Set up the projection (we only need the ID)
        String[] projection = { MediaStore.Images.Media._ID };

// Match on the file path
        String selection = MediaStore.Images.Media.DATA + " = ?";
        String[] selectionArgs = new String[] { path };

// Query for the ID of the media matching the file path
        Uri queryUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        ContentResolver contentResolver = context.getContentResolver();
        Cursor c = contentResolver.query(queryUri, projection, selection, selectionArgs, null);
        if (c.moveToFirst()) {
            // We found the ID. Deleting the item via the content provider will also remove the file
            long id = c.getLong(c.getColumnIndexOrThrow(MediaStore.Images.Media._ID));
            Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
            contentResolver.delete(deleteUri, null, null);
        } else {
            // File not found in media store DB
        }
        c.close();
    }


    // max size in pixels (which ever is bigger : width or height)
    public static Bitmap getImageWithMaxSize(Context context, Uri uri, int maxSize) throws  IOException{
        InputStream input = context.getContentResolver().openInputStream(uri);
        if(input == null){
            throw new IOException("Cannot find image at URI:" + uri);
        }

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither=true;//optional
        onlyBoundsOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        if(maxSize == 0 ){
            Log.w(TAG,"Max Size is ZERO - No scaling!!!");
        }
        double ratio = (originalSize > maxSize && maxSize != 0) ? (originalSize / maxSize) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither=true;//optional
        bitmapOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        input = context.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio){
        int k = Integer.highestOneBit((int)Math.floor(ratio));
        if(k==0) return 1;
        else return k;
    }


    public static long getFileSizeFromUri(Context context, Uri uri){
        /*
     * Get the file's content URI from the incoming Intent,
     * then query the server app to get the file's display name
     * and size.
     */

        if(uri.getScheme().startsWith("file")){
            File f = new File(uri.getPath());
            return f.length();
        }

        Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
    /*
     * Get the column indexes of the data in the Cursor,
     * move to the first row in the Cursor, get the data,
     * and display it.
     */
        if(returnCursor == null){
            return  0;
        }
      //  int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();

        //returnCursor.getString(nameIndex);
        return returnCursor.getLong(sizeIndex);
    }
}