package com.scanlibrary;

import android.os.Environment;

public class ScanConstants {

    public final static String ACTION_SCAN_TO_PDF = "com.crayonic.sigma.scanner.SCAN_TO_PDF";


    public final static int START_CAMERA_REQUEST_CODE = 2;
    public final static String IMAGE_PATH = Environment.getExternalStorageDirectory().getPath() + "/scanSample";

// COMMON INTENT EXTRAS CODES
    public static final String URL_DOWNLOAD = "WebIntent_url_download";
    public static final String URL_UPLOAD = "WebIntent_url_upload";
    public static final String SIGNATORY_JSON = "WebIntent_sigjson";
    public static final String CALLBACK_URL = "WebIntent_callbackurl";
    public static final String DOC_FORMAT = "WebIntent_docformat";
    public static final String PAGES_TO_SCAN = "WebIntent_pagestoscan";
    public static final String SCAN_COLOR = "WebIntent_scancolor";
    public static final String DOC_NAME = "WebIntent_docname";
    public static final String AES_KEY = "WebIntent_key";
    public static final String SHOW_QR = "WebIntent_showqr";
    public static final String CONTENT ="WebIntent_content" ;
    public static final String IDENTITY = "WebIntent_identity";
    public static final String USERNAME = "WebIntent_username";
    public static final String PWD = "WebIntent_pwd" ;

    public static final String UI_PRIMARY_LIGHT = "UI_PRIMARY_LIGHT";
    public static final String UI_PRIMARY_DARK = "UI_PRIMARY_DARK";
    public static final String UI_DIVIDER = "UI_DIVIDER";
    public static final String UI_PRIMARY_TEXT =  "UI_PRIMARY_TEXT";
    public static final String UI_SECONDARY_TEXT =  "UI_SECONDARY_TEXT";
    public static final String UI_PRIMARY = "UI_PRIMARY";
    public static final String UI_ACCENT = "UI_ACCENT";
    public static final String UI_FONT_PATH = "UI_FONT_PATH";


    public static final String SCAN_SIZE = "WebIntent_scansize";
    public static final String SCAN_QUALITY = "WebIntent_scanquality";
    public static final String SCAN_CROP = "WebIntent_scancrop" ;
}
