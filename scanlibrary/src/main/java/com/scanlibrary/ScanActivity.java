package com.scanlibrary;

import android.Manifest;
import android.app.Activity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;


public class ScanActivity extends Activity {
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 100;
    private static final int MAX_SCAN_SIZE_PX = 2048;

    private static String TAG = "ScanActivity";

    private static final String SERVICE_CLASS = "com.crayonic.sigma.services.Scans2PDFService";  // name of class of running service to post scan images to
    private static final String CALLBACK_CLASS = "com.crayonic.sigma.pdf.SigmaActivity";         // name of class to call when done scanning
    private static final String CALLING_PACKAGE = "com.crayonic.sigma";     // name of package to which both above classes belong to

    private Button cancelButton;
    private Button finishButton;  // same as back back button
    private Button addAndScanButton;
    private ImageView sourceImageView;
    private TextView pageCounter;
    private FrameLayout sourceFrame;
    private PolygonView polygonView;
    private Bitmap original;
    private Context mContext;
    private ProgressDialog progress;


    // private final String tmpScanFilePath = Environment.getExternalStorageDirectory().getPath() + "/scanSample/scan.jpg";
    private Uri tmpScanfileUri; // for testing = Uri.fromFile(new File(tmpScanFilePath));
    private String mColorspace, mFormat;
    private int mPageCount;
    private int mCurrentPageCount;
    private boolean mhaveImageFromCamera;
    private boolean mReturningFromCamera;

    private ArrayList<Uri> mImageUris;
    private int mScanSize;
    private int mJPGQuality;
    private String mCropSetTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        progress = null;
        mCurrentPageCount = 0;
        // restore tmpScanfileUri if we got killed
        if (savedInstanceState != null) {
            String pp = savedInstanceState.getString("photoPath");
            if (pp != null) tmpScanfileUri = Uri.parse(pp);
        }

        setContentView(R.layout.scan_layout);
        init();

        // bind to PDF creation service
        doBindService();

    }

    private boolean hasScanPermissions() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Ext Storage");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Ext Storage");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Use Camera");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(ScanActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return false;
            }
            ActivityCompat.requestPermissions(ScanActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return false;
        }

        return true;


    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(ScanActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(ScanActivity.this, permission))
                return false;
        }
        return true;
    }

    private void init() {

        if (getIntent().hasExtra(ScanConstants.UI_FONT_PATH)) {
            // if custom UI font then replace SANS_SERIF with it
            String fontPath = getIntent().getStringExtra(ScanConstants.UI_FONT_PATH);
            File test = new File(fontPath);
            if (test.exists()) {
                Log.i(TAG, "Loading font: " + fontPath);
                setDefaultFont("SANS_SERIF", fontPath);  // replaces Sans Serif font with given TTF file
            }

        }
        sourceImageView = (ImageView) findViewById(R.id.sourceImageView);
        cancelButton = (Button) findViewById(R.id.cancelButton);
        if (getIntent().hasExtra(ScanConstants.UI_PRIMARY))
            cancelButton.setBackgroundColor(getIntent().getIntExtra(ScanConstants.UI_PRIMARY, 0));
        if (getIntent().hasExtra(ScanConstants.UI_PRIMARY_TEXT))
            cancelButton.setTextColor(getIntent().getIntExtra(ScanConstants.UI_PRIMARY_TEXT, 0));
        cancelButton.setOnClickListener(new CancelButtonListener());

        finishButton = (Button) findViewById(R.id.finishButton);
        if (getIntent().hasExtra(ScanConstants.UI_ACCENT))
            finishButton.setBackgroundColor(getIntent().getIntExtra(ScanConstants.UI_ACCENT, 0));
        if (getIntent().hasExtra(ScanConstants.UI_PRIMARY_TEXT))
            finishButton.setTextColor(getIntent().getIntExtra(ScanConstants.UI_PRIMARY_TEXT, 0));
        finishButton.setOnClickListener(new FinishButtonClickListener());

        addAndScanButton = (Button) findViewById(R.id.addAndScanButton);
        if (getIntent().hasExtra(ScanConstants.UI_ACCENT))
            addAndScanButton.setBackgroundColor(getIntent().getIntExtra(ScanConstants.UI_ACCENT, 0));
        if (getIntent().hasExtra(ScanConstants.UI_PRIMARY_TEXT))
            addAndScanButton.setTextColor(getIntent().getIntExtra(ScanConstants.UI_PRIMARY_TEXT, 0));

        addAndScanButton.setOnClickListener(new addAndScanButtonClickListener());

        sourceFrame = (FrameLayout) findViewById(R.id.sourceFrame);
        polygonView = (PolygonView) findViewById(R.id.polygonView);

        TextView pageCounterText = (TextView) findViewById(R.id.pageCounterText);
        RelativeLayout pageCountDivider = (RelativeLayout) findViewById(R.id.pageCountLayout);

        pageCounter = (TextView) findViewById(R.id.pageCounter);
        if (getIntent().hasExtra(ScanConstants.UI_SECONDARY_TEXT)) {
            pageCounter.setTextColor(getIntent().getIntExtra(ScanConstants.UI_SECONDARY_TEXT, 0));
            pageCounterText.setTextColor(getIntent().getIntExtra(ScanConstants.UI_SECONDARY_TEXT, 0));
        }

        if (getIntent().hasExtra(ScanConstants.UI_DIVIDER)) {
            pageCountDivider.setBackgroundColor(getIntent().getIntExtra(ScanConstants.UI_DIVIDER, 0));
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Log.i(TAG, "Req code: " + requestCode);
        mReturningFromCamera = true;

        long len = Utils.getFileSizeFromUri(mContext, tmpScanfileUri);
        if (len > 0) {
            mCurrentPageCount += 1;
            new getBitmapAsyncTask().execute();
            mhaveImageFromCamera = true;
        } else {
            mhaveImageFromCamera = false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Initial
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    // openCamera();
                } else {
                    // Permission Denied
                    Toast.makeText(ScanActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);  --> android bug????  http://stackoverflow.com/questions/7469082/getting-exception-illegalstateexception-can-not-perform-this-action-after-onsa/10261438#10261438
        if (tmpScanfileUri != null) outState.putString("photoPath", tmpScanfileUri.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePageCounter();

        //finishButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_can));
        // cancelButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_box_white_36dp));
        //retakeButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_redo_white_36dp));
//        if (progress == null) {
//            progress = ProgressDialog.show(mContext, getString(R.string.settingup), "", true);
//        } else {
//            if (!progress.isShowing()) {
//                progress.show();
//            }
//        }

        Intent i = getIntent();
        dumpIntent(i); //FIXME

        // try getting intent data if none set and are available
        if (this.mColorspace == null) {
            this.mColorspace = i.hasExtra(ScanConstants.SCAN_COLOR) ? i.getExtras().getString(ScanConstants.SCAN_COLOR) : null;
            Log.i(TAG, "Scanning in colorspace:"  +mColorspace);
        }
        if (this.mFormat == null) {
            this.mFormat = i.hasExtra(ScanConstants.DOC_FORMAT) ? i.getExtras().getString(ScanConstants.DOC_FORMAT) : null;
            Log.i(TAG, "Scanning in format:"  + mFormat);
        }
        if (this.mPageCount == 0) {
            this.mPageCount = i.hasExtra(ScanConstants.PAGES_TO_SCAN) ? i.getExtras().getInt(ScanConstants.PAGES_TO_SCAN) : 0;
            Log.i(TAG, "# of Pages to scan:"  + mPageCount);
        }

        if (this.mScanSize == 0) {
            this.mScanSize = i.hasExtra(ScanConstants.SCAN_SIZE) ?  i.getExtras().getInt(ScanConstants.SCAN_SIZE) : MAX_SCAN_SIZE_PX;
            Log.i(TAG, "Scanning to max size in px:"  +mScanSize);
        }

        if (this.mJPGQuality == 0) {
            this.mJPGQuality = i.hasExtra(ScanConstants.SCAN_QUALITY) ?  i.getExtras().getInt(ScanConstants.SCAN_QUALITY) : 100;
            Log.i(TAG, "Scanning in JPG quality:"  +mJPGQuality);
        }

        if (this.mCropSetTo == null) {
            this.mCropSetTo = i.hasExtra(ScanConstants.SCAN_CROP) ?  i.getExtras().getString(ScanConstants.SCAN_CROP) : "auto";
            Log.i(TAG, "Scanning Crop option:"  +mCropSetTo);
        }



        // check for image sent by intent
        if ((Intent.ACTION_SEND.equals(i.getAction()) || Intent.ACTION_VIEW.equals(i.getAction())) && i.getType() != null) {
            if (i.getType().startsWith("image/")) {
                // Handle single image being sent
                tmpScanfileUri = (Uri) i.getParcelableExtra(Intent.EXTRA_STREAM);
                long len = Utils.getFileSizeFromUri(mContext, tmpScanfileUri);
                if (len > 0) {
                    mCurrentPageCount += 1;
                    updatePageCounter();
                    new getBitmapAsyncTask().execute();
                    return;
                }
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(i.getAction()) && i.getType() != null) {
            if (i.getType().startsWith("image/")) {
                // Handle multiple images being sent
                mImageUris = i.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
                if (mImageUris != null && !mImageUris.isEmpty()) {
                    // Update UI to reflect multiple images being shared
                    // Handle first image being sent
                    tmpScanfileUri = mImageUris.remove(0);  //pop out first image to work on
                    long len = Utils.getFileSizeFromUri(mContext, tmpScanfileUri);
                    if (len > 0) {
                        mCurrentPageCount += 1;
                        updatePageCounter();
                        new getBitmapAsyncTask().execute();
                        return;
                    }
                }
            }
        }

        if (hasScanPermissions()) {
            if (!mhaveImageFromCamera && !mReturningFromCamera) {
                openCamera();
            } else if (!mhaveImageFromCamera && mReturningFromCamera) {
                //  means cancel pressed in the camera
                if (mCurrentPageCount == 0) {
                    onScanFinish();  // no file or already scanned pages then and returning from camera means exit scanner
                }
            }
        }
    }

    private void updatePageCounter() {
        if (mPageCount > 0)
            pageCounter.setText(mCurrentPageCount + "/" + mPageCount);
        else
            pageCounter.setText(mCurrentPageCount + "");
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    public void openCamera() {
        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            Uri intentURI = FileProvider.getUriForFile(this, "com.crayonic.sigma.scan.provider", createImageFile());
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, intentURI);
            startActivityForResult(cameraIntent, ScanConstants.START_CAMERA_REQUEST_CODE);
        } catch (IOException e) {
            e.printStackTrace(); // cannot create file
        }
    }


    private class getBitmapAsyncTask extends AsyncTask<Void, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                return getBitmap(tmpScanfileUri);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null) {
                setBitmap(bitmap); // start image processing fragment

                if (progress != null && progress.isShowing()) {
                    progress.dismiss();
                    progress = null;
                }
            }
        }
    }


    private Bitmap getBitmap(Uri selectedimg) throws IOException {
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inSampleSize = 2; // downsample to half scale to save memory
//        options.inPreferQualityOverSpeed = true;

//        AssetFileDescriptor fileDescriptor = null;
//        fileDescriptor = getContentResolver().openAssetFileDescriptor(selectedimg, "r");
//        Bitmap original = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);


        Bitmap original = Utils.getImageWithMaxSize(mContext, selectedimg, mScanSize);

        return original;
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "scan" + timeStamp;
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "scans");
        storageDir.mkdirs();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        tmpScanfileUri = Uri.parse("file:" + image.getAbsolutePath());
        Log.i(TAG, "Scanning into temp file:" + tmpScanfileUri.getEncodedPath());
        return image;
    }


    public Void onScanRetake() {
        openCamera();
        return null;
    }

    // add one page to scanned doc
    public void onScanAdd(Uri uri) {
        if (mIsBound) {
            if (mService != null) {
                // first remove scan file from external cache and all references to it
                String path = tmpScanfileUri.getEncodedPath();// only returns real path for files created by our scanner thus it cannot delete external image files!!!
                if(path != null) {
                    File f = new File(path);
                    if (f.delete()) // remove scan now and if success then remove index (only for locally taken photos)
                    {
                        Utils.deleteFromGalleryIndex(mContext, path);
                    }
                }
                tmpScanfileUri = null;
                original.recycle();
                original = null;
                //...and  send out message with a new URI to processed image
                try {
                    path = uri.getEncodedPath(); //getRealPathFromURI(this, uri);
                    Bundle b = new Bundle();

                    b.putString(BUNDLE_SCAN, path);
                    Message msg = Message.obtain(null, MSG_ADD_SCAN);
                    msg.setData(b);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                }
            }else{
                Log.e(TAG, "No service to add scan to!!!");
            }
        }
    }

    // finish scanned doc
    public Void onScanFinish() {
        // send finish scanning request to PDF scan service
        if (mIsBound) {
            if (mService != null) {
                progress = ProgressDialog.show(mContext, getString(R.string.finishing), "", true);
                try {
                    Message msg = Message.obtain(null, MSG_FINISH_SCANNING);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                }
            }
        }
        return null;
    }

    //----------------------------------------------------

    private void setBitmap(Bitmap original) {
        if (original.getHeight() < original.getWidth()) {
            // rotate bitmap so its longer then wider
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap rotated = Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight(), matrix, true);
            original.recycle();
            original = rotated;
            this.original = rotated;
        } else {
            this.original = original;
        }
        Bitmap scaledBitmap = scaledBitmap(original, sourceFrame.getWidth(), sourceFrame.getHeight());
        sourceImageView.setImageBitmap(scaledBitmap);
        Bitmap tempBitmap = ((BitmapDrawable) sourceImageView.getDrawable()).getBitmap();
        Map<Integer, PointF> pointFs = getEdgePoints(tempBitmap);
        polygonView.setPoints(pointFs);
        if (original.getHeight() > 10 && !mCropSetTo.equalsIgnoreCase("off"))
            polygonView.setVisibility(View.VISIBLE); // dont show polygon for "empty" bitmap
        else
            polygonView.setVisibility(View.GONE);

        int padding = (int) getResources().getDimension(R.dimen.scanPadding);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(tempBitmap.getWidth() + 2 * padding, tempBitmap.getHeight() + 2 * padding);
        layoutParams.gravity = Gravity.CENTER;
        polygonView.setLayoutParams(layoutParams);
    }

    private Map<Integer, PointF> getEdgePoints(Bitmap tempBitmap) {
        List<PointF> pointFs = getContourEdgePoints(tempBitmap);
        Map<Integer, PointF> orderedPoints = orderedValidEdgePoints(tempBitmap, pointFs);
        return orderedPoints;
    }

    private List<PointF> getContourEdgePoints(Bitmap tempBitmap) {
        float[] points = getPoints(tempBitmap);
        float x1 = points[0];
        float x2 = points[1];
        float x3 = points[2];
        float x4 = points[3];

        float y1 = points[4];
        float y2 = points[5];
        float y3 = points[6];
        float y4 = points[7];
        List<PointF> pointFs = new ArrayList<>();
        pointFs.add(new PointF(x1, y1));
        pointFs.add(new PointF(x2, y2));
        pointFs.add(new PointF(x3, y3));
        pointFs.add(new PointF(x4, y4));
        return pointFs;
    }

    private Map<Integer, PointF> getOutlinePoints(Bitmap tempBitmap) {
        Map<Integer, PointF> outlinePoints = new HashMap<>();
        outlinePoints.put(0, new PointF(0, 0));
        outlinePoints.put(1, new PointF(tempBitmap.getWidth(), 0));
        outlinePoints.put(2, new PointF(0, tempBitmap.getHeight()));
        outlinePoints.put(3, new PointF(tempBitmap.getWidth(), tempBitmap.getHeight()));
        return outlinePoints;
    }

    private Map<Integer, PointF> orderedValidEdgePoints(Bitmap tempBitmap, List<PointF> pointFs) {
        Map<Integer, PointF> orderedPoints = polygonView.getOrderedPoints(pointFs);
        if (!polygonView.isValidShape(orderedPoints)) {
            orderedPoints = getOutlinePoints(tempBitmap);
        }
        return orderedPoints;
    }


    private void addScan(Callable<Void> myFunc) {
        if (original != null) {

            Map<Integer, PointF> points = polygonView.getPoints();
            if (isScanPointsValid(points)) {

                polygonView.setVisibility(View.INVISIBLE);
                //            finishButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
                //   retakeButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_scanner_white_36dp));
                new ScanAsyncTask(points, myFunc).execute();
            }
//            else {
//                showErrorDialog();
//            }
        } else {
            try {
                myFunc.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private class CancelButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // just finish
            onScanFinish();
        }
    }

    private class FinishButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // add scan page and finish
            addScan(new Callable<Void>() {
                public Void call() {
                    return onScanFinish();
                }
            });
        }
    }

    private class addAndScanButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // add scan page and start taking another pic afterward or just used the next image from intent
            if (mImageUris != null && !mImageUris.isEmpty()) {
                addScan(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        tmpScanfileUri = mImageUris.remove(0); // pop another image....
                        long len = Utils.getFileSizeFromUri(mContext, tmpScanfileUri);
                        if (len > 0) {
                            mCurrentPageCount += 1;
                            updatePageCounter();
                            new getBitmapAsyncTask().execute();
                        }
                        return null;
                    }
                });
            } else {
                addScan(new Callable<Void>() {
                    public Void call() {
                        return onScanRetake();
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        // exit only on double back button hit or when not logged in instantly
        onScanFinish();
    }


    private boolean isScanPointsValid(Map<Integer, PointF> points) {
        return points.size() == 4;
    }

    private Bitmap scaledBitmap(Bitmap bitmap, int width, int height) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, width, height), Matrix.ScaleToFit.CENTER);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
    }

    private Bitmap getScannedBitmap(Bitmap original, Map<Integer, PointF> points) {
        int width = original.getWidth();
        int height = original.getHeight();
        float xRatio = (float) original.getWidth() / sourceImageView.getWidth();
        float yRatio = (float) original.getHeight() / sourceImageView.getHeight();
        float x1 = (points.get(0).x) * xRatio;
        float x2 = (points.get(1).x) * xRatio;
        float x3 = (points.get(2).x) * xRatio;
        float x4 = (points.get(3).x) * xRatio;
        float y1 = (points.get(0).y) * yRatio;
        float y2 = (points.get(1).y) * yRatio;
        float y3 = (points.get(2).y) * yRatio;
        float y4 = (points.get(3).y) * yRatio;
        Log.d("", "POints(" + x1 + "," + y1 + ")(" + x2 + "," + y2 + ")(" + x3 + "," + y3 + ")(" + x4 + "," + y4 + ")");
        Bitmap _bitmap = original;
        // crop only if allowed
        if(!mCropSetTo.equalsIgnoreCase("off")) {
            _bitmap = getScannedBitmap(original, x1, y1, x2, y2, x3, y3, x4, y4);
        }

        if (mColorspace != null) {
            switch (mColorspace) {
                case "BW":
                    return getBWBitmap(_bitmap);
                // break;
                case "GRAY":
                    return getGrayBitmap(_bitmap);
                // break;

                case "COLOR":
                default:
                    // just return the original
                    break;

            }
        }
        return _bitmap;
    }

    private class ScanAsyncTask extends AsyncTask<Void, Void, Bitmap> {

        private Map<Integer, PointF> points;
        private Callable<Void> afterScanF;

        public ScanAsyncTask(Map<Integer, PointF> points, Callable<Void> myFunc) {
            this.points = points;
            this.afterScanF = myFunc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(mContext, getString(R.string.addPage), "", true);

        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            return getScannedBitmap(original, points);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            // process bitmap and save as another JPG
            setBitmap(Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888));
            Uri uri = null;
            try {
                uri = Utils.getUri(mContext, bitmap, mJPGQuality);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Cannot create processed file from scanned bitmap");
            }
            bitmap.recycle();
            onScanAdd(uri);
            if (progress != null && progress.isShowing()) progress.dismiss();
            try {
                afterScanF.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /*-----------------------TEMPORARY Integration via Service -  This should be replaced by some plugin architecture-----------------------*/
    //FIXME
    // These constants have to be synced with Scans2PDFService.java constants
    public static final String PDF_FILE_PATH = "pdf";
    public static final String PDF_SCAN_COUNT = "count";
    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;
    static final int MSG_ADD_SCAN = 3;
    static final int MSG_FINISH_SCANNING = 4;
    static final String BUNDLE_SCAN = "scan";
    //--------------END of synced constants for messaging


    final Messenger mMessenger = new Messenger(new IncomingHandler());
    Messenger mService = null;
    boolean mIsBound;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_FINISH_SCANNING:  // recieve confirmation from service on finished PDF

//                    Intent intentMessage = new Intent();
//                    // put the message to return as result in Intent
//                    intentMessage.putExtra(PDF_FILE_PATH, msg.getData().getString(PDF_FILE_PATH));
//                    intentMessage.putExtra(PDF_SCAN_COUNT, msg.getData().getInt(PDF_SCAN_COUNT));
//                    // Send back to calling activity info from the scanning service
//                    setResult(0, intentMessage);

                    // call our callback (SigmaActivity) Activity

                    int count = msg.getData().getInt(PDF_SCAN_COUNT);
                    if (count != 0) {
                        // if scan created some pages then show them in PDF view
                        Intent intent = new Intent();
                        intent.setData(Uri.parse(msg.getData().getString(PDF_FILE_PATH)));
                        intent.setPackage(CALLING_PACKAGE);
                        intent.setComponent(new ComponentName(CALLING_PACKAGE, CALLBACK_CLASS));
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.putExtras(getIntent()); // copy all calling extras
                        intent.putExtra(PDF_SCAN_COUNT, count);
                        startActivity(intent);
                    }
                    finish();  //just quit activity if we received end of scan message
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            try {
                Message msg = Message.obtain(null, MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
                Log.e(TAG, "Scan service has crashed on connection from " + TAG);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
        }
    };


    void doBindService() {
        Intent intent = new Intent();
        intent.setPackage(CALLING_PACKAGE);
        intent.setComponent(new ComponentName(CALLING_PACKAGE, SERVICE_CLASS));
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }


    public static void dumpIntent(Intent i) {
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            Log.e("UTILS", ".............Dumping Intent start..............");
            Log.e("UTILS", "In object: ScanActivity.java");
            Log.e("UTILS", "Intent Action: " + i.getAction());
            Log.e("UTILS", "Intent Data: " + i.getDataString());
            while (it.hasNext()) {
                String key = it.next();
                Log.e("UTILS", "[" + key + " = " + bundle.get(key) + "]");
            }
            Log.e("UTILS", ".............Dumping Intent end..............");
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ScanActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    /*---------UI UTILS ---------------------------------------------------*/
    private void setDefaultFont(String staticTypefaceFieldName, String filePath) {
        final Typeface regular = Typeface.createFromFile(filePath);
        replaceFont(staticTypefaceFieldName, regular);
    }

    private static void replaceFont(final String staticTypefaceFieldName, final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    /*----------------------------Native Calls-----------------------------------*/

    public native Bitmap getScannedBitmap(Bitmap bitmap, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);

    public native Bitmap getGrayBitmap(Bitmap bitmap);

    public native Bitmap getMagicColorBitmap(Bitmap bitmap);

    public native Bitmap getBWBitmap(Bitmap bitmap);

    public native float[] getPoints(Bitmap bitmap);

    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("Scanner");
    }
}



