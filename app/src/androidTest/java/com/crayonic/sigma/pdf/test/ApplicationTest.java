package com.crayonic.sigma.pdf.test;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;

import com.crayonic.sigma.pdf.SigmaActivity;
//import com.crayonic.sigma.MainActivity;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfAppearance;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.TextField;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.xml.xmp.DublinCoreSchema;
import com.itextpdf.text.xml.xmp.PdfSchema;
import com.itextpdf.text.xml.xmp.XmpArray;
import com.itextpdf.text.xml.xmp.XmpSchema;
import com.itextpdf.text.xml.xmp.XmpWriter;
import com.robotium.solo.Solo;
import com.itextpdf.text.Document;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import junit.framework.Assert;

import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;


public class ApplicationTest extends ActivityInstrumentationTestCase2<SigmaActivity> {
    static {
        Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);
    }

    BouncyCastleProvider provider;

    private Solo solo;
    private static final String TEST_FILE_NAME_NEW = "unitTestPDF.pdf";
    private static final String TEST_FILE_NAME_SIGNED = "unitTestPDFSigned.pdf";

    public static final String signatureImageFile = "signature.png";
    public static final String signatureCert = "cert/certificate.crt";
    public static final String PKCS12CertAndKey = "cert/certificate.pfx";
    public static final String verifyRootCert = "cert/RootCertificate.crt";

    final String CURRENT_NAME_1 = "CurrentScreenshotFile_Page1";
    final String ORIGINAL_NAME_1 = "OriginalScreenshotFile_Page1";
    final String CURRENT_NAME_2 = "CurrentScreenshotFile_Page2";
    final String ORIGINAL_NAME_2 = "OriginalScreenshotFile_Page2";

    //!!!! ROBOTIUM Takes screenshots to SDCard folder /Robotium-Screenshots
    final File originalScreenFile1 = new File(Environment.getExternalStorageDirectory() + "/Robotium-Screenshots/", ORIGINAL_NAME_1 + ".jpg");
    final File currentScreenFile1 = new File(Environment.getExternalStorageDirectory() + "/Robotium-Screenshots/", CURRENT_NAME_1 + ".jpg");
    final File originalScreenFile2 = new File(Environment.getExternalStorageDirectory() + "/Robotium-Screenshots/", ORIGINAL_NAME_2 + ".jpg");
    final File currentScreenFile2 = new File(Environment.getExternalStorageDirectory() + "/Robotium-Screenshots/", CURRENT_NAME_2);

    final int TIMEOUT = 2000;

    public static final String PASSWORD = "test";

    public static final int  MAX_NUMBER_OF_PAGES = 50;

    int deviceWidth;
    int deviceHeight;

    Context applicationContext;
    Context instrumentationContext;

    private static String TAG = "ApplicationTest";

    public ApplicationTest() throws ClassNotFoundException {
        super(SigmaActivity.class);
    }

    static private File getTestFileHandle(String fileName ){
    // assumes mounted external storage
        File docPath = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS);


        File robotPath =  new File(docPath + "/Robotium/");
        robotPath.mkdirs();
        File fh = new File(robotPath, fileName);
        Log.w(TAG, "File handle test directory here:" + fh.getAbsolutePath());
        return fh;
    }

    @Override
    public void setUp() throws Exception {

        super.setUp();

        // get both contexts one for the running app and the other for instrumentation
        applicationContext = this.getInstrumentation().getTargetContext().getApplicationContext();
        instrumentationContext = this.getInstrumentation().getContext();

        //copy test.pdf from asset to sdcard of emulator
        //copyTestAssets();
        provider =  new BouncyCastleProvider();

        clearHistory();
        getDeviceProperty();

        createTestPDF();  // create new PDF with Acrofields
        PADESLTVwOCSPSignCert_noTSA(instrumentationContext); //Signed it with PADES LTV

        //set target file path to signed PDF
        String path = getTestFileHandle(TEST_FILE_NAME_SIGNED).getAbsolutePath();
        Uri uri = Uri.parse(path);

        // ...pass it to the view activity
        Intent intent = new Intent(getActivity().getApplicationContext(), SigmaActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(uri);
        setActivityIntent(intent);
        getActivity().startActivity(intent);

        // ...and start messing with it in the UI
        solo = new Solo(getInstrumentation(), getActivity());
        solo.waitForActivity(SigmaActivity.class);
        solo.sleep(TIMEOUT);

    }

    private void getDeviceProperty() {
        Display display = ((WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        deviceWidth = display.getWidth();
        deviceHeight = display.getHeight();
    }

    private void clearHistory() {
        //remove first page history image.
        if (currentScreenFile1.exists()) {
            currentScreenFile1.delete();
        }
        //remove second page history image.
        if (currentScreenFile2.exists()) {
            currentScreenFile2.delete();
        }

    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

    @SmallTest
    public void testStartRun() {
        //first page test
        if (originalScreenFile1.exists()) {
            Log.w(TAG, "Taking current screenshot page1.");
            solo.takeScreenshot(CURRENT_NAME_1);
        } else {
            Log.e(TAG, "No original screenshot of page 1 - creating one now.");
            solo.takeScreenshot(ORIGINAL_NAME_1);
        }
             //scroll to next page.
        final float fromX = deviceWidth / 2;
        float toX = fromX;
        final float fromY = deviceHeight / 2;
        float toY = deviceHeight / 4;
        solo.drag(fromX,toX,fromY,toY,1);

        solo.sleep(TIMEOUT);
        if (originalScreenFile2.exists()) {
            Log.w(TAG, "Taking current screenshot page2.");
            solo.takeScreenshot(CURRENT_NAME_2);
        } else {
            Log.e(TAG, "No original screenshot of page 2 - creating one now.");
            solo.takeScreenshot(ORIGINAL_NAME_2);
        }
        solo.sleep(TIMEOUT);
        compare();
    }

    public void compare() {
        assertTrue("First Image Original Exists",originalScreenFile1.exists());
        assertTrue("First Image Screenshot Exists",currentScreenFile1.exists());
        Log.d("ROBOTEST", originalScreenFile1.getAbsolutePath());

        if ((originalScreenFile1.exists()) && (currentScreenFile1.exists())) {
            assertTrue("First Image Matched", compareScreenShot(originalScreenFile1, currentScreenFile1));
        }

        if ((originalScreenFile2.exists()) && (currentScreenFile2.exists())) {
            assertTrue("Next Image Matched", compareScreenShot(originalScreenFile2, currentScreenFile2));
        }
    }

    //compare two jpeg files
    private boolean compareScreenShot(File file1, File file2) {
        try {
            Bitmap originalBmp = BitmapFactory.decodeFile(file1.getPath());
            Bitmap currentBmp = BitmapFactory.decodeFile(file2.getPath());

            int width1 = originalBmp.getWidth();
            int width2 = currentBmp.getWidth();
            int height1 = originalBmp.getHeight();
            int height2 = currentBmp.getHeight();
            if ((width1 != width2) || (height1 != height2)) {
                System.err.println("Error: Images dimensions mismatch");
                System.exit(1);
            }
            long diff = 0;
            for (int y = 0; y < height1; y++) {
                for (int x = 0; x < width1; x++) {
                    int rgb1 = originalBmp.getPixel(x, y);
                    int rgb2 = currentBmp.getPixel(x, y);
                    int r1 = (rgb1 >> 16) & 0xff;
                    int g1 = (rgb1 >> 8) & 0xff;
                    int b1 = (rgb1) & 0xff;
                    int r2 = (rgb2 >> 16) & 0xff;
                    int g2 = (rgb2 >> 8) & 0xff;
                    int b2 = (rgb2) & 0xff;
                    diff += Math.abs(r1 - r2);
                    diff += Math.abs(g1 - g2);
                    diff += Math.abs(b1 - b2);
                }
            }
            double n = width1 * height1 * 3;
            double p = diff / n / 255.0;
                //0 = < p < = 1
                //the smaller p is , the correctly images match
            return (p < 0.1);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
        //copy assets from assets holder to sdcard
    private void copyTestAssets() {
        AssetManager assetManager = instrumentationContext.getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");

        } catch (IOException e) {
            Log.e("tag", "Failed to get asset file list.", e);
        }

        for(String filename : files) {
            Log.d("SIGMA_TEST", filename);
            if (filename.compareTo(TEST_FILE_NAME_SIGNED) == 0) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(filename);
                    File outFile = new File(Environment.getDataDirectory().toString(), filename);
                    out = new FileOutputStream(outFile);
                    copyFile(in, out);
                } catch (IOException e) {
                    Log.e("SIGMA_TEST", "Failed to copy asset file: " + filename, e);
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            // NOOP
                        }
                    }
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e) {
                        }
                    }
                }
            }
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    public void createTestPDF() {
        try {
            Document document = new Document();
            // step 2
            // Creating a PDF 1.7 document\

            FileOutputStream fout = new FileOutputStream(getTestFileHandle(TEST_FILE_NAME_NEW));
            //FileOutputStream fout = new FileOutputStream(new File(unitTestPDF));  // uncomment to create reference file

            PdfWriter writer = PdfWriter.getInstance(document, fout);
            writer.setPdfVersion(PdfWriter.VERSION_1_7);

            // first add xmp metadata
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            XmpWriter xmp = new XmpWriter(os);
            XmpSchema dc = new com.itextpdf.text.xml.xmp.DublinCoreSchema();
            XmpArray subject = new XmpArray(XmpArray.UNORDERED);
            subject.add("Hello World");
            subject.add("XMP & Metadata");
            subject.add("Metadata");
            dc.setProperty(DublinCoreSchema.SUBJECT, subject);
            xmp.addRdfDescription(dc);
            PdfSchema pdf = new PdfSchema();
            pdf.setProperty(PdfSchema.KEYWORDS, "Hello World, XMP, Metadata");
            pdf.setProperty(PdfSchema.VERSION, "1.7");
            xmp.addRdfDescription(pdf);
            xmp.close();
            writer.setXmpMetadata(os.toByteArray());

            // open for writing content
            document.open();

            // plain text first
            BaseFont bf = BaseFont.createFont();
            PdfContentByte directcontent = writer.getDirectContent();
            directcontent.beginText();
            directcontent.setFontAndSize(bf, 12);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "Agree?", 36, 770, 0);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "YES", 58, 750, 0);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "NO", 102, 750, 0);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "Name", 36, 730, 0);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "Check me", 36, 630, 0);
            directcontent.endText();

            // create a radio button field
            PdfFormField agree = PdfFormField.createRadioButton(writer, true);
            agree.setFieldName("r1");
            agree.setValueAsName("Yes");
            Rectangle rectYes = new Rectangle(40, 766, 56, 744);
            RadioCheckField yes = new RadioCheckField(writer, rectYes, null, "Yes");
            yes.setChecked(true);
            agree.addKid(yes.getRadioField());
            Rectangle rectNo = new Rectangle(84, 766, 100, 744);
            RadioCheckField no = new RadioCheckField(writer, rectNo, null, "No");
            no.setChecked(false);
            agree.addKid(no.getRadioField());
            writer.addAnnotation(agree);

            // create a text field
            Rectangle rect = new Rectangle(40, 710, 200, 726);
            TextField fname = new TextField(writer, rect, "t1");
            fname.setBorderColor(GrayColor.GRAYBLACK);
            fname.setBorderWidth(0.5f);
            writer.addAnnotation(fname.getTextField());

            // create single checkbox
            // define the coordinates of the middle
            float x = 40;
            float y = 600;
            // define the position of a check box that measures 20 by 20
            rect = new Rectangle(x - 10, y - 10, x + 10, y + 10);
            // define the check box
            RadioCheckField checkbox = new RadioCheckField(
                    writer, rect, "c1", "Yes");
            checkbox.setCheckType(RadioCheckField.TYPE_CHECK);
            writer.addAnnotation(checkbox.getCheckField());


            // create signature
            PdfFormField field = PdfFormField.createSignature(writer);
            field.setWidget(new Rectangle(72, 532, 144, 580), PdfAnnotation.HIGHLIGHT_INVERT);
            field.setFieldName("s1");
            field.setFlags(PdfAnnotation.FLAGS_PRINT);
            field.setPage();
            field.setMKBorderColor(BaseColor.BLACK);
            field.setMKBackgroundColor(BaseColor.WHITE);
            PdfAppearance tp = PdfAppearance.createAppearance(writer, 72, 48);
            tp.rectangle(0.5f, 0.5f, 71.5f, 47.5f); // make signature into a simple box

            tp.stroke();
            field.setAppearance(PdfAnnotation.APPEARANCE_NORMAL, tp);
            writer.addAnnotation(field);

            for(int i = 0; i < MAX_NUMBER_OF_PAGES; i++) {
                document.newPage();
                document.add(new Paragraph("Page " + i));
            }




            document.close();
        }catch (Exception ex){
            Assert.assertTrue("Exception when trying create unit test PDF " + ex, false);
        }
    }


    public void PADESLTVwOCSPSignCert_noTSA(Context context) {

        try {

            // load up PKC12 cert and private key into new BC keystore and cert chain using predefined password
            KeyStore ks = KeyStore.getInstance("pkcs12", provider.getName());
            ks.load(context.getAssets().open(PKCS12CertAndKey) , PASSWORD.toCharArray());
            String alias = (String) ks.aliases().nextElement();
            Certificate[] chain = ks.getCertificateChain(alias);
            PrivateKey pk = (PrivateKey) ks.getKey(alias, PASSWORD.toCharArray());

            // reader and stamper
            PdfReader reader = new PdfReader(new FileInputStream(getTestFileHandle(TEST_FILE_NAME_NEW)));
            System.out.println("SD Card folder : " + Environment.getDataDirectory());
            FileOutputStream fout = new FileOutputStream(getTestFileHandle(TEST_FILE_NAME_SIGNED));
            //FileOutputStream fout = new FileOutputStream(new File(refPDF)); // uncomment to create reference file

            PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0');

            // Setup signature appearance...
            PdfSignatureAppearance sap = stp.getSignatureAppearance();
            sap.setReason("I'm really approving this.");
            sap.setLocation("Foobar");
            byte[] image = new byte[1500*1500*4]; // a slightly oversized image
            context.getAssets().open(signatureImageFile).read(image);
            sap.setImage(Image.getInstance(image));
            // ... and define position and page number where to place test signature appearance
            //sap.setVisibleSignature(new Rectangle(72, 732, 144, 780), 1, "Signature");
            // ...or define name of acrofield of type signature
            sap.setVisibleSignature("s1");


            // digital signature will be provided by bouncycastle crypto engine with SHA digest
            ExternalSignature es = new PrivateKeySignature(pk, "SHA-256", "BC");
            // and Bouncycastle digest engine
            ExternalDigest digest = new BouncyCastleDigest();

            // OCSP client also from bouncycastle
            OcspClient ocspClient = new OcspClientBouncyCastle();

            // sign the PDF with OCSP and no timestamp
            MakeSignature.signDetached(sap, digest, es, chain, null, ocspClient, null, 0, MakeSignature.CryptoStandard.CMS);

            // cant really compare 2 PDF's for multiple reasons according to: http://stackoverflow.com/a/21992070/1997362
            //  Assert.assertTrue(compareFileToReferenceInAssets(dstPDF, refPDF));
        }catch (Exception ex){
            Assert.assertTrue("Exception when trying to sign PDF: " + ex, false);
        }
    }
}