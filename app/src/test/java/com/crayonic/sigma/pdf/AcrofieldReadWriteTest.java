package com.crayonic.sigma.pdf;
import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.test.InstrumentationTestCase;


import com.crayonic.sigma.BuildConfig;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfAppearance;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.TextField;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.CertificateUtil;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;
import com.itextpdf.text.xml.xmp.DublinCoreSchema;
import com.itextpdf.text.xml.xmp.PdfSchema;
import com.itextpdf.text.xml.xmp.XmpArray;
import com.itextpdf.text.xml.xmp.XmpSchema;
import com.itextpdf.text.xml.xmp.XmpWriter;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.manifest.AndroidManifest;
import org.robolectric.shadows.ShadowEnvironment;
import org.robolectric.shadows.ShadowLog;

//import org.spongycastle.jce.provider.BouncyCastleProvider;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.List;
import java.util.Set;

import dalvik.annotation.TestTarget;

import static org.junit.Assert.*;
import org.apache.commons.io.FileUtils;
/**
 * Created by kolarov on 22.4.2015.
 */



@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class , sdk=21, manifest = "app/src/main/AndroidManifest.xml", assetDir = "src/androidTest/assets")
public class AcrofieldReadWriteTest {

    //input files for unit tests
   // public static final String srcPDF = "Contract.pdf";
    public static final String signatureImageFile = "signature.png";
    public static final String signatureCert = "cert/certificate.crt";
    public static final String PKCS12CertAndKey = "cert/certificate.pfx";
    public static final String verifyRootCert = "cert/RootCertificate.crt";

    // output files for unit tests
    public static final String dstPDF = "unitTestPDFSigned.pdf";
    public static final String dstPDF2 = "unitTestPDFFilled.pdf";
    public static final String acrofieldListTXT = "exportedFields.txt";

    public static final String unitTestPDF = "unitTestPDF.pdf";  // this gets created before tests

    // reference output files for unit tests for compare purposes
    public static final String refPDF = "unitTestPDFSignedRef.pdf";
    public static final String refPDF2 = "unitTestPDFFilledRef.pdf";
    public static final String refAcrofieldListTXT = "exportedFieldsRef.txt";


    public static final String PASSWORD = "test";

    Context context = RuntimeEnvironment.application ;
/* ROBOLECTRIC uses system JVM and thus must use signed bouncy castle JAR (Jar added to ext libs in JRE and also set as JCE provider within java security)*/
//    static {
//        Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);
//
//    }

    BouncyCastleProvider provider;

    @Before
    public void setUp() throws Exception {
        ShadowLog.stream = System.out;
        provider =  new BouncyCastleProvider();
        createTestPDF();
    }


    public void createTestPDF() {
        try {
            Document document = new Document();
            // step 2
            // Creating a PDF 1.7 document\

            //FileOutputStream fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory(),unitTestPDF));
            FileOutputStream fout = new FileOutputStream(new File(unitTestPDF));  // uncomment to create reference file

            PdfWriter writer = PdfWriter.getInstance(document, fout);
            writer.setPdfVersion(PdfWriter.VERSION_1_7);

            // first add xmp metadata
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            XmpWriter xmp = new XmpWriter(os);
            XmpSchema dc = new com.itextpdf.text.xml.xmp.DublinCoreSchema();
            XmpArray subject = new XmpArray(XmpArray.UNORDERED);
            subject.add("Hello World");
            subject.add("XMP & Metadata");
            subject.add("Metadata");
            dc.setProperty(DublinCoreSchema.SUBJECT, subject);
            xmp.addRdfDescription(dc);
            PdfSchema pdf = new PdfSchema();
            pdf.setProperty(PdfSchema.KEYWORDS, "Hello World, XMP, Metadata");
            pdf.setProperty(PdfSchema.VERSION, "1.7");
            xmp.addRdfDescription(pdf);
            xmp.close();
            writer.setXmpMetadata(os.toByteArray());

            // open for writing content
            document.open();

            // plain text first
            BaseFont bf = BaseFont.createFont();
            PdfContentByte directcontent = writer.getDirectContent();
            directcontent.beginText();
            directcontent.setFontAndSize(bf, 12);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "Agree?", 36, 770, 0);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "YES", 58, 750, 0);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "NO", 102, 750, 0);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "Name", 36, 730, 0);
            directcontent.showTextAligned(Element.ALIGN_LEFT, "Check me", 36, 630, 0);
            directcontent.endText();

            // create a radio button field
            PdfFormField agree = PdfFormField.createRadioButton(writer, true);
            agree.setFieldName("r1");
            agree.setValueAsName("Yes");
            Rectangle rectYes = new Rectangle(40, 766, 56, 744);
            RadioCheckField yes = new RadioCheckField(writer, rectYes, null, "Yes");
            yes.setChecked(true);
            agree.addKid(yes.getRadioField());
            Rectangle rectNo = new Rectangle(84, 766, 100, 744);
            RadioCheckField no = new RadioCheckField(writer, rectNo, null, "No");
            no.setChecked(false);
            agree.addKid(no.getRadioField());
            writer.addAnnotation(agree);

            // create a text field
            Rectangle rect = new Rectangle(40, 710, 200, 726);
            TextField fname = new TextField(writer, rect, "t1");
            fname.setBorderColor(GrayColor.GRAYBLACK);
            fname.setBorderWidth(0.5f);
            writer.addAnnotation(fname.getTextField());

            // create single checkbox
            // define the coordinates of the middle
            float x = 40;
            float y = 600;
            // define the position of a check box that measures 20 by 20
            rect = new Rectangle(x - 10, y - 10, x + 10, y + 10);
            // define the check box
            RadioCheckField checkbox = new RadioCheckField(
                    writer, rect, "c1", "Yes");
            checkbox.setCheckType(RadioCheckField.TYPE_CHECK);
            writer.addAnnotation(checkbox.getCheckField());


            // create signature
            PdfFormField field = PdfFormField.createSignature(writer);
            field.setWidget(new Rectangle(72, 532, 144, 580), PdfAnnotation.HIGHLIGHT_INVERT);
            field.setFieldName("s1");
            field.setFlags(PdfAnnotation.FLAGS_PRINT);
            field.setPage();
            field.setMKBorderColor(BaseColor.BLACK);
            field.setMKBackgroundColor(BaseColor.WHITE);
            PdfAppearance tp = PdfAppearance.createAppearance(writer, 72, 48);
            tp.rectangle(0.5f, 0.5f, 71.5f, 47.5f); // make signature into a simple box

            tp.stroke();
            field.setAppearance(PdfAnnotation.APPEARANCE_NORMAL, tp);
            writer.addAnnotation(field);


            document.close();
        }catch (Exception ex){
            Assert.assertTrue("Exception when trying create unit test PDF " + ex, false);
        }
    }


    @Test
    /*
    This tests if certificate contains OCSP URL in order to conform with PADES
     */
    public void OCSPAvailableForCertificate() {
        try {

            KeyStore ks = KeyStore.getInstance("pkcs12", provider.getName());
            ks.load(context.getAssets().open(PKCS12CertAndKey) , PASSWORD.toCharArray());
            String alias = (String) ks.aliases().nextElement();
            Certificate[] chain = ks.getCertificateChain(alias);
            for (int i = 0; i < chain.length; i++) {
                X509Certificate cert = (X509Certificate) chain[i];
                System.out.println(String.format("[%s] %s", i, cert.getSubjectDN()));
                System.out.println(CertificateUtil.getOCSPURL(cert));
                assertNotNull(CertificateUtil.getOCSPURL(cert));
            }
        }catch (Exception ex){
            Assert.assertTrue("Exception when checking OCSP URL in certs: " + ex, false);
        }
    }



    @Test
    /*
        Test if we can sign the PDF with PADES LTV (no time stamp yet)
     */
           public void PADESLTVwOCSPSignCert_noTSA() {

            try {

                // load up PKC12 cert and private key into new BC keystore and cert chain using predefined password
                KeyStore ks = KeyStore.getInstance("pkcs12", provider.getName());
                ks.load(context.getAssets().open(PKCS12CertAndKey) , PASSWORD.toCharArray());
                String alias = (String) ks.aliases().nextElement();
                Certificate[] chain = ks.getCertificateChain(alias);
                PrivateKey pk = (PrivateKey) ks.getKey(alias, PASSWORD.toCharArray());

                // reader and stamper
                //PdfReader reader = new PdfReader(new FileInputStream(new File(Environment.getExternalStorageDirectory(),unitTestPDF)));
                PdfReader reader = new PdfReader(new FileInputStream(new File(unitTestPDF))); // ref file
                System.out.println("SD Card folder : " + Environment.getExternalStorageDirectory());
                //FileOutputStream fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory(),dstPDF));
                FileOutputStream fout = new FileOutputStream(new File(refPDF)); // uncomment to create reference file

                PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0');

                // Setup signature appearance...
                PdfSignatureAppearance sap = stp.getSignatureAppearance();
                sap.setReason("I'm really approving this.");
                sap.setLocation("Foobar");
                byte[] image = new byte[1500*1500*4]; // a slightly oversized image
                context.getAssets().open(signatureImageFile).read(image);
                sap.setImage(Image.getInstance(image));
                // ... and define position and page number where to place test signature appearance
                //sap.setVisibleSignature(new Rectangle(72, 732, 144, 780), 1, "Signature");
                // ...or define name of acrofield of type signature
                sap.setVisibleSignature("s1");


                // digital signature will be provided by bouncycastle crypto engine with SHA digest
            ExternalSignature es = new PrivateKeySignature(pk, "SHA-256", "BC");
            // and Bouncycastle digest engine
            ExternalDigest digest = new BouncyCastleDigest();

            // OCSP client also from bouncycastle
            OcspClient ocspClient = new OcspClientBouncyCastle();

            // sign the PDF with OCSP and no timestamp
            MakeSignature.signDetached(sap, digest, es, chain, null, ocspClient, null, 0, MakeSignature.CryptoStandard.CMS);

            // cant really compare 2 PDF's for multiple reasons according to: http://stackoverflow.com/a/21992070/1997362
          //  Assert.assertTrue(compareFileToReferenceInAssets(dstPDF, refPDF));
        }catch (Exception ex){
            Assert.assertTrue("Exception when trying to sign PDF: " + ex, false);
        }
    }

    private boolean compareFileToReferenceInAssets(String src, String assetReference) throws Exception {
        FileInputStream srcFIS =  new FileInputStream(new File(Environment.getExternalStorageDirectory(),src));
        return IOUtils.contentEquals(srcFIS, context.getAssets().open(assetReference));
    }



    @Test
    /*
    Test to read and write acrofields within PDF
     */
    public void readWriteAcroformFieldValues() {

        try {

            PrintWriter writer
            //        = new PrintWriter(new FileOutputStream(new File(Environment.getExternalStorageDirectory(),acrofieldListTXT)));
                   = new PrintWriter(new FileOutputStream(new File(refAcrofieldListTXT))); // uncomment to create reference file
            writer.println("Exporting values from:" + unitTestPDF);
            // Create a reader to extract info
            //PdfReader reader = new PdfReader(new FileInputStream(new File(Environment.getExternalStorageDirectory(),unitTestPDF)));
            PdfReader reader = new PdfReader(new FileInputStream(new File(unitTestPDF)));

           // FileOutputStream fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory(),dstPDF2));
            FileOutputStream fout = new FileOutputStream(new File(refPDF2));  // uncomment to create reference file

            PdfStamper stp = new PdfStamper(reader, fout);

            // Get the fields from the reader (read-only!!!)
            AcroFields form = stp.getAcroFields();
            // Loop over the fields and get info about them
            Set<String> fields = form.getFields().keySet();
            for (String key : fields) {
                writer.print(key + ": ");

                //figure out type
                switch (form.getFieldType(key)) {
                    case AcroFields.FIELD_TYPE_CHECKBOX:
                        writer.println("Checkbox");
                        form.setFieldProperty(key, "clrfflags", PdfFormField.FF_READ_ONLY, null); // make them writable
                        form.setField(key, "Yes");
                        break;
                    case AcroFields.FIELD_TYPE_COMBO:
                        writer.println("Combobox");
                        break;
                    case AcroFields.FIELD_TYPE_LIST:
                        writer.println("List");
                        break;
                    case AcroFields.FIELD_TYPE_NONE:
                        writer.println("None");
                        break;
                    case AcroFields.FIELD_TYPE_PUSHBUTTON:
                        writer.println("Pushbutton");
                        break;
                    case AcroFields.FIELD_TYPE_RADIOBUTTON:
                        writer.println("Radiobutton");
                        break;
                    case AcroFields.FIELD_TYPE_SIGNATURE:
                        writer.println("Signature");
                        //form.removeField(key);
                        break;
                    case AcroFields.FIELD_TYPE_TEXT:
                        writer.println("Text");
                        form.setFieldProperty(key, "clrfflags", PdfFormField.FF_READ_ONLY, null); // make them writable
                        form.setField(key, "test");
                        break;
                    default:
                        writer.println("?");
                }
                //read the actual value
                writer.println("Possible values for:" + key);
                String[] states = form.getAppearanceStates(key);
                if (states != null && states.length > 0)
                    for (int i = 0; i < states.length; i++) {
                        writer.print(", ");
                        writer.print(states[i]);
                    }

                writer.print("Current value for: " + key + "  is:  ");
                writer.println(form.getField(key)); // get actual value for the field
                writer.println(form.getFieldRichValue(key)); // get actual value for the field

                List<AcroFields.FieldPosition> positions = form.getFieldPositions(key);

                for (AcroFields.FieldPosition pos : positions) {
                    writer.println("Widget on page:" + pos.page);
                    writer.println("Widget position topleft X:" + pos.position.getLeft() + " topleft Y:" + pos.position.getTop() +
                            " width:" + pos.position.getWidth() + " height:" + pos.position.getHeight());
                }
                writer.println(" ");
            }
            //writer.println(states[states.length - 1]);
            // flush and close the report file
            writer.flush();
            writer.close();

            stp.setFormFlattening(true);
            stp.close();
            reader.close();

           // compare results both PDF and TXT

           // Assert.assertTrue(compareFileToReferenceInAssets(acrofieldListTXT, refAcrofieldListTXT));
            // cant really compare 2 PDF's for multiple reasons according to: http://stackoverflow.com/a/21992070/1997362
           // Assert.assertTrue(compareFileToReferenceInAssets(dstPDF2, refPDF2));

        }catch (Exception ex){

            Assert.assertTrue("Exception when trying read acrofields " + ex, false);
    }
    }




}
