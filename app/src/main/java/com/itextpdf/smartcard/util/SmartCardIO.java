/*
 * $Id$
 *
 * This file is part of the iText (R) project.
 * Copyright (c) 1998-2014 iText Group NV
 * Author: Bruno Lowagie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation with the addition of the
 * following permission added to Section 15 as permitted in Section 7(a):
 * FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY
 * ITEXT GROUP. ITEXT GROUP DISCLAIMS THE WARRANTY OF NON INFRINGEMENT
 * OF THIRD PARTY RIGHTS
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA, 02110-1301 USA, or download the license from the following URL:
 * http://itextpdf.com/terms-of-use/
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License,
 * a covered work must retain the producer line in every PDF that is created
 * or manipulated using iText.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving the iText software without
 * disclosing the source code of your own applications.
 * These activities include: offering paid services to customers as an ASP,
 * serving PDFs on the fly in a web application, shipping iText with a closed
 * source product.
 *
 * For more information, please contact iText Software Corp. at this
 * address: sales@itextpdf.com
 */
package com.itextpdf.smartcard.util;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


import com.crayonic.smartcardio.CardException;
import com.crayonic.smartcardio.CommandAPDU;
import com.crayonic.smartcardio.ResponseAPDU;
import com.crayonic.smartcardio.CardChannel;

import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;


/**
 * Class that reads bytes from a SmartCard.
 */
public class SmartCardIO {
    /**
     * Logger instance.
     */
    
    /**
     * Block size
     */
    private static final int BLOCK_SIZE = 0xff;
    private static final String TAG = "SmartCardIO";


    /**
     * Selects a file on a card, reads it, and returns the bytes.
     *
     * @param channel The CardChannel.
     * @param fileId  A file ID referring to the file you want to read.
     * @return a byte array containing the file
     * @throws CardException
     * @throws IOException
     */
    public static synchronized byte[] readFile(CardChannel channel, byte[] fileId) throws CardException, IOException {
        int length = selectFileCraytoken(channel, fileId);
        Log.i(TAG, "Selected file with length: " + length);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(length > 0 ) {
            byte[] data = readBinaryCraytoken(channel, length);
            Log.i(TAG, "Done reading...");
            return data;
        }else{
            Log.i(TAG, "No file to read from card...");
            return null;
        }
    }


    public static synchronized void writeFile(CardChannel channel, byte[] fileId, byte[] fileBytes) throws CardException, IOException {
        int length = selectFileCraytoken(channel, fileId);
        Log.i(TAG, "Selected file with length: " + length);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeBinaryCraytoken(channel, fileBytes);
        Log.i(TAG, "Done writing...");
    }


    /**
     * Selects a file on the card.
     *
     * @param channel The CardChannel.
     * @param fileId  A file ID referring to the file you want to read.
     * @throws CardException
     * @throws FileNotFoundException
     */
    private static void selectFile(CardChannel channel, byte[] fileId) throws CardException, FileNotFoundException {
        Log.i(TAG, "Selecting file...");
        // Create a command to select a file
        CommandAPDU selectFileApdu = new CommandAPDU(
                IsoIec7816.CLA_00, IsoIec7816.INS_SELECT,
                IsoIec7816.P1_SELECT_FROM_MF,
                IsoIec7816.P2_ONLY_OCCURRENCE_NO_RESPONSE_DATA,
                fileId);
        // execute the command
        ResponseAPDU responseApdu = transmit(channel, selectFileApdu);
        int sw = responseApdu.getSW();
        if (sw != IsoIec7816.SW_NO_FURTHER_QUALIFICATION) {
            throw new FileNotFoundException(
                    "Wrong status after selecting file: 0x"
                            + Integer.toHexString(sw));
        }

    }

    /**
     * Selects a file on the card.
     *
     * @param channel The CardChannel.
     * @param fileId  A file ID referring to the file you want to read.
     * @return length   returns length of the file (0 if it doesn not exist)
     * @throws CardException
     * @throws FileNotFoundException
     */
    private static int selectFileCraytoken(CardChannel channel, byte[] fileId) throws CardException, FileNotFoundException {
        Log.i(TAG, "Selecting file with info...");
       // channel.getCard().transmitControlCommand(0, "ADMIN_APDU\n".getBytes() ); //FIXME
        // Create a command to select a file
        CommandAPDU selectFileApdu = new CommandAPDU(
                IsoIec7816.CLA_FF, IsoIec7816.INS_SELECT,
                IsoIec7816.P1_SELECT_BY_ID,
                00,
                fileId, 24); // expecting length to be returned as  0x81(tag) 0xXX 0xXX 0x82(tag) 0xXX ....20bytes of hash...0xXX
        // execute the command
        ResponseAPDU responseApdu = transmit(channel, selectFileApdu);
        int sw = responseApdu.getSW();
        if (sw != IsoIec7816.SW_NO_FURTHER_QUALIFICATION) {
            throw new FileNotFoundException(
                    "Wrong status after selecting file: 0x"
                            + Integer.toHexString(sw));
        }

        // get length
        byte[] data = responseApdu.getData();
        if (((int) (data[0]) & 0xff) == 0x81) // check if Length of the file TAG is in the response for sanity
            return data[2] + data[1] * 256;
        else
            throw new CardException("Card does not recognize select by ID with Length in response!");
    }

    /**
     * Reads binary data from a card after you've selected a file.
     *
     * @param channel The CardChannel.
     * @return a byte array containing the file
     * @throws CardException
     * @throws IOException
     */
    private static byte[] readBinary(CardChannel channel) throws CardException, IOException {
        Log.i(TAG, "Reading binary...");
        int offset = 0;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] data;
        do {
            // create command read a block of bytes
            CommandAPDU readBinaryApdu = new CommandAPDU(
                    IsoIec7816.CLA_00, IsoIec7816.INS_READ_BINARY,
                    offset >> 8, offset & 0xFF, BLOCK_SIZE);
            // execute the command
            ResponseAPDU responseApdu = transmit(channel, readBinaryApdu);
            // check the status bytes as a single word
            int sw = responseApdu.getSW();
            if (sw == IsoIec7816.SW_WRONG_PARAMETERS)
                break;
            if (sw != IsoIec7816.SW_NO_FURTHER_QUALIFICATION)
                throw new IOException("APDU response error: 0x" + Integer.toHexString(sw));
            // get the data
            data = responseApdu.getData();
            baos.write(data);
            offset += data.length;
        } while (BLOCK_SIZE == data.length);

        return baos.toByteArray();
    }


    /**
     * Reads binary data from a Crayonic card after you've selected a file.
     *
     * @param channel The CardChannel.
     * @return a byte array containing the file
     * @throws CardException
     * @throws IOException
     */
    private static byte[] readBinaryCraytoken(CardChannel channel, int length) throws CardException, IOException {
        Log.i(TAG, "Reading binary...");
        byte[] data;

       // channel.getCard().transmitControlCommand(0, "ADMIN_APDU\n".getBytes() ); //FIXME
        // create command read a block of bytes
        CommandAPDU readBinaryApdu = new CommandAPDU(
                IsoIec7816.CLA_FF, IsoIec7816.INS_READ_BINARY,
                00, 00, length);
        // execute the command
        ResponseAPDU responseApdu = transmit(channel, readBinaryApdu);
        // check the status bytes as a single word
        int sw = responseApdu.getSW();

        if (sw != IsoIec7816.SW_NO_FURTHER_QUALIFICATION)
            throw new IOException("APDU response error: 0x" + Integer.toHexString(sw));
        // get the data
        return responseApdu.getData();
    }


    /**
     * Writes binary data to a Crayonic token after you've selected a file. (Class FF)
     *
     * @param channel The CardChannel.
     * @param fBytes  a byte array to write to the file
     * @throws CardException
     * @throws IOException
     */
    private static void writeBinaryCraytoken(CardChannel channel, byte[] fBytes) throws CardException, IOException {
        Log.i(TAG, "Writing binary...");
       // channel.getCard().transmitControlCommand(0, "ADMIN_APDU\n".getBytes() ); //FIXME
        // create command write a block of bytes
        CommandAPDU writeBinaryApdu = new CommandAPDU(
                IsoIec7816.CLA_FF, IsoIec7816.INS_WRITE_BINARY,
                00, 00, fBytes, 0,fBytes.length, 0);
        // execute the command
        ResponseAPDU responseApdu = transmit(channel, writeBinaryApdu);
        // check the status bytes as a single word
        int sw = responseApdu.getSW();
        if (sw == IsoIec7816.SW_WRONG_PARAMETERS)
            throw new CardException("Wrong parameters in Craytoken write: " + writeBinaryApdu.toString());
        if (sw != IsoIec7816.SW_NO_FURTHER_QUALIFICATION)
            throw new IOException("APDU response error: 0x" + Integer.toHexString(sw));
    }

    /**
     * Communicates with a smart card using an
     * application protocol data unit command and response.
     *
     * @param channel     The CardChannel.
     * @param commandApdu The CommandAPDU send to the card
     * @return The ResponseAPDU received from the card
     * @throws CardException
     */
    public static ResponseAPDU transmit(CardChannel channel, CommandAPDU commandApdu)
            throws CardException {
        Log.i(TAG, "start transmitting...");
        ResponseAPDU responseApdu = channel.transmit(commandApdu);
        if (responseApdu == null) {
            throw new CardException("No response from card within time limit!");
        }
        if (IsoIec7816.SW1_ABORTED == responseApdu.getSW1()) {
            /*
             * A minimum delay of 10 msec between the answer and the
			 * next APDU is mandatory for eID v1.0 and v1.1 cards.
			 */
            Log.i(TAG, "sleeping...");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new CardException(e);
            }
            responseApdu = channel.transmit(commandApdu);
        }
        return responseApdu;
    }
}
