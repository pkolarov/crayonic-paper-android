/*
 * $Id$
 *
 * This file is part of the iText (R) project.
 * Copyright (c) 1998-2014 iText Group NV
 * Author: Bruno Lowagie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation with the addition of the
 * following permission added to Section 15 as permitted in Section 7(a):
 * FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY
 * ITEXT GROUP. ITEXT GROUP DISCLAIMS THE WARRANTY OF NON INFRINGEMENT
 * OF THIRD PARTY RIGHTS
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA, 02110-1301 USA, or download the license from the following URL:
 * http://itextpdf.com/terms-of-use/
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License,
 * a covered work must retain the producer line in every PDF that is created
 * or manipulated using iText.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving the iText software without
 * disclosing the source code of your own applications.
 * These activities include: offering paid services to customers as an ASP,
 * serving PDFs on the fly in a web application, shipping iText with a closed
 * source product.
 *
 * For more information, please contact iText Software Corp. at this
 * address: sales@itextpdf.com
 */
package com.itextpdf.smartcard.crayonicid;

import android.util.Log;

import com.crayonic.sigma.crypto.craytoken.AdminCommand;
import com.crayonic.sigma.crypto.craytoken.Craytoken;
import com.crayonic.sigma.crypto.craytoken.ResponseListener;
import com.crayonic.smartcardio.CardException;
import com.crayonic.smartcardio.CardTerminal;

import com.crayonic.smartcardio.CommandAPDU;
import com.crayonic.smartcardio.ResponseAPDU;
import com.itextpdf.smartcard.Features;
import com.itextpdf.smartcard.SmartCardWithKey;
import com.itextpdf.smartcard.util.DigestAlgorithms;
import com.itextpdf.smartcard.util.IsoIec7816;
import com.itextpdf.smartcard.util.SmartCardIO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * An instance of the Belgian eID card.
 */
public class CrayonicIDCard extends SmartCardWithKey {
    private static final String TAG = "CrayonicIDCard";

	/** A pattern that all Belgian eID cards have in common. */
	public final static byte[] PATTERN = new byte[] { 0x3b, (byte) 0x98,
		0x00, 0x40, 0x00, (byte) 0x00, 0x00, 0x00, 0x01, 0x01, (byte) 0xad,
		0x13, 0x10 };

	/** The part of the pattern all Belgian eID cards have in common. */
	public final static byte[] MASK = new byte[] { (byte) 0xff,
		(byte) 0xff, 0x00, (byte) 0xff, 0x00, 0x00, 0x00, 0x00,
		(byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xf0 };


    /**
	 * Creates a CrayonicIDCard instance.
	 * @param	cardTerminal	the card terminal with the Belgian eID. 
	 */
	public CrayonicIDCard(CardTerminal cardTerminal) throws CardException {
		super(cardTerminal, CrayonicIDCertificates.NON_REPUDIATION_KEY_ID, "RSA");
	}
	
	/**
	 * @see com.itextpdf.smartcard.SmartCard#getPattern()
	 */
	public byte[] getPattern() {
		return null;
	}
	
	/**
	 * @see com.itextpdf.smartcard.SmartCard#getMask()
	 */
	public byte[] getMask() {
		return null;
	}


	public void switchToAPDU(){
        Craytoken pen = (Craytoken) this.getCard();

        byte[] cmdToSend = (AdminCommand.ADMIN_APDU + "\n").getBytes();

        pen.getBLEStack().setResponseListener(null);
        try {
            pen.getBLEStack().transmitBytes(cmdToSend);
        } catch (CardException e) {
            e.printStackTrace();
        }

    }

	/*
	    This is special control command for Crayonic Token - generates CSR directly in pen given principal string and listener
	 */
	public void requestCSR(String principal, ResponseListener responseListener) {
        Craytoken pen = (Craytoken) this.getCard();

        byte[] cmdToSend = (AdminCommand.ADMIN_CSR.name() + " " + principal + "\n").getBytes();

        pen.getBLEStack().setResponseListener(responseListener);
        try {
            pen.getBLEStack().transmitBytes(cmdToSend);
        } catch (CardException e) {
            e.printStackTrace();
        }

    }




    /**
     * Signs a message digest on the smart card.
     * @param digest	the message digest
     * @param algorithm	the	algorithm used to create the message digest
     * @return	a signed digest
     * @throws CardException
     * @throws IOException
     *
     * The encryption block(EB) during signing is built as follows:
    EB = 00 || 01 || PS || 00 || T
    :: where T is the DER encoding of :
    digestInfo ::= SEQUENCE {
    digestAlgorithm AlgorithmIdentifier of SHA-256,
    digest OCTET STRING
    }
    :: PS is an octet string of length k-3-||T|| with value FF. The length of PS must be at least 8 octets.
    :: k is the RSA modulus size.
    DER encoded SHA-256 AlgorithmIdentifier = 30 31 30 0d 06 09 60 86 48 01 65 03 04 02 01 05 00 04 20.
     *
     */
    public byte[] sign(byte[] digest, String algorithm) throws CardException, IOException {
        Log.i(TAG, "Signing a digest created with " + algorithm);
        Log.i(TAG, "Signing digest long: " + digest.length);

        //channel.getCard().transmitControlCommand(0, "ADMIN_APDU\n".getBytes() );

        byte algobyte;
        if ("SHA-1-PSS".equals(algorithm)) {
            algobyte = 0x10;
        } else if ("SHA-256-PSS".equals(algorithm)) {
            algobyte = 0x20;
        } else {
            algobyte = 0x01;
        }


        Log.i(TAG, "Creating bytes for signing");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (algobyte == 0x01) {
            byte[] prefix = DigestAlgorithms.DIGESTS.get(algorithm);
            if (prefix != null) {
                baos.write(prefix);
            }
            else if (DigestAlgorithms.PLAIN_TEXT.equals(algorithm)) {
                System.out.println(digest.length);
                prefix = Arrays.copyOf(
                        DigestAlgorithms.PLAIN_TEXT_PREFIX,
                        DigestAlgorithms.PLAIN_TEXT_PREFIX.length);
                prefix[1] = (byte) (digest.length + 13);
                prefix[14] = (byte) digest.length;
                System.out.println(prefix[14]);
                baos.write(prefix);
            }
        }
        baos.write(digest);
        ByteArrayOutputStream paddedDigest = new ByteArrayOutputStream();
        paddedDigest.write(new byte[] { 0x00, 0x01} );
        int ffPaddingCount = 256 - 3 - baos.size();
        for(int i = 0; i < ffPaddingCount; i++) {
            paddedDigest.write(0xff);
        }
        paddedDigest.write(0);
        paddedDigest.write(baos.toByteArray(),0, baos.size());

        Log.i(TAG, "Signing digest after processing  long: " + paddedDigest.size());

        Log.i(TAG, "Sign the bytes");
        CommandAPDU commandAPDU = new CommandAPDU(
                IsoIec7816.CLA_FF, IsoIec7816.INS_PERFORM_SECURITY_OPERATION,
                0x80, 0x86,     //Craytoken params ....
                paddedDigest.toByteArray(), 256); // expect response for 2048 RSA to be 256 byte long
        ResponseAPDU responseAPDU = SmartCardIO.transmit(channel, commandAPDU);

        int sw = responseAPDU.getSW();
        // A pin is needed, and it isn't cached on the reader
        if (sw == IsoIec7816.SW_SECURITY_STATUS_NOT_SATISFIED) {
            Log.i(TAG, "Pin code couldn't be verified");
            throw new CardException("Pen is locked");
        }
        if (sw == IsoIec7816.SW_NO_FURTHER_QUALIFICATION) {
            Log.i(TAG, "Signing done");
            return responseAPDU.getData();
        }
        else {
            throw new IOException("Digest could not be signed " + Integer.toHexString(sw));
        }
    }


}
