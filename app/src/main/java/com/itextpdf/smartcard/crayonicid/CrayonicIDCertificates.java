/*
 * $Id$
 *
 * This file is part of the iText (R) project.
 * Copyright (c) 1998-2014 iText Group NV
 * Author: Bruno Lowagie
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation with the addition of the
 * following permission added to Section 15 as permitted in Section 7(a):
 * FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY
 * ITEXT GROUP. ITEXT GROUP DISCLAIMS THE WARRANTY OF NON INFRINGEMENT
 * OF THIRD PARTY RIGHTS
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA, 02110-1301 USA, or download the license from the following URL:
 * http://itextpdf.com/terms-of-use/
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License,
 * a covered work must retain the producer line in every PDF that is created
 * or manipulated using iText.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving the iText software without
 * disclosing the source code of your own applications.
 * These activities include: offering paid services to customers as an ASP,
 * serving PDFs on the fly in a web application, shipping iText with a closed
 * source product.
 *
 * For more information, please contact iText Software Corp. at this
 * address: sales@itextpdf.com
 */
package com.itextpdf.smartcard.crayonicid;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.crypto.CryptoException;
import com.crayonic.sigma.crypto.PKIUtils;
import com.crayonic.sigma.crypto.craytoken.Craytoken;
import com.crayonic.smartcardio.CardException;

import com.itextpdf.smartcard.SmartCardWithKey;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;

import org.spongycastle.jcajce.provider.asymmetric.X509;

/**
 * The Belgian eID card can be used to add digital signatures using specific certificates.
 */
public class CrayonicIDCertificates {

	/** Logger instance. */
	private final static Logger LOGGER = LoggerFactory.getLogger(CrayonicIDCertificates.class);
	
	/** The ID for the authentication certificate. */
	public static final byte AUTHENTICATION_KEY_ID = (byte) 0x82;
	
	/** The ID for the non-repudiation certificate. */
	public static final byte NON_REPUDIATION_KEY_ID = (byte) 0x83;
	
	/** Certificate file ID for the sign certificate (non-repudiation). */
	public static final byte[] SIGN_CERT_FILE_ID = new byte[] { 0x3F, 0x00,
		(byte) 0xDF, 0x00, 0x50, 0x39 };
	
	/** Certificate file ID for the CA certificate. */
	public static final byte[] CA_CERT_FILE_ID = new byte[] { 0x3F, 0x00,
		(byte) 0xDF, 0x00, 0x50, 0x3A };
	
	/** Certificate file ID for the root certificate. */
	public static final byte[] ROOT_CERT_FILE_ID = new byte[] { 0x3F, 0x00,
		(byte) 0xDF, 0x00, 0x50, 0x3B };

	/** Certificate file ID for the authentication. */
	public static final byte[] AUTHN_CERT_FILE_ID = new byte[] { 0x50, 0x01};

	/** Certificate file ID for the national registry certificate. */
	public static final byte[] RRN_CERT_FILE_ID = new byte[] { 0x3F, 0x00,
		(byte) 0xDF, 0x00, 0x50, 0x3C };
	
	/**
	 * Generates a certificate chain that can be used for signing
	 * @param card	an instance of the CrayonicIDCard
	 * @return	a List of X509
	 * @throws CertificateException
	 * @throws CardException
	 * @throws IOException
	 */
	public static Certificate[] getSignCertificateChain(SmartCardWithKey card)
            throws  CertificateException, CardException, IOException, org.spongycastle.crypto.CryptoException {
		LOGGER.info("reading test PEM...");
		List<X509Certificate> signCertificateChain = new LinkedList<X509Certificate>();
        InputStream is= Sigma.getContext().getAssets().open("cert/esignWS.pem");
        Certificate[] chain = PKIUtils.loadCertificateChainFromPEM(new InputStreamReader(is));


//        LOGGER.info("Writing cert 1: " + ((X509Certificate)chain[0]).getIssuerDN());
//		card.writeCertificate(AUTHN_CERT_FILE_ID,(X509Certificate)chain[0]);
        ((CrayonicIDCard) card).switchToAPDU();

		LOGGER.info("reading signinng certificate 1...");
		signCertificateChain.add(card.readCertificate(AUTHN_CERT_FILE_ID));

        for(int i = 0; i < chain.length; i++){
            signCertificateChain.add((X509Certificate) chain[i]);
        }

        Certificate[] certs = new Certificate[signCertificateChain.size()];
		int i = 0;
		for (X509Certificate c : signCertificateChain) {
			certs[i++] = c;
		}

//        if(Arrays.equals(chain[0].getEncoded(), signCertificateChain.get(0).getEncoded())){
//            LOGGER.info("YEAH the certs are equal 1");
//        }
//
//        if(Arrays.equals(chain[0].getEncoded(), signCertificateChain.get(1).getEncoded())){
//            LOGGER.info("YEAH the certs are equal 2");
//        }
//
//        if(Arrays.equals(chain[0].getEncoded(), signCertificateChain.get(2).getEncoded())){
//            LOGGER.info("YEAH the certs are equal 1");
//        }

        return certs;
	}
}
