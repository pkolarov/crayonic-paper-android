/*
 * Copyright (c) 2005, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.crayonic.smartcardio;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

import com.crayonic.sigma.crypto.craytoken.BLEStack;
import com.crayonic.sigma.crypto.craytoken.Craytoken;

import java.util.UUID;


/**
 * A Smart Card terminal, sometimes referred to as a Smart Card Reader.
 */
public class CardTerminal {

    public static final long CONNECT_TIMEOUT = 1000;
    private static final String TAG = "CardTerminal";
    private String mac; // card MAC address
    private Craytoken card; // card in terminal
    private String terminalName;
    private BLEStack bleStack;

    /**
     * Constructs a new CardTerminal object from mac address.
     */
    public CardTerminal(String mac, BLEStack ble) {
        this.mac = mac;
        this.bleStack = ble;
        this.terminalName = UUID.randomUUID().toString();
    }

    public CardTerminal(BLEStack ble) {
        this(null, ble);
    }


    /**
     * Returns the unique name of this terminal.
     *
     * @return the unique name of this terminal.
     */
    public String getName() {
        return terminalName;
    }

    /**
     * Establishes a connection to the card.
     * If a connection has previously established using
     * the specified protocol, this method returns the same Card object as
     * the previous call.
     *
     * @param protocol the protocol to use ("T=0", "T=1", or "T=CL"), or "*" to
     *                 connect using any available protocol.
     * @return the card the connection has been established with
     * @throws NullPointerException     if protocol is null
     * @throws IllegalArgumentException if protocol is an invalid protocol
     *                                  specification
     * @throws CardNotPresentException  if no card is present in this terminal
     * @throws CardException            if a connection could not be established
     *                                  using the specified protocol or if a connection has previously been
     *                                  established using a different protocol
     * @throws SecurityException        if a SecurityManager exists and the
     *                                  caller does not have the required
     *                                  {@linkplain CardPermission permission}
     */
    public Card connect(String protocol) throws CardException {
        if (isCardPresent()) {
            // connect and wait in separate thread and sleep this one
            final Object sync = new Object();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (card.connect()) {  // connect now
                            while (!card.isConnected()) {
                                SystemClock.sleep(1);
                            }
                        } else
                            Log.w(TAG, "Could not connect to Craytoken!");
                    } finally {
                        synchronized (sync) {
                            sync.notifyAll();
                        }
                    }
                }
            }).start();

            synchronized (sync) {
                try {
                    sync.wait();  //CONNECT_TIMEOUT
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (card.isConnected())
                return card;
            else
                throw new CardException("Timeout while connecting to Craytoken!");

        } else
            throw new CardNotPresentException("Craytoken not present!");
    }

    /**
     * Returns whether a card is present in this terminal.
     *
     * @return whether a card is present in this terminal.
     * @throws CardException if the status could not be determined
     */
    public boolean isCardPresent() throws CardException {
        waitForCardPresent(1000);

        if (card == null)
            return false;
        else
            return true;
    }

    /**
     * Waits until a card is present in this terminal or the timeout
     * expires. If the method returns due to an expired timeout, it returns
     * false. Otherwise it return true.
     * <p/>
     * <P>If a card is present in this terminal when this
     * method is called, it returns immediately.
     *
     * @param timeout if positive, block for up to <code>timeout</code>
     *                milliseconds; if zero, block indefinitely; must not be negative
     * @return false if the method returns due to an expired timeout,
     * true otherwise.
     * @throws IllegalArgumentException if timeout is negative
     * @throws CardException            if the operation failed
     */
    public boolean waitForCardPresent(final long timeout) throws CardException {
        if (timeout < 0)
            throw new IllegalArgumentException("Negative timeout");

        if (mac == null && bleStack.isConnected()) {
            // new instance of terminal and token already conected then just copy info about it
            card = bleStack.getSelectedPen();
            mac = card.getMAC();
            return true;
        }

        if (mac != null && bleStack.getSelectedPen().getMAC().equalsIgnoreCase(mac) && bleStack.isConnected()) {
            card = bleStack.getSelectedPen();
            return true;  // already connected to requested craytoken
        }

        final Object sync = new Object();
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    bleStack.startSelectPen(mac);
                    if (timeout == 0) {
                        SystemClock.sleep(2000);  // 2 seconds should be indefinite enough :)
                    } else {
                        SystemClock.sleep(timeout);
                    }
                    bleStack.stopSelectPen();
                } finally {
                    synchronized (sync) {
                        sync.notifyAll();
                    }
                }
            }
        }).start();

        synchronized (sync) {
            try {
                sync.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        card = bleStack.getSelectedPen();
        if (card != null) {
            mac = card.getMAC();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Waits until a card is absent in this terminal or the timeout
     * expires. If the method returns due to an expired timeout, it returns
     * false. Otherwise it return true.
     * <p/>
     * <P>If no card is present in this terminal when this
     * method is called, it returns immediately.
     *
     * @param timeout if positive, block for up to <code>timeout</code>
     *                milliseconds; if zero, block indefinitely; must not be negative
     * @return false if the method returns due to an expired timeout,
     * true otherwise.
     * @throws IllegalArgumentException if timeout is negative
     * @throws CardException            if the operation failed
     */
    public boolean waitForCardAbsent(long timeout) throws CardException {
        bleStack.disconnect();
        SystemClock.sleep(timeout);
        mac = null;
        card = null;
        return true;
    }

}
