package com.crayonic.sigma;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.crayonic.sigma.crypto.craytoken.LineResponseListener;
import com.crayonic.sigma.identity.CryptoBoundListener;
import com.crayonic.sigma.identity.Identity;
import com.crayonic.sigma.identity.InternalKeyStoreIdentity;
import com.crayonic.sigma.identity.PenIdentity;
import com.crayonic.sigma.monitoring.URLMonitor;
import com.crayonic.sigma.QR.CreateQRCode;
import com.crayonic.sigma.pdf.SigmaActivity;
import com.crayonic.sigma.pdf.UserAuthenticatedListener;
import com.crayonic.sigma.pdf.Utils;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.crayonic.sigma.services.HTTPServices;
import com.crayonic.sigma.services.Scans2PDFService;
import com.crayonic.sigma.tools.ByteStreams;
import com.crayonic.sigma.tools.Crypto;
import com.crayonic.sigma.tools.SecureURL;
import com.crayonic.sigma.tools.UnzipUtility;
import com.crayonic.sigma.web.JS2JavaInterface;
import com.crayonic.sigma.web.SigmaWebViewClient;
import com.crayonic.sigma.web.SigmaWebViewDownloadListener;
import com.crayonic.sigma.web.WebIntent;
import com.crayonic.sigma.scanner.ScannerConstants;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import org.json.JSONException;
import org.json.JSONObject;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.xwalk.core.XWalkPreferences;
import org.xwalk.core.XWalkView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends AppCompatActivity
        implements ProgressDialogListener, UserAuthenticatedListener, CryptoBoundListener, LineResponseListener {
    private static final String TAG = "MainActivity";

    // get mBaseURL for this web session
    public String mBaseURL;
    private Context mContext;
    private XWalkView mXWalkView;
    private ProgressDialog mProgress;
    private AlertDialog.Builder mAlertBuilder;
    private boolean mDoubleBackToExitPressedOnce;
    private Identity mSigmaIdentity;
    private String mApplicationVersionName;


    static {
        Security.addProvider(new BouncyCastleProvider());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //setup view
        setContentView(R.layout.activity_main);


        try {
            mApplicationVersionName = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        //Sigma.init(this.getApplicationContext()); // store global app context so we can access it statically
        mContext = this;
        mAlertBuilder = new AlertDialog.Builder(this);
        mDoubleBackToExitPressedOnce = false;


        registerLocalBroadcastReceivers();

        // Xwalk preferences
        XWalkPreferences.setValue(XWalkPreferences.REMOTE_DEBUGGING, true);
        mXWalkView = (XWalkView) findViewById(R.id.web_content);
        // decide if to enter callback URL or just main UI page

        browseURL();


        mXWalkView.setVisibility(View.VISIBLE);


        // force landscape if set
        if (Sigma.applicationProperties.getProperty("landscape_mode", "true").equalsIgnoreCase("true")) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        // go to splash screen if no permissions
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(this, SplashScreenActivity.class);
            startActivity(intent);
            finish();
        }


        // Bind to identity creation/selection service if no identity selected yet
//        if (Sigma.authenticatedIdentity == null || mSigmaIdentity == null) {
//            bindIdentity();
//        } else {
        startActivityOnIntent();
//        }
    }


    private void browseURL() {

        // .. and preproces main HTML UI so we can show version etc.
        processHTMLTemplate(Sigma.getWWWPath() + Sigma.applicationProperties.getProperty("main_page"));  // replace some basic tags with dynamic data e.g. app version number
        processHTMLTemplate(Sigma.getWWWPath() + Sigma.applicationProperties.getProperty("identity_page"));  // replace some basic tags with dynamic data e.g. app version number


        Intent intent = getIntent();
        if (intent.hasExtra(WebIntent.CALLBACK_URL))
            webBrowse(intent.getExtras().getString(WebIntent.CALLBACK_URL), null, null); // go straight to callback URL if any on restart
        else {
            String mainPage = Sigma.getWWWPath() + Sigma.applicationProperties.getProperty("main_page");
            webBrowse(mainPage, null, null);
        }
    }


    private void checkHostsConnectivity() {
        (new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... arg0) {  // return cert Aliases

                Set<String> names = Sigma.applicationProperties.stringPropertyNames();
                for (String prop : names) {
                    if (prop.endsWith("url")) {
                        String url = Sigma.applicationProperties.getProperty(prop);
                        if (url != null && !url.isEmpty()) {
                            try {
                                URI uri = new URI(url);
                                if (!URLMonitor.pingHost(uri.getHost(), uri.getPort(), uri.getScheme(), 3000)) {
                                    return uri.getHost();// host not available
                                }
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                                Log.wtf(TAG, "Invalid URL specified in property file: " + prop);
                            }
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(String unreachableHost) {
                if (unreachableHost != null) {
                    final AlertDialog alert = mAlertBuilder.create();
                    alert.setTitle(getString(R.string.warning));
                    alert.setMessage(getString(R.string.host) + " " + unreachableHost + " " + getString(R.string.is_unreachable));
                    alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.okay), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            alert.dismiss();
                        }
                    });
                    alert.show();
                }
            }

        }).execute();
    }

    //
    private void webBrowse(String url, String content, String matchPDFinLinkRegexp) {
        mXWalkView.setResourceClient(new SigmaWebViewClient(mXWalkView, matchPDFinLinkRegexp, this));
        mXWalkView.setDownloadListener(new SigmaWebViewDownloadListener(this));
        mXWalkView.setLongClickable(false);
        mXWalkView.addJavascriptInterface(new JS2JavaInterface(this), "Sigma");
        mXWalkView.setUserAgentString("CrayonicSigma " + mApplicationVersionName);
        String www = url;

        //FIXME should also sanitize here NOT TO OPEN files from outside of getFilesDir()
        if (url != null && !(url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("file"))) {
            // if not HTTPS or FILE protocol - assume its a local file and create file:///asdfa/  URL with file protocol
            www = Uri.fromFile(new File(url)).toString();
        }
        mXWalkView.load(www, content);
        mXWalkView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    protected void onResume() {

        super.onResume();

        //check connectivity to URLs in prop file
        checkHostsConnectivity();


    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        // unregister receivers for upload/download services
        unregisterLocalBroadcastReceivers();

        if (mSigmaIdentity != null) {
            mSigmaIdentity.close();
            mSigmaIdentity = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {  // only works for singleTop activity
        setIntent(intent); // pass new intent instead of old
        startActivityOnIntent(); // do what you need to do now
        mXWalkView.onNewIntent(intent);
    }

    private void registerLocalBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mDownloadReceiver,
                new IntentFilter(HTTPServices.ACTION_DOWNLOAD_RESULT));
        LocalBroadcastManager.getInstance(this).registerReceiver(mUploadReceiver,
                new IntentFilter(HTTPServices.ACTION_UPLOAD_RESULT));
        LocalBroadcastManager.getInstance(this).registerReceiver(mGetReceiver,
                new IntentFilter(HTTPServices.ACTION_GET_RESULT));
        LocalBroadcastManager.getInstance(this).registerReceiver(mUploadCSRReceiver,
                new IntentFilter(HTTPServices.ACTION_UPLOAD_CSR_RESULT));
    }

    private void unregisterLocalBroadcastReceivers() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mDownloadReceiver);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mUploadReceiver);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mGetReceiver);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mUploadCSRReceiver);
    }


    @Override
    public void cryptoInitialized() {

        if (getIntent().getAction().equalsIgnoreCase(Sigma.CREATE_IDENTITY_ACTION)) {
            // now its safe to call create identity...
            String identityJson = getIntent().getStringExtra(WebIntent.IDENTITY);
            String user = getIntent().getStringExtra(WebIntent.USERNAME);
            String pwd = getIntent().getStringExtra(WebIntent.PWD);
            mSigmaIdentity.createIdentity(identityJson, user, pwd);
        }
    }

    @Override
    public void userAuthenticated() {

        if (mSigmaIdentity == null) {
            Log.wtf(TAG, "This should not happen but if it does exit gracefully");
            Toast.makeText(mContext, "Error looking for identity! - please restart app...", Toast.LENGTH_LONG).show();
            finishAffinity();
        }

        Sigma.authenticatedIdentity = mSigmaIdentity.getSelectedIdentity();
        if (Sigma.authenticatedIdentity == null) {
            // reselect or recreate if none around
            mSigmaIdentity.selectIdentity(Sigma.applicationProperties.getProperty("cert_alias"));
        } else {
            //continue to main menu
            webBrowse(Sigma.getWWWPath() + Sigma.applicationProperties.getProperty("main_page"), null, null);
        }
    }


    private void startActivityOnIntent() {
        String url = null;
        Intent intent = getIntent();

        Utils.dumpIntent(intent); //FIXME

        if (isFinishing() || intent.getAction() == null)
            return; // dont parse intent if another activity is scheduled (e.g. from onActivityResult)


        if (intent.getData() != null) {
            url = intent.getData().toString();
        }

        if (intent.getAction() == null) {
            return;
        }


        // start filtering by actions
        switch (intent.getAction()) {
            case Intent.ACTION_VIEW:
                if (url == null) break;

                String mimeType = FileUtilsPlus.getMimeType(Sigma.getContext(), intent.getData());

                if (mimeType != null && mimeType.contains("pdf")) {
                    // for local files try opening with PDF viewer
                    openSigma(getIntent());
                } else if (mimeType != null && mimeType.contains("image")) {
                    openScanner(intent, Intent.ACTION_SEND);
                } else {
                    // for http URIs do below...
                    if (intent.hasExtra(WebIntent.URL_DOWNLOAD)) {
                        // standard download was already tried so open url in browser
                        webBrowse(url, null, null);  // TODO get regexp from intent here instead null !!!
                        intent.removeExtra(WebIntent.URL_DOWNLOAD);  // reset flag for download/browse choice
                        return;
                    }

                    // no web view flag set so try dowload of file

                    // try to start download of remote PDF in URI with PDF mimetype or possibly  restart activity with URL for HTML page to browse
                    if (startDownload(url) != null) {
                        // ....show waiting dialog
                        mProgress = ProgressDialog.show(mContext, getString(R.string.downloading), getString(R.string.make_sure_online), true);
                    } else {
                        Toast.makeText(mContext, getString(R.string.could_not_download) + url, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "Could not start download for url:" + url);
                    }
                }
                return;
            case Sigma.DOWNLOAD_INTENT_ACTION:
                if (url == null) break;
                // start download of remote PDF in URI without PDF mimetype e.g. encrypted blob or possibly  restart activity with URL for HTML page to browse
                if (startDownload(url) != null) {
                    // ....show waiting dialog
                    mProgress = ProgressDialog.show(mContext, getString(R.string.downloading), getString(R.string.make_sure_online), true);
                } else {
                    Toast.makeText(mContext, getString(R.string.could_not_download) + url, Toast.LENGTH_LONG).show();
                    Log.e(TAG, "Could not start download for url:" + url);
                }
                return;
            case Sigma.UPLOAD_INTENT_ACTION:
                if (url == null) break;
                // if no upload url then go get one and come back here with it again....
                if (!intent.hasExtra(WebIntent.URL_UPLOAD)) {
                    //Use default eSign.io URL if none has been passed in
                    intent.putExtra(WebIntent.URL_UPLOAD, Sigma.applicationProperties.getProperty("esign_default_upload_url") );
                }

                // start upload of local file in URI to URL passed in the extra data
                Toast.makeText(mContext, getString(R.string.uploading) + url, Toast.LENGTH_LONG).show();
                if (intent.hasExtra(WebIntent.URL_UPLOAD)) {
                    String fileName = intent.hasExtra(WebIntent.DOC_NAME) ?
                            intent.getExtras().getString(WebIntent.DOC_NAME) : null;


                    // Decide whether to create new encryption key before upload if none exist
                    String encPassword = null;
                    if(Boolean.valueOf(Sigma.applicationProperties.getProperty("always_encrypt_on_upload", "false")) ){
                         encPassword = Crypto.passwordGen(128);
                    }

                    String key = intent.hasExtra(WebIntent.AES_KEY) ?
                            intent.getExtras().getString(WebIntent.AES_KEY) : encPassword;

                    if(key != null) {
                        intent.putExtra(WebIntent.AES_KEY, key); // and save it
                    }

                    if (startUpload(intent.getData(), intent.getExtras().getString(WebIntent.URL_UPLOAD), fileName, key)) {
                        // ....show waiting dialog
                        mProgress = ProgressDialog.show(mContext, getString(R.string.uploading), getString(R.string.make_sure_online), true);
                    } else {
                        Toast.makeText(mContext, getString(R.string.could_not_upload) + url, Toast.LENGTH_LONG).show();
                        Log.e(TAG, "Could not start upload for url:" + url);
                    }
                }
                return;
            case Sigma.SCAN_INTENT_ACTION:
                // start scanner plugin
                //Toast.makeText(mContext, "Starting scanner", Toast.LENGTH_LONG).show();
                openScanner(intent, ScanConstants.ACTION_SCAN_TO_PDF);
//                openScanner(intent.getExtras().getString(JS2JavaInterface.DOC_NAME), intent.getExtras().getString(JS2JavaInterface.DOC_FORMAT),
//                        pagesCount, intent.getExtras().getString(JS2JavaInterface.SCAN_COLOR));
                return;
            case Sigma.READQR_INTENT_ACTION:
                // start qr code reader
                intent.setAction(Intent.ACTION_DEFAULT); // just some plain action so the scanner does not restart when it comes back onResult
                //Toast.makeText(mContext, "Starting QR reader", Toast.LENGTH_LONG).show();
                openQRReader();
                return;

            case Sigma.CREATE_IDENTITY_ACTION:
                // first just bind to identity service ... on crypto initialized callback do create...
                String identityJson = intent.getStringExtra(WebIntent.IDENTITY);
                String user = intent.getStringExtra(WebIntent.USERNAME);
                String pwd = intent.getStringExtra(WebIntent.PWD);

               //check which identity needs to be create in pen or in key store
                if(Sigma.authenticatedIdentity != null) {
                    mSigmaIdentity = new PenIdentity(this, Sigma.applicationProperties.getProperty("cert_alias", InternalKeyStoreIdentity.DEF_USER),
                            Sigma.applicationProperties.getProperty("sign_csr", "false").equalsIgnoreCase("true"));
                }else{
                    mSigmaIdentity = new InternalKeyStoreIdentity(this, Sigma.applicationProperties.getProperty("cert_alias", InternalKeyStoreIdentity.DEF_USER),
                            Sigma.applicationProperties.getProperty("sign_csr", "false").equalsIgnoreCase("true"));
                }


                return;

            case Intent.ACTION_SEND:
                mimeType = null;
                if (intent.getType() != null) {
                    mimeType = intent.getType();
                } else if (intent.getData() != null) {
                    mimeType = getContentResolver().getType(intent.getData());
                }

                if (mimeType == null) {
                    return;
                }

                if (mimeType.contains("pdf")) {
                    // for local files try opening with PDF viewer
                    openSigma(getIntent());
                } else if (mimeType.startsWith("image/")) {
                    openScanner(intent, Intent.ACTION_SEND);
                } else if (mimeType.startsWith("text/html")) {
                    webBrowse(null, intent.getStringExtra(Intent.EXTRA_TEXT), null);
                }
                return;
            case Intent.ACTION_SEND_MULTIPLE:
                mimeType = null;
                if (intent.getType() != null) {
                    mimeType = intent.getType();
                } else if (intent.getData() != null) {
                    mimeType = getContentResolver().getType(intent.getData());
                }

                if (mimeType == null) {
                    return;
                }

                if (mimeType.startsWith("image/")) {
                    openScanner(intent, Intent.ACTION_SEND_MULTIPLE);
                }
                return;
        }
    }

    private boolean startUpload(Uri fileUri, String uploadURL, String fileName, String key) {
        if (uploadURL != null && fileUri != null && uploadURL.length() > 0) {
            //FIXME fileUri should be checked for content://  instead of just passing enc. path
            HTTPServices.startUpload(this, uploadURL, fileUri.getEncodedPath(), fileName, key, null, null);
            return true;
        }
        return false;
    }

    // Read the json response from the server and display QR code base on it and the key
    private void showQR(String url, String key, String jsonResponse) {

        if (jsonResponse == null || jsonResponse.length() < 2) {
            return;  // sanity check
        }

        StringBuilder sb = new StringBuilder(100);
        sb.append("");
        JSONObject jObj;
        try {
            jObj = new JSONObject(jsonResponse);
            if (!jObj.has("key")) throw new JSONException("No 'key' name in JSON response");
            URL sURL = new URL(url);
            sb.append(sURL.getProtocol()).append("://").append(sURL.getAuthority()).append("/"); //.append("l/") -- REMOVED !!!! //  we should have "https://esign.ws/l/"
            sb.append(jObj.getString("key"));  // now we have "https://server.com/adfhklkey"
            if (key != null) {
                sb.append("\n").append(key); // if we have key then QR will have another line added with key string
            }
            Bitmap codeimg;
            try {
                codeimg = CreateQRCode.encodeAsBitmap(sb.toString(), BarcodeFormat.QR_CODE, 800, 800);
            } catch (WriterException e) {
                Log.e(TAG, "Cannot create QR code: " + e);
                Toast.makeText(mContext, "Cannot create QR code", Toast.LENGTH_LONG).show();
                return;
            }
            ImageView v = new ImageView(mContext);
            v.setImageBitmap(codeimg);
            final AlertDialog QRalert = mAlertBuilder.create();
            QRalert.setTitle("Scan this QR");
            QRalert.setView(v);
            QRalert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.okay), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    QRalert.dismiss();
                }
            });
            QRalert.show();
        } catch (Exception e) {
            Toast.makeText(mContext, "Bad server response", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Server unexpected response on upload :" + jsonResponse);
        }
    }


    private void openFileBrowser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        try {
            //  intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivityForResult(intent, Sigma.PICKFILE_RESULT_CODE);
        } catch (ActivityNotFoundException e) {
            Log.e("tag", "No activity can handle picking a file. Showing alternatives.");
        }
    }

    private void openQRReader() {
        final IntentIntegrator integrator = new IntentIntegrator((MainActivity) mContext);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt(getString(R.string.read_qr));
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBeepEnabled(true);
        integrator.initiateScan();
    }

    private void openScanner(Intent origIntent, String action) {
        // create some temp file and scan into it
        try {
            String docName = origIntent.hasExtra(WebIntent.DOC_NAME) ? origIntent.getExtras().getString(WebIntent.DOC_NAME)
                    : "scan" + new Date().getTime() + ".pdf";

            String filePath = new File(mContext.getCacheDir(), docName).getAbsolutePath();

            // Better if this service would be started by Scanner?
            Intent serviceIntent = new Intent(MainActivity.this, Scans2PDFService.class);
            serviceIntent.setData(Uri.parse(filePath));
            serviceIntent.putExtras(origIntent);

            startService(serviceIntent); // start the PDF scan service accepting page additions from scanner activity

            // now start the scanner of choice implicitly so other scnners could be implemented externaly
            Intent actIntent = null;
            actIntent = new Intent(origIntent); // copy orig intent data if we have some image data in it already
            actIntent.setClass(mContext, ScanActivity.class);

//            if (Intent.ACTION_SEND.equals(origIntent.getAction()) && origIntent.getType() != null) {
//                if (origIntent.getType().startsWith("image/")) {
//                    actIntent = new Intent(origIntent); // copy orig intent data if we have some image data in it already
//                    actIntent.setClass(mContext, ScanActivity.class);
//                }
//            } else {
//                //.. if not than create new one
//                actIntent = new Intent(mContext, ScanActivity.class);
//                actIntent.putExtras(origIntent); // copy extras
//            }

            // set action according to orig intent (etiher starts a cam or just uses image)

            actIntent.setAction(action);


            // add UI colors
            actIntent.putExtra(ScannerConstants.UI_ACCENT, Sigma.mdpAccent);
            actIntent.putExtra(ScannerConstants.UI_PRIMARY, Sigma.mdpPrimary);
            actIntent.putExtra(ScannerConstants.UI_PRIMARY_TEXT, Sigma.mdpPrimaryText);
            actIntent.putExtra(ScannerConstants.UI_DIVIDER, Sigma.mdpDivider);
            actIntent.putExtra(ScannerConstants.UI_PRIMARY_DARK, Sigma.mdpPrimaryDark);
            actIntent.putExtra(ScannerConstants.UI_PRIMARY_LIGHT, Sigma.mdpPrimaryLight);
            actIntent.putExtra(ScannerConstants.UI_SECONDARY_TEXT, Sigma.mdpSecondaryText);
            String fontPath = Sigma.applicationProperties.getProperty("mdp_primary_font");
            if (fontPath != null && fontPath.length() > 0) {
                fontPath = Sigma.getFontsPath() + fontPath;
                actIntent.putExtra(ScannerConstants.UI_FONT_PATH, fontPath);
            }

            String max_px_size  = Sigma.applicationProperties.getProperty("max_scan_px_size");
            if(max_px_size != null){
                if(TextUtils.isDigitsOnly(max_px_size)){
                    actIntent.putExtra(WebIntent.SCAN_SIZE, Integer.valueOf(max_px_size));
                }
            }


            String jpgQuality  = Sigma.applicationProperties.getProperty("scan_quality");
            if(jpgQuality != null) {
                if (TextUtils.isDigitsOnly(jpgQuality)) {
                    actIntent.putExtra(WebIntent.SCAN_QUALITY, Integer.valueOf(jpgQuality));
                }
            }


            String cropOption  = Sigma.applicationProperties.getProperty("scan_crop", "auto");
            actIntent.putExtra(WebIntent.SCAN_CROP, cropOption);


            startActivity(actIntent);
        } catch (Exception e) {
            Toast.makeText(mContext, "Cannot make temp file for scan", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Cannot make temp file for scan" + e.toString());
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check return from  QR code scanner first
        final IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null && scanResult.getContents() != null) {
            String url = scanResult.getContents();

            Intent intent = new Intent(mContext, MainActivity.class);
            intent.setAction(Sigma.DOWNLOAD_INTENT_ACTION);
            intent.setData(Uri.parse(url));
            mContext.startActivity(intent);
            return;
        } else  // finally check if we are coming back from local file browser
            if (requestCode == Sigma.PICKFILE_RESULT_CODE && data != null) {
                if (resultCode == RESULT_OK) {
                    String filePath = data.getData().getPath();
                    openSigma(new File(filePath), null, null, null, null, null);
                    setIntent(new Intent()); // clear intent so we dont start any in onResume
                }
            }

        mXWalkView.onActivityResult(requestCode, resultCode, data);  // pass to web view as well
        //mXWalkView.reload(XWalkView.RELOAD_NORMAL);
    }


    /*
    Parses URL for HTTP/FQR protocol and downloads (optionaly decrypts) remote PDF file into temp file in cache
        returns true if download started
     */
    private Intent startDownload(String secureUrl) {
        if (secureUrl != null && secureUrl.length() > 0) {
            SecureURL secureURL = new SecureURL(secureUrl).parse();
            // create temp file to download into
            String filePath;
            try {
                filePath = File.createTempFile("sigma", ".pdf", mContext.getCacheDir()).getAbsolutePath();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "Cannot create temp file!");
                return null;
            }
            // finally start download of a file
            return HTTPServices.startDownload(mContext, secureURL.getUrl(), filePath, secureURL.getKey(), null, null);

        }
        return null;
    }


    // Requests filename and opens the new scanned doc with Sigma Activity
    private void requestFileNameAndOpenPDF(final String filePath) {
        final String docname;
        final Intent i = getIntent();

        docname = i.hasExtra(WebIntent.DOC_NAME) ? i.getExtras().getString(WebIntent.DOC_NAME) : null;
        if (docname == null) { // ask for doc name if one is not given via dialog
            View fnView = getLayoutInflater().inflate(R.layout.request_filename, null);
            final EditText input = (EditText) fnView.findViewById(R.id.filenameinput);
            input.setInputType(EditorInfo.TYPE_CLASS_TEXT);
            String fname = new File(filePath).getName();
            input.setText(fname.substring(0, fname.lastIndexOf(".pdf"))); // set input box to original filename - extension
            final AlertDialog alert = mAlertBuilder.create();
            alert.setTitle(R.string.enter_filename);
            alert.setView(fnView);
            alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.okay),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String fileName = input.getText().toString();
                            if (fileName.length() == 0) {
                                // if no data inside editbox then create some  name
                                fileName = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
                            }
                            fileName = fileName.concat(".pdf");
                            // rename old file ".../scanxxxxx.pdf" to new file ".../somename.pdf"
                            File file = new File(filePath);
                            File renamedFile = new File(file.getParentFile(), fileName);
                            boolean renamed = file.renameTo(renamedFile);
                            if (renamed) {
                                file = renamedFile;
                            }
                            alert.dismiss();
                            i.setData(Uri.fromFile(file));
                            i.putExtra(WebIntent.DOC_NAME, docname);
                        }
                    });
            alert.show();
        } else {
            // open Sigma without asking for name
            i.setData(Uri.parse(filePath));
        }

        openSigma(i);
    }


    /*
    The main call for opening PDF view
     */
    public void openSigma(File file, String docname, String uploadURL, String callbackURL, String signatoryJSON, String key) {
        // finally open the scanned doc by starting PDF activity
        Intent intent = new Intent(mContext, SigmaActivity.class);
        Uri uri = Uri.parse(file.getAbsolutePath());
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(uri); //  local file  to open

        if (docname != null) {
            intent.putExtra(WebIntent.DOC_NAME, docname);
        }
        if (uploadURL != null) {
            intent.putExtra(WebIntent.URL_UPLOAD, uploadURL);
        }
        if (callbackURL != null) {
            intent.putExtra(WebIntent.CALLBACK_URL, callbackURL);
        }
        if (signatoryJSON != null) {
            intent.putExtra(WebIntent.SIGNATORY_JSON, signatoryJSON);
        }
        if (key != null) {
            intent.putExtra(WebIntent.AES_KEY, key);
        }

        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private void openSigma(Intent i) {
        // finally open the scanned doc by starting PDF activity
        Intent intent = new Intent(i);
        intent.setClass(mContext, SigmaActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    // check for availability of implict intent called activity
    private static boolean isAvailable(Context ctx, Intent intent) {
        final PackageManager mgr = ctx.getPackageManager();
        List<ResolveInfo> list =
                mgr.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }


    @Override
    public void onBackPressed() {
        // exit only on double back button hit or when not logged in instantly
        if (mDoubleBackToExitPressedOnce || Sigma.authenticatedIdentity == null) {
            // clean up all internal file cache
            FileUtilsPlus.cleanTMPFileCache(mContext);
            finishAffinity();
            return;
        }

        this.mDoubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.back_again_to_exit), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDoubleBackToExitPressedOnce = false;
            }
        }, 2500);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }


    @Override
    public void createProgressDialog(String msg) {
        // ....show waiting dialog
        mProgress = ProgressDialog.show(mContext, getString(R.string.wait), msg, true);
    }

    @Override
    public void dismissProgressDialog() {
        if (mProgress != null) {
            mProgress.dismiss();  // kill any waiting dialog
            mProgress = null;
        }
    }

    private void reStartActivity(String providerName, String hash) {
        // start splash screen activity if new package has been downloaded
        Intent intent = new Intent(mContext, SplashScreenActivity.class);
//        intent.setAction(Intent.ACTION_DEFAULT);
//        if (providerName != null) {
//            intent.putExtra(Sigma.ASSET_PROVIDER_NAME, providerName);
//            intent.putExtra(Sigma.ASSET_PROVIDER_HASH, hash);
//        }

        if (providerName != null)
            ((Sigma) getApplicationContext()).switchAssetProvider(providerName, hash);


        // shut down service when restarting so cryptoservice reloads certs on connect
        if (mSigmaIdentity != null) {
            mSigmaIdentity.close();
        }

        ((MainActivity) mContext).finishAffinity();
        startActivity(intent);
        Sigma.authenticatedIdentity = null; // logout user
    }


    /*
    Disable back button in XWalkView
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_UP)
                onBackPressed();

            return true;
        }
        return false;
    }

    private void processHTMLTemplate(String path) {
        // replace some special tags in local template
        try {
            String html = ByteStreams.convertStreamToString(new FileInputStream(new File(path)));
            html = html.replaceAll("(?i)%VERSION.*%", "%VERSION " + mApplicationVersionName + "%");
            // replace all tags that match properties
            // first find all template variables between %...% tags
            Matcher matcher = Pattern.compile("%.*%").matcher(html);
            List<String> variables = new ArrayList<>(10);
            while (matcher.find()) {
                variables.add(html.substring(matcher.start() + 1, matcher.end() - 1).toLowerCase());
            }
            if (!variables.isEmpty()) {
                // then see if they match any property and if yes replace with prop value
                for (String variable : variables) {
                    String varValue = Sigma.applicationProperties.getProperty(variable);
                    if (varValue != null && !varValue.isEmpty()) {
                        html = html.replaceAll("(?i)%" + variable + "%", varValue);
                    }
                }
            }
            FileUtilsPlus.saveBinaryFile(path, html.getBytes());
        } catch (Exception e) {
            Log.e(TAG, "HTML Assets error" + e);
            Toast.makeText(this, "HTML Assets error", Toast.LENGTH_LONG).show();
        }
    }

    /*---------------------------------------LOCAL Broadcast Receivers---------------------------------*/
    private BroadcastReceiver mUploadCSRReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(HTTPServices.ACTION_UPLOAD_CSR_RESULT)) {
                dismissProgressDialog();

                if (intent.hasExtra(HTTPServices.EXTRA_ERROR)) {
                    Toast.makeText(mContext, "Identity CSR Error:" + intent.getExtras().getString(HTTPServices.EXTRA_ERROR),
                            Toast.LENGTH_LONG).show();
                }
                else if (intent.hasExtra(HTTPServices.EXTRA_DATA_UPLOADRESPONSE)) {
                    mSigmaIdentity.storeIdentity(
                            intent.getStringExtra(HTTPServices.EXTRA_DATA_UPLOADRESPONSE),
                            intent.getStringExtra(HTTPServices.EXTRA_CSR_ALIAS) );
                } else {
                    //No or bad response from the server
                    Log.e(TAG, "Unexpected response by CSR service");
                    Toast.makeText(mContext, mContext.getString(R.string.csr_failed), Toast.LENGTH_LONG).show();
                }

                // restart with splash screen activity now...
                reStartActivity(null, null);
            }
        }
    };


    // Broadcast receiver for receiving result from the GET IntentService
    private BroadcastReceiver mGetReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(HTTPServices.ACTION_GET_RESULT)) {
                dismissProgressDialog();
                if (intent.hasExtra(HTTPServices.EXTRA_ERROR)) {
                    Toast.makeText(mContext, "Get Error:" + intent.getExtras().getString(HTTPServices.EXTRA_ERROR),
                            Toast.LENGTH_LONG).show();
                }
//                // if we were calling GET just to obtain dynamic URL from appengine (not having upload URL) then restart activity with result
//                if (intent.hasExtra(HTTPServices.EXTRA_DATA_GETRESPONSE) &&
//                        !((MainActivity) mContext).getIntent().hasExtra(WebIntent.URL_UPLOAD)) {
//                    // update ACtivity intent with upload URL
//                    String url = intent.getExtras().getString(HTTPServices.EXTRA_DATA_GETRESPONSE);
//
//                    try {
//                        if (new URL(url).toURI().getHost().contains("esign")) {
//                            ((MainActivity) mContext).getIntent().putExtra(WebIntent.URL_UPLOAD, url);  // response should contain URL string
//                            ((MainActivity) mContext).getIntent().setFlags(((MainActivity) mContext).getIntent().getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
//                            ((MainActivity) mContext).recreate(); // ...and restart activity
//                        }
//                    } catch (Exception e) {
//                        Log.e(TAG, "Bad response for dynamic url: " + url);
//                        Toast.makeText(mContext, "Bad URL response.", Toast.LENGTH_LONG).show();
//                    }
//                }
            }
        }
    };


    // Broadcast receiver for receiving result from the download IntentService
    private BroadcastReceiver mDownloadReceiver = new BroadcastReceiver() {
        // Called when the BroadcastReceiver gets an Intent it's registered to receive
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(HTTPServices.ACTION_DOWNLOAD_RESULT)) {
                dismissProgressDialog();
                if (intent.hasExtra(HTTPServices.EXTRA_ERROR)) {
                    Toast.makeText(mContext, "Download Error:" + intent.getExtras().getString(HTTPServices.EXTRA_ERROR),
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (intent.hasExtra(HTTPServices.EXTRA_DATA_FILENAME) && intent.hasExtra(HTTPServices.EXTRA_FILE_PATH)) {
                    // Download completed OK and open it in Sigma
                    // copy intent data from original main activity to sigma activity e.g. the upload url, signatory data, etc.
                    String fpath = intent.getStringExtra(HTTPServices.EXTRA_FILE_PATH);
                    int i = fpath.lastIndexOf('.');
                    if (i < 1) {
                        Log.e(TAG, "Downloaded something without proper filename - ignored");
                        return;
                    }
                    String filename = intent.getStringExtra(HTTPServices.EXTRA_DATA_FILENAME);

                    String ext = filename.substring(filename.lastIndexOf('.') + 1);
                    if (ext.equalsIgnoreCase("pdf")) {
                        Intent origIntent = getIntent();
                        origIntent.setData(Uri.parse(fpath));
                        origIntent.putExtras(intent.getExtras()); // copy extras e.g. name, key, file origin, local file
                        openSigma(origIntent);
                    } else if (ext.equalsIgnoreCase("zip")) {
                        // Make filename the destination folder name and the name of the assets provider!
                        final String providerName = filename.substring(0, filename.length() - 4);
                        try {
                            // check HASH of ZIP and unzip only if different
                            final String hash = Crypto.sha1(new File(fpath));
                            if (Sigma.isSamePackageHash(hash)) {
                                Log.i(TAG, "Downloaded ZIP provider package is same as one on the device....skipping unzip.");
                                return;
                            }

                            UnzipUtility.unzip(fpath, Sigma.getDirForPackage(providerName));
                            // restart app after reconfiguring
                            final AlertDialog alert = mAlertBuilder.create();
                            alert.setTitle(getString(R.string.new_config_package_restart));
                            alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.okay), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    reStartActivity(providerName, hash);
                                }
                            });
                            alert.show();
                        } catch (IOException e) {
                            Log.wtf(TAG, "Cannot unzip or hash downloaded package: " + fpath + "  to: " + Sigma.getDirForPackage(providerName));
                        }
                    }

                    //check for HTML content present
                } else if (intent.hasExtra(HTTPServices.EXTRA_FILE_PATH) &&
                        intent.getExtras().getString(HTTPServices.EXTRA_FILE_PATH).equalsIgnoreCase("HTML")) { // is the download URL  web page? -> call web view
                    Intent htmlIntent = new Intent(getIntent());  // copy orig intent data
                    htmlIntent.setAction(Intent.ACTION_VIEW);
                    htmlIntent.putExtra(WebIntent.URL_DOWNLOAD, intent.getExtras().getString(HTTPServices.EXTRA_REMOTE_URL)); // specify that file download was already tried
                    if (intent.hasExtra(HTTPServices.EXTRA_AES_KEY)) {
                        htmlIntent.putExtra(WebIntent.AES_KEY, intent.getExtras().getString(HTTPServices.EXTRA_AES_KEY)); // transfer enc. key if one is present
                    }
                    //htmlIntent.setFlags(htmlIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(htmlIntent);
                }
//                else {
//                    // download failed....
//                    if (intent.hasExtra(HTTPServices.EXTRA_REMOTE_URL)) {
//                        Toast.makeText(mContext, "Failed to download:" + intent.getExtras().getString(HTTPServices.EXTRA_REMOTE_URL),
//                                Toast.LENGTH_LONG).show();
//                        Log.e(TAG, "Failed to download:" + intent.getExtras().getString(HTTPServices.EXTRA_REMOTE_URL));
//                    }
//                    Log.e(TAG, "Failed to download!");
//                }
            }
        }
    };


    private BroadcastReceiver mUploadReceiver = new BroadcastReceiver() {
        // Broadcast receiver for receiving result from the upload IntentService
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(HTTPServices.ACTION_UPLOAD_RESULT)) {
                dismissProgressDialog();
                if (intent.hasExtra(HTTPServices.EXTRA_ERROR)) {
                    Toast.makeText(mContext, "Upload Error:" + intent.getExtras().getString(HTTPServices.EXTRA_ERROR),
                            Toast.LENGTH_LONG).show();

//                    Toast.makeText(mContext, getString(R.string.failed_to_upload) + intent.getExtras().getString(HTTPServices.EXTRA_DATA_FILENAME),
//                            Toast.LENGTH_LONG).show();
                    Log.e(TAG, "Failed to upload " + intent.getExtras().getString(HTTPServices.EXTRA_DATA_FILENAME) +
                            " to URL:" + intent.getExtras().getString(HTTPServices.EXTRA_REMOTE_URL));
                    return;
                }


                if (intent.hasExtra(HTTPServices.EXTRA_DATA_UPLOADRESPONSE)) {
                    String response = intent.getExtras().getString(HTTPServices.EXTRA_DATA_UPLOADRESPONSE);
                    Log.d(TAG, "Upload  response:" + response);
                    // Upload completed OK
                    //...so call webview if callback URL is via intent to Main Activity
                    if (((MainActivity) mContext).getIntent().hasExtra(WebIntent.CALLBACK_URL)) {
                        Intent webIntent = new Intent(mContext, MainActivity.class);
                        webIntent.setAction(Intent.ACTION_VIEW);
                        webIntent.putExtras(((MainActivity) mContext).getIntent()); // first copy orig and then override some extras
                        String callbackURL = ((MainActivity) mContext).getIntent().getExtras().getString(WebIntent.CALLBACK_URL);
                        webIntent.putExtra(WebIntent.URL_DOWNLOAD, callbackURL);
                        webIntent.setData(Uri.parse(callbackURL));
                        // webIntent.setFlags(webIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(webIntent);
                    } else if (((MainActivity) mContext).getIntent().hasExtra(WebIntent.SHOW_QR) && intent.hasExtra(HTTPServices.EXTRA_REMOTE_URL)) {
                        // or show qr code if one is requested
                        String key = intent.hasExtra(HTTPServices.EXTRA_AES_KEY) ? intent.getExtras().getString(HTTPServices.EXTRA_AES_KEY)
                                : null;
                        String url = intent.getExtras().getString(HTTPServices.EXTRA_REMOTE_URL);
                        showQR(url, key, response);
                    } else {
                        //... or just let user know all is good
                        Toast.makeText(mContext, getString(R.string.document_uploaded), Toast.LENGTH_LONG).show();
                    }

                }
//                else {
//                    // Upload failed....
//                    if (intent.hasExtra(HTTPServices.EXTRA_REMOTE_URL) && intent.hasExtra(HTTPServices.EXTRA_DATA_FILENAME)) {
//                        Toast.makeText(mContext, getString(R.string.failed_to_upload) + intent.getExtras().getString(HTTPServices.EXTRA_DATA_FILENAME),
//                                Toast.LENGTH_LONG).show();
//                        Log.e(TAG, "Failed to upload " + intent.getExtras().getString(HTTPServices.EXTRA_DATA_FILENAME) +
//                                " to URL:" + intent.getExtras().getString(HTTPServices.EXTRA_REMOTE_URL));
//                    }
//                    Log.e(TAG, "Failed to Upload");
//                }
            }
        }
    };


    @Override
    public void writeLine(final String line) {
        // recieved new line from Pen Admin command response - show to user
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(mContext, line, Toast.LENGTH_LONG).show();
            }
        });
    }
}

