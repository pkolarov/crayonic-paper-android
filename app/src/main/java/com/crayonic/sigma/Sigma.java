package com.crayonic.sigma;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.util.Log;

import com.crayonic.sigma.pdf.file.FileUtilsPlus;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Properties;

/**
 * Created by kolarov on 1.10.2015.
 */
public class Sigma extends android.app.Application {
    public static final int PICKFILE_RESULT_CODE = 10;
    // INTENT ACTIONS SUPPORTED BY APP
    public static final String UPLOAD_INTENT_ACTION = "com.crayonic.sigma.UPLOAD";  // Uploads files if network is available (TODO queues if no net)
    public static final String DOWNLOAD_INTENT_ACTION = "com.crayonic.sigma.DOWNLOAD"; // Downloads files/html wizards if network available (TODO  queues if no net)
    public static final String SCAN_INTENT_ACTION = "com.crayonic.sigma.SCAN";      // Opens document scanning activity - creates new PDFs by scanning
    public static final String READQR_INTENT_ACTION = "com.crayonic.sigma.READQR";      // QR reading activity for obtaining wizard or PDF URL (including FQR)
    public static final String CREATE_IDENTITY_ACTION = "com.crayonic.sigma.CREATE_IDENTITY";  // creates identity using certificate and optionaly signing it
    private static final String TAG = "Sigma";
    //    public static final String ASSET_PROVIDER_NAME = "com.crayonic.sigma.PROVIDER_NAME"; // which asset package to start the app with (if not present then default)
//    public static final String ASSET_PROVIDER_HASH = "com.crayonic.sigma.PROVIDER_HASH"; // hash of asset package to start the app with (if not present then default)
    private static Context instance;


    public static final String ASSET_PATH = "assets"; // destination of configuration assets copy  /data/data/{packagename}/ASSET_PATH  (for default configuration only)
    private static final String DEFAULT_PROVIDER_PACKAGE = ASSET_PATH;
    public static final String WWW_ASSETS = "www";  // folder in assets holding build in HTML UI
    public static final String CERT_ASSETS = "cert"; // folder in assets holding trusted certs
    public static final String PROP_ASSETS = "props"; // folder in assets holding app properties
    public static final String FONT_ASSETS = "fonts";  // folder in assets holding build in HTML UI
    private static final String PROP_FILE_NAME = "Application.properties";  // this file  must be present in /props folder!!!



    public static String authenticatedIdentity;   // certificate alias that has been authenticated to use for signatures and SSL connection
    public static KeyStore applicationKeyStore;    // keystore that should hold the above alias certificate and its private key along with SSL certificate/key if different (cannot be null when signing doc)
    public static KeyStore applicationTrustStore;  // trust store for the application (if null the default OS trust store is used in Signature verification and SSL context)

    private static String providerAssetsDir;  // path to static assets of given provider (e.g. enterprise assets)
    private static String providerAssetsHash; // SHA of ZIP file for checking
    // (NOTE: access to the applicationKeyStore should be primarily via crypto services since the crypto engine may take a long time to acceess key store )

    public static Properties applicationProperties;

    public static int mdpPrimary ;
    public static int mdpPrimaryDark ;
    public static int mdpPrimaryLight ;
    public static int mdpAccent ;
    public static int mdpPrimaryText ;
    public static int mdpSecondaryText ;
    public static int mdpIcons ;
    public static int mdpDivider ;



    @Override
    public void onCreate(){
        super.onCreate();
        //initialize the default external  Assets Dir (internal assets are moved here)
        providerAssetsDir = (new File(getFilesDir(), Sigma.ASSET_PATH)).getAbsolutePath(); // set initially to default assets path;
        instance = getApplicationContext();
        //Create data dir
        File rootDataPath = getFilesDir();
        if (!rootDataPath.exists()) rootDataPath.mkdir();


        // see if another external provider package exists yet
        SharedPreferences prefs = getSharedPreferences("com.crayonic.sigma", MODE_PRIVATE);

        // for debugging always copy default assets so we can debug them too (not needed in production)
        if (BuildConfig.DEBUG) {
            FileUtilsPlus.copyAssetFolder(getAssets(), Sigma.WWW_ASSETS, Sigma.getWWWPath());
            FileUtilsPlus.copyAssetFolder(getAssets(), Sigma.CERT_ASSETS, Sigma.getCertPath());
            FileUtilsPlus.copyAssetFolder(getAssets(), Sigma.PROP_ASSETS, Sigma.getPropPath());
            FileUtilsPlus.copyAssetFolder(getAssets(), Sigma.FONT_ASSETS, Sigma.getFontsPath());
        }

        if (prefs.contains("assetPackage")) {
            String assetPackage =  prefs.getString("assetPackage", DEFAULT_PROVIDER_PACKAGE);
            String assetPackageHash = prefs.getString("assetPackageHash", "0");
            switchAssetProvider(assetPackage, assetPackageHash);
        }else {
            // if no external assets provider then use default provider from internal assets
            FileUtilsPlus.copyAssetFolder(getAssets(), Sigma.WWW_ASSETS, Sigma.getWWWPath());
            FileUtilsPlus.copyAssetFolder(getAssets(), Sigma.CERT_ASSETS, Sigma.getCertPath());
            FileUtilsPlus.copyAssetFolder(getAssets(), Sigma.PROP_ASSETS, Sigma.getPropPath());
            FileUtilsPlus.copyAssetFolder(getAssets(), Sigma.FONT_ASSETS, Sigma.getFontsPath());
            // and switch to it
            switchAssetProvider(DEFAULT_PROVIDER_PACKAGE, "0");
        }
    }



    public static Context getContext() {
        return instance;
    }



    public  void switchAssetProvider(String assetPackage, String assetPackageHash) {
        if (assetPackage != null) {
            Log.d(TAG, "Switching asset package from: "+ providerAssetsDir);
            SharedPreferences prefs = getSharedPreferences("com.crayonic.sigma", MODE_PRIVATE);
            // found some provider package so set it
            setProviderAssets(assetPackage, assetPackageHash);
            Log.d(TAG, "...to: "+ providerAssetsDir);
            // and save for next session
            prefs.edit().putString("assetPackage", assetPackage).apply();
            prefs.edit().putString("assetPackageHash", assetPackageHash).apply();
        }

        // now its a good time to load our properties
        loadAppProperties();
    }


    private void loadAppProperties() {
        try {
            applicationProperties = new Properties();
            FileInputStream inputStream = new FileInputStream(new File(providerAssetsDir + File.separator + PROP_ASSETS + File.separator + PROP_FILE_NAME));
            //loading of the applicationProperties
            applicationProperties.load(inputStream);
        } catch (IOException e) {
            Log.e("PropertiesReader", e.toString());
        }

        initUIColors();
    }

    private void initUIColors() {
        mdpPrimary = Color.parseColor(Sigma.applicationProperties.getProperty("mdp_primary", "#607D8B"));
        mdpPrimaryDark = Color.parseColor(Sigma.applicationProperties.getProperty("mdp_primary_dark", "#455A64"));
        mdpPrimaryLight = Color.parseColor(Sigma.applicationProperties.getProperty("mdp_primary_light", "#CFD8DC"));
        mdpAccent = Color.parseColor(Sigma.applicationProperties.getProperty("mdp_accent", "#536DFE"));
        mdpPrimaryText = Color.parseColor(Sigma.applicationProperties.getProperty("mdp_primary_text", "#212121"));
        mdpSecondaryText = Color.parseColor(Sigma.applicationProperties.getProperty("mdp_secondary_text", "#727272"));
        mdpIcons = Color.parseColor(Sigma.applicationProperties.getProperty("mdp_icons", "#FFFFFF"));
        mdpDivider = Color.parseColor(Sigma.applicationProperties.getProperty("mdp_divider", "#B6B6B6"));
    }


    public static String getDirForPackage(String assetPackage){
        return  (new File(instance.getFilesDir(), assetPackage)).getAbsolutePath(); // set initially to default assets path
    }

    public static boolean isSamePackageHash(String packageHash){
        Log.i("SIGMA","Comparing incoming hash: " + packageHash + " with existing hash: " + providerAssetsHash.trim());
        return providerAssetsHash != null && providerAssetsHash.trim().equalsIgnoreCase(packageHash);
    }

    public static String getProviderAssetsHash(){
        return providerAssetsHash;
    }

    private void setProviderAssets(String assetPackage, String hash) {
        providerAssetsDir =  (new File(instance.getFilesDir(), assetPackage)).getAbsolutePath(); // set initially to default assets path
        providerAssetsHash = hash;
    }


    public static String getCertPath(){
        return providerAssetsDir + File.separator + CERT_ASSETS + File.separator;
    }

    public static String getPropPath(){
        return providerAssetsDir + File.separator + PROP_ASSETS + File.separator;
    }

    public static String getWWWPath(){
        return providerAssetsDir + File.separator + WWW_ASSETS + File.separator;
    }

    public static String getFontsPath(){
        return providerAssetsDir + File.separator + FONT_ASSETS + File.separator;
    }


}
