package com.crayonic.sigma.SSL;

import android.util.Log;

import org.spongycastle.crypto.CryptoException;

import java.io.IOException;
import java.net.Socket;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.X509ExtendedKeyManager;

/**
 * Created by peter kolarov on 26/11/15.
 */
public class SSLKeyManager extends X509ExtendedKeyManager {
    private static final String TAG = "SSLKeyManager";
    public static final String SIGNED_POSTFIX = "";//"-Signed";
    private final KeyStore keystore;
    private final String certificateAlias;

    public SSLKeyManager(KeyStore keystore, String certificateAlias){
        this.keystore = keystore;
        this.certificateAlias = certificateAlias;
    }

    /**
     * Chooses an alias for the client side of an SSL connection to authenticate
     * it with the specified public key type and certificate issuers.
     *
     * @param keyType the list of public key algorithm names.
     * @param issuers the list of certificate issuers, or {@code null} if any issuer
     *                will do.
     * @param socket  the socket for the connection, or {@code null} if
     *                the alias selected does not depend on a specific socket.
     * @return the alias name of a matching key or {@code null} if there are no
     * matches.
     */
    @Override
    public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {
        return certificateAlias;
    }

    /**
     * Chooses an alias for the server side of an SSL connection to authenticate
     * it with the specified public key type and certificate issuers.
     *
     * @param keyType the list of public key algorithm type names.
     * @param issuers the list of certificate issuers, or {@code null} if any issuer
     *                will do.
     * @param socket  the socket for the connection, or {@code null} if
     *                the alias selected does not depend on a specific socket.
     * @return the alias name of a matching key or {@code null} if there are no
     * matches.
     */
    @Override
    public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {
        return null;
    }

    /**
     * Returns the certificate chain for the specified alias.
     *
     * @param alias the alias to get the certificate chain for.
     * @return the certificate chain for the specified alias, or {@code null} if
     * the alias cannot be found.
     */
    @Override
    public X509Certificate[] getCertificateChain(String alias) {
        try {
            X509Certificate[] x509Certificates = getX509CertificatesForPKAlias(keystore, certificateAlias);


            return x509Certificates;
            
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Cannot find signed Certificate  for authenticated user alias " + alias );
        }

        return null;
    }


    public static  X509Certificate[] getX509CertificatesForPKAlias(KeyStore keystore, String alias) throws KeyStoreException, IOException, CryptoException {
        // get signed cert
        Certificate[] certificates = keystore.getCertificateChain(alias + SIGNED_POSTFIX);
        X509Certificate[] x509Certificates = Arrays.copyOf(certificates, certificates.length, X509Certificate[].class);

/*
        InputStream is = Sigma.getContext().getAssets().open("cert/AmicoesignuserCA-chain.pem");
        String pemChain = ByteStreams.convertStreamToString(is);
        StringReader sr = new StringReader(pemChain);
        Certificate[] chain = PKIUtils.loadCertificateChainFromPEM(sr);
*/
        // merge & cast
/*
        X509Certificate[] x509Certificates = new X509Certificate[certificates.length];
        System.arraycopy(certificates, 0,  x509Certificates, 0, certificates.length);  // client cert is first and root cert is last
        System.arraycopy(chain,0,  x509Certificates, 1, chain.length);
*/
        return x509Certificates;
    }

    /**
     * Returns the client aliases for the specified public key type and list of
     * certificate issuers.
     *
     * @param keyType the public key algorithm type name.
     * @param issuers the list of certificate issuers, or {@code null} if any issuer
     *                will do.
     * @return the client aliases for the specified public key type, or
     * {@code null} if there are no matching aliases.
     */
    @Override
    public String[] getClientAliases(String keyType, Principal[] issuers) {
        return new String[]{ certificateAlias };
    }

    /**
     * Returns the server aliases for the specified public key type and list of
     * certificate issuers.
     *
     * @param keyType the public key algorithm type name.
     * @param issuers the list of certificate issuers, or {@code null} if any issuer
     *                will do.
     * @return the client aliases for the specified public key type, or
     * {@code null} if there are no matching aliases.
     */
    @Override
    public String[] getServerAliases(String keyType, Principal[] issuers) {
        return null;
    }

    /**
     * Returns the private key for the specified alias.
     *
     * @param alias the alias to get the private key for.
     * @return the private key for the specified alias, or {@code null} if the
     * alias cannot be found.
     */
    @Override
    public PrivateKey getPrivateKey(String alias) {
        if(alias != certificateAlias) { //sanity check
            Log.wtf(TAG, String.format("X509ExtendedKeyManager is asking for privateKey with unknown alias %s. Expecting it to ask for %s", alias, certificateAlias));
            return null;
        }

        try {
            return ((KeyStore.PrivateKeyEntry)keystore.getEntry(alias, null)).getPrivateKey();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
