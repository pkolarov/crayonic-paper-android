package com.crayonic.sigma.SSL;

import android.util.Log;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 * Created by peter on 26/11/15.
 */
public class SSLTrustManager implements X509TrustManager {
    private static final String TAG = "SSLTrustManager" ;

    /**
     * Checks whether the specified certificate chain (partial or complete) can
     * be validated and is trusted for client authentication for the specified
     * authentication type.
     *
     * @param chain    the certificate chain to validate.
     * @param authType the authentication type used.
     * @throws CertificateException     if the certificate chain can't be validated or isn't trusted.
     * @throws IllegalArgumentException if the specified certificate chain is empty or {@code null},
     *                                  or if the specified authentication type is {@code null} or an
     *                                  empty string.
     */
    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        Log.d(TAG, "Called to check up on chain!");

    }

    /**
     * Checks whether the specified certificate chain (partial or complete) can
     * be validated and is trusted for server authentication for the specified
     * key exchange algorithm.
     *
     * @param chain    the certificate chain to validate.
     * @param authType the key exchange algorithm name.
     * @throws CertificateException     if the certificate chain can't be validated or isn't trusted.
     * @throws IllegalArgumentException if the specified certificate chain is empty or {@code null},
     *                                  or if the specified authentication type is {@code null} or an
     *                                  empty string.
     */
    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

        // code to validate server's cert in here
        Log.d(TAG, "Check if server trusted given key exchange: " + authType);
    }

    /**
     * Returns the list of certificate issuer authorities which are trusted for
     * authentication of peers.
     *
     * @return the list of certificate issuer authorities which are trusted for
     * authentication of peers.
     */
    @Override
    public X509Certificate[] getAcceptedIssuers() {

        // TODO all CAs are trusted for now - add our Root CA cert later
        return null;
    }
}
