package com.crayonic.sigma.SSL;


import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;

import org.spongycastle.util.encoders.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by peter k on 03/06/16.
 * <p/>
 * Mutual SSL authentication implementation of TSA timestamp
 */
public class TSAClientMutualSSL extends TSAClientBouncyCastle {

    private final KeyStore keyStore;
    private final KeyStore trustStore;
    private final String keyStorePassword;

    public TSAClientMutualSSL(String url, KeyStore keyStore, KeyStore trustStore, String keyStorePassword) {
        super(url, null, null, DEFAULTTOKENSIZE, DEFAULTHASHALGORITHM);
        this.keyStore = keyStore;
        this.trustStore = trustStore;
        this.keyStorePassword = keyStorePassword;
    }

    @Override
    public byte[] getTSAResponse(byte[] requestBytes) throws IOException {
        //Setup SSL Context
        try {
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, keyStorePassword.toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustStore);

            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
            SSLSocketFactory sf = ctx.getSocketFactory();

            URL url = new URL(this.tsaURL);
            HttpsURLConnection tsaconn = (HttpsURLConnection) url.openConnection();
            tsaconn.setSSLSocketFactory(sf);
            tsaconn.setConnectTimeout(60000);
            tsaconn.setDoInput(true);
            tsaconn.setDoOutput(true);
            tsaconn.setUseCaches(false);
            tsaconn.setRequestProperty("Content-Type", "application/timestamp-query");
            tsaconn.setRequestProperty("Content-Transfer-Encoding", "binary");
            tsaconn.connect();
            OutputStream out = tsaconn.getOutputStream();
            out.write(requestBytes);
            out.close();
            InputStream inp = tsaconn.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte['?'];
            int bytesRead = 0;
            while ((bytesRead = inp.read(buffer, 0, buffer.length)) >= 0) {
                baos.write(buffer, 0, bytesRead);
            }
            byte[] respBytes = baos.toByteArray();
            String encoding = tsaconn.getContentEncoding();
            if ((encoding != null) && (encoding.equalsIgnoreCase("base64"))) {
                respBytes = Base64.decode(new String(respBytes));
            }
            return respBytes;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException(e);
        }
    }
}
