package com.crayonic.sigma;

import android.Manifest;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.crayonic.sigma.identity.CryptoBoundListener;
import com.crayonic.sigma.identity.Identity;
import com.crayonic.sigma.identity.InternalKeyStoreIdentity;
import com.crayonic.sigma.pdf.UserAuthenticatedListener;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.crayonic.sigma.tools.Crypto;
import com.crayonic.sigma.tools.HTTPGETUtilities;
import com.crayonic.sigma.tools.UnzipUtility;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Splash screen activity receives all of the intents and passes them onto MainActivity
 * It also initializes the package provider and creates initial package with properties, certs and UI
 */

public class SplashScreenActivity extends AppCompatActivity implements UserAuthenticatedListener,
        CryptoBoundListener, ProgressDialogListener {
    private static final int PERMISSION_ALL = 300;
    // check first permissions
    private static String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.BLUETOOTH};
    private static final String TAG = "SplashScreen";
    private AlertDialog.Builder mAlertBuilder;
    private Intent mSavedIntent;
    private Context mContext;
    private Identity mSigmaIdentity;

    Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    //start main activity with delay
                    startMainActivity();
                default:
                    break;
            }
            return false;
        }
    });
    private ProgressDialog mProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mSavedIntent = getIntent(); //save original intent for restart

        FileUtilsPlus.cleanTMPFileCache(this); // clear cache before start as well
        PackageManager pm = this.getPackageManager();

        // add SMS permission only on phones
        if (pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
            PERMISSIONS = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.BLUETOOTH,
                    Manifest.permission.SEND_SMS};
        }


        // request permissions
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            provisionAndStart();
        }
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            provisionAndStart();
        }
    }


    private void provisionAndStart() {
        // initialize all app stuff here once
        mAlertBuilder = new AlertDialog.Builder(this);

        // Check if device is secured by PIN/PASSWORD/PATTERN first
        if (isDeviceSecured())
            return;

        Log.i(TAG, "Device is secured.");

        Toast.makeText(SplashScreenActivity.this, "Please Wait.... Initializing", Toast.LENGTH_LONG).show();

        //find identity and initilize security and trust/key stores...if crypto is inited callback will initiate package download
        mSigmaIdentity = new InternalKeyStoreIdentity(this, Sigma.applicationProperties.getProperty("cert_alias", InternalKeyStoreIdentity.DEF_USER),
                Sigma.applicationProperties.getProperty("sign_csr", "false").equalsIgnoreCase("true"));

    }


    private boolean isDeviceSecured() {
        if (!((KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE)).isKeyguardSecure()) {

            final AlertDialog alert = mAlertBuilder.create();
            alert.setTitle(getString(R.string.no_lock_screen_error));
            alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.okay), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finishAffinity();
                }
            });
            alert.show();
            return true;
        }
        return false;
    }


    private String downloadProviderPackage(String filePath) {
        String url = Sigma.applicationProperties.getProperty("provider_package_url");
        if (url == null || url.isEmpty()) {
            url = Sigma.applicationProperties.getProperty("provider_package_url_hashcheck"); // check if we have package provider url that checks using hash code
            if (url != null && !url.isEmpty()) {
                // we have previous asset package hash code so append it to URL to allow service to  double check if download is needed
                try {
                    URL u = new URL(url);
                    String existingAssetsHash = Sigma.getProviderAssetsHash();
                    if (existingAssetsHash == null) {
                        existingAssetsHash = "0"; // create dummy hash for assets package that is part of APK (this should download any new existing package on server)
                    }

                    URL urlWithHash = new URL(u.getProtocol(), u.getHost(), u.getPort(), u.getFile() + "/" + existingAssetsHash, null);
                    url = urlWithHash.toString();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }
        String key = Sigma.applicationProperties.getProperty("provider_package_key");
        // download package only if host is accessible and with good connectivity
        if (url != null && !url.isEmpty()) {
            Log.d(TAG, "Downloading new provider package to: " + filePath);
            Log.d(TAG, "...from URL: " + url);
            String name = null;
            try {
                name = HTTPGETUtilities.downloadAndDecryptFile(this, url, filePath, key, null, null, Sigma.applicationTrustStore, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return name;
        }

        return null; // no download started
    }

    private void downloadProviderPackageAsync() {
        final String filePath;
        try {
            filePath = File.createTempFile("provider", ".zip", getCacheDir()).getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }


        (new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... arg0) {  // return cert Aliases
                return downloadProviderPackage(filePath);
            }

            @Override
            protected void onPostExecute(String filename) {
                if (filename != null) {
                    if (filename.endsWith("zip")) {
                        // Make filename the destination folder name and the name of the assets provider!
                        final String providerName = filename.substring(0, filename.length() - 4);
                        Log.d(TAG, "...for provider name:" + providerName);
                        try {
                            // check HASH of ZIP and unzip only if different
                            final String hash = Crypto.createSha1(new File(filePath));
                            if (Sigma.isSamePackageHash(hash)) {
                                Log.i(TAG, "Downloaded ZIP provider package is same as on device....skipping unzip.");
                            } else {
                                UnzipUtility.unzip(filePath, Sigma.getDirForPackage(providerName));
                                // switch assest and restart app after reconfiguring
                                final AlertDialog alert = mAlertBuilder.create();
                                alert.setTitle(getString(R.string.new_config_package_restart));
                                alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.okay), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        reStartActivity(providerName, hash);
                                    }
                                });
                                alert.setCancelable(false);
                                alert.show();
                                return;
                            }
                        } catch (Exception e) {
                            Log.wtf(TAG, "Cannot unzip or hash downloaded package: " + filePath + "  to: " + Sigma.getDirForPackage(providerName));
                        }
                    } else {
                        Log.e(TAG, "Package is not a ZIP file - ignoring it...");

                    }
                } else {
                    Log.w(TAG, "Failed to download from: " + Sigma.applicationProperties.getProperty("provider_package_url"));
                    //Toast.makeText(mContext, "Security update not downloaded.", Toast.LENGTH_LONG).show();
                }

                //if no new package has been downloaded do the authentication
                if (Sigma.authenticatedIdentity == null) {
                    mSigmaIdentity.selectIdentity(Sigma.applicationProperties.getProperty("cert_alias", "USER")); // now its safe to select identity ASAP
                } else {
                    userAuthenticated();
                }
            }

        }).execute();
    }


    private void downloadAPKAsync() {
        final String filePath;
        try {
            filePath = File.createTempFile("sigma", ".apk", getCacheDir()).getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        (new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... arg0) {  // return cert Aliases
                return downloadAPK(filePath);
            }

            @Override
            protected void onPostExecute(String filename) {
                if (filename != null) {
                    if (filename.endsWith("apk")) {
                        // reinstall app upon download
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        Uri uri = Uri.fromFile(new File(filePath));
                        intent.setDataAndType(uri, "application/vnd.android.package-archive");
                        finishAffinity();  // close app
                        startActivity(intent); //start install
                    }
                } else {
                    Log.w(TAG, "Failed to download app update from: " + Sigma.applicationProperties.getProperty("apk_package_url"));
                    //Toast.makeText(mContext, "Security update not downloaded.", Toast.LENGTH_LONG).show();
                }

            }

        }).execute();
    }


    private String downloadAPK(String filePath) {

        // add in current version to the URL
        try {
            String url = Sigma.applicationProperties.getProperty("apk_package_url");
            if (url != null && !url.isEmpty()) {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                URL urlWithVersion = new URL(url + pInfo.versionCode);
                url = urlWithVersion.toString();
                if (url != null && !url.isEmpty()) {
                    Log.i(TAG, "...from URL: " + url);
                    String name = null;
                    try {
                        Log.i(TAG, "Trying to download APK update from: " + url);
                        name = HTTPGETUtilities.downloadAndDecryptFile(this, url, filePath, null, null, null, Sigma.applicationTrustStore, null, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return name;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; // no download started
    }

    private void startMainActivity() {
        Intent intent = new Intent(mSavedIntent);
        intent.setClass(mContext, MainActivity.class);
        finish();
        startActivity(intent);
    }


    private void reStartActivity(String providerName, String hash) {
        // shut down service when restarting so cryptoservice reloads certs on connect
        if (mSigmaIdentity != null) {
            mSigmaIdentity.close();
        }


        // add to original intent new package details and restart app now
        if (providerName != null) {
            ((Sigma) getApplicationContext()).switchAssetProvider(providerName, hash);
        }
        finishAffinity();
        startActivity(mSavedIntent);
    }

    @Override
    public void userAuthenticated() {
        if (mSigmaIdentity == null) {
            Log.wtf(TAG, "This should not happen but if it does exit gracefully");
            Toast.makeText(mContext, "Error looking for identity! - please restart app...", Toast.LENGTH_LONG).show();
            // shut down service
            if (mSigmaIdentity != null) {
                mSigmaIdentity.close();
            }

            finishAffinity();
            return;
        }
        Sigma.authenticatedIdentity = mSigmaIdentity.getSelectedIdentity();
        if (Sigma.authenticatedIdentity == null) {
            Log.wtf(TAG, "Authenticated yet no identity????!!!!");
            mSigmaIdentity.selectIdentity(Sigma.applicationProperties.getProperty("cert_alias"));
        } else {
            // finally copy data from original intent and launch Main
            mHandler.sendEmptyMessageDelayed(1, 200);
        }

        // shutdown identity service now
        if (mSigmaIdentity != null) {
            mSigmaIdentity.close();
        }
    }

    @Override
    public void createProgressDialog(String msg) {
        // ....show waiting dialog
        mProgress = ProgressDialog.show(mContext, getString(R.string.wait), msg, true);
    }

    @Override
    public void dismissProgressDialog() {
        if (mProgress != null) {
            mProgress.dismiss();  // kill any waiting dialog
            mProgress = null;
        }
    }

    @Override
    public void cryptoInitialized() {
        // after crypto and trust and key store are initialized you can start downloading...
        downloadProviderPackageAsync();
        downloadAPKAsync();  // try downloading new APK as well
    }

//    @Override
//    public void onPause(){
//        super.onPause();
//        if (mSigmaIdentity != null) {
//            mSigmaIdentity.close();
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSigmaIdentity != null) {
            mSigmaIdentity.close();
        }
    }
}
