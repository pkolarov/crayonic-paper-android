package com.crayonic.sigma.scanner;

import android.os.Environment;

/**
 * Created by peter on 01/11/15.
 */
public class ScannerConstants {

    public final static String ACTION_SCAN_TO_PDF = "com.crayonic.sigma.scanner.SCAN_TO_PDF";

    public final static int PICKFILE_REQUEST_CODE = 1;
    public final static int START_CAMERA_REQUEST_CODE = 2;
    public final static String OPEN_INTENT_PREFERENCE = "selectContent";
    public final static String IMAGE_BASE_PATH_EXTRA = "ImageBasePath";
    public final static int OPEN_CAMERA = 4;
    public final static int OPEN_MEDIA = 5;
    public final static String SCANNED_RESULT = "scannedResult";
    public final static String IMAGE_PATH = Environment.getExternalStorageDirectory().getPath() + "/scanSample";

    public final static String SELECTED_BITMAP = "selectedBitmap";
    public final static String PAGES = "pages";
    public final static String COLORSPACE = "colorspace";
    public final static String FORMAT = "format";
    public static final String SCAN = "SCAN";
    public static final String UI_PRIMARY_LIGHT = "UI_PRIMARY_LIGHT";
    public static final String UI_PRIMARY_DARK = "UI_PRIMARY_DARK";
    public static final String UI_DIVIDER = "UI_DIVIDER";
    public static final String UI_PRIMARY_TEXT =  "UI_PRIMARY_TEXT";
    public static final String UI_SECONDARY_TEXT =  "UI_SECONDARY_TEXT";
    public static final String UI_PRIMARY = "UI_PRIMARY";
    public static final String UI_ACCENT = "UI_ACCENT";
    public static final String UI_FONT_PATH = "UI_FONT_PATH";
}