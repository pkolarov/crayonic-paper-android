package com.crayonic.sigma.identity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.crayonic.sigma.ProgressDialogListener;
import com.crayonic.sigma.R;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.MainActivity;
import com.crayonic.sigma.SplashScreenActivity;
import com.crayonic.sigma.crypto.ExternalSignatureProvider;
import com.crayonic.sigma.crypto.PKIUtils;
import com.crayonic.sigma.pdf.UserAuthenticatedListener;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.crayonic.sigma.services.CryptoServices;
import com.crayonic.sigma.services.HTTPServices;
import com.crayonic.sigma.web.WebIntent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Thread.sleep;

/**
 * Created by kolarov on 28.9.2015.
 */
public class InternalKeyStoreIdentity implements Identity {
    private static final String TAG = "KeyStoreIdentity";

    protected static final int CRYPTO_SERVICE_TIMEOUT = 9000;
    public static final String DEF_USER = "USER";
    protected ProgressDialogListener mProgressDL;


    protected Context mContext;

    protected AsyncTask<Void, Void, Void> mAsyncTask;
    protected AsyncTask<Void, Void, String> mAsyncTaskString;

    protected boolean waitingForRemoteService;


    protected String mCSRFilePath;
    protected boolean mVerificationRequested;

    protected String mRequestedAlias; // identity alias requested by constructor
    // identity is alias and the cert chain
    protected String mCertificateAlias; // selected identity certificate (also stored in Sigma app object)
    protected Certificate[] mCertificateChain; // loaded chain for the given alias


    //  private boolean mAlreadySelecting = false;
    public InternalKeyStoreIdentity() {
    }

    public InternalKeyStoreIdentity(Context c, String requestedAlias, boolean verificationRequested) {
        mContext = c;
        mRequestedAlias = requestedAlias;

        mVerificationRequested = verificationRequested; // true if the default cert needs to be verified and signed

        if (c instanceof ProgressDialogListener) {
            mProgressDL = (ProgressDialogListener) c;
            //  mProgressDL.createProgressDialog(mContext.getString(R.string.binding_service));
        } else {
            mProgressDL = null;
        }

        selectCryptoProvider();
    }


    protected void selectCryptoProvider() {
        Intent intent = new Intent(mContext, CryptoServices.class);
        // Start crypto service (only starts if not running)
        // first try internal keystore prefered crypto provider for getting/creating identity
        intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_PRIMARY, "com.crayonic.sigma.crypto.providers.CrayonicPenWithAndroidKeyStoreCryptoProvider");
        // fallback to external pen  provider if e.g. pen is not present
        intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_SECONDARY, "com.crayonic.sigma.crypto.providers.CrayonicPenP12KeyStoreCryptoProvider");
        // fallback to SW (BouncyCastle) key store
        intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_TERTIARY, "com.crayonic.sigma.crypto.providers.BCKeyStoreCryptoProvider");
        mContext.startService(intent); // start the Crypto services given preferred providers in order of importance
        // ...and bind to it
        doBindService(intent);
    }


    public void selectIdentity(final String certAliasRequested) {
        mProgressDL.createProgressDialog(mContext.getString(R.string.searching_identity) + "\n" + certAliasRequested);

        // setup callback for return from cert selection request dialog
//        final AlertDialogList.Callable callBack = new AlertDialogList.Callable() {
//            @Override
//            public void callback(String result, int position) {
//                // get our result processed here
//                if (result == null) {
//                    Toast.makeText(mContext, mContext.getString(R.string.no_identity_selected) + "\n" + certAliasRequested, Toast.LENGTH_LONG).show();
//                }
//                mCertAlias = result;
//                // certificate loaded correctly let them know... even when none selected
//                if (mContext instanceof UserAuthenticatedListener)
//                    ((UserAuthenticatedListener) mContext).userAuthenticated();
//            }
//        };


        mCertificateAlias = null; // reset certificate just in case

        mAsyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... arg0) {  // return cert Aliases
                Log.i(TAG, "Selecting  identity...");
                //....try to get certificate for requested alias
                Message msg = Message.obtain(null, CryptoServices.MSG_GET_CERT_CHAIN);
                msg.replyTo = mMessenger;
                Bundle b = new Bundle();
                b.putString(CryptoServices.CS_CERT_ALIAS, certAliasRequested);
                msg.setData(b);

                // stalls Async thread until result returns from service
                callRemoteServiceBlocking(msg, CRYPTO_SERVICE_TIMEOUT);
                return null;
            }

            @Override
            protected void onPostExecute(Void arg) {
                if (mProgressDL != null)
                    mProgressDL.dismissProgressDialog();

                if (mCertificateAlias != null && mContext instanceof UserAuthenticatedListener) {
                    // identity chain verification here...
                    if (!mVerificationRequested) {
                        Log.i(TAG, "Identity found ... verification not requested!");
                        // user is good to go...
                        if (mContext instanceof UserAuthenticatedListener)
                            ((UserAuthenticatedListener) mContext).userAuthenticated();

                        return;

                    } else {
                        // first verify certs then auth is complete...
                        Log.i(TAG, "Identity found ... verification requested!");
                        Set<X509Certificate> certs = new HashSet<>(mCertificateAlias.length());
                        X509Certificate cert = null;
                        for (int i = 0; i < mCertificateChain.length; i++) {
                            X509Certificate x509Certificate = (X509Certificate) mCertificateChain[i];
                            if (i == 0) {
                                cert = x509Certificate;  // get the cert...
                            } else {
                                certs.add(x509Certificate);  // ...and its trust chain
                            }
                        }
                        try {
                            //FIXME CertificateVerifier.verifyCertificate(cert, certs);
                            // user is good to go...
                            Log.i(TAG, "Identity found ... verified - OK!");
                            if (mContext instanceof UserAuthenticatedListener)
                                ((UserAuthenticatedListener) mContext).userAuthenticated();

                            return;
                        } catch (Exception e) {
                            Log.e(TAG, "Verification of certificate failed! Get a new cert");
                            Toast.makeText(mContext, "Your certificate is invalid or expired - please try to get a new one.", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                // no (verified) identities found in key store -- so get some personal data for a new one
                if (mProgressDL != null) {
                    mProgressDL.dismissProgressDialog();
                }
                Intent webIntent = new Intent(mContext, MainActivity.class);
                webIntent.setAction(Intent.ACTION_VIEW);
                webIntent.putExtras(((Activity) mContext).getIntent()); // copy original data too
                webIntent.putExtra(WebIntent.URL_DOWNLOAD, Sigma.getWWWPath() + Sigma.applicationProperties.getProperty("identity_page"));
                webIntent.setData(Uri.parse(Sigma.getWWWPath() + Sigma.applicationProperties.getProperty("identity_page")));
                webIntent.setFlags(webIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                //((MainActivity) mContext).startActivityForResult(webIntent, MainActivity.IDENTITY_DIALOG_CODE);
                mContext.startActivity(webIntent);
            }
        };
        mAsyncTask.execute();
    }

    public String getSelectedIdentity() {
        return mCertificateAlias;
    }

    public void close() {

        doUnbindService();

        Intent intent = new Intent(mContext, CryptoServices.class);
        mContext.stopService(intent); // and kill service
    }


    public void createIdentity(final String identityJSON, final String user, final String password) {

        if (CryptoServices.isRunning() && mProgressDL != null) {
            mProgressDL.createProgressDialog(mContext.getString(R.string.creating_identity));

            mAsyncTaskString = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... arg0) {
                    Log.i(TAG, "Creating identity...");
                    // in case we are not bound yet - wait a bit for binding to take place
//                    long shouldEndBy = System.currentTimeMillis() + 6000;  // binding timeout to service 100ms
//                    while (shouldEndBy > System.currentTimeMillis() && mServiceMsg == null) {
//                        try {
//                            sleep(5);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }

                    try {
                        JSONArray jsonarray = new JSONArray(identityJSON);
//                        if (jsonarray.length() == 0 || jsonarray.getJSONObject(0).optString("CN").length() == 0) {
//                            Log.e(TAG, "Not enough identity parameters to create identity (at least CN needs to exist)");
//                            return (String) null;
//                        }
                        // Get alias for the certificate and X500 principal e.g. "CN=Duke, OU=JavaSoft, O=Sun Microsystems, C=US"

                        final StringBuilder principal = new StringBuilder(200);
                        String CNValue = null;
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                            String name = jsonobject.keys().next();
                            if (name.equalsIgnoreCase("CN")) {
                                CNValue = jsonobject.getString(name);
                            } else {
                                String val = jsonobject.getString(name);
                                principal.append(name).append("=").append(val).append(",");
                            }
                        }
                        //principal.deleteCharAt(principal.lastIndexOf(","));
                        // append CN to the end or make it up if none exists
                        if (CNValue != null && CNValue.length() > 0) {
                            principal.append("CN").append("=").append(CNValue);
                        } else {
                            //use values of the first two keys to create CN (hopefully there are at least 2
                            String firstKey = jsonarray.getJSONObject(0).keys().next();
                            String secondKey = jsonarray.getJSONObject(1).keys().next();
                            String val1 = jsonarray.getJSONObject(0).getString(firstKey);
                            String val2 = jsonarray.getJSONObject(1).getString(secondKey);
                            CNValue = val1 + " " + val2;
                            principal.append("CN").append("=").append(CNValue);
                        }


                        // Prepare message to send
                        final Message msg = Message.obtain(null, CryptoServices.MSG_CREATE_CSR);
                        msg.replyTo = mMessenger;
                        Bundle b = new Bundle();
                        msg.setData(b);
                        b.putString(CryptoServices.CS_CERT_ALIAS, mRequestedAlias);
                        b.putString(CryptoServices.CS_CERT_PRINCIPAL, principal.toString());
                        Log.i(TAG, "Executed remote service and waiting for result:" + msg.toString());
                        callRemoteServiceBlocking(msg, CRYPTO_SERVICE_TIMEOUT);
                        return mRequestedAlias;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Bad JSON from HTMl form passed into createIdentity!");
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String alias) {
                    if (mProgressDL != null)
                        mProgressDL.dismissProgressDialog();

                    if (mCSRFilePath == null) {
                        Toast.makeText(mContext, mContext.getString(R.string.cannot_create_tmp_cert), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (mVerificationRequested) {
                        //send csr for signing if verification requested
                        approveIdentity(mCSRFilePath, alias, user, password);
                    } else {
                        Toast.makeText(mContext, mContext.getString(R.string.tmp_cert_created), Toast.LENGTH_LONG).show();
                        // if its created then select it
                        selectIdentity(mRequestedAlias);
                    }
                }
            };
            mAsyncTaskString.execute();
        }
    }

    @Override
    public void approveIdentity(String mCSRFilePath, String alias, String user, String password) {
        // try getting the CSR signed and return real certificate
        mProgressDL.createProgressDialog(mContext.getString(R.string.sending_identity_req));
        HTTPServices.startCSRUpload(mContext, Sigma.applicationProperties.getProperty("csr_upload_url"), mCSRFilePath, alias, user, password);
    }

    @Override
    public void storeIdentity(final String csrResponseJSON, final String alias) {
        // real cert should be in the JSON response so store accordingly

        mProgressDL.createProgressDialog(mContext.getString(R.string.storing_identity) + "\n" + alias);
        mCertificateAlias = alias; // make sure we remember the right alias for this cert

        mAsyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... arg0) {  // return cert Aliases
                Log.i(TAG, "Storing  identity...");
                //....try to get certificate for requested alias
                Message msg = Message.obtain(null, CryptoServices.MSG_WRITE_CERT);
                msg.replyTo = mMessenger;
                Bundle b = new Bundle();
                b.putString(CryptoServices.CS_CERT_ALIAS, alias);
                //convert JSON response to DER crt
                byte[] crt = null;
                try {
                    JSONObject jObj = new JSONObject(csrResponseJSON);
                    if (!jObj.has("csralias")) {
                        Log.e(TAG, "No 'csralias' name in JSON response to CSR request");
                        mCertificateAlias = null;
                        return null;
                    }
                    if (!jObj.has("certificate")) {
                        Log.e(TAG, "No 'certificate' name in JSON response to CSR request");
                        mCertificateAlias = null;
                        return null;
                    }

                    String certAlias = jObj.getString("csralias");
                    if (!certAlias.equalsIgnoreCase(alias)) {
                        Log.e(TAG, "No 'csralias' does not match alias in JSON response to CSR request");
                        mCertificateAlias = null;
                        return null;
                    }
                    String pemcert = jObj.getString("certificate"); //this should be the whole chain, but proxy can give only one cert
                    StringReader sr = new StringReader(pemcert);
                    Certificate[] chain = PKIUtils.loadCertificateChainFromPEM(sr);
                    crt = chain[0].getEncoded();
                    b.putByteArray(CryptoServices.CS_CERT, crt);
                    msg.setData(b);

                    // stalls Async thread until result returns from service
                    callRemoteServiceBlocking(msg, CRYPTO_SERVICE_TIMEOUT);
                    return null;
                }catch (Exception e){
                    mCertificateAlias = null;
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Void arg) {
                if (mProgressDL != null)
                    mProgressDL.dismissProgressDialog();

                if (mCertificateAlias != null && mContext instanceof UserAuthenticatedListener) {
                    // we must have certificate in key store now
                    Toast.makeText(mContext, mContext.getString(R.string.csr_success), Toast.LENGTH_LONG).show();

                    // restart with new keystore identity  -> shutdown service and restart app
                    close();
                    if (mContext instanceof Activity) {
                        Intent intent = new Intent(mContext, SplashScreenActivity.class);
                        ((Activity) mContext).finishAffinity();
                        mContext.startActivity(intent);
                        Sigma.authenticatedIdentity = null; // logout user
                    }

                } else {
                    // remove csr from internal key store
                    Toast.makeText(mContext, mContext.getString(R.string.cannot_store_identity), Toast.LENGTH_LONG).show();
                    removeIdentity(alias);
                }

            }
        };
        mAsyncTask.execute();
    }

    @Override
    public void removeIdentity(final String alias) {

        mProgressDL.createProgressDialog(mContext.getString(R.string.removing_identity) + "\n" + alias);
        mCertificateAlias = alias; // make sure we remember the  alias this operation is for

        mAsyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... arg0) {  // return cert Aliases
                Log.i(TAG, "Removing  identity...");
                //....try to get certificate for requested alias
                Message msg = Message.obtain(null, CryptoServices.MSG_REMOVE_CERT);
                msg.replyTo = mMessenger;
                Bundle b = new Bundle();
                b.putString(CryptoServices.CS_CERT_ALIAS, alias);
                msg.setData(b);

                // stalls Async thread until result returns from service
                callRemoteServiceBlocking(msg, CRYPTO_SERVICE_TIMEOUT);
                return null;
            }

            @Override
            protected void onPostExecute(Void arg) {
                if (mProgressDL != null)
                    mProgressDL.dismissProgressDialog();

                if (mCertificateAlias != null && mContext instanceof UserAuthenticatedListener) {
                    Toast.makeText(mContext, mContext.getString(R.string.cert_removed), Toast.LENGTH_LONG).show();
                } else {
                    // cannot remove cert
                    Toast.makeText(mContext, mContext.getString(R.string.cannot_remove_identity), Toast.LENGTH_LONG).show();
                    // try again
                    removeIdentity(alias);
                }

            }
        };
        mAsyncTask.execute();
    }

    protected void callRemoteServiceBlocking(Message msg, long timeout) {

        if (mServiceMsg == null) {
            Log.wtf(TAG, "Could not bind to service in time! Message not sent: " + msg.toString());
            return;
        }

        // send to remote  process and wait here for the result
        try {
            mServiceMsg.send(msg);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even do anything with it
            Log.e(TAG, "Crypto service has crashed on connection from " + TAG);
        }

        // lock up thread and wait for response or timeout to get result
        long shouldEndBy = System.currentTimeMillis() + timeout;
        waitingForRemoteService = true;
        while (shouldEndBy > System.currentTimeMillis() && waitingForRemoteService) {
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (waitingForRemoteService) {
            Log.wtf(TAG, "TIMEOUT: Crypto Service did not respond in time! Stop waiting....");
        }

    }

    //----------------------------SERVICE COMM CLIENT code----------------------------------------
    protected final Messenger mMessenger = new Messenger(new IncomingHandler(this));
    protected Messenger mServiceMsg = null;
    protected boolean mIsBound;

    static class IncomingHandler extends Handler {

        private final WeakReference<InternalKeyStoreIdentity> mService;

        IncomingHandler(InternalKeyStoreIdentity service) {
            mService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            InternalKeyStoreIdentity cps = mService.get();
            if (msg.getData() != null) {
                switch (msg.what) {
                    case CryptoServices.MSG_GET_CERT_CHAIN:
                        String error = msg.getData().getString(CryptoServices.CS_ERROR);
                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error during cert listing: " + error);
                            cps.mCertificateAlias = null;
                        } else {

                            try {
                                byte[] certs = msg.getData().getByteArray(CryptoServices.CS_RET_CERTS);
                                //deserialize
                                ByteArrayInputStream bis = new ByteArrayInputStream(certs);
                                ObjectInput in = null;
                                in = new ObjectInputStream(bis);
                                List<Certificate> certLits = (List<Certificate>) in.readObject();
                                bis.close();
                                cps.mCertificateChain = (Certificate[]) certLits.toArray(); // save chain
                                cps.mCertificateAlias = msg.getData().getString(CryptoServices.CS_CERT_ALIAS); //... and its alias
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        cps.waitingForRemoteService = false;
                        break;

                    case CryptoServices.MSG_CREATE_CSR:
                        error = msg.getData().getString(CryptoServices.CS_ERROR);
                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error during CSR creation: " + error);

                        } else {
                            // cps.mCertAlias = msg.getData().getString(CryptoServices.CS_CERT_ALIAS);
                            try {
                                cps.mCSRFilePath = File.createTempFile("sigma", ".csr", Sigma.getContext().getCacheDir()).getAbsolutePath();
                                Log.i(TAG, "Created CSR file: " + cps.mCSRFilePath);
                                byte[] csr = msg.getData().getByteArray(CryptoServices.CS_RET_CSR);
                                FileUtilsPlus.saveBinaryFile(cps.mCSRFilePath, csr);
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.wtf(TAG, "Cannot create CSR temp file!");
                            }
                        }
                        cps.waitingForRemoteService = false;
                        break;

                    case CryptoServices.MSG_WRITE_CERT:
                        error = msg.getData().getString(CryptoServices.CS_ERROR);
                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error during CSR writing: " + error);
                            // flag the failed request as a null alias
                            cps.mCertificateAlias = null;
                        }
                        cps.waitingForRemoteService = false;
                        break;

                    case CryptoServices.MSG_REMOVE_CERT:
                        error = msg.getData().getString(CryptoServices.CS_ERROR);
                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error during cert removal: " + error);
                            // flag the failed request as a null alias
                            cps.mCertificateAlias = null;
                        }
                        cps.waitingForRemoteService = false;
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        }
    }


    protected ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mServiceMsg = new Messenger(service);
            //           mProgressDL.dismissProgressDialog();
//            try {
//                Message msg = Message.obtain(null, CryptoServices.MSG_REGISTER_CLIENT);
//                msg.replyTo = mMessenger;
//                mServiceMsg.send(msg);
//            } catch (RemoteException e) {
//                // In this case the service has crashed before we could even do anything with it
//                Log.e(TAG, "Scan service has crashed on connection from " + TAG);
//            }
            Log.d(TAG, "Successfully connected to crypto service!");
//            if(mProgressDL!= null)
//                 mProgressDL.dismissProgressDialog();

            mIsBound = true;

            if (mContext instanceof CryptoBoundListener) {
                ((CryptoBoundListener) mContext).cryptoInitialized();
            }


        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            Log.d(TAG, "Successfully disconnected from crypto service!");
            mServiceMsg = null;
        }
    };


    protected void doBindService(Intent i) {
//        Intent intent = new Intent();
//        intent.setPackage("com.crayonic.sigma.pdf");
//        intent.setComponent(new ComponentName("com.crayonic.sigma.pdf", "com.crayonic.sigma.pdf.services.CryptoServices"));
        mContext.bindService(i, mConnection, Context.BIND_AUTO_CREATE);

    }

    protected void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
//            if (mServiceMsg != null) {
//                try {
//                    Message msg = Message.obtain(null, CryptoServices.MSG_UNREGISTER_CLIENT);
//                    msg.replyTo = mMessenger;
//                    mServiceMsg.send(msg);
//                } catch (RemoteException e) {
//                    // There is nothing special we need to do if the service has crashed.
//                }
//            }
            // Detach our existing connection.
            mContext.unbindService(mConnection);
            mIsBound = false;
        }
    }
}
