package com.crayonic.sigma.identity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.crayonic.sigma.ProgressDialogListener;
import com.crayonic.sigma.R;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.crayonic.sigma.services.CryptoServices;
import com.crayonic.sigma.services.HTTPServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by peter on 05/03/2017.
 */

public class PenIdentity extends InternalKeyStoreIdentity implements Identity {
    protected static final String TAG = "PenIdentity";

    public PenIdentity(Context c, String requestedAlias, boolean verificationRequested) {
        mContext = c;
        mRequestedAlias = requestedAlias;

        mVerificationRequested = verificationRequested; // true if the default cert needs to be verified and signed

        if (c instanceof ProgressDialogListener) {
            mProgressDL = (ProgressDialogListener) c;
            //  mProgressDL.createProgressDialog(mContext.getString(R.string.binding_service));
        } else {
            mProgressDL = null;
        }

        selectCryptoProvider();
    }

    @Override
    protected void selectCryptoProvider() {
        Intent intent = new Intent(mContext, CryptoServices.class);
        // Start crypto service (only starts if not running)
        // first try pen crypto provider for getting/creating identity
        intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_PRIMARY, "com.crayonic.sigma.crypto.providers.CrayonicPenP12KeyStoreCryptoProvider");
        // fallback to internal HW key store if e.g. pen is not present
        intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_SECONDARY, "com.crayonic.sigma.crypto.providers.null");
        // fallback to SW (BouncyCastle) key store
        intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_TERTIARY, "com.crayonic.sigma.crypto.providers.null");
        mContext.startService(intent); // start the Crypto services given preferred providers in order of importance
        // ...and bind to it
        doBindService(intent);
    }


    @Override
    public void createIdentity(final String identityJSON, final String user, final String password) {

        if (CryptoServices.isRunning() && mProgressDL != null) {
            mProgressDL.createProgressDialog(mContext.getString(R.string.creating_identity));

            mAsyncTaskString = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... arg0) {
                    Log.i(TAG, "Creating Pen identity...");

                    try {
                        JSONArray jsonarray = new JSONArray(identityJSON);
                        // Get alias for the certificate and X500 principal e.g. "CN=Duke, OU=JavaSoft, O=Sun Microsystems, C=US"

                        final StringBuilder principal = new StringBuilder(200);
                        String CNValue = null;
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                            String name = jsonobject.keys().next();
                            if (name.equalsIgnoreCase("CN")) {
                                CNValue = jsonobject.getString(name);
                            } else {
                                String val = jsonobject.getString(name);
                                principal.append(name).append("=").append(val).append(",");
                            }
                        }
                        //principal.deleteCharAt(principal.lastIndexOf(","));
                        // append CN to the end or make it up if none exists
                        if (CNValue != null && CNValue.length() > 0) {
                            principal.append("CN").append("=").append(CNValue);
                        } else {
                            //use values of the first two keys to create CN (hopefully there are at least 2
                            String firstKey = jsonarray.getJSONObject(0).keys().next();
                            String secondKey = jsonarray.getJSONObject(1).keys().next();
                            String val1 = jsonarray.getJSONObject(0).getString(firstKey);
                            String val2 = jsonarray.getJSONObject(1).getString(secondKey);
                            CNValue = val1 + " " + val2;
                            principal.append("CN").append("=").append(CNValue);
                        }

                        // Prepare message to send
                        final Message msg = Message.obtain(null, CryptoServices.MSG_CREATE_CSR);
                        msg.replyTo = mMessenger;
                        Bundle b = new Bundle();
                        msg.setData(b);
                        b.putString(CryptoServices.CS_CERT_ALIAS, mRequestedAlias);
                        b.putString(CryptoServices.CS_CERT_PRINCIPAL, principal.toString());
                        Log.i(TAG, "Executed remote service and waiting for result:" + msg.toString());
                        callRemoteServiceBlocking(msg, CRYPTO_SERVICE_TIMEOUT * 100); // pen takes long time to create KEY pair
                        return mRequestedAlias;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Bad JSON from HTMl form passed into createIdentity!");
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String alias) {
                    if (mProgressDL != null)
                        mProgressDL.dismissProgressDialog();

                    if (mCSRFilePath == null) {
                        Toast.makeText(mContext, mContext.getString(R.string.cannot_create_tmp_cert), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (mVerificationRequested) {
                        //send csr for signing if verification requested
                        mProgressDL.createProgressDialog(mContext.getString(R.string.sending_identity_req));
                        HTTPServices.startCSRUpload(mContext, Sigma.applicationProperties.getProperty("csr_upload_url"), mCSRFilePath, alias, user, password);
                    } else {
                        Toast.makeText(mContext, mContext.getString(R.string.tmp_cert_created), Toast.LENGTH_LONG).show();


                        //  sign CSR with device key pair to  create "self" signed cert and write it back to pen
                        //FIXME just write to pen CSR as cert
                        byte[] crtDER = new byte[0];
                        try {
                            crtDER = FileUtilsPlus.readBinaryFile(mCSRFilePath);
                            writeCerticate(crtDER);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            };
            mAsyncTaskString.execute();
        }
    }

    private void writeCerticate(final byte[] certDER) {
        if (CryptoServices.isRunning() && mProgressDL != null) {
            mProgressDL.createProgressDialog(mContext.getString(R.string.writing_cert));

            mAsyncTaskString = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... arg0) {
                    Log.i(TAG, "Writing certificate...");
                    Bundle b = new Bundle();
                    final Message msg = Message.obtain(null, CryptoServices.MSG_WRITE_CERT);
                    msg.setData(b);
                    b.putString(CryptoServices.CS_CERT_ALIAS, mRequestedAlias);
                    b.putByteArray(CryptoServices.CS_CERT, certDER);
                    Log.i(TAG, "Executed remote service and waiting for result:" + msg.toString());
                    callRemoteServiceBlocking(msg, CRYPTO_SERVICE_TIMEOUT * 10); // pen takes long time to create KEY pair
                    return mRequestedAlias;
                }

                @Override
                protected void onPostExecute(String alias) {
                    if (mProgressDL != null)
                        mProgressDL.dismissProgressDialog();

                    Log.i(TAG, "Written certificate to Pen for alias: " + alias);

                    // Try reading it back now and compare

                }
            };
            mAsyncTaskString.execute();
        }
    }


}
