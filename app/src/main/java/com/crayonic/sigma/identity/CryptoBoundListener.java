package com.crayonic.sigma.identity;

/**
 * Created by peter on 22/09/2016.
 */
public interface CryptoBoundListener {

    public void cryptoInitialized();
}
