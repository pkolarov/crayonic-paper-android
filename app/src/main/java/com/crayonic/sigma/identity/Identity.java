package com.crayonic.sigma.identity;

import android.content.Context;

/**
 * Created by peter on 04/11/15.
 */
public interface Identity {
    void selectIdentity(String alias);
    void createIdentity(String identityJSON, String user, String password);
    void approveIdentity( String mCSRFilePath, String alias, String user, String password);
    void storeIdentity(String csrResponseJSON, String alias);
    void removeIdentity(String alias);

    void close(); //shutdown identity service
    String getSelectedIdentity();
}

