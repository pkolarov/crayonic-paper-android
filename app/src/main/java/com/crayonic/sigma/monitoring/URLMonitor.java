package com.crayonic.sigma.monitoring;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by peter on 12/07/16.
 */
public class URLMonitor {


    public static boolean pingHost(String host, int port, String scheme, int timeout) {
        //return true;
        if(port < 0){
            if(scheme != null && scheme.equalsIgnoreCase("http"))
                port = 80;
            else if(scheme != null && scheme.equalsIgnoreCase("https"))
                port = 443;
            else
                return false;
        }

        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port), timeout);
            return true;
        } catch (IOException e) {
            return false; // Either timeout or unreachable or failed DNS lookup.
        }
    }
}
