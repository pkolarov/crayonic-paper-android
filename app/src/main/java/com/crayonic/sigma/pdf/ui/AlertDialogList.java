package com.crayonic.sigma.pdf.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.method.PasswordTransformationMethod;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;


import com.crayonic.sigma.R;

import java.util.List;

/**
 * Created by peter kolarov on 29.9.2015.
 * <p/>
 * This is helper class to display list selection alert dialog
 */
public class AlertDialogList {
    public interface Callable {
        void callback(String result, int position);
    }


    public static AlertDialog.Builder createAlertDialogList(final Context context, final List<String> theList,
                                                            final String title, final String selected, final Callable callback) {
        if (theList == null || theList.size() < 1) return null;

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setIcon(R.mipmap.ic_launcher);
        builderSingle.setTitle(title);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);

        for (String listItem : theList) {
            arrayAdapter.add(listItem);
        }


        builderSingle.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                callback.callback(null, -1);  // pressing back is same as cancel
            }
        });

        builderSingle.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callback.callback(null, -1);
            }
        });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String strName = arrayAdapter.getItem(which);
                        final int position = which;
                        AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                        builderInner.setMessage(strName);
                        builderInner.setTitle(selected);
                        builderInner.setPositiveButton(
                                context.getString(R.string.ok),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        callback.callback(strName, position);
                                    }
                                });
                        builderInner.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                dialog.dismiss();
                                callback.callback(null, -1);  // pressing back is same as cancel
                            }
                        });
                        dialog.dismiss();
                        builderInner.show();
                    }
                });
        builderSingle.show();
        return builderSingle;
    }


    public static void requestPassword(final Context c, final Callable callback) {
        final EditText input = new EditText(c);
        input.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
        input.setTransformationMethod(new PasswordTransformationMethod());

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(c);
        builderSingle.setIcon(R.mipmap.ic_launcher);
        builderSingle.setTitle(R.string.enter_password);
        builderSingle.setView(input);
        builderSingle.setNegativeButton(c.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callback.callback(null, -1);
            }
        });
        builderSingle.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                callback.callback(null, -1);  // pressing back is same as cancel
            }
        });
        builderSingle.setPositiveButton(c.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callback.callback(input.getText().toString(), -1);
            }
        });
        builderSingle.show();
    }
}
