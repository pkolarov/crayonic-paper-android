package com.crayonic.sigma.pdf;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;

import com.crayonic.sigma.pdf.curves.CardinalCurveWithCanvas;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by kolarov on 4.6.2015.
 */
public class DrawSignature {


    private static final float INK_THICKNESS = 6;// TODO: modify thickness according to pressure
    private static final int INK_COLOR = 0xFF0000FF; // blue ink


    public static Bitmap onBitmap(ArrayList<ArrayList<SignaturePoint>> signature,int width, int height, int offsetX, int offsetY, float scale ){
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(0);  // create transparent bitmap
        Canvas c = new Canvas(bitmap); // and setup its drawable

        Paint paint = new Paint();

        onCanvas(c, scale, false, offsetX, offsetY, paint, signature);

        return bitmap;
    }

    public static Bitmap onBitmapCurve(ArrayList<ArrayList<SignaturePoint>> signature,int width, int height, int offsetX, int offsetY, float scale ){
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(0);  // create transparent bitmap
        Canvas c = new Canvas(bitmap); // and setup its drawable

        CardinalCurveWithCanvas mCurve = new CardinalCurveWithCanvas(width, height);
        mCurve.setWidth(1, INK_THICKNESS);
        mCurve.getPaint().setAntiAlias(true);
        mCurve.getPaint().setColor(INK_COLOR);

        // find real signature boundaries in case the vectors went over the bounding box (prevents signature edge clipping)
     /*   for (ArrayList<SignaturePoint> signaturePoints : signature) {
            for (SignaturePoint signaturePoint : signaturePoints) {
                PointF p = signaturePoint.toPointF();
                float px = p.x + offsetX;
                float py = p.y + offsetY;
                if(px*scale  > width){
                    width = (int)(px + offsetX);
                }
                if((p.y + offsetY) > height){
                    height = (int)(py + offsetY);
                }
                if((p.x + offsetX) < 0){
                    width = (int)(width - (p.x + offsetX)*scale);
                    offsetX = offsetX + (int)((p.x + offsetX))*scale;
                }
                if((p.y + offsetY) < 0){
                    height = (int)(height - (p.y + offsetY));
                    offsetY = offsetY + (int)(p.y + offsetY);
                }
            }
        }
*/
        for (ArrayList<SignaturePoint> signaturePoints : signature) {
            for (SignaturePoint signaturePoint : signaturePoints) {
                PointF p = signaturePoint.toPointF();
                p.offset(offsetX, offsetY);
                mCurve.addPoint(p.x*scale,p.y*scale, signaturePoint.getPressure(), signaturePoint.getTime() );
            }
            mCurve.clearPoints();
        }

        mCurve.draw(c);
        return bitmap;
    }


    public static void onCanvasCurve(Canvas canvas,  Paint paint, ArrayList<ArrayList<SignaturePoint>> signature) {
        CardinalCurveWithCanvas mCurve = new CardinalCurveWithCanvas(canvas);
        mCurve.setWidth(1, INK_THICKNESS);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(INK_THICKNESS);
        paint.setColor(INK_COLOR);
        mCurve.setPaint(paint);

        for (ArrayList<SignaturePoint> signaturePoints : signature) {
            for (SignaturePoint signaturePoint : signaturePoints) {
                PointF p = signaturePoint.toPointF();
                PointF o = signaturePoint.getViewOffset();
                mCurve.addPoint(p.x + o.x,p.y + o.y, signaturePoint.getPressure(), signaturePoint.getTime() );
            }
            mCurve.clearPoints();
        }
    }

    public static void onCanvas(Canvas canvas, float scale,boolean applyViewOffset, int canvasXoffset, int canvasYoffset, Paint paint, ArrayList<ArrayList<SignaturePoint>> mSignature) {
        Path path = new Path();
        PointF p, o;

        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);

        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(INK_THICKNESS * scale);
        paint.setColor(INK_COLOR);

        Iterator<ArrayList<SignaturePoint>> it = mSignature.iterator();
        while (it.hasNext()) {
            ArrayList<SignaturePoint> arc = it.next();
            if (arc.size() >= 2) {
                Iterator<SignaturePoint> iit = arc.iterator();
                SignaturePoint point = iit.next();
                p = point.toPointF();
                o = point.getViewOffset();
                if(!applyViewOffset){
                    o = new PointF(0,0);  // dont apply view offset recorded in signature data e.g. when drawing on bitmap instead of view canvas
                }
                o.offset(canvasXoffset, canvasYoffset);  // always apply canvas offset

                float mX = (p.x + o.x) * scale;
                float mY = (p.y + o.y) * scale;
                path.moveTo(mX, mY);
                while (iit.hasNext()) {
                    p = iit.next().toPointF();
                    float x = (p.x + o.x) * scale;
                    float y = (p.y + o.y) * scale;
                    path.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                    mX = x;
                    mY = y;
                }
                path.lineTo(mX, mY);
            } else {
                SignaturePoint point = arc.get(0);
                p = point.toPointF();
                o = point.getViewOffset();
                if(!applyViewOffset){
                    o = new PointF(0,0);
                }
                o.offset(canvasXoffset, canvasYoffset);  // always apply canvas offset
                canvas.drawCircle((p.x+o.x) * scale, (p.y + o.y) * scale, INK_THICKNESS * scale / 2 , paint);// TODO: modify thickness according to pressure
            }
        }

        paint.setStyle(Paint.Style.STROKE);
        canvas.drawPath(path, paint);
    }


}
