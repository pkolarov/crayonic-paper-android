package com.crayonic.sigma.pdf;

import android.graphics.PointF;
import android.graphics.RectF;

;import com.crayonic.sigma.crypto.ExternalSignatureProvider;

import java.util.ArrayList;

public interface MuPDFView {
	public void setPage(int page, PointF size);
	public void setScale(float scale);
	public int getPage();
	public void blank(int page);
	public Hit passClickEvent(float x, float y);
	public LinkInfo hitLink(float x, float y);
	public void selectText(float x0, float y0, float x1, float y1);
	public void deselectText();
	public boolean copySelection();
	public boolean markupSelection(Annotation.Type type);
	public void deleteSelectedAnnotation();
	public void setSearchBoxes(RectF searchBoxes[]);
	public void setWidgetHighlights(ArrayList<AcroField> widgets);
	public void setLinkHighlighting(boolean f);
	public void deselectAnnotation();
	public void startDraw(float x, float y);
	public void continueDraw(float x, float y);
	public int startSignatureLine(float x, float y, float z, long t);
	public int continueSignatureLine(float x, float y, float z, long t);
	public void updateSignatureLine(float x, float y, float z, long t, int index);
	public ArrayList<SignaturePoint> getCurrentSignatureLine();
	public void cancelDraw();
	public boolean saveDraw();
//	public boolean clearSignature();
	public void cancelSignature();
	public void saveSignature(String certAlias, ExternalSignatureProvider mExternalSignatureProvider);
	public void setChangeReporter(Runnable reporter);
	public void update();
	public void updateHq(boolean update);
	public void removeHq();
	public void releaseResources();
	public void releaseBitmaps();
}
