package com.crayonic.sigma.pdf;

/**
 * Created by kolarov on 30.9.2015.
 *
 */
public interface UserAuthenticatedListener {

    void userAuthenticated();

}
