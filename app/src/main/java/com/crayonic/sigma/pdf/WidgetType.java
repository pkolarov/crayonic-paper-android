package com.crayonic.sigma.pdf;

public enum WidgetType {
	NONE,
	TEXT,
	LISTBOX,
	COMBOBOX,
	SIGNATURE
}
