package com.crayonic.sigma.pdf;

/**
 * Created by kolarov on 22.5.2015.
 */
enum ValidatorSourceType {

    CRL, OCSP, TRUSTED_LIST, SELF_SIGNED

}