package com.crayonic.sigma.pdf;

/**
 * Created by kolarov on 17.6.2015.
 */

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;

public class CachedFileProvider extends ContentProvider {
    private static final String TAG = "CachedFileProvider";
    public static final String AUTHORITY = "com.crayonic.sigma.pdf.provider";

    private String SHARED_FILE_FOLDER;

    private UriMatcher uriMatcher;

    @Override
    public boolean onCreate() {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "*", 1);
        SHARED_FILE_FOLDER =  getContext().getCacheDir().getAbsolutePath()   + File.separator;
        return true;
    }

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        Log.v(TAG, "Called with uri: '" + uri + "'." + uri.getLastPathSegment());
        switch (uriMatcher.match(uri)) {
            case 1:
                // The desired file name is specified by the last segment of the
                // path  E.g. 'content://com.crayonic....provider/my.pdf'
                // Take this and build the path to the file
                String fileLocation = SHARED_FILE_FOLDER  + uri.getLastPathSegment();
                File file = new File(fileLocation);
                if( file.exists() && file.length() > 0) {
                    ParcelFileDescriptor pfd = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
                    return pfd;
                }else{
                    Log.e(TAG, "Did not find file: " + file.getAbsolutePath() + " for the file sharing uri: " + uri.toString());
                    return  null;
                }
            default:
                Log.v(TAG, "Unsupported uri: '" + uri + "'.");
                throw new FileNotFoundException("Unsupported uri: "
                        + uri.toString());
        }
    }

    @Override
    public int update(Uri uri, ContentValues contentvalues, String s,
                      String[] as) {
        return 0;
    }

    @Override
    public int delete(Uri uri, String s, String[] as) {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentvalues) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            // If it returns 1 - then it matches the Uri defined in onCreate
            case 1:
                return "application/pdf"; // Use an appropriate mime type here
            default:
                return null;
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String s, String[] as1,
                        String s1) {
        switch (uriMatcher.match(uri)) {
            // If it returns 1 - then it matches the Uri defined in onCreate
            case 1:
                MatrixCursor cursor = null;
                File file = new File(SHARED_FILE_FOLDER + uri.getLastPathSegment());
                if (file.exists() && file.length() > 0 ) {
                    cursor = new MatrixCursor(new String[]{
                            OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE});
                    cursor.addRow(new Object[]{uri.getLastPathSegment(),
                            file.length()});
                    return cursor;
                }else{
                    Log.e(TAG, "Did not find file: " + file.getAbsolutePath() + " for the file sharing uri:" + uri.toString());
                    return  null;
                }


            default:
                return null;
        }
    }
}