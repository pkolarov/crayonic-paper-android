package com.crayonic.sigma.pdf.Signature;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.crayonic.sigma.R;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.crypto.CryptoProvider;
import com.crayonic.sigma.pdf.CertificateStatus;
import com.crayonic.sigma.pdf.CertificateValidity;
import com.crayonic.sigma.pdf.SigmaActivity;
import com.crayonic.sigma.pdf.SignatureField;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.CRLVerifier;
import com.itextpdf.text.pdf.security.CertificateInfo;
import com.itextpdf.text.pdf.security.CertificateVerification;
import com.itextpdf.text.pdf.security.CrlClient;
import com.itextpdf.text.pdf.security.LtvTimestamp;
import com.itextpdf.text.pdf.security.LtvVerification;
import com.itextpdf.text.pdf.security.OCSPVerifier;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.VerificationException;
import com.itextpdf.text.pdf.security.VerificationOK;

import org.spongycastle.cert.ocsp.BasicOCSPResp;
import org.spongycastle.tsp.TimeStampToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Security;
import java.security.cert.CRL;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by kolarov on 22.5.2015.
 */
public class VerifySignature {
    private static final String TAG = "VerifySignature";
    private static final String MANDATORY_SIGNATURES = "mandatorySignatures";





    /*
            This is called on the document open so check if there is a valid verification signature covering the whole document and we trust it (show warning to user if not)
            Also collect any verifications info on all signed signatures
     */
    public static boolean areDocumentSignaturesValid(final String path, final SigmaActivity activity) {
        Log.d(TAG, "Checking existing signatures for " + path);
        final String newFilePath;
        final File tmpf;
//        // first make a copy of file for parallel processing (original is used by the renderer)
//        try {
//            tmpf = File.createTempFile("tmp", "", activity.getCacheDir());
//            FileUtilsPlus.copy(new File(path), tmpf);
//            newFilePath = tmpf.getAbsolutePath();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.e(TAG, "Cannot create temp file for sig validator");
//            return false;
//        }

        newFilePath = path;

        final String signatureFieldName = Sigma.applicationProperties.getProperty("verification_signature_fieldname");
        List<String> mandatorySignatures = new ArrayList<>(0);
        // get blank Signature fields registered and validate signed signatures
        try {
            final PdfReader reader = new PdfReader(new FileInputStream(new File(newFilePath))); // ref file
            Map<String, String> info = reader.getInfo(); // get metadata
            if(info.containsKey(MANDATORY_SIGNATURES)){
                String commaDelimMandatorySignatures = info.get(MANDATORY_SIGNATURES);
                mandatorySignatures = Arrays.asList(commaDelimMandatorySignatures.split("\\s*,\\s*"));
            }

            // Get the fields from the reader (read-only!!!)
            final AcroFields form = reader.getAcroFields();

            ArrayList<String> blankSignatures = form.getBlankSignatureNames();
            ArrayList<String> signedSignatures = form.getSignatureNames();
            activity.mHasSignatureFields = blankSignatures.size() > 0;


            // record positions of all blank signatures
            for (String name : blankSignatures) {
                SignatureField sig = new SignatureField(name, form.getFieldPositions(name).get(0), null);
                if(mandatorySignatures.contains(name)){
                    sig.isRequired = true;
                }
                activity.mSignatures.add(sig);
            }

            //boolean lastValidSignatureFound = false;
            // ...adn record positions and validity of all signed signatures
            for (String name : signedSignatures) {
                //Check validity of the verifying  first signature (no others must be present)- if not valid show dialog
                // ... this can be the signature to protected integrity
                if (name.equalsIgnoreCase(signatureFieldName) && signedSignatures.size() == 1) {
                    if (!isVerifyingSignatureValid(form, name)) {
                        Log.e(TAG, "Signature in field " + name + " is not valid - exiting document!");
                        Resources res = activity.getResources();
                        final AlertDialog alert = activity.mAlertBuilder.create();
                        alert.setTitle(String.format(res.getString(R.string.cannot_open_document_Reason),
                                res.getString(R.string.reason_bad_signature)));
                        alert.setButton(AlertDialog.BUTTON_POSITIVE, activity.getString(R.string.gotit), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        alert.setButton(AlertDialog.BUTTON_NEGATIVE, activity.getString(R.string.exit), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                        alert.show();
                    }
                }
           /*     tmpname = name;

                // start to verify other signatures now... asynchronously
                new Thread(new Runnable() {
                    public void run() {
                        SignatureField signature = new SignatureField(tmpname, form.getFieldPositions(tmpname).get(0), form.verifySignature(tmpname));
                        activity.mSignatures.add(signature);
                        signature.hasIntegrity = !documentModifiedAfterSignature(signature.signature);
                        signature.hasIdentity = isSignatureCertInTrustStore(signature);
                        signature.hasTimeStamp = isSignatureTimestampValid(signature.signature);
                        signature.certStatus = checkCertificateTimeValidity(signature.signature);
                        signature.revocations = verifyOCSPSignature(signature.signature);
                        signature.allValidatorsFinished = true;

                    }
                }).start();
*/
                List<AcroFields.FieldPosition> field = form.getFieldPositions(name);
                AcroFields.FieldPosition pos = null;
                if(field != null && field.size() > 0){
                    pos = field.get(0); // get position on the doc if one is available
                }

                  SignatureField signature = new SignatureField(name, pos , form.verifySignature(name,"SC"));
                  activity.mSignatures.add(signature);


             new AsyncTask<SignatureField, Void, Void>() {
                    @Override
                    protected Void doInBackground(SignatureField... params) {
                        //FIXME should be more parallelized
                        params[0].hasIntegrity = !documentModifiedAfterSignature(params[0].signature);
                        params[0].hasIdentity = isSignatureCertInTrustStore(params[0]);
                        params[0].hasTimeStamp = isSignatureTimestampValid(params[0].signature);
                        params[0].certStatus = checkCertificateTimeValidity(params[0].signature);
                        params[0].revocations = verifyOCSPSignature(params[0].signature);  //this could take a along time to finish
                        params[0].allValidatorsFinished = true;
                        return null;
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,signature);
            }

            // close reader only when all background tasks are finished
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {


                    while(true) {
                        synchronized (this) {
                            //sanity checks
                            if(activity == null || activity.mSignatures == null || activity.mSignatures.size()<1 ){
                                if(reader != null){
                                    reader.close();
                                }
                                return null;
                            }
                            boolean done = true;
                            for (SignatureField sig : activity.mSignatures) {
                                if (!sig.allValidatorsFinished) {
                                    done = false; // some validator still checking - dont close reader
                                    break;
                                }
                            }
                            if(done) {
                                reader.close();
                                return null;
                            }
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


            //}
        } catch (Exception ex) {
            Log.e(TAG, "Cannot open PDF file for reading! " + ex);
            Resources res = activity.getResources();
            AlertDialog alert = activity.mAlertBuilder.create();
            alert.setTitle(String.format(res.getString(R.string.cannot_open_document_Reason), "Cannot check PDF signatures: " + ex.toString()));
            alert.setButton(AlertDialog.BUTTON_POSITIVE, activity.getString(R.string.dismiss), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    activity.finish();
                }
            });
            alert.show();
            return false;
        }
        return true;
    }


    // verify signature field 'name' against root certs in KeyStore
    public static boolean isVerifyingSignatureValid(AcroFields af, String name) {
        try {
            // check if signature covers everything first
            if (!af.signatureCoversWholeDocument(name)) {
                Log.e(TAG, "Signature: " + name + " does not cover whole document!");
                return false;
            }
            Log.d(TAG, "Verifying document revision: " + af.getRevision(name) + " of " + af.getTotalRevisions());
            PdfPKCS7 pk = af.verifySignature(name);
            Calendar cal = pk.getSignDate();
            Certificate[] pkc = pk.getSignCertificateChain();
            Log.d(TAG, "Subject: " + CertificateInfo.getSubjectFields(pk.getSigningCertificate()));
            if (!pk.verify()) {
                Log.e(TAG, "Doc revision modified after signature!");
                return false;
            }

            X509Certificate cert = (X509Certificate) pkc[0];
            if (cert.getIssuerDN().toString().equalsIgnoreCase(cert.getSubjectDN().toString())) {
                // skip further verification of self signed cert
                Log.w(TAG, "Signature field " + name + " is signed with self signed cert!");
                return false;
            }
//
//            if (Sigma.applicationTrustStore.getCertificate(CryptoProvider.TRUSTED_ROOT_CA_ALIAS) == null) {
//                Log.e(TAG, "No Root CA present in truststore!?  Cannot verify main signature.");
//                return false;
//            }
            // verify against root cert in trust(key)store
            List<VerificationException> errors = CertificateVerification.verifyCertificates(pkc, Sigma.applicationTrustStore
                    , null, cal); // null could be replaced by some CRL
            if (errors.size() == 0) {
                Log.d(TAG, "Certificates verified against the KeyStore");
                return true;
            } else {
                Log.e(TAG, "Certificate verification failed! Errors:" + errors);
                return false;
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception when verifying signature: " + name + " \n" + ex);
            return false;
        }
    }

    public static boolean documentModifiedAfterSignature(PdfPKCS7 pk) {
        try {
            if (!pk.verify()) {
                Log.e(TAG, "Doc revision modified after signature!");
                return true;
            } else {
                return false;
            }

        } catch (Exception ex) {
            Log.e(TAG, "Exception when verifying signature: "  + ex);
            return true;
        }
    }

    public static boolean isSignatureCertInTrustStore(SignatureField signatureField) {

        try {
            PdfPKCS7 pk = signatureField.signature;
            Calendar cal = pk.getSignDate();
            Certificate[] pkc = pk.getSignCertificateChain();
            if(pkc.length < 2){
                Log.d(TAG, "Self Signed signature is not trusted by default");
                signatureField.errors = new ArrayList<>(1);
                signatureField.errors.add(new VerificationException(pkc[0], "Self signed certificate is not trusted"));
                return false;
            }
            Log.d(TAG, "Subject: " + CertificateInfo.getSubjectFields(pk.getSigningCertificate()));
            // verify against root cert in trust(key)store
            List<VerificationException> errors = CertificateVerification.verifyCertificates(pkc, Sigma.applicationTrustStore, null, cal); // null could be replaced by some CRL
            signatureField.errors = errors;
            if (errors.size() == 0) {
                Log.d(TAG, "Certificates verified against the KeyStore");
                return true;
            } else {
                Log.e(TAG, "Certificate verification failed! Errors:" + errors);
                return false;
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception when verifying signature: " + ex);
            return false;
        }
    }

    public static CertificateStatus checkCertificateTimeValidity(PdfPKCS7 sig) {
        CertificateStatus certStat = new CertificateStatus();

        Certificate[] certs = sig.getSignCertificateChain();
        X509Certificate cert = (X509Certificate) certs[0];
        Date signDate = sig.getSignDate().getTime();
        certStat.setCertificate(cert);
        if (certs.length > 1)
            certStat.setIssuerCertificate((X509Certificate) certs[1]);


        StringBuilder out = new StringBuilder();
        String nl = String.format("%n");
        out.append("Issuer: " + cert.getIssuerDN()).append(nl);
        certStat.setValidationDate(new Date());
        certStat.setRevocationDate(cert.getNotAfter());

        out.append("Subject: " + cert.getSubjectDN()).append(nl);
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
        out.append("Valid from: " + date_format.format(cert.getNotBefore())).append(nl);
        out.append("Valid to: " + date_format.format(cert.getNotAfter())).append(nl);
        try {
            cert.checkValidity(signDate);
            out.append("The certificate was valid at the time of signing.").append(nl);
            certStat.setValidity(CertificateValidity.VALID);

        } catch (CertificateExpiredException e) {
            out.append("The certificate was expired at the time of signing.").append(nl);
            certStat.setValidity(CertificateValidity.EXPIRED);

        } catch (CertificateNotYetValidException e) {
            out.append("The certificate wasn't valid yet at the time of signing.").append(nl);
            certStat.setValidity(CertificateValidity.EXPIRED);
        }
        try {
            cert.checkValidity();
            out.append("The certificate is still valid.");
        } catch (CertificateExpiredException e) {
            out.append("The certificate has expired.");
        } catch (CertificateNotYetValidException e) {
            out.append("The certificate isn't valid yet.");
        }
        Log.i("VerifySignature", "" + out);
        return certStat;
    }


    private static boolean checkOCSPRevocation(PdfPKCS7 pkcs7, X509Certificate signCert, X509Certificate issuerCert, Date date)
            throws GeneralSecurityException, IOException {
        List<BasicOCSPResp> ocsps = new ArrayList<BasicOCSPResp>();
        if (pkcs7.getOcsp() != null)
            ocsps.add(pkcs7.getOcsp());
        OCSPVerifier ocspVerifier = new OCSPVerifier(null, ocsps);
        List<VerificationOK> verification =
                ocspVerifier.verify(signCert, issuerCert, date);
        if (verification.size() == 0) {
            List<X509CRL> crls = new ArrayList<X509CRL>();
            if (pkcs7.getCRLs() != null) {
                for (CRL crl : pkcs7.getCRLs())
                    crls.add((X509CRL) crl);
            }
            CRLVerifier crlVerifier = new CRLVerifier(null, crls);
            crlVerifier.setOnlineCheckingAllowed(false);
            verification.addAll(crlVerifier.verify(signCert, issuerCert, date));
        }
        if (verification.size() == 0) {
            Log.w(TAG, "The signing certificate couldn't be verified");
            return false;
        } else {
            for (VerificationOK v : verification)
                Log.d(TAG, "OCSP verif results: " + v);
        }
        return true;
    }

    // verify validity of one keystore certificate stored under name 'alias' via  OCSP if possible
    public static String verifyOCSPSignature(PdfPKCS7 sig) {
//        OcspClientBouncyCastle ocspClient = new OcspClientBouncyCastle();
//        OCSPCertificateVerifier ocspVerifier = new OCSPCertificateVerifier(ocspClient);
        Certificate[] certs = sig.getSignCertificateChain();
        X509Certificate signCert = (X509Certificate) certs[0];
        X509Certificate issuerCert = (certs.length > 1 ? (X509Certificate) certs[1] : null);
        try {
            if (checkOCSPRevocation(sig, signCert, issuerCert, sig.getSignDate().getTime()))
                return "Certificate was valid at the time of signing!";
        } catch (GeneralSecurityException e) {
            return "OCSP check - security error";
        } catch (IOException e) {
            return "Unable to run OCSP check - no conectivity to OCSP server";
        }
        return "The certificate could not be verified!";

//        for (int i = 0; i < chain.length; i++) {
//            X509Certificate cert = (X509Certificate) chain[i];
//            Log.d(TAG, String.format("[%s] %s", i, cert.getSubjectDN()));
//            // once we have OCSP setup then use the URL_DOWNLOAD to get OCSP response (checked below)
//            Log.d(TAG, CertificateUtil.getOCSPURL(cert));
//            if(CertificateUtil.getOCSPURL(cert) == null) {
//                Log.w(TAG, "Certificate" + cert.getSubjectDN() +  " does not carry OCSP URL_DOWNLOAD - OCSP revocation status check skipped!");
//                continue; // if cert has no OCSP URL_DOWNLOAD - just skip the OCSP check
//            }
//
//            //TODO: this could be optimized so only one call is made to getBasic... since it will return the whole chain anyway?
//            BasicOCSPResp ocspResp = ocspClient.getBasicOCSPResp(cert, rootCert, null);
//            // verify that OCSP response came from trusted OCSP server (OCSP server cert must be in key store
//            boolean ok = CertificateVerification.verifyOcspCertificates(ocspResp, ks, "SC");
//            if(!ok){
//                Log.e(TAG, "Cannot verify OCSP servers response signature!");
//                return true;
//            }
//            CertificateStatus cs = ocspVerifier.check(ocspResp, cert, rootCert, new Date()); // verify cert with rooCert in this response against todays Date
//            if(!cs.getValidity().equals(CertificateValidity.VALID)){
//                return true;
//            }
//       }
//        return false;
//    }

    }

    public static boolean isSignatureTimestampValid(PdfPKCS7 pkcs7) {
        if (pkcs7.getTimeStampDate() != null) {
//            System.out.println("TimeStamp: " +
//                    date_format.format(pkcs7.getTimeStampDate().getTime()));
            TimeStampToken ts = pkcs7.getTimeStampToken();
            //  System.out.println("TimeStamp service: " + ts.getTimeStampInfo().getTsa());
            try {
                if (pkcs7.verifyTimestampImprint()) {
                    return true;
                } else {
                    return false;
                }

            } catch (GeneralSecurityException e) {
                Log.i("VerifySignatureTS", "" + e);
                return false;
            }

        } else {
            return false;
        }


    }

/* __________Adding LTV to signed docs___________
    http://stackoverflow.com/questions/27892960/enabling-ltv-for-timestamp-signature-in-pdf-document
    This code identifies the most recently filled signature field of the PDF and checks whether it is a document time stamp or an usual signature.

If it is a document time stamp, the code adds validation information only for this document timestamp. Otherwise the code adds validation information for all signatures.

(The assumed work flow behind this is that the document is signed (for certification and/or approval) a number of times first, and then the document enters LTV cycles adding validation information and document time stamps but no usual signatures anymore. Your work flow may vary and, therefore, your program logic, too.)

Only after all this is done, a new document time stamp is added.

For this finally added time stamp no validation information are explicitly added to the PDF (if document time stamps from the same TSA have been applied in short succession, validation information included for a prior time stamp may be applicable). And this is why Adobe Reader/Acrobat usually does not consider this document time stamp LTV enabled.

If you need validation information for this final document time stamp, too, simply apply this method (the same as the method above, merely not adding a document time stamp) to the file with the document time stamp:

     */

    public void addLtv(String src, String dest, OcspClient ocsp, CrlClient crl, TSAClient tsa) throws IOException, DocumentException, GeneralSecurityException {

        PdfReader r = new PdfReader(src);
        FileOutputStream fos = new FileOutputStream(dest);
        PdfStamper stp = PdfStamper.createSignature(r, fos, '\0', null, true);
        LtvVerification v = stp.getLtvVerification();
        AcroFields fields = stp.getAcroFields();
        List<String> names = fields.getSignatureNames();
        String sigName = names.get(names.size() - 1);
        PdfPKCS7 pkcs7 = fields.verifySignature(sigName);
        if (pkcs7.isTsp()) {
            v.addVerification(sigName, ocsp, crl,
                    LtvVerification.CertificateOption.SIGNING_CERTIFICATE,
                    LtvVerification.Level.OCSP_CRL,
                    LtvVerification.CertificateInclusion.NO);
        } else {
            for (String name : names) {
                v.addVerification(name, ocsp, crl,
                        LtvVerification.CertificateOption.WHOLE_CHAIN,
                        LtvVerification.Level.OCSP_CRL,
                        LtvVerification.CertificateInclusion.NO);
            }
        }
        PdfSignatureAppearance sap = stp.getSignatureAppearance();
        LtvTimestamp.timestamp(sap, tsa, null);
    }
}
