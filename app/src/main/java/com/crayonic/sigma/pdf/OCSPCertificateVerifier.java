package com.crayonic.sigma.pdf;

import android.util.Log;

import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;


import org.spongycastle.asn1.oiw.OIWObjectIdentifiers;
import org.spongycastle.asn1.x509.AlgorithmIdentifier;
import org.spongycastle.cert.X509CertificateHolder;
import org.spongycastle.cert.ocsp.BasicOCSPResp;
import org.spongycastle.cert.ocsp.CertificateID;
import org.spongycastle.cert.ocsp.RevokedStatus;
import org.spongycastle.cert.ocsp.SingleResp;
import org.spongycastle.cert.ocsp.UnknownStatus;
import org.spongycastle.crypto.Digest;
import org.spongycastle.crypto.digests.SHA1Digest;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.operator.DigestCalculator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by kolarov on 22.5.2015.
 */



public class OCSPCertificateVerifier {

    private static final String TAG = OCSPCertificateVerifier.class.getName();

    private final OcspClientBouncyCastle ocspSource;


    public OCSPCertificateVerifier(OcspClientBouncyCastle ocspSource) {
        //Security.addProvider(new BouncyCastleProvider());
        this.ocspSource = ocspSource;
    }

    public CertificateStatus check(BasicOCSPResp ocspResp, X509Certificate childCertificate, X509Certificate certificate, Date validationDate) {

        CertificateStatus status = new CertificateStatus();
        status.setCertificate(childCertificate);
        status.setValidationDate(validationDate);
        status.setIssuerCertificate(certificate);

        if (ocspSource == null) {
            Log.d(TAG, "OCSPSource null");
            return null;
        }

        try {
//            BasicOCSPResp ocspResp = ocspSource.getBasicOCSPResp(childCertificate,  certificate, null);
//            if (null == ocspResp) {
//               // LOG.info("OCSP response not found");
//                return null;
//            }

          //SHA1DigestCalculator sha1dc = new
            X509CertificateHolder cerhold = new X509CertificateHolder(certificate.getTBSCertificate());
            CertificateID certificateId = new CertificateID( new SHA1DigestCalculator(), cerhold, childCertificate.getSerialNumber());
            // run through the whole ocsp response and look for the cert id we are checking
            SingleResp[] singleResps = ocspResp.getResponses();
            for (SingleResp singleResp : singleResps) {

                CertificateID responseCertificateId = singleResp.getCertID();

                if (false == certificateId.equals(responseCertificateId)) {
                    continue;
                }

                Date thisUpdate = singleResp.getThisUpdate();
                Log.d(TAG, "OCSP thisUpdate: " + thisUpdate);
                Log.d(TAG, "OCSP nextUpdate: " + singleResp.getNextUpdate());

                status.setStatusSourceType(ValidatorSourceType.OCSP);
                status.setStatusSource(ocspResp);
                status.setRevocationObjectIssuingTime(ocspResp.getProducedAt());

                if (null == singleResp.getCertStatus()) {
                    Log.d(TAG, "OCSP OK for: " + childCertificate.getSubjectX500Principal());
                    status.setValidity(CertificateValidity.VALID);
                } else {
                    Log.d(TAG, "OCSP certificate status: " + singleResp.getCertStatus().getClass().getName());
                    if (singleResp.getCertStatus() instanceof RevokedStatus) {
                        Log.d(TAG, "OCSP status revoked");
                        if (validationDate.before(((RevokedStatus) singleResp.getCertStatus()).getRevocationTime())) {
                            Log.d(TAG, "OCSP revocation time after the validation date, the certificate was valid at "
                                     + validationDate);
                            status.setValidity(CertificateValidity.VALID);
                        } else {
                            status.setRevocationDate(((RevokedStatus) singleResp.getCertStatus()).getRevocationTime());
                            status.setValidity(CertificateValidity.REVOKED);
                        }
                    } else if (singleResp.getCertStatus() instanceof UnknownStatus) {
                        Log.d(TAG, "OCSP status unknown");
                        status.setValidity(CertificateValidity.UNKNOWN);
                    }
                }

                return status;
            }

            Log.d(TAG, "no matching OCSP response entry");
            return null;
        } catch (IOException ex) {
            Log.e(TAG, "OCSP exception: " + ex.getMessage());
            return null;
        } catch (Exception ex) {
            Log.e(TAG, "OCSP exception: " + ex.getMessage());
            throw new RuntimeException(ex);
        }

    }


}