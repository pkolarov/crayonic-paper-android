package com.crayonic.sigma.pdf.Signature;

import android.util.Log;

import com.crayonic.sigma.crypto.ExternalSignatureProvider;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalSignatureContainer;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.TSAClient;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Calendar;



/**
 * Created by  kolarov on 09/11/15.
 */
public class SigmaExternalSignatureContainer implements ExternalSignatureContainer {

    private static final String TAG = "SigmaExternalSignatureC";
    private final OcspClient ocspClient;
    private final TSAClient tsaClient;

    private final ExternalSignatureProvider es;

    public SigmaExternalSignatureContainer(ExternalSignatureProvider es, OcspClient ocspClient, TSAClient tsaClient) {
        this.es = es;
        this.ocspClient = ocspClient;
        this.tsaClient = tsaClient;

    }

    @Override
    public byte[] sign(InputStream is) throws GeneralSecurityException {
        try {
            // this is our doc digester (not a signature digester)
            BouncyCastleDigest digest = new BouncyCastleDigest();
            // hash the document content based on prefered hash algorithm of external signature provider (should be same hash alg as PKCS7 hash of attribs)
            byte hash[] = DigestAlgorithms.digest(is, digest.getMessageDigest(es.getHashAlgorithm()));
            //get signature time
            Calendar cal = Calendar.getInstance();
            // create PKCS7 object with hash of the doc, timestamp and ocsp responses (if any)
            PdfPKCS7 sgn = new PdfPKCS7(null, es.getCertificateChain(), es.getHashAlgorithm(), null, digest, false);

            // try to get OCSP responses for singing certs
            Certificate[] chain = es.getCertificateChain();
            byte[] ocsp = null;
            if(chain.length >= 2 && ocspClient != null) {
                ocsp = ocspClient.getEncoded((X509Certificate)chain[0], (X509Certificate)chain[1], null);  // check cert at 0 with parent cert at [1] (use OCSP url from cert 0
            }

            byte[] sh = sgn.getAuthenticatedAttributeBytes(hash, ocsp, null, MakeSignature.CryptoStandard.CMS);
            // finally hash and sign the PKCS7 authenticated attributes
            byte[] extSignature = es.sign(sh);
            // set external signature OIDs
            sgn.setExternalDigest(extSignature, null, es.getEncryptionAlgorithm());
            // return DER encoded PKCS7 to be inserted into /Contents field of signature dictionary
            try {
                return sgn.getEncodedPKCS7(hash, tsaClient, ocsp, null, MakeSignature.CryptoStandard.CMS);
            }catch (Exception e){
                Log.w(TAG, "Trying signing without timestamp!");
                return sgn.getEncodedPKCS7(hash,  null, ocsp, null, MakeSignature.CryptoStandard.CMS);

            }
        }
        catch (Exception ioe) {
            throw new ExceptionConverter(ioe);
        }
    }

    @Override
    public void modifySigningDictionary(PdfDictionary pdfDictionary) { }
}
