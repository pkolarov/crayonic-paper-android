package com.crayonic.sigma.pdf;

import android.graphics.RectF;

import com.itextpdf.text.pdf.AcroFields;

/**
 * Created by kolarov on 26.6.2015.
 */
public abstract class AcroField {
    public String fieldName;            // acrofield name in PDF doc
    public AcroFields.FieldPosition fieldPosition;  // PDF doc position
    public RectF viewBox; // this is android view coordinates translated from fieldPosition
}
