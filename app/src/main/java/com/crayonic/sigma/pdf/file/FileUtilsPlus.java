package com.crayonic.sigma.pdf.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.crayonic.sigma.pdf.Utils;
import com.crayonic.sigma.tools.ByteStreams;
import com.crayonic.sigma.web.WebIntent;

public class FileUtilsPlus {

    private static final String TAG = "DocFileUtils";


    public static byte[] getBytesFromBinaryFileInAsset(Context ctx, String name) {

        AssetManager am = ctx.getAssets();
        InputStream inp;
        try {
            inp = am.open(name);
            return ByteStreams.toByteArray(inp);
        } catch (IOException e) {
            Log.e(TAG, "File read error:" + e);
        }
        return null;
    }


//    public static void copyAsset(AssetManager am, String filesDir, String name) throws IOException {
//
//        InputStream inp = am.open(name);
//        FileOutputStream outp = new FileOutputStream(filesDir + "/" + name);
//        byte[] buf = new byte[512];
//        int len;
//        while ((len = inp.read(buf)) > 0) {
//            outp.write(buf, 0, len);
//        }
//        outp.close();
//    }


    static public String getMimeType(Context ctx, Uri uri) {
        String mimeType = null;
        if(uri == null)
            return null;

        if (uri.getScheme() != null && uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = ctx.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }


    static public void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


    public static boolean copyAssetFolder(AssetManager assetManager, String fromAssetPath, String toPath) {
        try {
            String[] files = assetManager.list(fromAssetPath);
            new File(toPath).mkdirs();
            boolean res = true;
            for (String file : files)
                if (file.contains("."))
                    res &= copyAsset(assetManager, fromAssetPath + "/" + file, toPath + "/" + file);
                else
                    res &= copyAssetFolder(assetManager, fromAssetPath + "/" + file, toPath + "/" + file);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean copyAsset(AssetManager assetManager, String fromAssetPath, String toPath) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(fromAssetPath);
            new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public static String readRawTextFile(String path) {
        StringBuilder text = new StringBuilder();
        BufferedReader buffreader = null;
        try {
            InputStream inputStream = new FileInputStream(path);

            InputStreamReader inputreader = new InputStreamReader(inputStream);
            buffreader = new BufferedReader(inputreader);
            String line;

            while ((line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return "";
        } finally {
            if (buffreader != null)
                try {
                    buffreader.close();
                } catch (IOException e) {
                    Log.e(TAG, e.toString());
                }
        }
        return text.toString();
    }

    public byte[] readAsset(Context ctx, String name) throws IOException {
        AssetManager am = ctx.getAssets();
        InputStream inp = am.open(name);
        int n = inp.available();
        byte[] buf = new byte[n];
        int len = inp.read(buf);
        if (len != n) {
            throw new IOException("short read");
        }
        return buf;
    }

    public static void saveBinaryFile(String filePath, byte[] bytes) throws IOException {
        FileOutputStream fos = new FileOutputStream(filePath);
        fos.write(bytes);
        fos.close();
    }


    public static byte[] readBinaryFile(String filePath) throws IOException {

        InputStream inp = new FileInputStream(new File(filePath));
        int n = inp.available();
        byte[] buf = new byte[n];
        int len = inp.read(buf);
        if (len != n) {
            throw new IOException("short read");
        }
        return buf;
    }

    /**
     * return file name from path with timestamp flag
     *
     * @param path
     * @return
     */
    public static String getUniqueName(String path) {
        int i = path.lastIndexOf(".");
        return path.substring(path.lastIndexOf("/") + 1, i) + "_" + System.currentTimeMillis() + path.substring(i, path.length());
    }

    // Loads local file either from content provider, cache or file browser given input URI as string and output path for destination
    // returns original filename
    static public String loadLocalFile(Context c, Intent i, String outputPath) throws IOException {
        byte buffer[];
        InputStream is;
        String fname = null;
        Uri uri = i.getData();
        // first check local content scheme
        if (uri != null && outputPath != null) {
            Log.e(TAG, "URI to open is: " + uri);
            if (uri.toString().startsWith("content://")) {
                is = c.getContentResolver().openInputStream(uri);
                fname = Utils.getContentName(c.getContentResolver(), uri); // try to get original name
                int len = is.available();
                buffer = new byte[len];
                is.read(buffer, 0, len);
                is.close();
                // read from provider to buffer
                // if read to buffer - create temp file in local cache
                if (len > 0) {
                    FileOutputStream fos = new FileOutputStream(outputPath); // c.openFileOutput(outputPath, Context.MODE_PRIVATE);//
                    fos.write(buffer);
                    fos.close();
                    if (fname == null || fname.length() < 1)
                        fname = "toSign.pdf"; // content provider could not obtain filename
                } else {
                    return null;
                }
            } else {
                //input is a file name (from file browser or such) so make sure we work with copy (not original)
                String path = Uri.decode(uri.getEncodedPath());
                fname = uri.getLastPathSegment();
                copy(new File(path), new File(outputPath));
            }
        }
        // check if we have stored original filename to be used
        if (i.hasExtra(WebIntent.DOC_NAME)) {
            return i.getExtras().getString(WebIntent.DOC_NAME);
        } else {
            return fname;// return filename ready to open and saved in outputPath or Null if no file read
        }
    }


    public static void cleanTMPFileCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot clean up files in cache!" + e);
        }
    }

    // recursive dir/file deleter
    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                boolean success = deleteDir(new File(dir, child));
                if (!success) return false;
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }


    private static final Pattern DIR_SEPARATOR = Pattern.compile("/");

    /**
     * Raturns all available SD-Cards in the system (include emulated)
     * <p/>
     * Warning: Hack! Based on Android source code of version 4.3 (API 18)
     * Because there is no standart way to get it.
     * TODO: Test on future Android versions 4.4+
     *
     * @return paths to all available SD-Cards in the system (include emulated)
     */
    public static String[] getStorageDirectories() {
        // Final set of paths
        final Set<String> rv = new HashSet<String>();
        // Primary physical SD-CARD (not emulated)
        final String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
        // All Secondary SD-CARDs (all exclude primary) separated by ":"
        final String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
        // Primary emulated SD-CARD
        final String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
        if (TextUtils.isEmpty(rawEmulatedStorageTarget)) {
            // Device has physical external storage; use plain paths.
            if (TextUtils.isEmpty(rawExternalStorage)) {
                // EXTERNAL_STORAGE undefined; falling back to default.
                rv.add("/storage/sdcard0");
            } else {
                rv.add(rawExternalStorage);
            }
        } else {
            // Device has emulated storage; external storage paths should have
            // userId burned into them.
            final String rawUserId;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                rawUserId = "";
            } else {
                final String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                final String[] folders = DIR_SEPARATOR.split(path);
                final String lastFolder = folders[folders.length - 1];
                boolean isDigit = false;
                try {
                    Integer.valueOf(lastFolder);
                    isDigit = true;
                } catch (NumberFormatException ignored) {
                }
                rawUserId = isDigit ? lastFolder : "";
            }
            // /storage/emulated/0[1,2,...]
            if (TextUtils.isEmpty(rawUserId)) {
                rv.add(rawEmulatedStorageTarget);
            } else {
                rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
            }
        }
        // Add all secondary storages
        if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
            // All Secondary SD-CARDs splited into array
            final String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
            Collections.addAll(rv, rawSecondaryStorages);
        }
        return rv.toArray(new String[rv.size()]);
    }


    public static File findFolderOnExtStorage(String folder) {
        String storages[] = getStorageDirectories();
        for (String store : storages) {
            File storeF = new File(store, folder);
            if (storeF.exists() && storeF.isDirectory()) {
                return storeF;
            }
        }
        return null;
    }


    // fast file copy (for files less than 2GB)
    public static void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        copy(inStream, outStream);
    }

    public static void copy(FileInputStream inStream, FileOutputStream outStream) throws IOException {
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

}
