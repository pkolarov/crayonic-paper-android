package com.crayonic.sigma.pdf;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PointF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.crayonic.branding.Config;
import com.crayonic.sigma.MainActivity;
import com.crayonic.sigma.ProgressDialogListener;
import com.crayonic.sigma.R;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.crypto.ExternalSignatureProvider;
import com.crayonic.sigma.crypto.craytoken.BasicComm;
import com.crayonic.sigma.crypto.craytoken.LineResponseListener;
import com.crayonic.sigma.crypto.craytoken.ResponseListener;
import com.crayonic.sigma.databinding.ButtonsBinding;
import com.crayonic.sigma.identity.Identity;
import com.crayonic.sigma.identity.InternalKeyStoreIdentity;
import com.crayonic.sigma.pdf.Signature.VerifySignature;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.crayonic.sigma.pdf.ui.VerticalSeekBar;
import com.crayonic.sigma.web.WebIntent;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.Security;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;


class ThreadPerTaskExecutor implements Executor {
    public void execute(Runnable r) {
        new Thread(r).start();
    }
}

public class SigmaActivity extends Activity
        implements UserAuthenticatedListener, SignatureListener, ProgressDialogListener, LineResponseListener {
    private final static String TAG = "SigmaActivity";
    private static final String DIGEST_ALGORITHM = "SHA-256";
    private static final int SIGMA_REQUEST_WRITE_EXT_PERMISSIONS = 200;
    private static final int SIGMA_REQUEST_LOC_PERMISSIONS = 300;
    private static final  String BOX_POSITION_BOTTOM = "bottom";
    private static final  String BOX_POSITION_TOP = "top";
    private static final long PEN_RECONNECT_TIMEOUT = 1000;  // try reconnecting every 1 second when in signing dialog

    public List<SignatureField> mSignatures;//  document Signatures  (signed and blank) - global and public for the app
    public SignatureField mSigningSignatureField; // used by others to see the acrofield being signed

    private int mScreenWidth;
    private int mScreenHeight;
   // private Runnable mActionBtnRunnable;

    private boolean mDoubleBackToExitPressedOnce;
    private boolean mUnsignedRequiredSignature;
    private Identity mSigmaIdentity;


    static {
        Security.addProvider(new org.spongycastle.jce.provider.BouncyCastleProvider());
    }

    private ProgressDialog mProgress;
    private File mDocFile;  // keep it while we ask for permission

    //access to pen communication API
    public BasicComm mPenComm;
    private Config mBrandingConfig;


    @Override
    public void writeLine(final String line) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(mContext, line, Toast.LENGTH_LONG).show();
            }
        });

    }

//    Runnable mPenReconnector = new Runnable() {
//        @Override
//        public void run() {
//            if (mPenComm != null && !mPenComm.isConnected() && mDocView.getMode() == SigmaReaderView.Mode.Signing) {
//                mPenComm.findAndConnectPen();
//                mHandler.postDelayed(mPenReconnector, PEN_RECONNECT_TIMEOUT);
//            }
//
//        }
//    };



    /* The core rendering instance */
    enum TopBarMode {
        Main, Search, Annot, Delete, Accept, Exit, Sign
    }

    enum AcceptMode {Highlight, Underline, StrikeOut, Ink, CopyText, Signature}

    private MuPDFCore core;
    private SigmaReaderView mDocView;
    private View mButtonsView;
    private boolean mButtonsVisible;
    private TextView mFilenameView;
    private VerticalSeekBar mPageSlider;
    private int mPageSliderRes;
    private TextView mPageNumberView;
    private TextView mInfoView;
    // private ImageButton mSearchButton;
    private TextView mAnnotTypeText;
    // private ImageButton mAnnotButton;
    private ViewAnimator mTopBarSwitcher;
    private TopBarMode mTopBarMode = TopBarMode.Main;
    private AcceptMode mAcceptMode;
    private ImageButton mSearchBack;
    private ImageButton mSearchFwd;
    private EditText mSearchText;
    private SearchTask mSearchTask;
    public AlertDialog.Builder mAlertBuilder;
    private final Handler mHandler = new Handler();
    private boolean mAlertsActive = false;
    private AsyncTask<Void, Void, MuPDFAlert> mAlertTask;
    private AlertDialog mAlertDialog;
    private String mOriginalInputFileName; // since we copy input file to tmp folder we need to remember original inpu file if any
    public boolean mHasSignatureFields; // true if the doc contains signature acrofields e.g. dont display sign anywhere button
    // private FloatingActionButton mSignButton;
    private boolean mActionButtonVisible = false;
    private Context mContext;
    private ExternalSignatureProvider mExternalSignatureProvider;



    public void createAlertWaiter() {
        mAlertsActive = true;
        // All mupdf library calls are performed on asynchronous tasks to avoid stalling
        // the UI. Some calls can lead to javascript-invoked requests to display an
        // alert dialog and collect a reply from the user. The task has to be blocked
        // until the user's reply is received. This method creates an asynchronous task,
        // the purpose of which is to wait of these requests and produce the dialog
        // in response, while leaving the core blocked. When the dialog receives the
        // user's response, it is sent to the core via replyToAlert, unblocking it.
        // Another alert-waiting task is then created to pick up the next alert.
        if (mAlertTask != null) {
            mAlertTask.cancel(true);
            mAlertTask = null;
        }
        if (mAlertDialog != null) {
            mAlertDialog.cancel();
            mAlertDialog = null;
        }
        mAlertTask = new AsyncTask<Void, Void, MuPDFAlert>() {

            @Override
            protected MuPDFAlert doInBackground(Void... arg0) {
                if (!mAlertsActive)
                    return null;

                return core.waitForAlert();
            }

            @Override
            protected void onPostExecute(final MuPDFAlert result) {
                // core.waitForAlert may return null when shutting down
                if (result == null)
                    return;
                final MuPDFAlert.ButtonPressed pressed[] = new MuPDFAlert.ButtonPressed[3];
                for (int i = 0; i < 3; i++)
                    pressed[i] = MuPDFAlert.ButtonPressed.None;
                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mAlertDialog = null;
                        if (mAlertsActive) {
                            int index = 0;
                            switch (which) {
                                case AlertDialog.BUTTON1:
                                    index = 0;
                                    break;
                                case AlertDialog.BUTTON2:
                                    index = 1;
                                    break;
                                case AlertDialog.BUTTON3:
                                    index = 2;
                                    break;
                            }
                            result.buttonPressed = pressed[index];
                            // Send the user's response to the core, so that it can
                            // continue processing.
                            core.replyToAlert(result);
                            // Create another alert-waiter to pick up the next alert.
                            createAlertWaiter();
                        }
                    }
                };
                mAlertDialog = mAlertBuilder.create();
                mAlertDialog.setTitle(result.title);
                mAlertDialog.setMessage(result.message);

                switch (result.iconType) {
                    case Error:
                        break;
                    case Warning:
                        break;
                    case Question:
                        break;
                    case Status:
                        break;
                }
                switch (result.buttonGroupType) {
                    case OkCancel:
                        mAlertDialog.setButton(AlertDialog.BUTTON2, getString(R.string.cancel), listener);
                        pressed[1] = MuPDFAlert.ButtonPressed.Cancel;
                    case Ok:
                        mAlertDialog.setButton(AlertDialog.BUTTON1, getString(R.string.okay), listener);
                        pressed[0] = MuPDFAlert.ButtonPressed.Ok;
                        break;
                    case YesNoCancel:
                        mAlertDialog.setButton(AlertDialog.BUTTON3, getString(R.string.cancel), listener);
                        pressed[2] = MuPDFAlert.ButtonPressed.Cancel;
                    case YesNo:
                        mAlertDialog.setButton(AlertDialog.BUTTON1, getString(R.string.yes), listener);
                        pressed[0] = MuPDFAlert.ButtonPressed.Yes;
                        mAlertDialog.setButton(AlertDialog.BUTTON2, getString(R.string.no), listener);
                        pressed[1] = MuPDFAlert.ButtonPressed.No;
                        break;
                }
                mAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        mAlertDialog = null;
                        if (mAlertsActive) {
                            result.buttonPressed = MuPDFAlert.ButtonPressed.None;
                            core.replyToAlert(result);
                            createAlertWaiter();
                        }
                    }
                });

                mAlertDialog.show();
            }
        };

        mAlertTask.executeOnExecutor(new ThreadPerTaskExecutor());
    }

    public void destroyAlertWaiter() {
        mAlertsActive = false;
        if (mAlertDialog != null) {
            mAlertDialog.cancel();
            mAlertDialog = null;
        }
        if (mAlertTask != null) {
            mAlertTask.cancel(true);
            mAlertTask = null;
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // do full screen here
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mContext = this;
        mDoubleBackToExitPressedOnce = false;

        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(dm);
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;
        mAlertBuilder = new AlertDialog.Builder(this);

        // Bind to identity creation/selection service if no identity selected yet
        if (Sigma.authenticatedIdentity == null) {
          //  LocalBroadcastManager.getInstance(this).registerReceiver(mUploadCSRReceiver, new IntentFilter(HTTPServices.ACTION_UPLOAD_CSR_RESULT));

            // initialize/select identity (also inits crypto services and trust store) if not yet selected
            mSigmaIdentity = new InternalKeyStoreIdentity(this, Sigma.applicationProperties.getProperty("cert_alias", InternalKeyStoreIdentity.DEF_USER),
                    Sigma.applicationProperties.getProperty("sign_csr","false").equalsIgnoreCase("true"));

            // check if already coming back from creating identity UI or we need to create one
            if (getIntent().getAction().equals(Sigma.CREATE_IDENTITY_ACTION) && getIntent().hasExtra(WebIntent.IDENTITY)) {
                String usernameBasicAuth = getIntent().getExtras().getString(WebIntent.USERNAME);
                String pwdBasicAuth = getIntent().getExtras().getString(WebIntent.PWD);
                mSigmaIdentity.createIdentity(getIntent().getExtras().getString(WebIntent.IDENTITY), usernameBasicAuth, pwdBasicAuth);
            } else {
                Log.d(TAG, "Waiting for identity service?");
            }

        }
    }

    private View inflateBrandableButtonsView() {
        // Brand UI using properties


        String fontPath = Sigma.applicationProperties.getProperty("mdp_primary_font");
        if (fontPath != null && fontPath.length() > 0) {
            fontPath = Sigma.getFontsPath() + fontPath;
        } else {
            fontPath = null;
        }
        mBrandingConfig = new Config(Sigma.mdpPrimary, Sigma.mdpPrimaryDark, Sigma.mdpPrimaryLight, Sigma.mdpPrimaryText,
                Sigma.mdpSecondaryText, Sigma.mdpAccent, Sigma.mdpIcons, Sigma.mdpDivider, fontPath);

        ButtonsBinding binding = ButtonsBinding.inflate(getLayoutInflater());
        binding.setBrandingConfig(mBrandingConfig);
        return binding.getRoot();

    }

    @Override
    public void userAuthenticated() {
        Sigma.authenticatedIdentity = mSigmaIdentity.getSelectedIdentity();
        if (Sigma.authenticatedIdentity == null) {
            // reselect or recreate if none around
            mSigmaIdentity.selectIdentity(Sigma.applicationProperties.getProperty("cert_alias"));
        }
    }

    @Override
    protected void onStart() {


        if (core != null) {
            core.startAlerts();
            createAlertWaiter();
        }
        // Display doc ASAP...
        if (core == null && !isFinishing()) {
            File readyToOpen = openURIfromIntent(); // make local or remote file available in cache
            if (readyToOpen != null) {
                // there is some local file to open
                try {
                    openDocument(readyToOpen.getAbsolutePath(), 0);
                } catch (Exception e) {
                    Log.e(TAG, "Cannot open doc: " + readyToOpen.getAbsolutePath() + " \n" + e);
                    Toast.makeText(this, getString(R.string.cannot_open_document) + readyToOpen.getName(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }

        // Get signing identity if any...
        //  mSigningCertificateAlias = getIntent().hasExtra(MainActivity.CERT_ALIAS) ? getIntent().getExtras().getString(MainActivity.CERT_ALIAS) : null;

        //... check for alternative signing identity passed in as a principal string
        Intent i = getIntent();
        String principal = null;
        ArrayList<String> principals = new ArrayList<>(2);
        if (i.hasExtra(WebIntent.SIGNATORY_JSON)) {
            String json = i.getExtras().getString(WebIntent.SIGNATORY_JSON);
            try {
                JsonElement jelement = json != null ? new JsonParser().parse(json) : null;

                JsonArray jsonArray = jelement != null ? jelement.getAsJsonArray() : null; // gets root json aray
                for (JsonElement principalElement : jsonArray) {
                    JsonObject jobject = principalElement != null ? principalElement.getAsJsonObject() : null; // gets root json object
                    JsonPrimitive jprincipal = jobject != null ? jobject.getAsJsonPrimitive("principal") : null;
                    principal = jprincipal != null ? jprincipal.getAsString() : null;
                    principals.add(principal);
                }
            } catch (Exception e) {
                Log.e(TAG, "Bad SIGNATORY_JSON string passed in the intent! " + json);
            }
        }
        // ...and bind with it to signing service
        try {
            mExternalSignatureProvider = new ExternalSignatureProvider(mContext, DIGEST_ALGORITHM, Sigma.authenticatedIdentity, principals);
        } catch (Exception e) {
            Log.wtf(TAG, "Cannot create external signature provider: " + e.getStackTrace());
        }
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ContextCompat.checkSelfPermission(SigmaActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SigmaActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    SIGMA_REQUEST_LOC_PERMISSIONS);
        }
    }

    // returns cached file to open for signing or null if nothing can be opened
    // sets global original filename for later reference
    private File openURIfromIntent() {
        Intent intent = getIntent();


        File tmpFile = null;
        // try loading local  PDF file
        try {
            if (intent.getData() != null) {
                tmpFile = File.createTempFile("sigma", ".pdf", getCacheDir()); // create new tmp file path for loading
                // first try loading file from local content provider or such
                mOriginalInputFileName = FileUtilsPlus.loadLocalFile(mContext, intent, tmpFile.getAbsolutePath());
                if (mOriginalInputFileName == null) {
                    throw new Exception("Giving up on loading:" + intent.getData());
                }
            }
        } catch (Exception e) {

            Log.e(TAG, e.toString());
            Resources res = getResources();
            AlertDialog alert = mAlertBuilder.create();
            setTitle(String.format(res.getString(R.string.cannot_open_document_Reason), "File not found!"));
            alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dismiss),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            alert.show();
            return null;
        }

        // if nothing opens finally try opening the unit test -- TODO: remove for production
        // Note: unit test dont use tmp files - all stuff happens to original for easier debuging
        boolean isDebuggable = (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
        if (mOriginalInputFileName == null && isDebuggable) {
            File docPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File testPath = new File(docPath + "/Robotium/unitTestPDF.pdf");
            try {
                tmpFile = File.createTempFile("sigma", ".pdf", getCacheDir());
                FileUtilsPlus.copy(testPath, tmpFile);
                mOriginalInputFileName = testPath.getName();
            } catch (IOException e) {
                //e.printStackTrace();
                return null;
            }
        }

        return tmpFile;  // returns file to open
    }


    private void requestPassword() {
        final EditText input = new EditText(this);
        input.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
        input.setTransformationMethod(new PasswordTransformationMethod());

        AlertDialog alert = mAlertBuilder.create();
        alert.setTitle(R.string.enter_password);
        alert.setView(input);
        alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.okay),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (core.authenticatePassword(input.getText().toString())) {
                            VerifySignature.areDocumentSignaturesValid(core.mLastOpenFilePath, (SigmaActivity) mContext);
                            createUI();
                        } else {
                            requestPassword();
                        }
                    }
                });
        alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alert.show();
    }


    private void createUI() {
        if (core == null) {
            // if no doc is loaded then no UI
            return;
        }

        // Now create the UI.
        // First create the document view
        mDocView = new SigmaReaderView(this) {
            @Override
            protected void onMoveToChild(int i) {
                if (core == null)
                    return;

                mTopBarMode = TopBarMode.Main;
                mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
                showButtons();

                mPageNumberView.setText(String.format("%d / %d", i + 1,
                        core.countPages()));
                mPageSlider.setMax((core.countPages() - 1) * mPageSliderRes);
                mPageSlider.setProgress(i * mPageSliderRes);
                super.onMoveToChild(i);
            }

            @Override
            protected void onTapMainDocArea() {
                // toggle  zoom/preview
                if (mDocView.getZoom() != mDocView.getScaleToFitWidth(mScreenWidth)) {
                    mDocView.zoomTo = false;
                    mDocView.zoomTo(mScreenWidth / 2, mScreenHeight / 2, mDocView.getScaleToFitWidth(mScreenWidth), mScreenWidth, mScreenHeight);// mDocView.getZoom()
                    hideButtons();
                } else {
                    mDocView.zoomTo = false;
                    mDocView.zoomTo(mScreenWidth / 2, mScreenHeight / 2, 1, mScreenWidth, mScreenHeight);
                    mTopBarMode = TopBarMode.Main;
                    mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
                    showButtons(); // only show buttons on preview

                }

            }

            @Override
            protected void onDocMotion() {
                if (mDocView.getZoom() != 1.0f) {
                  //  hideButtons(); // hide buttons when scrolling in zoomed in mode only

                }
//                if (mActionBtnRunnable != null) {
//                    mHandler.removeCallbacks(mActionBtnRunnable); //remove wait task for action button
//                    mActionBtnRunnable = null;
//                }
            }

//            @Override
//            protected void onScaleChild(View v, Float scale) {
//                super.onScaleChild(v, scale);
//                if (mDocView.getZoom() != 1.0f && mDocView.getMode() != Mode.Signing) {
//                    showActionButton();
//                }
//
//            }


            @Override
            protected void onSettle(View v) {
                super.onSettle(v);

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mDocView.getZoom() != 1.0f && mDocView.getMode() != Mode.Signing)
                            showActionButton();
                    }
                }, 900);
            }

            @Override
            protected void onHit(Hit item) {
                switch (mTopBarMode) {
                    case Annot:
                        if (item == Hit.Annotation) {
                            showButtons();
                            mTopBarMode = TopBarMode.Delete;
                            mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
                        }
                        break;
                    case Delete:
                        mTopBarMode = TopBarMode.Annot;
                        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
                        // fall through
                    default:
                        // Not in annotation editing mode, but the pageview will
                        // still select and highlight hit annotations, so
                        // deselect just in case.
                        MuPDFView pageView = (MuPDFView) mDocView.getDisplayedView();
                        if (pageView != null)
                            pageView.deselectAnnotation();
                        break;
                }
            }
        };
        mDocView.setAdapter(new MuPDFPageAdapter(this, core));

        mSearchTask = new SearchTask(this, core) {
            @Override
            protected void onTextFound(SearchTaskResult result) {
                SearchTaskResult.set(result);
                // Ask the ReaderView to move to the resulting page
                mDocView.setDisplayedViewIndex(result.pageNumber);
                // Make the ReaderView act on the change to SearchTaskResult
                // via overridden onChildSetup method.
                mDocView.resetupChildren();
            }
        };

        // Make the buttons overlay, and store all its
        // controls in variables
        makeButtonsView();

        // Set up the page slider
        int smax = Math.max(core.countPages() - 1, 1);
        mPageSliderRes = ((10 + smax - 1) / smax) * 2;

        // Set the file-name text
        mFilenameView.setText(mOriginalInputFileName);

        // Activate the seekbar
        mPageSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
                mDocView.setDisplayedViewIndex((seekBar.getProgress() + mPageSliderRes / 2) / mPageSliderRes);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                updatePageNumView((progress + mPageSliderRes / 2) / mPageSliderRes);
            }
        });


        // Search invoking buttons are disabled while there is no text specified
        mSearchBack.setEnabled(false);
        mSearchFwd.setEnabled(false);
        mSearchBack.setColorFilter(Color.argb(255, 128, 128, 128));
        mSearchFwd.setColorFilter(Color.argb(255, 128, 128, 128));

        // React to interaction with the text widget
        mSearchText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                boolean haveText = s.toString().length() > 0;
                setButtonEnabled(mSearchBack, haveText);
                setButtonEnabled(mSearchFwd, haveText);

                // Remove any previous search results
                if (SearchTaskResult.get() != null && !mSearchText.getText().toString().equals(SearchTaskResult.get().txt)) {
                    SearchTaskResult.set(null);
                    mDocView.resetupChildren();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        //React to Done button on keyboard
        mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                    search(1);
                return false;
            }
        });

        mSearchText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER)
                    search(1);
                return false;
            }
        });

        // Activate search invoking buttons
        mSearchBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                search(-1);
            }
        });
        mSearchFwd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                search(1);
            }
        });

        // Reenstate last state if it was recorded
        //SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        mDocView.setDisplayedViewIndex(0);//prefs.getInt("page"+mLastOpenFilePath, 0));  --- PK: Always start from beginning

        mUnsignedRequiredSignature = false; //assume no required signatures
        if (mSignatures != null) {
            for (SignatureField mSignature : mSignatures) {
                if (mSignature.signature == null) {
                    //only add unsigned signatures for highlighting
                    mDocView.addWidget(mSignature);
                    if (mSignature.isRequired) {
                        mUnsignedRequiredSignature = true;
                    }

                }
            }

        }

        mButtonsVisible = false;
        showButtons();
        // Stick the document view and the buttons overlay into a parent view
        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(mDocView);
        layout.addView(mButtonsView);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(layout);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSearchTask != null)
            mSearchTask.stop();
    }

    @Override
    public void onDestroy() {
        closeDocument();

       // LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mUploadCSRReceiver);
        // shut down identity service
        if (mSigmaIdentity != null) {
            mSigmaIdentity.close();
            mSigmaIdentity = null;
        }

        super.onDestroy();
    }

    public void closeDocument() {
        if (mDocView != null) {
            mDocView.applyToChildren(new ReaderView.ViewMapper() {
                void applyToView(View view) {
                    ((MuPDFView) view).releaseBitmaps();
                }
            });
        }
        if (core != null)
            core.onDestroy();
        if (mAlertTask != null) {
            mAlertTask.cancel(true);
            mAlertTask = null;
        }
        core = null;
        mSignatures = null;

    }

    public void openDocument(String path, int pageNum) throws Exception {
        core = new MuPDFCore(mContext, path);

        if (core.needsPassword()) {
            requestPassword();
            return;
        }
        if (core.countPages() == 0) {
            core = null;
            throw new Exception("File has no pages!");
        }

        mSignatures = new ArrayList<>(10);  // initialize for now as no signatures (will be filled asynchronously with signature validator below)
        mHasSignatureFields = false;

        VerifySignature.areDocumentSignaturesValid(core.mLastOpenFilePath, this);
        createUI();
        mDocView.setDisplayedViewIndex(pageNum);
    }


    private void setButtonEnabled(ImageButton button, boolean enabled) {
        button.setEnabled(enabled);
        button.setColorFilter(enabled ? Color.argb(255, 255, 255, 255) : Color.argb(255, 128, 128, 128));
    }


    private void showButtons() {
        if (core == null)
            return;
        if (!mButtonsVisible) {
            mButtonsVisible = true;
            // Update page number text and slider
            int index = mDocView.getDisplayedViewIndex();
            updatePageNumView(index);
            mPageSlider.setMax((core.countPages() - 1) * mPageSliderRes);
            mPageSlider.setProgress(index * mPageSliderRes);
            if (mTopBarMode == TopBarMode.Search) {
                mSearchText.requestFocus();
                showKeyboard();
            }

            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            //TranslateAnimation(0, 0, -mTopBarSwitcher.getHeight(), 0);
            anim.setDuration(400);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    mTopBarSwitcher.setVisibility(View.VISIBLE);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                }
            });
            mTopBarSwitcher.startAnimation(anim);

            anim = new AlphaAnimation(0.0f, 1.0f);//TranslateAnimation(0, 0, mPageSlider.getHeight(), 0);
            anim.setDuration(400);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    mPageSlider.setVisibility(View.VISIBLE);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    mPageNumberView.setVisibility(View.VISIBLE);
                }
            });
            mPageSlider.startAnimation(anim);

            //hideActionButton();
        }
    }

    private void showActionButton() {
        if (mButtonsVisible && (mTopBarMode == TopBarMode.Accept || mTopBarMode == TopBarMode.Exit))
            return;

        if (!mHasSignatureFields) {
            mTopBarMode = TopBarMode.Sign;
            mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
            showButtons();
        }
    }

    private void hideButtons() {


        if (mDocView.getMode() == SigmaReaderView.Mode.Signing)
            return; // dont try hiding buttons while signing

        if (mButtonsVisible) {
            mButtonsVisible = false;
            hideKeyboard();

            Animation anim = new AlphaAnimation(1.0f, 0.0f);//TranslateAnimation(0, 0, 0, -mTopBarSwitcher.getHeight());
            anim.setDuration(300);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    mTopBarSwitcher.setVisibility(View.INVISIBLE);
                    mTopBarMode = TopBarMode.Main;  // make main menu default
                    mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
                }
            });
            mTopBarSwitcher.startAnimation(anim);

            anim = new AlphaAnimation(1.0f, 0.0f);//new TranslateAnimation(0, 0, 0, mPageSlider.getHeight());
            anim.setDuration(300);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    // mPageNumberView.setVisibility(View.INVISIBLE);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    mPageSlider.setVisibility(View.INVISIBLE);
                }
            });
            mPageSlider.startAnimation(anim);
        }
    }


    private void searchModeOn() {
        if (mTopBarMode != TopBarMode.Search) {
            mTopBarMode = TopBarMode.Search;
            //Focus on EditTextWidget
            mSearchText.requestFocus();
            showKeyboard();
            mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
        }
    }

    private void searchModeOff() {
        if (mTopBarMode == TopBarMode.Search) {
            mTopBarMode = TopBarMode.Main;
            hideKeyboard();
            mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
            SearchTaskResult.set(null);
            // Make the ReaderView act on the change to mSearchTaskResult
            // via overridden onChildSetup method.
            mDocView.resetupChildren();
        }
    }

    private void updatePageNumView(int index) {
        if (core == null)
            return;
        mPageNumberView.setText(String.format("%d / %d", index + 1, core.countPages()));
    }


    private void showInfo(String message) {
        mInfoView.setText(message);

        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            new SafeAnimatorInflater(this, R.animator.info, mInfoView);
        } else {
            mInfoView.setVisibility(View.VISIBLE);
            mHandler.postDelayed(new Runnable() {
                public void run() {
                    mInfoView.setVisibility(View.INVISIBLE);
                }
            }, 500);
        }
    }

    private void makeButtonsView() {

        mButtonsView = inflateBrandableButtonsView();

        mFilenameView = (TextView) mButtonsView.findViewById(R.id.docNameText);
        mPageSlider = (VerticalSeekBar) mButtonsView.findViewById(R.id.pageSlider);
        mPageNumberView = (TextView) mButtonsView.findViewById(R.id.pageNumber);
        mInfoView = (TextView) mButtonsView.findViewById(R.id.info);
        //mSearchButton = (ImageButton) mButtonsView.findViewById(R.id.searchButton);
        //mAnnotButton = (ImageButton) mButtonsView.findViewById(R.id.editAnnotButton);
        mAnnotTypeText = (TextView) mButtonsView.findViewById(R.id.annotType);

        mTopBarSwitcher = (ViewAnimator) mButtonsView.findViewById(R.id.switcher);
        mSearchBack = (ImageButton) mButtonsView.findViewById(R.id.searchBack);
        mSearchFwd = (ImageButton) mButtonsView.findViewById(R.id.searchForward);
        mSearchText = (EditText) mButtonsView.findViewById(R.id.searchText);
        mButtonsView.findViewById(R.id.sign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSignButtonClicked(v);  // due to bug in appcompat https://code.google.com/p/android/issues/detail?id=174871
            }
        });
        mButtonsView.findViewById(R.id.sign).setBackgroundTintList(ColorStateList.valueOf(mBrandingConfig.accent));

        mButtonsView.findViewById(R.id.upload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUploadButtonClicked(v);
            }
        });
        mButtonsView.findViewById(R.id.upload).setBackgroundTintList(ColorStateList.valueOf(mBrandingConfig.accent));

        mButtonsView.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShareButtonClicked(v);
            }
        });

        mButtonsView.findViewById(R.id.share).setBackgroundTintList(ColorStateList.valueOf(mBrandingConfig.accent));

        mButtonsView.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveButtonClicked(v);
            }
        });
        mButtonsView.findViewById(R.id.save).setBackgroundTintList(ColorStateList.valueOf(mBrandingConfig.accent));

        mTopBarSwitcher.setVisibility(View.INVISIBLE);
        mPageNumberView.setVisibility(View.INVISIBLE);
        mInfoView.setVisibility(View.INVISIBLE);
        mPageSlider.setVisibility(View.INVISIBLE);
        //mSignButton.setVisibility(View.INVISIBLE);
    }


    public void OnEditAnnotButtonClick(View v) {
        mTopBarMode = TopBarMode.Annot;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
    }

    public void OnCancelAnnotButtonClick(View v) {
        mTopBarMode = TopBarMode.Main;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
    }

    public void OnHighlightButtonClick(View v) {
        mTopBarMode = TopBarMode.Accept;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
        mAcceptMode = AcceptMode.Highlight;
        mDocView.setMode(SigmaReaderView.Mode.Selecting);
        mAnnotTypeText.setText(R.string.highlight);
        showInfo(getString(R.string.select_text));
    }

    public void OnUnderlineButtonClick(View v) {
        mTopBarMode = TopBarMode.Accept;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
        mAcceptMode = AcceptMode.Underline;
        mDocView.setMode(SigmaReaderView.Mode.Selecting);
        mAnnotTypeText.setText(R.string.underline);
        showInfo(getString(R.string.select_text));
    }

    public void OnStrikeOutButtonClick(View v) {
        mTopBarMode = TopBarMode.Accept;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
        mAcceptMode = AcceptMode.StrikeOut;
        mDocView.setMode(SigmaReaderView.Mode.Selecting);
        mAnnotTypeText.setText(R.string.strike_out);
        showInfo(getString(R.string.select_text));
    }

    public void OnInkButtonClick(View v) {
        mTopBarMode = TopBarMode.Accept;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
        mAcceptMode = AcceptMode.Ink;
        mDocView.setMode(SigmaReaderView.Mode.Drawing);
        mAnnotTypeText.setText(R.string.ink);
        showInfo(getString(R.string.draw_annotation));
    }


    public void OnCancelAcceptButtonClick(View v) {
        MuPDFView pageView = (MuPDFView) mDocView.getDisplayedView();
        if (pageView != null) {
            pageView.deselectText();
            pageView.cancelDraw();
            pageView.cancelSignature();
        }
        mDocView.setMode(SigmaReaderView.Mode.Viewing);
        switch (mAcceptMode) {
            case CopyText:
            case Signature:
                mPenComm.stopScanForPen(); // stop any ongoing pen scanning
                // and disconnect if connected in a second
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                            mPenComm.disconnectPen();
                    }
                }, 100);

                //zoom out onto the center of the signature box
                mDocView.zoomTo = false;
                mDocView.zoomTo(mScreenWidth / 2, mScreenHeight / 2, mDocView.getScaleToFitWidth(mScreenWidth), mScreenWidth, mScreenHeight);// mDocView.getZoom()
                mSigningSignatureField = null;
                break;
            default:
                mTopBarMode = TopBarMode.Annot;
                mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
                break;
        }

        hideButtons();
    }

    public void OnAcceptButtonClick(View v) {
        MuPDFView pageView = (MuPDFView) mDocView.getDisplayedView();
        boolean success = false;
        switch (mAcceptMode) {

            case Highlight:
                if (pageView != null)
                    success = pageView.markupSelection(Annotation.Type.HIGHLIGHT);
                mTopBarMode = TopBarMode.Annot;
                if (!success)
                    showInfo(getString(R.string.no_text_selected));
                break;

            case Underline:
                if (pageView != null)
                    success = pageView.markupSelection(Annotation.Type.UNDERLINE);
                mTopBarMode = TopBarMode.Annot;
                if (!success)
                    showInfo(getString(R.string.no_text_selected));
                break;

            case StrikeOut:
                if (pageView != null)
                    success = pageView.markupSelection(Annotation.Type.STRIKEOUT);
                mTopBarMode = TopBarMode.Annot;
                if (!success)
                    showInfo(getString(R.string.no_text_selected));
                break;

            case Ink:
                if (pageView != null)
                    success = pageView.saveDraw();
                mTopBarMode = TopBarMode.Annot;
                if (!success)
                    showInfo(getString(R.string.nothing_to_save));
                break;
            case Signature:
                mPenComm.stopScanForPen(); // stop any ongoing pen scanning

                mTopBarMode = TopBarMode.Main;
                if (pageView != null) {
                    // save doc before signing it if needed
                    if (core != null && core.hasChanges()) {
                        core.save();
                    }
                    // Select certificate alias for signing with...
                    String certAlias = Sigma.authenticatedIdentity; //TODO could be a multiple choice in future

                    pageView.saveSignature(certAlias, mExternalSignatureProvider); // this starts async task in the PageView
                }

                //zoom out onto the center of the signature box
                mDocView.zoomTo = false; // this clamps the view if needed
                mDocView.zoomTo(mScreenWidth / 2, mScreenHeight / 2, mDocView.getScaleToFitWidth(mScreenWidth), mScreenWidth, mScreenHeight);// mDocView.getZoom()
                mSigningSignatureField = null;  // reset signing field

                SigmaPageView spv = mDocView.getDisplayedView() instanceof SigmaPageView ? (SigmaPageView) mDocView.getDisplayedView() : null;
                if (spv != null) {
                    spv.update();
                }
                break;
        }
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
        mDocView.setMode(SigmaReaderView.Mode.Viewing);
        hideButtons();
    }



    protected AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
    protected AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);

    @Override
    public void onSignatureClicked(SignatureField signatureF) {
        //mDocView.zoomToFitWidth(false);
        mTopBarMode = TopBarMode.Accept;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
        mAcceptMode = AcceptMode.Signature;
        mDocView.setMode(SigmaReaderView.Mode.Signing);
        mAnnotTypeText.setText(getString(R.string.sign_below));
        mAnnotTypeText.startAnimation(fadeIn);
        mAnnotTypeText.startAnimation(fadeOut);
        fadeIn.setStartOffset(2000);
        fadeIn.setDuration(2000);
        fadeIn.setFillAfter(true);
        fadeOut.setDuration(1200);
        fadeOut.setFillAfter(true);
        fadeOut.setStartOffset(4200 + fadeIn.getStartOffset());
        mSigningSignatureField = signatureF;
        showInfo(getString(R.string.signing_doc));
        showButtons();
        //zoom onto the center of the signature box
        mDocView.zoomTo = true;

        SigmaPageView spv = mDocView.getDisplayedView() instanceof SigmaPageView ? (SigmaPageView) mDocView.getDisplayedView() : null;
        if (spv != null && signatureF != null) {

            // figure out the center of the box in screen coordinates
            PointF topRight = spv.PDF2ScreenCoordinates(signatureF.fieldPosition.position.getRight(), signatureF.fieldPosition.position.getTop());
            PointF bottomLeft = spv.PDF2ScreenCoordinates(signatureF.fieldPosition.position.getLeft(), signatureF.fieldPosition.position.getBottom());
            float boxWidth = (topRight.x - bottomLeft.x);
            float boxHeight = (bottomLeft.y - topRight.y);

            String boxPosition = Sigma.applicationProperties.getProperty("signature_box_v_position", "center");
            if (boxPosition.equalsIgnoreCase(BOX_POSITION_BOTTOM)) {
                topRight.offset(-boxWidth / 2, -boxHeight / 3); // now we have center of the box in screen
            } else if (boxPosition.equalsIgnoreCase(BOX_POSITION_TOP)) {
                topRight.offset(-boxWidth / 2, boxHeight ); // now we have center of the box in screen
            } else {
                topRight.offset(-boxWidth / 2, boxHeight /2 ); // now we have center of the box in screen

            }



            // figure out the zoom by keeping the signature box constant size e.g. (80 mm  wide is good for comfy signing)
            float pxWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 80, getResources().getDisplayMetrics());
            //float pxWidth = getResources().getDisplayMetrics().widthPixels - (getResources().getDisplayMetrics().widthPixels / 5); // 80% OF SCREEN WIDTH
            float scale = pxWidth / (boxWidth / mDocView.getZoom());
            mDocView.zoomTo((int) topRight.x, (int) topRight.y, scale, mScreenWidth, mScreenHeight);// mDocView.getZoom()
        }

        // and try to connect to pen in the background ...
        mPenComm = new BasicComm(mContext);
        mPenComm.setLineResponseListener(this);
        //mPenComm.disconnectPen(); // always look for new pen (e.g. pens could be switched for another signature)
        //mPenComm.findAndConnectPen();

        // and setup regular retry until connected or not in signing
        //mHandler.postDelayed(mPenReconnector, PEN_RECONNECT_TIMEOUT);
        mPenComm.startScanForPen();
    }




    public void onSignButtonClicked(View v) {
        AcroFields.FieldPosition fp = new AcroFields.FieldPosition();
        SigmaPageView spv = mDocView.getDisplayedView() instanceof SigmaPageView ? (SigmaPageView) mDocView.getDisplayedView() : null;
        if (spv != null) {

            PointF topRight = spv.screen2PDFCoordinates(getResources().getDisplayMetrics().widthPixels, 0);
            PointF bottomLeft = spv.screen2PDFCoordinates(0, getResources().getDisplayMetrics().heightPixels);

            fp.page = spv.getPage();
            fp.position = new Rectangle(0, 0, 0, 0);
            fp.position.setTop(topRight.y);
            fp.position.setRight(topRight.x);
            fp.position.setBottom(bottomLeft.y);
            fp.position.setLeft(bottomLeft.x);
            SignatureField signatureF = new SignatureField("S" + new Date().toString(), fp, null);
            signatureF.fieldExists = false; // field does not exist yet
            mSignatures.add(signatureF);

            mTopBarMode = TopBarMode.Accept;
            mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
            mAcceptMode = AcceptMode.Signature;
            mDocView.setMode(SigmaReaderView.Mode.Signing);
            mAnnotTypeText.setText(getString(R.string.sign_below));


            mAnnotTypeText.startAnimation(fadeIn);
            mAnnotTypeText.startAnimation(fadeOut);
            fadeIn.setStartOffset(2000);
            fadeIn.setDuration(2000);
            fadeIn.setFillAfter(true);
            fadeOut.setDuration(1200);
            fadeOut.setFillAfter(true);
            fadeOut.setStartOffset(4200 + fadeIn.getStartOffset());

            mSigningSignatureField = signatureF; // Activity tracks selected signature here
            spv.mSignatureField = signatureF; // view tracks selected signature here
            showInfo(getString(R.string.signing_doc));
            showButtons();

            // and try to connect to pen in the background ...
            mPenComm = new BasicComm(mContext);
            mPenComm.setLineResponseListener(this);
            //mPenComm.disconnectPen(); // always look for new pen (e.g. pens could be switched for another signature)
//            mPenComm.findAndConnectPen();
//            // and setup regular retry until connected or not in signing
//            mHandler.postDelayed(mPenReconnector, PEN_RECONNECT_TIMEOUT);
            mPenComm.startScanForPen();
        }
    }


    public void OnCancelSearchButtonClick(View v) {
        searchModeOff();
    }

    public void OnDeleteButtonClick(View v) {
        MuPDFView pageView = (MuPDFView) mDocView.getDisplayedView();
        if (pageView != null)
            pageView.deleteSelectedAnnotation();
        mTopBarMode = TopBarMode.Annot;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
    }

    public void OnCancelDeleteButtonClick(View v) {
        MuPDFView pageView = (MuPDFView) mDocView.getDisplayedView();
        if (pageView != null)
            pageView.deselectAnnotation();
        mTopBarMode = TopBarMode.Annot;
        mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.showSoftInput(mSearchText, 0);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
    }

    private void search(int direction) {
        hideKeyboard();
        int displayPage = mDocView.getDisplayedViewIndex();
        SearchTaskResult r = SearchTaskResult.get();
        int searchPage = r != null ? r.pageNumber : -1;
        mSearchTask.go(mSearchText.getText().toString(), direction, displayPage, searchPage);
    }

    @Override
    public boolean onSearchRequested() {
        if (mButtonsVisible && mTopBarMode == TopBarMode.Search) {
            hideButtons();
        } else {
            showButtons();
            searchModeOn();
        }
        return super.onSearchRequested();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mButtonsVisible && mTopBarMode != TopBarMode.Search) {
            hideButtons();
        } else {
            showButtons();
            searchModeOff();
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    protected void onStop() {
        if (core != null) {
            destroyAlertWaiter();
            core.stopAlerts();
        }
        // it is safe to unbind here since camera activity must be after onStart() and bound to service thus not losing our service instance
        //    if(scanningServiceConnection != null) unbindService(scanningServiceConnection);
        if (mExternalSignatureProvider != null) {
            mExternalSignatureProvider.doUnbindService();
        }
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (mDocView == null) {
            super.onBackPressed();
            return;
        }
        if (mDocView.getMode() == SigmaReaderView.Mode.Signing) {
            OnCancelAcceptButtonClick(null);
            return; // pressing back during signing just cancels the signature
        }
        //FIXME  first save doc via muPDF!!!! if temp changes were made (should not be allowed after signing)
        if (core != null && core.hasChanges()) {
            core.save();
        }


        if (getIntent().hasExtra(WebIntent.URL_UPLOAD)) {
            if (mUnsignedRequiredSignature) {
                cannotUploadDialog();
            } else {
                uploadDialog();
            }
            return; // no double back action needed here
        } else {
            // show sharing/saving actions
            mTopBarMode = TopBarMode.Exit;
            mTopBarSwitcher.setDisplayedChild(mTopBarMode.ordinal());

            showButtons();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideButtons();
                }
            }, 4000);
        }


        // exit only on double back button hit with confirmation
        if (mDoubleBackToExitPressedOnce) {
            exitDialog();
            return;
        }

        this.mDoubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.back_again_to_exit), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDoubleBackToExitPressedOnce = false;

            }
        }, 2500);

    }

    //save on external media in documents folder or downloads (for jellybean androids)
    public void onSaveButtonClicked(View v) {
        String outputFilename = mOriginalInputFileName != null ? mOriginalInputFileName : "SignedDocument.pdf";
        mDocFile = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), outputFilename);
        // save to Documents external directory and finish
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.KITKAT) {
            // after KitKat there is a specific Documents folder
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
            dir.mkdirs();// but to make sure - try to make them
            mDocFile = new File(dir, outputFilename);
        }

        try {
            FileUtilsPlus.copy(new File(core.mLastOpenFilePath), mDocFile);
        } catch (IOException e) {
            e.printStackTrace();//FIXME
            Toast.makeText(mContext, "Error: Cannot Save! Did you allow it?", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(SigmaActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    SIGMA_REQUEST_WRITE_EXT_PERMISSIONS);
            return;
        }
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case SIGMA_REQUEST_WRITE_EXT_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    try {
                        FileUtilsPlus.copy(new File(core.mLastOpenFilePath), mDocFile);
                    } catch (IOException e) {
                        e.printStackTrace();//FIXME
                        Toast.makeText(mContext, "Error: Still cannot Save!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    finish();
                } else {
                    Toast.makeText(mContext, "Cannot save if you dont allow it :-(", Toast.LENGTH_LONG).show();
                    // permission denied
                }
                return;
            }
        }
    }

    public void onShareButtonClicked(View v) {
        String outputFilename = mOriginalInputFileName != null ? mOriginalInputFileName : "SignedDocument.pdf";
        shareByImplicitIntent(core.mLastOpenFilePath, outputFilename);
    }

    // upload to cloud and show QR link
    public void onUploadButtonClicked(View v) {
        // Note: Upload URL will be create dynamically in Main Activity

        Intent uploadIntent = new Intent(mContext, MainActivity.class);
        uploadIntent.putExtras(getIntent()); // copy all extras
        uploadIntent.setAction(Sigma.UPLOAD_INTENT_ACTION);
        //set additional data
        uploadIntent.setData(Uri.parse(core.mLastOpenFilePath));
        uploadIntent.putExtra(WebIntent.SHOW_QR, true);
        finish();
        startActivity(uploadIntent);
    }


    //@NonNull
    private void exitDialog() {
        Resources res = getResources();
        final AlertDialog alert = mAlertBuilder.create();
        alert.setTitle(String.format(res.getString(R.string.Exiting)));
        alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.Yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alert.dismiss();
                Intent mainIntent = new Intent(mContext, MainActivity.class);
                mainIntent.putExtras(getIntent()); // copy all extras
                mainIntent.setAction(Intent.ACTION_DEFAULT);
                //set additional data
                finish();
                startActivity(mainIntent);

            }
        });
        alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alert.dismiss();
            }
        });
        alert.show();
    }


    //@NonNull
    private void uploadDialog() {
        Resources res = getResources();
        final AlertDialog alert = mAlertBuilder.create();
        alert.setTitle(String.format(res.getString(R.string.finish_upload_and_quit_doc)));
        alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.save), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alert.dismiss();

                //Intent i = getIntent();

                // either do the upload from the main activity or show sharing options/local save
                Intent uploadIntent = new Intent(mContext, MainActivity.class);
                // pass all extra params to new intent
                uploadIntent.putExtras(getIntent());
                // set new Action and signed file path
                uploadIntent.setAction(Sigma.UPLOAD_INTENT_ACTION);
                uploadIntent.setData(Uri.parse(core.mLastOpenFilePath));
                finish();
                startActivity(uploadIntent);

            }
        });
        alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent mainIntent = new Intent(mContext, MainActivity.class);
                mainIntent.putExtras(getIntent()); // copy all extras
                mainIntent.setAction(Intent.ACTION_DEFAULT);
                //set additional data
                finish();
                startActivity(mainIntent);
            }
        });
        alert.show();
    }

    //@NonNull
    private void cannotUploadDialog() {
        Resources res = getResources();
        final AlertDialog alert = mAlertBuilder.create();
        alert.setTitle(String.format(res.getString(R.string.cannot_finish_upload_and_quit_doc)));
        alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alert.dismiss();
                ((SigmaActivity) mContext).finish();
            }
        });
        alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.No), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alert.dismiss();
            }
        });
        alert.show();
    }

    // share file using installed sharing apps accepting content:// intent
    private void shareByImplicitIntent(String finalSharedFilePath, String outputFilename) {
        Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        File fileWithinMyDir = new File(finalSharedFilePath);
        // rename the file last opened file to original filename
        File outputFile = new File(fileWithinMyDir.getParentFile(), outputFilename);
        if (outputFile.exists()) {
            outputFile.delete();  // the original in cache will now be the last viewed file so delete original if any
        }
        fileWithinMyDir.renameTo(outputFile);

        if (outputFile.exists()) {
            intentShareFile.setType("application/pdf");
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://" + CachedFileProvider.AUTHORITY + File.separator + outputFilename));
            intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intentShareFile.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.signed_document) + outputFilename);
            //intentShareFile.putExtra(Intent.EXTRA_EMAIL, new String[]{"pkolarov@gmail.com"}); // FIXME
            //intentShareFile.putExtra(Intent.EXTRA_TEXT, "Find attached signed document. ");
            intentShareFile.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            intentShareFile.setFlags(intentShareFile.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
            finish();
            startActivity(Intent.createChooser(intentShareFile, getString(R.string.send_doc_via)));
        }
    }

    @Override
    public void createProgressDialog(String msg) {
        // ....show waiting dialog
        mProgress = ProgressDialog.show(mContext, getString(R.string.wait), msg, true);
    }


    @Override
    public void dismissProgressDialog() {
        if (mProgress != null) {
            mProgress.dismiss();  // kill any waiting dialog
            mProgress = null;
        }
    }


    public void signatureOutOfBox() {
        Resources res = getResources();
        final AlertDialog alert = mAlertBuilder.create();
        alert.setTitle(String.format(res.getString(R.string.signature_out_of_box)));
        alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alert.dismiss();

            }
        });
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mDocView.clearSignature();
            }
        });
        alert.show();
    }
}


