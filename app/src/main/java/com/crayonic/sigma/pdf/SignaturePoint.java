package com.crayonic.sigma.pdf;

import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by kolarov on 4.6.2015.
 */
public class SignaturePoint implements Parcelable, Serializable {
    private final static long serialVersionUID = 1;  //dont forget to change this on field changes (dont remove fields!)

    public float x,y,z;
    public long t;
    public float accX, accY, accZ;
    public float gX,gY,gZ;
    public float viewOffsetX;
    public float viewOffsetY;



    public SignaturePoint(){
        x = y = z = accX = accY = accZ = gX = gY = gZ = 0f;
        t = 0;
    }

    public SignaturePoint(float x, float y, float z, long t, float ox, float oy){
        accX = accY = accZ = gX = gY = gZ = 0;
        this.x = x; this.y = y; this.z = z;
        this.t = t;
        this.viewOffsetX = ox;
        this.viewOffsetY = oy;
    }

    public SignaturePoint(float x, float y, float z, long t, float accX, float accY, float accZ, float gX, float gY, float gZ, float ox, float oy){
        this.x = x; this.y = y; this.z = z; this.accX = accX; this.accY = accY; this.accZ = accZ; this.gX = gX; this.gY = gY; this.gZ = gZ;
        this.t = t;
        this.viewOffsetX = ox;
        this.viewOffsetY = oy;
    }

    public PointF toPointF(){
        return new PointF(x, y);
    }
    public float getPressure(){ return z;}
    public long getTime(){ return  t;}

    public PointF getViewOffset(){ return new PointF(viewOffsetX, viewOffsetY);}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(x);
        parcel.writeFloat(y);
        parcel.writeFloat(z);
        parcel.writeLong(t);
        parcel.writeFloat(accX);
        parcel.writeFloat(accY);
        parcel.writeFloat(accZ);
        parcel.writeFloat(gX);
        parcel.writeFloat(gY);
        parcel.writeFloat(gZ);
        parcel.writeFloat(viewOffsetX);
        parcel.writeFloat(viewOffsetY);
    }

    public static final Parcelable.Creator<SignaturePoint> CREATOR
            = new Parcelable.Creator<SignaturePoint>() {
        public SignaturePoint createFromParcel(Parcel in) {
            return new SignaturePoint(in);
        }

        public SignaturePoint[] newArray(int size) {
            return new SignaturePoint[size];
        }
    };

    private SignaturePoint(Parcel in) {
        x = in.readFloat();
        y = in.readFloat();
        z = in.readFloat();
        t = in.readLong();
        accX = in.readFloat();
        accY = in.readFloat();
        accZ = in.readFloat();
        gX = in.readFloat();
        gY = in.readFloat();
        gZ = in.readFloat();
        viewOffsetX = in.readFloat();
        viewOffsetY = in.readFloat();
    }



}
