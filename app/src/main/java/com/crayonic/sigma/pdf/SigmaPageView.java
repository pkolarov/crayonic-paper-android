package com.crayonic.sigma.pdf;

import java.io.FileInputStream;
import java.security.PublicKey;
import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crayonic.sigma.R;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.crypto.ExternalSignatureProvider;
import com.crayonic.sigma.pdf.Signature.CreateSignature;
import com.crayonic.sigma.tools.Crypto;
import com.crayonic.sigma.tools.LastLocation;
import com.crayonic.sigma.tools.SignatureCertSelector;

/* This enum should be kept in line with the cooresponding C enum in mupdf.c */
enum SignatureState {
    NoSupport,
    Unsigned,
    Signed
}

abstract class PassClickResultVisitor {
    public abstract void visitText(PassClickResultText result);

    public abstract void visitChoice(PassClickResultChoice result);

    public abstract void visitSignature(PassClickResultSignature result);
}

class PassClickResult {
    public final boolean changed;

    public PassClickResult(boolean _changed) {
        changed = _changed;
    }

    public void acceptVisitor(PassClickResultVisitor visitor) {
    }
}

class PassClickResultText extends PassClickResult {
    public final String text;

    public PassClickResultText(boolean _changed, String _text) {
        super(_changed);
        text = _text;
    }

    public void acceptVisitor(PassClickResultVisitor visitor) {
        visitor.visitText(this);
    }
}

class PassClickResultChoice extends PassClickResult {
    public final String[] options;
    public final String[] selected;

    public PassClickResultChoice(boolean _changed, String[] _options, String[] _selected) {
        super(_changed);
        options = _options;
        selected = _selected;
    }

    public void acceptVisitor(PassClickResultVisitor visitor) {
        visitor.visitChoice(this);
    }
}

class PassClickResultSignature extends PassClickResult {
    public final SignatureState state;

    public PassClickResultSignature(boolean _changed, int _state) {
        super(_changed);
        state = SignatureState.values()[_state];
    }

    public void acceptVisitor(PassClickResultVisitor visitor) {
        visitor.visitSignature(this);
    }
}


public class SigmaPageView extends PageView implements MuPDFView {

    final static private String TAG = "SigmaPageView";
    public static final int MUPDF_ZOOM_FACTOR = 2;
    private static final float SIGNATURE_BITMAP_WIDTH = 500; // size of bitmap to create for handwritten signature image


    public class PassClickResultVisitorSigma extends PassClickResultVisitor {
        private float hitX, hitY;

        void setHit(float hitX, float hitY) {
            this.hitX = hitX;
            this.hitY = hitY;
        }

        @Override
        public void visitText(PassClickResultText result) {
            invokeTextDialog(result.text);
        }

        @Override
        public void visitChoice(PassClickResultChoice result) {
            invokeChoiceDialog(result.options);
        }

        @Override
        public void visitSignature(PassClickResultSignature result) {
            // Custom signature verification / signing process using iText/BouncyCastle library (MuPDF uses OpenSSL which is a bit heavy and harder to follow)
            //first find which signature field was clicked on by going through all of them
            SignatureField signatureF = null;
            for (SignatureField s : ((SigmaActivity) mContext).mSignatures) {
                if(s.fieldPosition != null) {
                    if (s.fieldPosition.position.getLeft() < hitX && s.fieldPosition.position.getRight() > hitX &&
                            s.fieldPosition.position.getTop() > hitY && s.fieldPosition.position.getBottom() < hitY) {
                        signatureF = s;
                        break;
                    }
                }
            }
            if (signatureF == null) {
                Log.wtf(TAG, "Why there is no signature field registered at the PDF  position by iText when MuPDF claims one exists on the position of click!!!");
                return;
            }

            if (signatureF.signature == null) {
                // for blank signature just invoke signing
                invokeSigning(signatureF);
            } else {
                // for signed signature - display dialog with its signer info
                invokeSignatureCheckingDialog(signatureF);
            }


        }
    }


    private final MuPDFCore mCore;
    private AsyncTask<Void, Void, PassClickResult> mPassClick;
    private RectF mWidgetAreas[];
    private Annotation mAnnotations[];
    private int mSelectedAnnotationIndex = -1;
    private AsyncTask<Void, Void, RectF[]> mLoadWidgetAreas;
    private AsyncTask<Void, Void, Annotation[]> mLoadAnnotations;
    private AlertDialog.Builder mTextEntryBuilder;
    private AlertDialog.Builder mChoiceEntryBuilder;
    private AlertDialog.Builder mSignatureReportBuilder;
    private AlertDialog.Builder mPasswordEntryBuilder;
    private EditText mPasswordText;
    private AlertDialog mTextEntry;
    private AlertDialog mPasswordEntry;
    private EditText mEditText;
    private AsyncTask<String, Void, Boolean> mSetWidgetText;
    private AsyncTask<String, Void, Void> mSetWidgetChoice;
    private AsyncTask<PointF[], Void, Void> mAddStrikeOut;
    private AsyncTask<PointF[][], Void, Void> mAddInk;
    private AsyncTask<ArrayList<ArrayList<SignaturePoint>>, Void, String> mAddSignature;
    private AsyncTask<Integer, Void, Void> mDeleteAnnotation;
    private AsyncTask<Void, Void, CertificateStatus> mCheckSignature;
    private Context mContext;

    public SignatureField mSignatureField;  // field selected for signing

    private Runnable changeReporter;

    public SigmaPageView(Context c, MuPDFCore core, Point parentSize, Bitmap sharedHqBm) {
        super(c, parentSize, sharedHqBm);
        mContext = c;

        mCore = core;
        mTextEntryBuilder = new AlertDialog.Builder(c);
        mTextEntryBuilder.setTitle(getContext().getString(R.string.fill_out_text_field));
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mEditText = (EditText) inflater.inflate(R.layout.textentry, null);
        mTextEntryBuilder.setView(mEditText);
        mTextEntryBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mTextEntryBuilder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mSetWidgetText = new AsyncTask<String, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(String... arg0) {
                        return mCore.setFocusedWidgetText(mPageNumber, arg0[0]);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        changeReporter.run();
                        if (!result)
                            invokeTextDialog(mEditText.getText().toString());
                    }
                };

                mSetWidgetText.execute(mEditText.getText().toString());
            }
        });
        mTextEntry = mTextEntryBuilder.create();

        mChoiceEntryBuilder = new AlertDialog.Builder(c);
        mChoiceEntryBuilder.setTitle(getContext().getString(R.string.choose_value));

//		mSigningDialogBuilder = new AlertDialog.Builder(c);
//		mSigningDialogBuilder.setTitle("Select certificate and sign?");
//		mSigningDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//			}
//		});
//		mSigningDialogBuilder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				FilePicker picker = new FilePicker(mFilePickerSupport) {
//					@Override
//					public void onPick(Uri uri) {
//						signWithKeyFile(uri);
//					}
//				};
//
//				picker.pick();
//			}
//		});

        mSignatureReportBuilder = new AlertDialog.Builder(c);
        mSignatureReportBuilder.setTitle(Sigma.getContext().getString(R.string.signature_checked));
        mSignatureReportBuilder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mPasswordText = new EditText(c);
        mPasswordText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
        mPasswordText.setTransformationMethod(new PasswordTransformationMethod());

        mPasswordEntryBuilder = new AlertDialog.Builder(c);
        mPasswordEntryBuilder.setTitle(R.string.enter_password);
        mPasswordEntryBuilder.setView(mPasswordText);
        mPasswordEntryBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mPasswordEntry = mPasswordEntryBuilder.create();
        mSignatureField = null;
        mSignature = null;
    }


    public LinkInfo hitLink(float x, float y) {
        // Since link highlighting was implemented, the super class
        // PageView has had sufficient information to be able to
        // perform this method directly. Making that change would
        // make MuPDFCore.hitLinkPage superfluous.
//		float scale = mSourceScale*(float)getWidth()/(float)mSize.x;
//		float docRelX = (x - getLeft())/scale;
//		float docRelY = (y - getTop())/scale;
//
//		for (LinkInfo l: mLinks)
//			if (l.rect.contains(docRelX, docRelY))
//				return l;

        return null;
    }

    private void invokeTextDialog(String text) {
        mEditText.setText(text);
        mTextEntry.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        mTextEntry.show();
    }

    private void invokeChoiceDialog(final String[] options) {
        mChoiceEntryBuilder.setItems(options, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mSetWidgetChoice = new AsyncTask<String, Void, Void>() {
                    @Override
                    protected Void doInBackground(String... params) {
                        String[] sel = {params[0]};
                        mCore.setFocusedWidgetChoiceSelected(sel);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        changeReporter.run();
                    }
                };

                mSetWidgetChoice.execute(options[which]);
            }
        });
        AlertDialog dialog = mChoiceEntryBuilder.create();
        dialog.show();
    }

    private void invokeSignatureCheckingDialog(final SignatureField signatureF) {

        AlertDialog report = mSignatureReportBuilder.create();
        report.setTitle(Sigma.getContext().getString(R.string.signature_checked));
        View verSigView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.verify_signature, null);
        ImageView vsi = (ImageView) verSigView.findViewById(R.id.verifySigDocValidIcon);
        TextView vst = (TextView) verSigView.findViewById(R.id.verifySigDocValidTxt);
        if (signatureF.hasIntegrity) {
            vsi.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
            vst.setText(mContext.getText(R.string.signatureOK));
        } else {
            vsi.setImageDrawable(getResources().getDrawable(R.drawable.ic_cancel));
            vst.setText(mContext.getText(R.string.signatureNOK));
        }

        vsi = (ImageView) verSigView.findViewById(R.id.verifySigIdentityIcon);
        vst = (TextView) verSigView.findViewById(R.id.verifySigIdentityTxt);
        if (signatureF.hasIdentity) {
            vsi.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
            vst.setText(mContext.getText(R.string.signatureIdentityOK));
        } else {
            vsi.setImageDrawable(getResources().getDrawable(R.drawable.ic_cancel));
            vst.setText(mContext.getText(R.string.signatureIdentityNOK));
        }

        vsi = (ImageView) verSigView.findViewById(R.id.verifySigTimestampIcon);
        vst = (TextView) verSigView.findViewById(R.id.verifySigTimestampTxt);
        if (signatureF.hasTimeStamp) {
            vsi.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
            vst.setText(mContext.getText(R.string.signatureTimestampOK));
        } else {
            vsi.setImageDrawable(getResources().getDrawable(R.drawable.ic_cancel));
            vst.setText(mContext.getText(R.string.signatureTimestampNOK));
        }
        report.setView(verSigView);
        report.show();
    }

    private void invokeSigning(SignatureField signatureF) {

        if (mContext instanceof SignatureListener) {
            mSignatureField = signatureF;
            ((SignatureListener) mContext).onSignatureClicked(signatureF);
        }
    }


    public void setChangeReporter(Runnable reporter) {
        changeReporter = reporter;
    }

    public PointF screen2PDFCoordinates(float sx, float sy) {
        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
        final float docRelX = (sx - getLeft()) / scale;
        final float docRelY = (sy - getTop()) / scale;
        // To Convert from bitmap to PDF coordinates just divide by zoom factor which is according to mupdf.c  glo->resolution / 72 where resolution is constant 160 and round up to next integer => 2
        //...and Y is flipped so subtract from the page height before the zoom
        // - > PDFx and PDFy are the real PDF document coordinates, while docRelX and docRelY are bitmap coordinates, x and y are screen coordinates
        final float PDFx = docRelX / MUPDF_ZOOM_FACTOR;
        final float PDFy = (mCore.getPageSize(mPageNumber).y - docRelY) / MUPDF_ZOOM_FACTOR; //TODO: check if document rotation factor does not affect the page size in y direction

        return new PointF(PDFx, PDFy);
    }

    public PointF PDF2ScreenCoordinates(float PDFx, float PDFy) {
        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;

        final float docRelX = PDFx * MUPDF_ZOOM_FACTOR;
        final float docRelY = mCore.getPageSize(mPageNumber).y - (PDFy * MUPDF_ZOOM_FACTOR); //TODO: check if document rotation factor does not affect the page size in y direction

        float x = docRelX * scale + getLeft();
        float y = docRelY * scale + getTop();
        return new PointF(x, y);
    }

    public PointF PDFSize2ScreenSize(float PDFw, float PDFh) {
        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;

        final float docRelX = PDFw * MUPDF_ZOOM_FACTOR;
        final float docRelY = PDFh * MUPDF_ZOOM_FACTOR; //TODO: check if document rotation factor does not affect the page size in y direction

        float x = docRelX * scale;
        float y = docRelY * scale;
        return new PointF(x, y);
    }

    public PointF PDF2ViewCoordinates(float PDFx, float PDFy) {
        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;

        final float docRelX = PDFx * MUPDF_ZOOM_FACTOR;
        final float docRelY = mCore.getPageSize(mPageNumber).y - (PDFy * MUPDF_ZOOM_FACTOR); //TODO: check if document rotation factor does not affect the page size in y direction

        float x = docRelX * 1;
        float y = docRelY * 1;
        return new PointF(x, y);
    }


    public Hit passClickEvent(final float x, final float y) {
        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
        final float docRelX = (x - getLeft()) / scale;
        final float docRelY = (y - getTop()) / scale;

        boolean hit = false;
        int i;

        if (mAnnotations != null) {
            for (i = 0; i < mAnnotations.length; i++)
                if (mAnnotations[i].contains(docRelX, docRelY)) {
                    hit = true;
                    break;
                }

            if (hit) {
                switch (mAnnotations[i].type) {
                    case HIGHLIGHT:
                    case UNDERLINE:
                    case SQUIGGLY:
                    case STRIKEOUT:
                    case INK:
                        mSelectedAnnotationIndex = i;
                        setItemSelectBox(mAnnotations[i]);
                        return Hit.Annotation;
                }
            }
        }

        mSelectedAnnotationIndex = -1;
        setItemSelectBox(null);

        if (!mCore.javascriptSupported())
            return Hit.Nothing;

        if (mWidgetAreas != null) {
            for (i = 0; i < mWidgetAreas.length && !hit; i++)
                if (mWidgetAreas[i].contains(docRelX, docRelY))
                    hit = true;
        }

        if (hit) {
            mPassClick = new AsyncTask<Void, Void, PassClickResult>() {
                @Override
                protected PassClickResult doInBackground(Void... arg0) {
                    return mCore.passClickEvent(mPageNumber, docRelX, docRelY);
                }

                @Override
                protected void onPostExecute(PassClickResult result) {
                    if (result.changed) {
                        changeReporter.run();
                    }
                    PassClickResultVisitorSigma pcrv = new PassClickResultVisitorSigma();
                    PointF p = screen2PDFCoordinates(x, y);
                    pcrv.setHit(p.x, p.y); // sent the real PDF coordinates
                    result.acceptVisitor(pcrv);
                }
            };

            mPassClick.execute();
            return Hit.Widget;
        }

        return Hit.Nothing;
    }

    @TargetApi(11)
    public boolean copySelection() {
        final StringBuilder text = new StringBuilder();

        processSelectedText(new TextProcessor() {
            StringBuilder line;

            public void onStartLine() {
                line = new StringBuilder();
            }

            public void onWord(TextWord word) {
                if (line.length() > 0)
                    line.append(' ');
                line.append(word.w);
            }

            public void onEndLine() {
                if (text.length() > 0)
                    text.append('\n');
                text.append(line);
            }
        });

        if (text.length() == 0)
            return false;

        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.content.ClipboardManager cm = (android.content.ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);

            cm.setPrimaryClip(ClipData.newPlainText("MuPDF", text));
        } else {
            android.text.ClipboardManager cm = (android.text.ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
            cm.setText(text);
        }

        deselectText();

        return true;
    }

    public boolean markupSelection(final Annotation.Type type) {
        final ArrayList<PointF> quadPoints = new ArrayList<PointF>();
        processSelectedText(new TextProcessor() {
            RectF rect;

            public void onStartLine() {
                rect = new RectF();
            }

            public void onWord(TextWord word) {
                rect.union(word);
            }

            public void onEndLine() {
                if (!rect.isEmpty()) {
                    quadPoints.add(new PointF(rect.left, rect.bottom));
                    quadPoints.add(new PointF(rect.right, rect.bottom));
                    quadPoints.add(new PointF(rect.right, rect.top));
                    quadPoints.add(new PointF(rect.left, rect.top));
                }
            }
        });

        if (quadPoints.size() == 0)
            return false;

        mAddStrikeOut = new AsyncTask<PointF[], Void, Void>() {
            @Override
            protected Void doInBackground(PointF[]... params) {
                addMarkup(params[0], type);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                loadAnnotations();
                update();
            }
        };

        mAddStrikeOut.execute(quadPoints.toArray(new PointF[quadPoints.size()]));

        deselectText();

        return true;
    }

    public void deleteSelectedAnnotation() {
        if (mSelectedAnnotationIndex != -1) {
            if (mDeleteAnnotation != null)
                mDeleteAnnotation.cancel(true);

            mDeleteAnnotation = new AsyncTask<Integer, Void, Void>() {
                @Override
                protected Void doInBackground(Integer... params) {
                    mCore.deleteAnnotation(mPageNumber, params[0]);
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    loadAnnotations();
                    update();
                }
            };

            mDeleteAnnotation.execute(mSelectedAnnotationIndex);

            mSelectedAnnotationIndex = -1;
            setItemSelectBox(null);
        }
    }

    public void deselectAnnotation() {
        mSelectedAnnotationIndex = -1;
        setItemSelectBox(null);
    }

    public boolean saveDraw() {
        PointF[][] path = getDraw();

        if (path == null)
            return false;

        if (mAddInk != null) {
            mAddInk.cancel(true);
            mAddInk = null;
        }
        mAddInk = new AsyncTask<PointF[][], Void, Void>() {
            @Override
            protected Void doInBackground(PointF[][]... params) {
                mCore.addInkAnnotation(mPageNumber, params[0]);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                loadAnnotations();
                update();
            }

        };

        mAddInk.execute(getDraw());
        cancelDraw();

        return true;
    }




    public void saveSignature(final String certAlias, final ExternalSignatureProvider es) {

        final ArrayList<ArrayList<SignaturePoint>> path = getSignature();

        if (path == null)
            return;

        final ProgressDialog dialog = ProgressDialog.show(mContext, mContext.getString(R.string.signing_doc), mContext.getString(R.string.wait), true);

        if (mAddSignature != null) {
            mAddSignature.cancel(true);
            mAddSignature = null;
        }
        mAddSignature = new AsyncTask<ArrayList<ArrayList<SignaturePoint>>, Void, String>() {

            @Override
            protected String doInBackground(ArrayList<ArrayList<SignaturePoint>>... params) {
                // first decide who is signing
                int principalId = SignatureCertSelector.selectSignatoryCert(mSignatureField, mContext);  // try to figure out from field name which certificate to use
                if (principalId < 0) {
                    if (!es.signWithExistingCertificate(certAlias)) {
                        return null;
                    }
                } else {
                    if (!es.signWithNewSelfSignedCertificate(principalId)) {
                        return null;
                    }
                }

                // create bitmap from vector  and start signature process
                PointF screenBoxSize = PDFSize2ScreenSize(mSignatureField.fieldPosition.position.getWidth(), mSignatureField.fieldPosition.position.getHeight()); //mSignatureField.viewBox.width();//SIGNATURE_BITMAP_WIDTH;
                PointF screenTopLeftOffset = PDF2ScreenCoordinates(mSignatureField.fieldPosition.position.getLeft(), mSignatureField.fieldPosition.position.getTop());

                //get screen size for scaling down painting
                DisplayMetrics dm = new DisplayMetrics();
                WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
                wm.getDefaultDisplay().getMetrics(dm);

                Bitmap bmp = DrawSignature.onBitmapCurve(params[0], (int) screenBoxSize.x, (int) screenBoxSize.y, (int) -screenTopLeftOffset.x, (int) -screenTopLeftOffset.y, 1);

                // flatten forms in PDF
                String flattenedPDF = null;
                String  sigBio;
                byte[] sigBioHash;

                if(Sigma.applicationProperties.getProperty("flatten_form_fields").equalsIgnoreCase("true")) {
                    try {
                        flattenedPDF = CreateSignature.flattenPDFFieldsExcludingSignatures(mCore.mLastOpenFilePath);
                    } catch (Exception e) {
                        Log.e(TAG, "File flattening  error!");
                        e.printStackTrace();
                        return null;
                    }
                }

                if (flattenedPDF == null) {
                    flattenedPDF = mCore.mLastOpenFilePath;
                }

                try {
                    // Get biometric encryption public key and encrypt biometrics using it as a master key
                    //mContext.getAssets().open("cert/public_key.der")
                    FileInputStream ks  = new FileInputStream(Sigma.getCertPath() + Sigma.applicationProperties.getProperty("biometry_encryption_public_key"));
                    PublicKey key = Crypto.readPublicKey(ks);
                    sigBio = Crypto.serializeToRSAEncryptedB64String(path, key);
                    sigBioHash = Crypto.sha256(path);
                } catch (Exception e) {
                    Log.e(TAG, "Biometric signature encryption error!");
                    e.printStackTrace();
                    return null;
                }

                String location = "Unknown";
                Location loc = LastLocation.getLastKnown(true, mContext);
                if (loc != null) {
                    location = "Lat: " + loc.getLatitude() + " Lon: " + loc.getLongitude() + " Acc: " + loc.getAccuracy();
                }

                String reason = Sigma.getContext().getString(R.string.signature_reason);

                String signedBy = Sigma.applicationProperties.getProperty("signed_by", "");
                String dateFormat = Sigma.applicationProperties.getProperty("date_format","dd.MM.yyyy");
                // do the itext signing magic
                String signedPDF = CreateSignature.signDocument(es, mSignatureField, bmp, flattenedPDF, sigBio, sigBioHash, location, reason, signedBy, dateFormat);

                return signedPDF;
            }

            @Override
            protected void onPostExecute(String filename) {
                dialog.dismiss();
                //loadAnnotations();
                // and lastly reopen the signed document
                if (filename != null) {
                    try {
                        int savePageNumber = mPageNumber;
                        ((SigmaActivity) mContext).closeDocument();
                        ((SigmaActivity) mContext).openDocument(filename, savePageNumber); // kills all views
                        //remove signature from highlighted widgets
                        // mWidgetsToHighlight.remove(mSignatureField);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //FIXME signature process messed up .... show something to user?
                    Log.wtf(TAG, "Signature process messed up - do something!");
                    Toast.makeText(mContext, mContext.getString(R.string.cannot_sign_field), Toast.LENGTH_SHORT).show();
                    //update screen
                    update();
                    mSignatureField = null;
                }
            }
        };

        mAddSignature.execute(getSignature());
        cancelSignature();
    }

    @Override
    public void cancelSignature() {
        super.cancelSignature();
    }


//    @Override
//    public boolean clearSignature() {
//        return cancelSignature();
//    }


    @Override
    protected CancellableTaskDefinition<Void, Void> getDrawPageTask(final Bitmap bm, final int sizeX, final int sizeY,
                                                                    final int patchX, final int patchY, final int patchWidth, final int patchHeight) {
        return new MuPDFCancellableTaskDefinition<Void, Void>(mCore) {
            @Override
            public Void doInBackground(MuPDFCore.Cookie cookie, Void... params) {
                // Workaround bug in Android Honeycomb 3.x, where the bitmap generation count
                // is not incremented when drawing.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB &&
                        Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                    bm.eraseColor(0);
                mCore.drawPage(bm, mPageNumber, sizeX, sizeY, patchX, patchY, patchWidth, patchHeight, cookie);
                return null;
            }
        };

    }

    protected CancellableTaskDefinition<Void, Void> getUpdatePageTask(final Bitmap bm, final int sizeX, final int sizeY,
                                                                      final int patchX, final int patchY, final int patchWidth, final int patchHeight) {
        return new MuPDFCancellableTaskDefinition<Void, Void>(mCore) {

            @Override
            public Void doInBackground(MuPDFCore.Cookie cookie, Void... params) {
                // Workaround bug in Android Honeycomb 3.x, where the bitmap generation count
                // is not incremented when drawing.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB &&
                        Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                    bm.eraseColor(0);
                mCore.updatePage(bm, mPageNumber, sizeX, sizeY, patchX, patchY, patchWidth, patchHeight, cookie);
                return null;
            }
        };
    }

    @Override
    protected LinkInfo[] getLinkInfo() {
        return mCore.getPageLinks(mPageNumber);
    }

    @Override
    protected TextWord[][] getText() {
        return mCore.textLines(mPageNumber);
    }

    @Override
    protected void addMarkup(PointF[] quadPoints, Annotation.Type type) {
        mCore.addMarkupAnnotation(mPageNumber, quadPoints, type);
    }

    private void loadAnnotations() {
        mAnnotations = null;
        if (mLoadAnnotations != null)
            mLoadAnnotations.cancel(true);
        mLoadAnnotations = new AsyncTask<Void, Void, Annotation[]>() {
            @Override
            protected Annotation[] doInBackground(Void... params) {
                return mCore.getAnnoations(mPageNumber);
            }

            @Override
            protected void onPostExecute(Annotation[] result) {
                mAnnotations = result;
            }
        };

        mLoadAnnotations.execute();
    }

    @Override
    public void setPage(final int page, PointF size) {
        loadAnnotations();

        mLoadWidgetAreas = new AsyncTask<Void, Void, RectF[]>() {
            @Override
            protected RectF[] doInBackground(Void... arg0) {
                return mCore.getWidgetAreas(page);
            }

            @Override
            protected void onPostExecute(RectF[] result) {
                mWidgetAreas = result;
            }
        };

        mLoadWidgetAreas.execute();
        convertWidgetHighlights(page);

        super.setPage(page, size);
    }

    private void convertWidgetHighlights(int page) {
        if (mWidgetsToHighlight == null || mWidgetsToHighlight.size() < 1) {
            return;
        }

        for (AcroField widget : mWidgetsToHighlight) {
            if ((page + 1) == widget.fieldPosition.page) {
                // convert only those on the current page

                PointF topLeft = PDF2ViewCoordinates(widget.fieldPosition.position.getLeft(), widget.fieldPosition.position.getTop());
                if (widget.viewBox == null) {
                    widget.viewBox = new RectF();
                }
                widget.viewBox.left = topLeft.x;
                widget.viewBox.top = topLeft.y;

                PointF bottomRight = PDF2ViewCoordinates(widget.fieldPosition.position.getRight(), widget.fieldPosition.position.getBottom());
                widget.viewBox.right = bottomRight.x;
                widget.viewBox.bottom = bottomRight.y;

            }
        }
    }

    public void setScale(float scale) {
        // This type of view scales automatically to fit the size
        // determined by the parent view groups during layout
    }

    @Override
    public void releaseResources() {
        if (mPassClick != null) {
            mPassClick.cancel(true);
            mPassClick = null;
        }

        if (mLoadWidgetAreas != null) {
            mLoadWidgetAreas.cancel(true);
            mLoadWidgetAreas = null;
        }

        if (mLoadAnnotations != null) {
            mLoadAnnotations.cancel(true);
            mLoadAnnotations = null;
        }

        if (mSetWidgetText != null) {
            mSetWidgetText.cancel(true);
            mSetWidgetText = null;
        }

        if (mSetWidgetChoice != null) {
            mSetWidgetChoice.cancel(true);
            mSetWidgetChoice = null;
        }

        if (mAddStrikeOut != null) {
            mAddStrikeOut.cancel(true);
            mAddStrikeOut = null;
        }

        if (mDeleteAnnotation != null) {
            mDeleteAnnotation.cancel(true);
            mDeleteAnnotation = null;
        }

        if (mAddSignature != null) {
            mAddSignature.cancel(true);
            mAddSignature = null;
        }


        super.releaseResources();
    }


}
