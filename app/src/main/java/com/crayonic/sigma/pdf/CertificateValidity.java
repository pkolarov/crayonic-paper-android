package com.crayonic.sigma.pdf;

/**
 * Created by kolarov on 22.5.2015.
 */
public enum CertificateValidity {
    VALID, REVOKED, EXPIRED, UNKNOWN
}
