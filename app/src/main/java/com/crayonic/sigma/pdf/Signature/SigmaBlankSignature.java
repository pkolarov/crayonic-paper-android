package com.crayonic.sigma.pdf.Signature;


import com.crayonic.sigma.BuildConfig;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfString;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.HashMap;


/**
 * Here we mostly make sure that you cannot just attach encrypted biometric data of signature to another doc and claim they are valid - we prove that we had access to biometic data
 * during the signing ceremony by knowing the hash of plain text of this data and then hashing it with time stamp and certificate for good measure to aslo tie them explicitly with this info
 * This final hash then becomes part of the doc before the range stream is calculated - thus it will create link between signature and doc
 *
 * Created by kolarov on 09/11/15.
 */
public class SigmaBlankSignature {
    // could be improved by hashing together hashOfClearTextSignatureData and sap.getRangeStream() but it would require multiple skipped ranges to work in iText (this is not critical though at all)
    public static void presign(PdfSignatureAppearance sap, byte[] hashOfClearTextSignatureData, int estimatedSize) throws GeneralSecurityException, IOException, DocumentException {
        PdfSignature dic = new PdfSignature(null, null);
        dic.put(PdfName.FILTER, PdfName.ADOBE_PPKLITE);
        dic.put(PdfName.SUBFILTER, PdfName.ADBE_PKCS7_DETACHED);
        dic.setReason(sap.getReason());
        dic.setLocation(sap.getLocation());
        dic.setSignatureCreator("Crayonic Sigma version:" + BuildConfig.VERSION_CODE);
        dic.setContact(sap.getContact());
        dic.setDate(new PdfDate(sap.getSignDate()));
        // combine hash of raw signature data and  crypto dictionary values since we cannot get the hash of the whole doc  content for now in order to link signature with the doc
        byte[] hashOfSigAndDoc = null;
        try {
            MessageDigest digest2 = MessageDigest.getInstance("SHA-256");
            digest2.update(hashOfClearTextSignatureData);
            digest2.update(sap.getSignDate().toString().getBytes());  // explicitly tie it with signature time and date
            hashOfSigAndDoc= digest2.digest(sap.getCertificate().getEncoded());  // tie it with user certificate
        } catch (Exception e) {
            e.printStackTrace();
        }
        dic.put(new PdfName("BioAndSigHash"), (new PdfString(hashOfSigAndDoc)).setHexWriting(true) ); // write our hash back to dic (dont forget to remove it before hashing when verifying)
        sap.setCryptoDictionary(dic);
        // exclude signature content byte range
        HashMap exc = new HashMap();
        exc.put(PdfName.CONTENTS, new Integer(estimatedSize * 2 + 2));
        //exc.put(new PdfName("BioAndSigHash"), new Integer(32*2+2)); TODO -- currently iText does not support multiple skipped ranges - throws new DocumentException("Single exclusion space supported");
        sap.preClose(exc);
        InputStream data = sap.getRangeStream();
        byte[] paddedSig = new byte[estimatedSize];
        PdfDictionary dic2 = new PdfDictionary();
        dic2.put(PdfName.CONTENTS, (new PdfString(paddedSig)).setHexWriting(true));
        //dic2.put(new PdfName("BioAndSigHash"), (new PdfString(hashOfSigAndDoc)).setHexWriting(true) );  -- TODO see exclusion above (this hash should go into PKCS7 object somehow)
        sap.close(dic2);
    }
}
