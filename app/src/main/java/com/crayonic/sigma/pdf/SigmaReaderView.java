package com.crayonic.sigma.pdf;

import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;

import com.crayonic.sigma.crypto.craytoken.BasicComm;
import com.itextpdf.text.Rectangle;

import java.util.ArrayList;

public class SigmaReaderView extends ReaderView {
    private ArrayList<AcroField> mWidgets;


    public enum Mode {Viewing, Selecting, Drawing, Signing}

    private final Context mContext;
    private boolean mLinksEnabled = false;
    private boolean mHighlightWidgets = true;
    private Mode mMode = Mode.Viewing;
    private boolean tapDisabled = false;
    private int tapPageMargin;

    protected void onTapMainDocArea() {
    }

    protected void onDocMotion() {
    }

    protected void onHit(Hit item) {
    }

    ;

    public void setLinksEnabled(boolean b) {
        mLinksEnabled = b;
        resetupChildren();
    }

    public void setMode(Mode m) {
        mMode = m;
    }

    public Mode getMode() {
        return mMode;
    }

    private void setup() {
        // Get the screen size etc to customise tap margins.
        // We calculate the size of 1 inch of the screen for tapping.
        // On some devices the dpi values returned are wrong, so we
        // sanity check it: we first restrict it so that we are never
        // less than 100 pixels (the smallest Android device screen
        // dimension I've seen is 480 pixels or so). Then we check
        // to ensure we are never more than 1/5 of the screen width.
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(dm);
        tapPageMargin = (int) dm.xdpi;
        if (tapPageMargin < 100)
            tapPageMargin = 100;
        if (tapPageMargin > dm.widthPixels / 5)
            tapPageMargin = dm.widthPixels / 5;
    }


    public SigmaReaderView(Context context) {
        super(context);
        mContext = context;
        setup();
    }

    public SigmaReaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setup();
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        LinkInfo link = null;

        if (mMode == Mode.Viewing && !tapDisabled) {
            MuPDFView pageView = (MuPDFView) getDisplayedView();
            Hit item = pageView.passClickEvent(e.getX(), e.getY());
            onHit(item);
            if (item == Hit.Nothing) {
                if (mLinksEnabled && pageView != null
                        && (link = pageView.hitLink(e.getX(), e.getY())) != null) {
                    link.acceptVisitor(new LinkInfoVisitor() {
                        @Override
                        public void visitInternal(LinkInfoInternal li) {
                            // Clicked on an internal (GoTo) link
                            setDisplayedViewIndex(li.pageNumber);
                        }

                        @Override
                        public void visitExternal(LinkInfoExternal li) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                                    .parse(li.url));
                            mContext.startActivity(intent);
                        }

                        @Override
                        public void visitRemote(LinkInfoRemote li) {
                            // Clicked on a remote (GoToR) link
                        }
                    });
                } else if (e.getY() < tapPageMargin) {
                    super.smartMoveBackwards();
                } else if (e.getY() > super.getHeight() - tapPageMargin) {
                    super.smartMoveForwards();
                } else {
                    onTapMainDocArea();
                }
            }
        }
        return super.onSingleTapUp(e);
    }

    @Override
    public boolean onDown(MotionEvent e) {

        return super.onDown(e);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        MuPDFView pageView = (MuPDFView) getDisplayedView();
        switch (mMode) {
            case Viewing:
                if (!tapDisabled)
                    onDocMotion();

                return super.onScroll(e1, e2, distanceX, distanceY);
            case Selecting:
                if (pageView != null)
                    pageView.selectText(e1.getX(), e1.getY(), e2.getX(), e2.getY());
                return true;
            default:
                return true;
        }
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                           float velocityY) {
        switch (mMode) {
            case Viewing:
                return super.onFling(e1, e2, velocityX, velocityY);
            default:
                return true;
        }
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector d) {
        // Disabled showing the buttons until next touch.
        // Not sure why this is needed, but without it
        // pinch zoom can make the buttons appear
        if (mMode == Mode.Signing)
            return true; // no zooming during signing

        tapDisabled = true;
        return super.onScaleBegin(d);
    }


    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        if (mMode == Mode.Signing)
            return true; // no zooming during signing

        return super.onScale(detector);
    }


    private int mPenPointerId = -1; // stored id of pen pointer (it is the one who touches signature box first)

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (mMode == Mode.Drawing) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    break;
            }
        }

        if (mMode == Mode.Signing) {
            if (getDisplayedView() == null || !(getDisplayedView() instanceof SigmaPageView))
                return false; // sanity check did not pass so leave without processing event


            long t = event.getEventTime();
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    int pointerIndex = event.getActionIndex();

                    float x = event.getX(pointerIndex);
                    float y = event.getY(pointerIndex);
                    float z = event.getPressure(pointerIndex);
                    float accel = 0;

                    BasicComm penAPI = ((SigmaActivity) mContext).mPenComm;

                    if (penAPI != null) {
                        // once we have API we can see if anything is in the pen Pressure (z = 0 for any problem)
                        penAPI.pointerDownNow(t); // tell BT when we touched to calibrate latency
                        float cz = penAPI.getPenPressure(t);
                        if (cz >= 0) {
                            z = cz;
                        }
                        accel = penAPI.getPenAccel(t);
                    }

                    //convert touch x,y to PDF coordinates
                    PointF touch = ((SigmaPageView) getDisplayedView()).screen2PDFCoordinates(x, y);

                    //check if touch came from within signature bounds first -- this provides some type of palm rejection
                    // get the pointer id of first pointer within bounds
                    if ( isScreenXYInTheSignatureBox(touch.x, touch.y, ((SigmaActivity) mContext).mSigningSignatureField.fieldPosition.position)) {
                        // Get the pointer ID of first touch in the box and save it
                        mPenPointerId = event.getPointerId(pointerIndex);
                        sig_start(x, y, z, t);
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    pointerIndex = event.findPointerIndex(mPenPointerId); // find our pointer index in this motion event
                    if (pointerIndex < 0)
                        break;

                    penAPI = ((SigmaActivity) mContext).mPenComm;

                    // first read historical data
                    final int historySize = event.getHistorySize();
                    for (int h = 0; h < historySize; h++) {
                        long ht = event.getHistoricalEventTime(h);
                        float hx = event.getHistoricalX(pointerIndex, h);
                        float hy = event.getHistoricalY(pointerIndex, h);
                        float hz = event.getHistoricalPressure(pointerIndex, h);
                        float haccel = 0;
                        if (penAPI != null) {
                            // once we have API we can see if anything is in the pen Pressure (z < 0 for any problem)
                            penAPI.pointerDownNow(t); // tell BT when we touched to calibrate latency
                            float hp = penAPI.getPenPressure(ht);
                            if (hp >= 0)
                                hz = hp;
                            haccel = penAPI.getPenAccel(ht);
                        }

                        // stop drawing for 0 pressure!
//                        if(hz == 0){
//                            sig_up(); // off the bounds stop recording signature
//                            //mPenPointerId = -1;
//                            return true;
//                        }

                        touch = ((SigmaPageView) getDisplayedView()).screen2PDFCoordinates(hx, hy);
                        if (isScreenXYInTheSignatureBox(touch.x, touch.y, ((SigmaActivity) mContext).mSigningSignatureField.fieldPosition.position)) {
                            sig_move(hx, hy, hz, ht);

                        } else {
                            sig_up(); // off the bounds stop recording signature
                            mPenPointerId = -1;
                            // throw error dialog - signature stays in box
                            ((SigmaActivity) mContext).signatureOutOfBox();
                            return true;
                        }
                    }


                    x = event.getX(pointerIndex);
                    y = event.getY(pointerIndex);
                    z = event.getPressure(pointerIndex);

                    accel = 0;

                    if (penAPI != null) {
                        // once we have API we can see if anything is in the pen Pressure (z = 0 for any problem)
                        penAPI.pointerDownNow(t); // tell BT when we touched to calibrate latency
                        float p = penAPI.getPenPressure(t);
                        if (p >= 0)
                            z = p;
                        accel = penAPI.getPenAccel(t);
                    }

                    // stop drawing for 0 pressure!
//                    if(z == 0){
//                        sig_up(); // off the bounds stop recording signature
//                        //mPenPointerId = -1;
//                        return true;
//                    }

                    touch = ((SigmaPageView) getDisplayedView()).screen2PDFCoordinates(x, y);
                    //    if (isScreenXYInTheSignatureBox(touch.x, touch.y, ((SigmaActivity) mContext).mSigningSignatureField.fieldPosition.position)) {
                    sig_move(x, y, z, t);
                    //    }
//                    else {
//                        // sig_up(); // off the bounds stop recording signature
//                        mPenPointerId = -1;
//                    }

                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    pointerIndex = event.getActionIndex();
                    if (mPenPointerId != event.getPointerId(pointerIndex))  // ignore pointers not started above
                        break;

                    sig_up();
                    mPenPointerId = -1;
                    break;
            }
            return true; // dont do any other touch event handling when signing!
        }
        if ((event.getAction() & event.getActionMasked()) == MotionEvent.ACTION_DOWN) {
            tapDisabled = false;
        }

        return super.onTouchEvent(event);
    }

    private float mX, mY;


    private static final float TOUCH_TOLERANCE = 2;
    private static final float SIG_TOUCH_TOLERANCE = 1;

    private void touch_start(float x, float y) {

        MuPDFView pageView = (MuPDFView) getDisplayedView();
        if (pageView != null) {
            pageView.startDraw(x, y);
        }
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {

        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            MuPDFView pageView = (MuPDFView) getDisplayedView();
            if (pageView != null) {
                pageView.continueDraw(x, y);
            }
            mX = x;
            mY = y;
        }
    }

    private void touch_up() {

        // NOOP
    }

    private void sig_start(float x, float y, float z, long t) {
        // update possibly previous line now
        updateLinePointsWithPenPressure();

        MuPDFView pageView = (MuPDFView) getDisplayedView();
        if (pageView != null) {
            pageView.startSignatureLine(x, y, z, t);
        }
        mX = x;
        mY = y;
    }

    private void sig_move(float x, float y, float z, long t) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= SIG_TOUCH_TOLERANCE || dy >= SIG_TOUCH_TOLERANCE) {
            MuPDFView pageView = (MuPDFView) getDisplayedView();
            if (pageView != null) {
                pageView.continueSignatureLine(x, y, z, t);
            }
            mX = x;
            mY = y;
        }
    }


    private void sig_up() {
        // good time to update signature line with BT data if any
        updateLinePointsWithPenPressure();

        // NOOP
    }

    // updates current line being drawn with pressure from pen if available
    private void updateLinePointsWithPenPressure() {
        BasicComm penAPI = ((SigmaActivity) mContext).mPenComm;
        if (penAPI != null) {
            MuPDFView pageView = (MuPDFView) getDisplayedView();
            if (pageView != null) {
                ArrayList<SignaturePoint> line = pageView.getCurrentSignatureLine();
                if (line != null) {
                    for (SignaturePoint point : line) {
                        float pressure = penAPI.getPenPressure(point.getTime());
                        if (pressure >= 0) {
                            point.z = pressure;
                        }
                    }
                }
            }
        }
    }

    public void clearSignature(){
        MuPDFView pageView = (MuPDFView) getDisplayedView();
        pageView.cancelSignature();
    }

    private boolean isScreenXYInTheSignatureBox(float x, float y, Rectangle sigbox) {
        return (x > sigbox.getLeft() &&
                x < sigbox.getRight() &&
                y > sigbox.getBottom() &&
                y < sigbox.getTop());
    }

    @Override
    protected void onChildSetup(int i, View v) {
        if (SearchTaskResult.get() != null
                && SearchTaskResult.get().pageNumber == i)
            ((MuPDFView) v).setSearchBoxes(SearchTaskResult.get().searchBoxes);
        else
            ((MuPDFView) v).setSearchBoxes(null);

        ((MuPDFView) v).setLinkHighlighting(mLinksEnabled);

        if (mHighlightWidgets) {
            ((MuPDFView) v).setWidgetHighlights(mWidgets);
        }

        ((MuPDFView) v).setChangeReporter(new Runnable() {
            public void run() {
                applyToChildren(new ReaderView.ViewMapper() {
                    @Override
                    void applyToView(View view) {
                        ((MuPDFView) view).update();
                    }
                });
            }
        });
    }

    @Override
    protected void onMoveToChild(int i) {
        if (SearchTaskResult.get() != null
                && SearchTaskResult.get().pageNumber != i) {
            SearchTaskResult.set(null);
            resetupChildren();
        }
    }

    @Override
    protected void onMoveOffChild(int i) {
        View v = getView(i);
        if (v != null)
            ((MuPDFView) v).deselectAnnotation();
    }

    @Override
    protected void onSettle(View v) {
        // When the layout has settled ask the page to render
        // in HQ
        ((MuPDFView) v).updateHq(false);

    }

    @Override
    protected void onUnsettle(View v) {
        // When something changes making the previous settled view
        // no longer appropriate, tell the page to remove HQ
        ((MuPDFView) v).removeHq();
    }

    @Override
    protected void onNotInUse(View v) {
        ((MuPDFView) v).releaseResources();
    }

    @Override
    protected void onScaleChild(View v, Float scale) {
        ((MuPDFView) v).setScale(scale);
    }

    public void setWidgets(ArrayList<AcroField> mWidgets) {
        this.mWidgets = mWidgets;
    }

    public void addWidget(AcroField widget) {
        if (mWidgets == null) {
            mWidgets = new ArrayList<AcroField>(10);
        }
        this.mWidgets.add(widget);
    }


//	@Override
//	protected void onLayout(boolean changed, int left, int top, int right,
//							int bottom) {
//		super.onLayout(changed, left, top, right, bottom);
//
//
//	}
}
