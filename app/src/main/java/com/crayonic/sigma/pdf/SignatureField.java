package com.crayonic.sigma.pdf;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import com.itextpdf.text.pdf.security.VerificationException;

import java.util.List;

/**
 * Created by kolarov on 21.5.2015.
 */
public class SignatureField extends AcroField{

    public PdfPKCS7 signature; // if the field is signed this is the signature
    public String fileName; // each signature may have associated file which it signed as a last signature
    public boolean fieldExists; // if true than the signature field exists in the PDF otherwise the acrofield has to get created
    public boolean hasIntegrity;  // true if signature was not broken by changes to PDF
    public boolean hasIdentity;  // true if signature certificate has been issued to person by trusted CA
    public boolean hasTimeStamp;  // true if timestamp is from TSA trusted authority
    public CertificateStatus certStatus; // status of signing cert for this signature
    public String revocations;  // verification of ocsp responses and crls possibly embedded in signature
    public List<VerificationException> errors; // verification errors
    public boolean isRequired; // true if signature is required before saving doc
    public boolean allValidatorsFinished; // true if all of the asynchronous validators have returned back with results


    public SignatureField(String fieldName, AcroFields.FieldPosition fieldPosition, PdfPKCS7 signature) {
        this.fieldName = fieldName;
        this.fieldPosition = fieldPosition;
        this.signature = signature;  // null if signature field is blank
        this.fieldExists = true; // by default field exist in PDF
        this.isRequired = false; // default sig is not required
        this.allValidatorsFinished = false;
    }
}
