package com.crayonic.sigma.pdf;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by kolarov on 17.6.2015.
 */
public class Utils {

    // fast file copy (returns dest path of copied file)
    public static String ffcopy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
        return dst.getAbsolutePath();
    }

    public static String getContentName(ContentResolver resolver, Uri uri){
        Cursor cursor = resolver.query(uri, null, null, null, null);
        cursor.moveToFirst();
        int nameIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME);
        if (nameIndex >= 0) {
            return cursor.getString(nameIndex);
        } else {
            return null;
        }
    }

    public static void dumpIntent(Intent i){
        if(i== null) return;
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            Log.e("UTILS",".............Dumping Intent start..............");
            if(i.getComponent()!= null) {
                Log.e("UTILS", "In the object: " + i.getComponent().toString());
            }
            if(i.getAction() != null) {
                Log.e("UTILS", "Intent Action: " + i.getAction());
            }
            if(i.getDataString()!= null) {
                Log.e("UTILS", "Intent Data: " + i.getDataString());
            }
            while (it.hasNext()) {
                String key = it.next();
                Log.e("UTILS","[ " + key + " = " + bundle.get(key)+" ]");
            }
            Log.e("UTILS",".............Dumping Intent end................");
        }
    }


    public static String getCallerCallerClassName() {
        StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
        String callerClassName = null;
        for (int i=1; i<stElements.length; i++) {
            StackTraceElement ste = stElements[i];
            if (!ste.getClassName().equals(Utils.class.getName())&& ste.getClassName().indexOf("java.lang.Thread")!=0) {
                if (callerClassName==null) {
                    callerClassName = ste.getClassName();
                } else if (!callerClassName.equals(ste.getClassName() )) {
                    return ste.getClassName();
                }
            }
        }
        return null;
    }
}
