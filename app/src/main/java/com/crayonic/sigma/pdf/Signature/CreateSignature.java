package com.crayonic.sigma.pdf.Signature;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import com.crayonic.sigma.SSL.TSAClientMutualSSL;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.crypto.ExternalSignatureProvider;
import com.crayonic.sigma.monitoring.URLMonitor;
import com.crayonic.sigma.pdf.SignatureField;
import com.crayonic.sigma.tools.SSLLoader;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.security.ExternalSignatureContainer;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * Created by kolarov on 5.6.2015.
 */
public class CreateSignature {
    private static final String TAG = "CreateSignature";
    private static final int TSA_HOST_TIMEOUT_MAX = 3000;  // if TSA host cannot bind socket under 3 secs. then skip TSA timestamp


//    static {  Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);   }

    // sign acrofield sigField in a given PDF file with  bitmap being the signature graphics
    // returned new filename of signed file (tmp file)
    public static String signDocument( ExternalSignatureProvider es,  SignatureField sigField,
                                      final Bitmap bmp, final String pdfFileName,
                                      final  String encryptedSignatureDataBase64Enc,
                                      byte[] hashOfClearTextSignatureData, String location, String reason, String signedBy, String dateFormat) {
        final String outFileName = pdfFileName + "_"+ new Date().getTime();
        try {
            PdfReader.unethicalreading = true;

            // reader and stamper
            PdfReader reader = new PdfReader(new FileInputStream(new File(pdfFileName)));

            PdfStamper stp = PdfStamper.createSignature(reader, new FileOutputStream(new File(outFileName)), '\0', null, true);
            PdfSignatureAppearance sap = stp.getSignatureAppearance();
            sap.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
            sap.setSignatureEvent(
                    new PdfSignatureAppearance.SignatureEvent() {
                        public void getSignatureDictionary(PdfDictionary sig) {
                            sig.put(new PdfName("SigBio"), new PdfString(encryptedSignatureDataBase64Enc));
                        }
                    }
            );
            sap.setReason(reason);

            sap.setLocation(location);

            sap.setCertificate(es.getCertificateChain()[0]);
           // add some text info into signature image
            SimpleDateFormat df =new SimpleDateFormat(dateFormat);
            boolean hasSignerInfo = Boolean.valueOf(Sigma.applicationProperties.getProperty("show_signer_info", "true"));
            Bitmap nbmp = bmp;
            if(hasSignerInfo) {
                nbmp = addSignerInfo(bmp, signedBy + " " + df.format(new Date()));
            }
            //convert bitmap to PNG and pass to iText Image class
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            nbmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            sap.setSignatureGraphic(Image.getInstance(stream.toByteArray()));
            sap.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
            // set name of acrofield of type signature
            if(sigField.fieldExists) {
                sap.setVisibleSignature(sigField.fieldName);
            }else{
                sap.setVisibleSignature(new Rectangle(sigField.fieldPosition.position.getLeft(),sigField.fieldPosition.position.getBottom()
                        , sigField.fieldPosition.position.getRight(), sigField.fieldPosition.position.getTop()), sigField.fieldPosition.page+1,sigField.fieldName );
            }

            SigmaBlankSignature.presign(sap, hashOfClearTextSignatureData, 16384);
            stp.close();
            reader.close();
            // ----doc has been pre signed at this point ---------------

            // reopen presigned PDF to insert actual signature
            reader = new PdfReader(new FileInputStream(new File(outFileName)));
            String outFileNameSigned = outFileName + "_S";
            FileOutputStream os = new FileOutputStream(outFileNameSigned);


            OcspClient ocspClient = new OcspClientBouncyCastle();
            String TSAURL = Sigma.applicationProperties.getProperty("tsa_url");

            // check if host if accessible first to avoid lengthy TCP timeouts
            URI uri = new URI(TSAURL);
            if (!URLMonitor.pingHost(uri.getHost(), uri.getPort(),uri.getScheme(), TSA_HOST_TIMEOUT_MAX)) {
                Log.w(TAG, "Timestamp host " +  uri.getHost() + " is not available!");// host not available
                TSAURL = null;  // dont do TSA
            }

            TSAClient tsaClient = null;
            if(TSAURL != null && !TSAURL.isEmpty()) {
                String user = Sigma.applicationProperties.getProperty("tsa_user");
                String pass = Sigma.applicationProperties.getProperty("tsa_password");
                String p12file = Sigma.applicationProperties.getProperty("tsa_p12_file");
                String p12pass = Sigma.applicationProperties.getProperty("tsa_p12_password");

                if(p12file==null){
                    Log.i(TAG, "Getting timestamp with basic auth from: " + TSAURL);
                    tsaClient = new TSAClientBouncyCastle(TSAURL,user, pass);
                }else{
                    if(p12pass!=null && p12file.length() > 0){
                        Log.i(TAG, "Getting timestamp with mutual SSL from: " + TSAURL);
                        FileInputStream fis = new FileInputStream(Sigma.getCertPath() + Sigma.applicationProperties.getProperty("tsa_p12_file"));
                        KeyStore ks = KeyStore.getInstance("pkcs12");
                        ks.load(fis, p12pass.toCharArray()); // load PFX file into temp PKCS12 keystore

                        KeyStore ts = Sigma.applicationTrustStore; // use our global app trust/key store to add additional trusted cert
                        SSLLoader.loadKSWithTrustedPemCerts(ts,Sigma.getCertPath() + Sigma.applicationProperties.getProperty("tsa_root_cert_file") );
                        tsaClient = new TSAClientMutualSSL(TSAURL, ks, ts, p12pass);
                    }

                }

            }
            ExternalSignatureContainer external = new SigmaExternalSignatureContainer(es,ocspClient,tsaClient);
            MakeSignature.signDeferred(reader, sigField.fieldName , os, external);

            //reopen signed doc for reading to verify signature and obtain PKCS7 reference
            reader = new PdfReader(new FileInputStream(new File(outFileNameSigned)));
            sigField.signature =   reader.getAcroFields().verifySignature(sigField.fieldName);
            reader.close();

            sigField.fileName = outFileNameSigned;
            if(!sigField.fieldExists){
                sigField.fieldExists = true;//  now its in PDF
            }
            return outFileNameSigned;
        }catch (Exception ex){
            Log.e("CreateSignature","Exception when trying to sign PDF: " + ex);
        }
        return null;
    }


    // TODO make this info render better - needs better relative sizing and centering
    private static Bitmap addSignerInfo(Bitmap bmp, String info) {
        Bitmap nbmp = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), Bitmap.Config.ARGB_8888);
        nbmp.eraseColor(0);  // create transparent bitmap
        Canvas c = new Canvas(nbmp); // and setup its drawable
        Paint paint = new Paint();
//        //paint.setColor(Color.WHITE);
//        paint.setStyle(Paint.Style.FILL);
//        c.drawPaint(paint);
        paint.setColor(Color.BLUE);
        paint.setTextSize(80);

        Rect bounds = new Rect();
        paint.getTextBounds(info, 0, info.length(), bounds);
        float scale = ((float)bounds.width()) / ((float) bmp.getWidth());
        int fsize = (int)(80/scale);
        fsize = fsize > 80 ? 80 : fsize; // cap to 80pt
        paint.setTextSize(fsize);
        bounds = new Rect();
        paint.getTextBounds(info, 0, info.length(), bounds);

        int midX = bmp.getWidth() / 2 - bounds.width() / 2;

        c.drawText(info,midX, bmp.getHeight() - bounds.height(), paint);
        c.drawBitmap(bmp,0,0,null);

        return nbmp;
    }


    public static String  flattenPDFFieldsExcludingSignatures(String pdfFileName) throws IOException, DocumentException {
        PdfStamper stp = null;
        final String outFileName = pdfFileName + "_"+ new Date().getTime();
        PdfReader reader = new PdfReader(new FileInputStream(new File(pdfFileName)));

        AcroFields form = reader.getAcroFields();
        // Loop over the fields and get info about them
        Set<String> fields = form.getFields().keySet();
        boolean somethingToFlatten = false;
        for (String key : fields) {
            if (form.getFieldType(key) != AcroFields.FIELD_TYPE_SIGNATURE) {
                if(!somethingToFlatten){
                    //open up stamper if we are going to flatten things
                    FileOutputStream fout = new FileOutputStream(new File(outFileName));
                    stp = new PdfStamper(reader, fout);
                }
                stp.partialFormFlattening(key); // add every non signature field name to be flattened
                somethingToFlatten = true;
            }
        }

        if(somethingToFlatten) {
            // only if something  is in the partial list call the form flattening - otherwise the sigs get flattened too!
            stp.setFormFlattening(true);
            stp.setAnnotationFlattening(true);
            stp.close();
            reader.close();
            return outFileName;
        }else {
            // nothing flattent nothing saved....
            reader.close();
            return null;
        }
    }

}
