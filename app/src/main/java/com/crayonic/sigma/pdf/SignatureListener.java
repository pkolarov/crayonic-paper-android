package com.crayonic.sigma.pdf;

/**
 * Created by kolarov on 5.6.2015.
 */
public interface SignatureListener {

   public void onSignatureClicked(SignatureField signatureF);
}
