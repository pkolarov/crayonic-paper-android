package com.crayonic.sigma.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.Header;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.scanlibrary.ScanConstants;


import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;


// This service creates new PDF file upon a first bounded client
// SigmaActivity should create PDF file and finalize it while CameraActivity should send photos to it
public class Scans2PDFService extends Service {
    private static final String TAG = "Scans2PDFService";

    // Other non APK clients should sync the constants below-------
    public static final String PDF_FILE_PATH = "pdf";
    public static final String PDF_SCAN_COUNT = "count";
    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final int MSG_ADD_SCAN = 3;
    public static final int MSG_FINISH_SCANNING = 4;
    public static final String BUNDLE_SCAN = "scan";
    // -------End of synced consts ------------------

    ArrayList<Messenger> mClients = new ArrayList<Messenger>(); // Keeps track of all current registered clients/scanners
    final Messenger mMessenger = new Messenger(new IncomingHandler()); // Target we publish for clients to send messages to IncomingHandler.


    private Document document;
    private String docPath;
    private int scanCounter;


    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class IncomingHandler extends Handler { // Handler of incoming messages from clients.
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:   // recieve and resend client binding  (may be we should only allow one client max?)
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:  // recieve and resend client unbinding info
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_ADD_SCAN:          // recieve add scan page by a client/scanner
                    String scanPath = msg.getData().getString(BUNDLE_SCAN);
                    addScanToPDF(scanPath);
                    break;
                case MSG_FINISH_SCANNING:  // recieve finish scan request by a client
                    Boolean docFinalized = finalizePDF();
                    sendFinishMessageToAllClients();
                    stopSelf(); // stop this service upon finish of scanning
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    }

    private void sendFinishMessageToAllClients() {
        for (int i = mClients.size() - 1; i >= 0; i--) {
            try {
                Bundle b = new Bundle();
                b.putString(PDF_FILE_PATH, docPath);
                b.putInt(PDF_SCAN_COUNT, scanCounter);
                Message msg = Message.obtain(null, MSG_FINISH_SCANNING);
                msg.setData(b);
                mClients.get(i).send(msg);

            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
    }

    /*
    Really only ONE client can effectively start scanning PDF doc - others will be ignored
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if ( document == null) {
            Log.i(TAG, "Starting scanning service! Received start id " + startId + ": " + intent);
            // We want this service to continue running until it is explicitly
            // stopped, so return sticky only if intent has valid inputs
            if (intent != null && intent.getData() != null) {
                File f = new File(intent.getData().getEncodedPath());
                if (f.isFile() && f.exists()) {
                    f.delete(); // remove possible old scan
                }
                String format = intent.hasExtra(ScanConstants.DOC_FORMAT) ? intent.getExtras().getString(ScanConstants.DOC_FORMAT) : null;
                createNewPDF(f, format);  // start new PDF scan

            } else {
                Log.e(TAG, "OS is restarting scanning service with null intent?!");
                return 0;
            }
        } else {
            Log.w(TAG, "Scanning service already scanning - cannot start again! Received start id " + startId + ": " + intent);
        }
        return START_REDELIVER_INTENT;
    }


    //starts a new empty PDF file ready to accept scans
    private void createNewPDF(File file, String format) {
        if (document == null) {
            try {
                docPath = file.getAbsolutePath();
                Field field = null;
                if (format != null) {
                    try {
                        field = PageSize.class.getField(format);
                    }catch (NoSuchFieldException e){
                        Log.w(TAG, "iText PageSize does not recognize format: " + format);
                    }
                }
                // set page format by reflection if possible from the format string e.g. "A4" = PageSize.A4
                if (field != null) {
                    document = new Document((Rectangle) field.get(null));
                } else {
                    document = new Document(PageSize.A4);  // default format is A4
                }
                PdfWriter.getInstance(document, new FileOutputStream(file));

                String filename = file.getName().substring(0, file.getName().length() - 4);
                document.addTitle(filename);

                try {
                    // try to separate session id into its own custom metadata
                    String sessionID = filename.substring(filename.lastIndexOf("-")+1);
                    if (sessionID != null && sessionID.length() == 10)
                        document.add(new Header("idApplication", sessionID));
                } catch (Exception e) {
                }

                document.setMargins(0, 0, 0, 0);

                document.open();
                scanCounter = 0;
            } catch (Exception e) {
                Log.e("Scans2PDFService", "Cannot create new PDF for scans. " + e);
                Toast.makeText(this, "Cannot create new PDF doc:" + docPath, Toast.LENGTH_LONG).show();
            }
        } else {
            Log.e("Scans2PDFService", "Cannot create new document for scanning before finalizing previous doc.");
        }
    }



    // adds another image to a new PDF page
    private void addScanToPDF(String scanPath) {
        if (document != null) {
            try {
                if (scanPath != null) {
                    File scanF = new File(scanPath);
                    if (scanF.exists()) {

//                        BitmapFactory.Options options = new BitmapFactory.Options();
//                        options.inSampleSize = 1;
//                        options.inPreferQualityOverSpeed = true;
//                        Bitmap scan = BitmapFactory.decodeFile(scanPath,options );

                        Image image = Image.getInstance(scanPath);
                        if (image.getHeight() < image.getWidth() && document.getPageSize().getHeight() > document.getPageSize().getWidth()) {
                            image.setRotationDegrees(90);
                            image.rotate();
                        }
                        float scaleH = ((document.getPageSize().getHeight() - document.topMargin() - document.bottomMargin()) / image.getHeight()) * 100;
                        float scaleW = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin()) / image.getWidth()) * 100;
                        float scale = scaleH < scaleW ? scaleH : scaleW;
                        image.scalePercent(scale);
                        document.add(image);
                        document.newPage(); //may be not needed
                        scanCounter++;
                        boolean deleted = scanF.delete(); // remove scan once it was accepted
                        if (deleted) {
                            Log.d(TAG, "Deleted scan:" + scanPath);
                        } else {
                            Log.d(TAG, "Cannot delete scan:" + scanPath);
                        }
                    } else {
                        Log.e(TAG, "Cannot add scan page since file: " + scanPath + " does not exist.");
                    }
                }
            } catch (Exception e) {
                Log.e("Scans2PDFService", "" + e);
                Toast.makeText(this, "Cannot add image to PDF doc:" + docPath, Toast.LENGTH_LONG).show();
            }
        } else {
            Log.e("Scans2PDFService", "No PDF created to attach scans to!");
        }

    }


    // finalize PDF if no more scans should be added
    private boolean finalizePDF() {
        if (document != null && scanCounter > 0) {
            document.close();
            document = null;
            Log.e("Scans2PDFService", "Closing PDF doc: " + docPath);
            Toast.makeText(this, "Document created", Toast.LENGTH_LONG).show();
            return true;
        } else {
            Log.e("Scans2PDFService", "No PDF created to finalize!");
            return false;
        }
    }

    private static boolean isRunning = false;

    public static boolean isRunning() {
        return isRunning;
    }

}
