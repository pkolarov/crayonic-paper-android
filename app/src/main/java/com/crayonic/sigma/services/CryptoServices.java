package com.crayonic.sigma.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.crayonic.sigma.crypto.CryptoException;
import com.crayonic.sigma.crypto.CryptoProvider;
import com.crayonic.sigma.pdf.Utils;

import org.spongycastle.pkcs.PKCS10CertificationRequest;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.security.cert.Certificate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
These service deals with loading external certificates into  keystore for further crypto operations
 */
public class CryptoServices extends Service {
    private static final String TAG = "CryptoServices";

    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;

    public static final int MSG_LIST_EXT_CERTS = 10;   // list all available external (e.g. on SD card or SE) certificates (identities)
    public static final int MSG_LOAD_CERT = 11;  // Load certificate (identity)from external source into keystore wallet for ops below
    public static final int MSG_SIGN = 12;       // sign hash using cert in keystore
    public static final int MSG_ENCRYPT = 13;    // encrypt data using public key of the cert in keystore
    public static final int MSG_DECRYPT = 14;    // decrypt data using private key of the cert in keystore
    public static final int MSG_CREATE_CSR = 15;   // create CSR for the keypair generated in the keystore
    public static final int MSG_GET_CERT_CHAIN = 16; //return the certificate chain of the cert in the keystore
    public static final int MSG_REMOVE_CERT = 17;   // remove the loaded cert from the keystore
    public static final int MSG_GET_ALGORITHM = 18; // return algorithm for the cert
    public static final int MSG_SELECT_CRYPTO_PROVIDER = 19;  // selects first available crypto and certificate provider
    public static final int MSG_SIGN_CSR = 20; // signs created CSR
    public static final int MSG_WRITE_CERT = 21; // write cert

    public static final int MSG_CRYPTO_RESULT = 100; // return result for the crypto operation


    public static final String CS_CERT_URI = "CS_CERT_URI";
    public static final String CS_CERT_PASSWORD = "CS_CERT_PASSWORD";
    public static final String CS_CERT_ALIAS = "CS_CERT_ALIAS";
    public static final String CS_HASH = "CS_HASH";
    public static final String CS_MESSAGE = "CS_MESSAGE";
    public static final String CS_ERROR = "CS_ERROR";
    public static final String CS_RET_CERTS = "CS_RET_CERTS";
    public static final String CS_RET_MSG = "CS_RET_MSG";
    public static final String CS_CERT_PRINCIPAL = "CS_CERT_PRINCIPAL";
    public static final String CS_RET_CSR = "CS_RET_CSR";
    public static final String CS_CRYPTO_PROVIDER_PRIMARY = "CS_CRYPTO_PROVIDER_PRIMARY";
    public static final String CS_CRYPTO_PROVIDER_SECONDARY = "CS_CRYPTO_PROVIDER_SECONDARY";
    public static final String CS_CRYPTO_PROVIDER_TERTIARY = "CS_CRYPTO_PROVIDER_TERTIARY";
    public static final String CS_RET_CERT = "CS_RET_CERT";
    public static final String CS_RET_ALGO = "CS_RET_ALGO";
    public static final String CS_CSR = "CS_CSR";
    public static final String CS_CERT_SIGNING_ALIAS = "CS_CERT_SIGNING_ALIAS";
    public static final String CS_SIGN_ALGO = "CS_SIGN_ALGO";
    public static final String CS_CERT = "CS_CERT";

    private static boolean isRunning = false;


    public ArrayList<Messenger> mClients = new ArrayList<>(); // Keeps track of all current registered clients/scanners
    final Messenger mMessenger = new Messenger(new IncomingHandler(this)); // Target we publish for clients to send messages to IncomingHandler.
    private CryptoProvider mCryptoProvider;

    static private String mPrimaryProvider, mSecondaryProvider, mTertiaryProvider; // these are crypto providers that should be tested in this order
    private Thread workerThread;

    public CryptoServices() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (mCryptoProvider == null) {
            try {
                mCryptoProvider = selectCryptoProvider(this, mPrimaryProvider, mSecondaryProvider, mTertiaryProvider);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
            if (mCryptoProvider == null) {
                // no provider picked so stop service
                // stop crypto service
                Log.wtf(TAG, "Cannot select crypto provider - WHY!!??");
                stopSelf();
                return null;

            }
        }

        return mMessenger.getBinder();
    }

    @Override
    public void onDestroy() {
        // reset crypto provider and all options
        mCryptoProvider = null;
        mPrimaryProvider = mSecondaryProvider = mTertiaryProvider = null;
        isRunning = false;
        super.onDestroy();
    }


    static class IncomingHandler extends Handler {
        private WeakReference<CryptoServices> mService;
        private Messenger messenger;
        private int what;
        //private Message msg;
        private Bundle inputBundle;

        IncomingHandler(CryptoServices service) {
            mService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(final Message message) {
            //msg = message;
            messenger = message.replyTo;
            what = message.what;
            inputBundle = message.getData();

            //Handle message in a separate thread
            // The thread doing the heavy lifting
            new Thread() {
                public void run() {
                    // prepare return message bundle
                    Bundle b = new Bundle();

                    CryptoServices service = mService.get();
                    if (service.mCryptoProvider == null) {
                        service.mCryptoProvider = selectCryptoProvider(service, mPrimaryProvider, mSecondaryProvider, mTertiaryProvider);
                        if (service.mCryptoProvider == null) {
                            // no provider picked so stop service
                            // stop crypto service
                            Log.wtf(TAG, "Again, Cannot select crypto provider - WHY!!??");
                            service.stopSelf();
                            return;
                        }
                    }

                    try {
                        switch (what) {
                            case MSG_REGISTER_CLIENT:
                                service.mClients.add(messenger);
                                break;
                            case MSG_UNREGISTER_CLIENT:
                                service.mClients.remove(messenger);
                                break;

                            case MSG_SELECT_CRYPTO_PROVIDER:
                                service.mCryptoProvider = selectCryptoProvider(service, mPrimaryProvider, mSecondaryProvider, mTertiaryProvider);
                                if (service.mCryptoProvider == null) {
                                    // no provider picked so stop service
                                    // stop crypto service
                                    service.stopSelf();
                                }
                                break;
                            case MSG_LIST_EXT_CERTS:
                                String uriPath = inputBundle.getString(CS_CERT_URI);  // get URI path to search
                                ArrayList<String> availableCertificatesAliases = service.mCryptoProvider.listCerts(uriPath);
                                b.putStringArrayList(CS_RET_CERTS, availableCertificatesAliases);
                                break;
                            case MSG_LOAD_CERT:
                                String uri = inputBundle.getString(CS_CERT_URI);
                                String pwd = inputBundle.getString(CS_CERT_PASSWORD);
                                String alias = service.mCryptoProvider.loadCertToKS(uri, pwd);
                                b.putString(CS_RET_CERT, alias);
                                break;
                            case MSG_SIGN:
                                String certAlias = inputBundle.getString(CS_CERT_ALIAS);
                                String algo = inputBundle.getString(CS_SIGN_ALGO);
                                byte[] hashToEncrypt = inputBundle.getByteArray(CS_HASH);
                                try {
                                    byte[] msgOut = service.mCryptoProvider.sign(certAlias, algo, hashToEncrypt);
                                    b.putByteArray(CS_RET_MSG, msgOut);
                                } catch (CryptoException e) {
                                    e.printStackTrace();
                                    b.putString(CS_ERROR, e.getMessage());
                                }
                                break;
                            case MSG_ENCRYPT:
                                String cert = inputBundle.getString(CS_CERT_ALIAS);
                                byte[] msgToEncrypt = inputBundle.getByteArray(CS_MESSAGE);
                                byte[] msgOut = service.mCryptoProvider.encryptWithPubKey(cert, msgToEncrypt);
                                b.putByteArray(CS_RET_MSG, msgOut);
                                break;
                            case MSG_DECRYPT:
                                cert = inputBundle.getString(CS_CERT_ALIAS);
                                msgToEncrypt = inputBundle.getByteArray(CS_MESSAGE);
                                msgOut = service.mCryptoProvider.decryptWithPrivKey(cert, msgToEncrypt);
                                b.putByteArray(CS_RET_MSG, msgOut);
                                break;
                            case MSG_CREATE_CSR:
                                cert = inputBundle.getString(CS_CERT_ALIAS);
                                String principal = inputBundle.getString(CS_CERT_PRINCIPAL);
                                byte[] csr = service.mCryptoProvider.createPKCS10CSR(cert, principal);
                                b.putByteArray(CS_RET_CSR, csr);  // send back encoded CSR in byte array
                                b.putString(CS_CERT_ALIAS, cert);  // ...and the requested alias as a confimation
                                break;
                            case MSG_SIGN_CSR:
                                PKCS10CertificationRequest pkcs10csr = new PKCS10CertificationRequest(inputBundle.getByteArray(CS_CSR));  // get CSR
                                cert = inputBundle.getString(CS_CERT_ALIAS); // get signed CSR cert alias
                                String certSigningAlias = inputBundle.getString(CS_CERT_SIGNING_ALIAS); // get signing certificate alias

                                Certificate certificate = service.mCryptoProvider.signCSR(cert, pkcs10csr, certSigningAlias);
                                b.putSerializable(CS_RET_CERT, certificate);  // send back signed cert in byte array
                                b.putString(CS_CERT_ALIAS, cert);  // ...and the requested alias as a confirmation
                                break;
                            case MSG_GET_CERT_CHAIN:
                                certAlias = inputBundle.getString(CS_CERT_ALIAS);
                                // The thread doing the heavy lifting
                                try {
                                    Certificate[] chain = service.mCryptoProvider.getCertChain(certAlias);
                                    // serialize certs to list of byte arrays
                                    List<Certificate> serCerts = Arrays.asList(chain);
                                    // Serialize list of certificates to one byte array for transfer
                                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                    ObjectOutput out = new ObjectOutputStream(bos);
                                    out.writeObject(serCerts);
                                    byte[] data = bos.toByteArray();
                                    bos.close();
                                    // send certs serialized
                                    b.putByteArray(CS_RET_CERTS, data);
                                    b.putString(CS_CERT_ALIAS, certAlias);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    b.putString(CS_ERROR, e.getMessage());
                                }
                                break;
                            case MSG_REMOVE_CERT:
                                try {
                                    cert = inputBundle.getString(CS_CERT_ALIAS);
                                    service.mCryptoProvider.deleteCertificateFromKeystore(cert);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    b.putString(CS_ERROR, e.getMessage());
                                }
                                break;
                            case MSG_GET_ALGORITHM:
                                cert = inputBundle.getString(CS_CERT_ALIAS);
                                String alg = service.mCryptoProvider.getPrivateKeyAlgorithm(cert);
                                b.putString(CS_RET_ALGO, alg);
                                break;

                            case MSG_WRITE_CERT:
                                try {
                                    cert = inputBundle.getString(CS_CERT_ALIAS);
                                    byte[] crt = inputBundle.getByteArray(CS_CERT);
                                    service.mCryptoProvider.writeCertificate(cert, crt);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    b.putString(CS_ERROR, e.getMessage());
                                }
                                break;
                            //default:
                            //    super.handleMessage(msg);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Operation: " + what + " failed due to: " + e.getMessage());
                        // return crypto error to all clients
                        b.putString(CS_ERROR, e.getMessage());
                    }

                    // send out response
                    Message m = Message.obtain(null, what); // return back the same message as recieved
                    m.setData(b);
                    try {
                        if(messenger != null) {
                            messenger.send(m);
                        }else {
                            Log.wtf(TAG, "Why is ReplyTo messenger NULL!!!");
                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

/*
    static private void sendMessageToAllClients(Message msg, CryptoServices service) {
        for (int i = service.mClients.size() - 1; i >= 0; i--) {
            try {
                service.mClients.get(i).send(msg);
            } catch (RemoteException e) {
                // The client is dead. Remove it from the list
                service.mClients.remove(i);
            }
        }
    }
*/

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Starting crypto service! Received start id " + startId + ": " + intent);
        if (mCryptoProvider == null) {
            if (intent != null) {
                Utils.dumpIntent(intent);
                mPrimaryProvider = intent.hasExtra(CS_CRYPTO_PROVIDER_PRIMARY) ? intent.getExtras().getString(CS_CRYPTO_PROVIDER_PRIMARY) : null;
                // TODO currently other cryptoproviders are removed (not maintained)!
                mSecondaryProvider = intent.hasExtra(CS_CRYPTO_PROVIDER_SECONDARY) ? intent.getExtras().getString(CS_CRYPTO_PROVIDER_SECONDARY) : null;
                // mTertiaryProvider = intent.hasExtra(CS_CRYPTO_PROVIDER_TERTIARY) ? intent.getExtras().getString(CS_CRYPTO_PROVIDER_TERTIARY) : null;
                mCryptoProvider = null; // selected provider is none at the start of service
            }
        }
        return START_STICKY;
    }


    static private CryptoProvider selectCryptoProvider(CryptoServices service, String primary, String secondary, String tertiary) {
        CryptoProvider cp;
        // This creates crypto service provider with key store or equivalent  to be used in all crypto operations

        if (primary != null) {
            try {
                Class<?> clazz = Class.forName(primary);
                Constructor<?> constructor = clazz.getConstructor(Context.class);
                cp = (CryptoProvider) constructor.newInstance(service);

                if (cp.isAvailable()) {
                    Log.d(TAG, "Using Primary crypto provider: " + primary);
                    return cp;
                } else {
                    Log.w(TAG, "Primary crypto not available!" + primary);
                }
            } catch (Exception e) {
                Log.e(TAG, "Cannot instantiate primary crypto  provider: " + primary + " Due to: " + e);
            }
        }

        if (secondary != null) {
            try {
                Class<?> clazz = Class.forName(secondary);
                Constructor<?> constructor = clazz.getConstructor(Context.class);
                cp = (CryptoProvider) constructor.newInstance(service);

                if (cp.isAvailable()) {
                    Log.d(TAG, "Using Secondary crypto provider: " + secondary);
                    return cp;
                } else {
                    Log.w(TAG, "Secondary crypto not available!" + secondary);
                }
            } catch (Exception e) {
                Log.e(TAG, "Cannot instantiate secondary crypto  provider: " + secondary + " Due to: " + e);
            }
        }

        if (tertiary != null) {
            try {
                Class<?> clazz = Class.forName(tertiary);
                Constructor<?> constructor = clazz.getConstructor(Context.class);
                cp = (CryptoProvider) constructor.newInstance(service);

                if (cp.isAvailable()) {
                    Log.d(TAG, "Using Tertiary crypto provider: " + tertiary);
                    return cp;
                } else {
                    Log.w(TAG, "Tertiary crypto not available!" + tertiary);
                }
            } catch (Exception e) {
                Log.e(TAG, "Cannot instantiate tertiary crypto  provider: " + tertiary + " Due to: " + e);
            }
        }

        // no providers available!
        Log.wtf(TAG, "No crypto providers available!!!");
        return null;
    }

    public static boolean isRunning() {
        return isRunning;
    }

}
