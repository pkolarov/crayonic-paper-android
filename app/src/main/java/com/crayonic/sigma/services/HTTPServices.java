package com.crayonic.sigma.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;

import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.crypto.PKIUtils;
import com.crayonic.sigma.pdf.Utils;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.crayonic.sigma.tools.ByteStreams;
import com.crayonic.sigma.tools.Crypto;
import com.crayonic.sigma.tools.HTTPGETUtilities;
import com.crayonic.sigma.tools.MultipartUtility;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.HashMap;


/**
 * An {@link IntentService} subclass for handling asynchronous HTTP (with optional AES encryption) tasks requests in
 * a service on a separate handler thread.
 * <p/>
 */
public class HTTPServices extends IntentService {
    private static final String TAG = "HTTPServices";

    // these are calling job intent actions
    private static final String ACTION_DOWNLOAD = "com.crayonic.sigma.pdf.services.action.DOWNLOAD";
    private static final String ACTION_UPLOAD = "com.crayonic.sigma.pdf.services.action.UPLOAD";
    private static final String ACTION_GET = "com.crayonic.sigma.pdf.services.action.GET";
    private static final String ACTION_UPLOAD_CSR = "com.crayonic.sigma.pdf.services.action.UPLOADCSR";


    // These are resulting intents called upon finishing the HTTP jobs
    public static final String ACTION_DOWNLOAD_RESULT = "com.crayonic.sigma.pdf.services.action.DOWNLOADRESULT";
    public static final String ACTION_UPLOAD_RESULT = "com.crayonic.sigma.pdf.services.action.UPLOADRESULT";
    public static final String ACTION_GET_RESULT = "com.crayonic.sigma.pdf.services.action.GETRESULT";
    public static final String ACTION_UPLOAD_CSR_RESULT = "com.crayonic.sigma.action.UPLOADCSRRESULT" ;


    // for passing data via intents
    public static final String EXTRA_REMOTE_URL = "com.crayonic.sigma.pdf.services.extra.URL";
    public static final String EXTRA_AES_KEY = "com.crayonic.sigma.pdf.services.extra.KEY";
    public static final String EXTRA_FILE_PATH = "com.crayonic.sigma.pdf.services.extra.FILE";
    public static final String EXTRA_DATA_FILENAME = "com.crayonic.sigma.pdf.services.action.FILENAME";
    public static final String EXTRA_DATA_UPLOADRESPONSE = "com.crayonic.sigma.pdf.services.action.UPLOADRESPONSE";
    public static final String EXTRA_DATA_GETRESPONSE = "com.crayonic.sigma.pdf.services.action.GETRESPONSE";
    public static final String EXTRA_USER = "com.crayonic.sigma.pdf.services.extra.USERNAME";
    public static final String EXTRA_PASSWORD = "com.crayonic.sigma.pdf.services.extra.PASSWORD";
    public static final String EXTRA_CSR_ALIAS = "com.crayonic.sigma.CSR_ALIAS";
    public static final String EXTRA_ERROR = "com.crayonic.sigma.ERROR";



    public static void startGet(Context context, String url) {
        Intent intent = new Intent(context, HTTPServices.class);
        intent.setAction(ACTION_GET);
        intent.putExtra(EXTRA_REMOTE_URL, url);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Download with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static Intent startDownload(Context context, String url, String filePath, String aesKey, String user, String pwd) {
        Intent intent = new Intent(context, HTTPServices.class);
        intent.setAction(ACTION_DOWNLOAD);
        intent.putExtra(EXTRA_REMOTE_URL, url);
        intent.putExtra(EXTRA_AES_KEY, aesKey);
        intent.putExtra(EXTRA_FILE_PATH, filePath);
        intent.putExtra(EXTRA_USER, user);
        intent.putExtra(EXTRA_PASSWORD, pwd);
        context.startService(intent);
        return intent;
    }

    /**
     * Starts this service to perform action Upload with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static Intent startUpload(Context context, String url, String filePath, String fileName, String aesKey, String user, String pwd) {
        Intent intent = new Intent(context, HTTPServices.class);
        intent.setAction(ACTION_UPLOAD);
        intent.putExtra(EXTRA_REMOTE_URL, url);
        intent.putExtra(EXTRA_AES_KEY, aesKey);
        intent.putExtra(EXTRA_FILE_PATH, filePath);
        intent.putExtra(EXTRA_DATA_FILENAME, fileName);
        intent.putExtra(EXTRA_USER, user);
        intent.putExtra(EXTRA_PASSWORD, pwd);
        context.startService(intent);
        return intent;
    }

    public static Intent startCSRUpload(Context context, String url, String filePath, String csrAlias, String user, String pwd) {
        Intent intent = new Intent(context, HTTPServices.class);
        intent.setAction(ACTION_UPLOAD_CSR);
        intent.putExtra(EXTRA_REMOTE_URL, url);
        intent.putExtra(EXTRA_FILE_PATH, filePath);
        intent.putExtra(EXTRA_CSR_ALIAS, csrAlias);
        intent.putExtra(EXTRA_USER, user);
        intent.putExtra(EXTRA_PASSWORD, pwd);
        context.startService(intent);
        return intent;
    }

    public HTTPServices() {
        super("HTTPServices");
        //client.networkInterceptors().add(new LoggingInterceptor());

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            Utils.dumpIntent(intent); //FIXME

            final String action = intent.getAction();
            if (ACTION_DOWNLOAD.equals(action)) {
                final String url = intent.getStringExtra(EXTRA_REMOTE_URL);
                final String key = intent.getStringExtra(EXTRA_AES_KEY);
                final String filePath = intent.getStringExtra(EXTRA_FILE_PATH);
                final String user = intent.getStringExtra(EXTRA_USER);
                final String pwd = intent.getStringExtra(EXTRA_PASSWORD);


                handleActionDownload(url, filePath,key, Sigma.authenticatedIdentity, Sigma.applicationKeyStore, Sigma.applicationTrustStore, user, pwd);
            } else if (ACTION_UPLOAD.equals(action)) {
                final String url = intent.getStringExtra(EXTRA_REMOTE_URL);
                final String key = intent.getStringExtra(EXTRA_AES_KEY);
                final String filePath = intent.getStringExtra(EXTRA_FILE_PATH);
                final String fileName = intent.getStringExtra(EXTRA_DATA_FILENAME);
                final String user = intent.getStringExtra(EXTRA_USER);
                final String pwd = intent.getStringExtra(EXTRA_PASSWORD);


                handleActionUpload(url, filePath, key, fileName, Sigma.authenticatedIdentity, Sigma.applicationKeyStore, Sigma.applicationTrustStore, user, pwd);
            } else if (ACTION_GET.equals(action)) {
                final String url = intent.getStringExtra(EXTRA_REMOTE_URL);
                handleActionGet(url);
            } else if(ACTION_UPLOAD_CSR.equals(action)){
                final String url = intent.getStringExtra(EXTRA_REMOTE_URL);
                final String filePath = intent.getStringExtra(EXTRA_FILE_PATH);
                final String csrAlias = intent.getStringExtra(EXTRA_CSR_ALIAS);
                final String user = intent.getStringExtra(EXTRA_USER);
                final String pwd = intent.getStringExtra(EXTRA_PASSWORD);
                handleActionCSRUpload(url, filePath, csrAlias, null, null, Sigma.applicationTrustStore, user, pwd);  // no client cert auth on CSR Sign req
            }
        }
    }

    @Deprecated
    private void handleActionGet(String url) {
        Intent localIntent = new Intent(ACTION_GET_RESULT);
        localIntent.putExtra(EXTRA_REMOTE_URL, url);
//        OkHttpClient client = new OkHttpClient();
        try {
//            Request request = new Request.Builder().url(url).build();
//
//            Response response = client.newCall(request).execute();
//            if (response == null || !response.isSuccessful()) {
//                throw new IOException("Unexpected code " + response);
//            }else{
//                String res = response.body().string();
//                localIntent.putExtra(EXTRA_DATA_GETRESPONSE, res );
//            }
            localIntent.putExtra(EXTRA_DATA_GETRESPONSE, HTTPGETUtilities.doGET(url) );

        }catch (Exception e){
            Log.e(TAG, "Cannot GET url: " + url + e);
            localIntent.putExtra(EXTRA_ERROR, e.getMessage());
        }
        // Broadcasts the Intent to receivers in this app only
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    /**
     * Handle action download and (optional decrypt) in the provided background thread with the provided
     * parameters.
     */
    private void handleActionDownload(String url, String filePath, String key, String certAlias, KeyStore keystore, KeyStore truststore, String user, String pwd) {
        Intent localIntent = new Intent(ACTION_DOWNLOAD_RESULT);
        localIntent.putExtra(EXTRA_AES_KEY, key);
        localIntent.putExtra(EXTRA_REMOTE_URL, url);
        try {

            String name = HTTPGETUtilities.downloadAndDecryptFile(this, url, filePath, key, certAlias, keystore, truststore, user, pwd);

            if(!name.equalsIgnoreCase("HTML")) {
                Log.i(TAG, "Downloaded file :" + name);
                // return downloaded filename
                localIntent.putExtra(EXTRA_DATA_FILENAME, name);

                File downloadedFile = new File(filePath);
                if (downloadedFile.exists() && downloadedFile.isFile() && downloadedFile.length() > 0) {
                    //localIntent.setData(Uri.fromFile(downloadedFile)); -- THIS DOES NOT WORK FOR LBM!!!
                    localIntent.putExtra(EXTRA_FILE_PATH, filePath);
                    //localIntent.setData(Uri.parse(new File(filePath).getAbsolutePath()));
                }
            }else {
                // URL is a standard web page so return signal to show web
                localIntent.putExtra(EXTRA_FILE_PATH, "HTML");
            }
        }
        catch (Exception e) {
            Log.e(TAG, "Cannot download file: " + e);
            e.printStackTrace();
            localIntent.putExtra(EXTRA_ERROR, e.getMessage());
        }
        // Broadcasts the Intent to receivers in this app only
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    /**
     * Handle action (optional encrypt) and upload in the provided background thread with the provided
     * parameters.
     */

    private void handleActionUpload(String url, String filePath, String key, String fileName, String certAlias,
                                    KeyStore keyStore, KeyStore trustStore, String user, String pwd) {
        //setup return status
        Intent localIntent = new Intent(ACTION_UPLOAD_RESULT);
        localIntent.putExtra(EXTRA_AES_KEY, key);
        localIntent.putExtra(EXTRA_REMOTE_URL, url);
        localIntent.putExtra(EXTRA_FILE_PATH, filePath);


        try {
            File encF = null;
            String userid = null;
            //MediaType mimetype = MEDIA_TYPE_PDF;
            if (key != null) { // see if we need to encrypt first
                encF = File.createTempFile("enc", null, getCacheDir());
                userid = Crypto.encryptFile(filePath, encF.getAbsolutePath(), key);
                filePath = encF.getAbsolutePath(); // upload the encrypted file instead of original if key is there

            }
            HashMap<String, String> values = new HashMap<>();
            if(fileName != null)  values.put("filename", fileName);
            if(userid != null)  values.put("userid", userid);
            String response = MultipartUtility.doMultipartPOSTwithFile(url, "file", filePath, values, certAlias, keyStore , trustStore, user , pwd);
            if (response!= null) {
                // if upload successful return response
                localIntent.putExtra(EXTRA_DATA_UPLOADRESPONSE, response);
            }
//            if (response.isSuccessful()) {
//                // if upload successful return response
//                localIntent.putExtra(EXTRA_DATA_UPLOADRESPONSE, response.body().toString());
//            }

            if (encF != null) encF.delete(); // try removing encrypted file so its not hanging around
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            localIntent.putExtra(EXTRA_ERROR, e.getMessage());
        }

        // Broadcasts the Intent to receivers in this app only
       // localIntent.setData(Uri.parse(new File(filePath).getAbsolutePath())); -- THIS DOES NOT WORK FOR LBM!!!!
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }


    /**
     * Handle action (optional encrypt) and upload in the provided background thread with the provided
     * parameters.
     *
     * csrAlias is just a form parameter
     * validCertAlias is optional alias in keystore with valid certificate
     */

    private void handleActionCSRUpload(String url, String filePath, String csrAlias, String validCertAlias,
                                    KeyStore keyStore, KeyStore trustStore, String user, String pwd) {
        //setup return status
        Intent localIntent = new Intent(ACTION_UPLOAD_CSR_RESULT);
        localIntent.putExtra(EXTRA_REMOTE_URL, url);
        localIntent.putExtra(EXTRA_FILE_PATH, filePath);
        localIntent.putExtra(EXTRA_CSR_ALIAS, csrAlias);

        try {
            HashMap<String, String> values = new HashMap<>();
            values.put("csralias", csrAlias);
            values.put("username", user);
            values.put("password", pwd);
            String pem = Base64.encodeToString( FileUtilsPlus.readBinaryFile(filePath), Base64.DEFAULT );
            values.put("csr", pem);

            String response = MultipartUtility.doMultipartPOSTwithFile(url, "csrfile", filePath, values, validCertAlias, keyStore , trustStore, user , pwd);
            if (response != null) {
                localIntent.putExtra(EXTRA_DATA_UPLOADRESPONSE, response); //new
            }
            localIntent.putExtra(EXTRA_CSR_ALIAS, csrAlias);        //new

 //           // FIXME remove from here to identity provider
            //-----remove from here
//            if (response != null) {
//                // if upload successful return response
//                if(storeCertificate(response, csrAlias)) {
//                    localIntent.putExtra(EXTRA_DATA_UPLOADRESPONSE, csrAlias);
//                }
//                else {
//                    removeCSRCertificate(csrAlias);
//                }
//
//
//            }else {
//                removeCSRCertificate(csrAlias);
//            }
            // ---- to here
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            localIntent.putExtra(EXTRA_ERROR, e.getMessage());
            // also remove
   //         removeCSRCertificate(csrAlias);
        }
        // Broadcasts the Intent to receivers in this app only
        // localIntent.setData(Uri.parse(new File(filePath).getAbsolutePath())); - THIS DOES NOT WORK FOR LBM (do not use)!!!!
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }



  /*  class LoggingInterceptor implements Interceptor {
        @Override public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            long t1 = System.nanoTime();
            Log.i(TAG, String.format("Sending request %s on %s%n%s  %s", request.url(), chain.connection(), request.headers(), request.body().contentLength()));

            Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            Log.i(TAG, String.format("Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers()));

            return response;
        }
    }
    */
}
