package com.crayonic.sigma;

/**
 * Created by peter on 19/11/15.
 */
public interface ProgressDialogListener {
    void createProgressDialog(String msg);
    void dismissProgressDialog();
}
