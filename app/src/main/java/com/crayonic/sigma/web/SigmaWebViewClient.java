package com.crayonic.sigma.web;

import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.util.Log;
import android.webkit.ValueCallback;

import com.crayonic.sigma.MainActivity;
import com.crayonic.sigma.SSL.SSLKeyManager;
import com.crayonic.sigma.Sigma;

import org.xwalk.core.ClientCertRequest;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkView;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;

/**
 * Created by peter on 25/01/16.
 */
public class SigmaWebViewClient extends XWalkResourceClient {

    private static final String TAG = "SigmaWebViewClient";
    private static final String DEFAULT_MATCH_PATTERN = "^.*\\.(pdf|PDF)$"; // regexp to search in URL_DOWNLOAD for positive match for PDF containing link

    private String matchPDFinLinkRegexp;
    private final MainActivity activity;

    public SigmaWebViewClient(XWalkView v, String matchPDFinLinkRegexp, MainActivity activity) {
        super(v);
        this.matchPDFinLinkRegexp = matchPDFinLinkRegexp;
        this.activity = activity;
        if(this.matchPDFinLinkRegexp == null){
            // set default regex match for links ending with .pdf
            this.matchPDFinLinkRegexp = DEFAULT_MATCH_PATTERN;
        }
    }


    // gets called when xwalkview does not trust server cert
    @Override
    public void onReceivedSslError(XWalkView view, ValueCallback<Boolean> callback, SslError error) {
        //super.onReceivedSslError(view, callback, error);
        Log.w(TAG, "Server may not be trusted or Root cert not availble?");
        SslCertificate cert = error.getCertificate();
        Log.w(TAG, "onReceivedSSLError - Error: "  + error.toString());
        Log.w(TAG, "onReceivedSSLError - Connecting to server with cert issued to DN:" + cert.getIssuedTo().getDName());
        Log.w(TAG, "onReceivedSSLError - Connecting to server with cert issued by DN:" + cert.getIssuedBy().getDName());
        // dont show the warning - we are relying on the first initial connection via our own URLconnection after QR code scan - our URL connection has cusotm truststore
        callback.onReceiveValue(true);
    }

    @Override
    public void onLoadStarted(XWalkView view, String url) {
        super.onLoadStarted(view, url);
        Log.d(TAG, "Load Started:" + url);

        // for each page update mBaseURL so we can use it in our app context
        try {
            URL bURL = new URL(url);
            activity.mBaseURL = bURL.getProtocol() + "://" + bURL.getHost() ;  // base url to append to external HTTP operations not handled by webview
            if(bURL.getPort()>0){
                activity.mBaseURL = activity.mBaseURL + ":" + bURL.getPort();
            }
        } catch (MalformedURLException e) {
            Log.wtf(TAG, "Cannot get baseUrl for: "+ url);
            e.printStackTrace();
        }
    }


    @Override
    public boolean shouldOverrideUrlLoading(XWalkView view, String url) {



        Log.i(TAG, "web view is trying to load URL:   " + url);
//        if (!url.matches(matchPDFinLinkRegexp)) {
//            // This is not PDF doc link, so do not override; let the  WebView load the page
//            return false;
//        }
//
//        // We hit the PDF download so come back to Sigma activity
//        JS2JavaInterface.openPDF(url, null, null, null,null, activity,"");
//            Intent intent = new Intent();
//            intent.putExtra(WebIntent.URL_DOWNLOAD, url);
//            setResult(Activity.RESULT_OK, intent);
//            finish();
        return false;
    }

    @Override
    public void onReceivedClientCertRequest(XWalkView view, ClientCertRequest handler) {
        try {
            List<X509Certificate> certs = Arrays.asList(SSLKeyManager.getX509CertificatesForPKAlias(Sigma.applicationKeyStore, Sigma.authenticatedIdentity));
            Log.i(TAG, "Client cert requested: " + handler.getPrincipals()[0].toString());
            PrivateKey privateKey = (PrivateKey) Sigma.applicationKeyStore.getKey(Sigma.authenticatedIdentity, null);
            handler.proceed(privateKey, certs);
        } catch (Exception e) {
            Log.e(TAG, "Cannot get private key and certificates for client authentication in browser! " + e.getMessage());
        }
    }
}
