package com.crayonic.sigma.web;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import org.xwalk.core.XWalkDownloadListener;

/**
 * Created by peter on 25/01/16.
 */
public class SigmaWebViewDownloadListener extends XWalkDownloadListener {
    private static final String TAG = "SigmaWebView";
    Context context;

    public SigmaWebViewDownloadListener(Context c) {
        super(c);
        this.context = c;
    }
    @Override
    public void onDownloadStart(String s, String s1, String s2, String s3, long l) {
        Log.w(TAG, "Requested download : " + s + "   " + s1 + "   " + s2 + "  " + s3 + "   " + l);
        if(s3.equalsIgnoreCase("application/pdf")){
           JS2JavaInterface.openPDF(s, null, null, null,null, (Activity) context ,"");
       }
    }

}
