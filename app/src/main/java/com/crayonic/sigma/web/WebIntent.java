package com.crayonic.sigma.web;

/**
 * Created by peter on 14/01/16.
 */
public class WebIntent {

    public static final String URL_DOWNLOAD = "WebIntent_url_download";
    public static final String URL_UPLOAD = "WebIntent_url_upload";
    public static final String SIGNATORY_JSON = "WebIntent_sigjson";
    public static final String CALLBACK_URL = "WebIntent_callbackurl";
    public static final String DOC_FORMAT = "WebIntent_docformat";
    public static final String PAGES_TO_SCAN = "WebIntent_pagestoscan";
    public static final String SCAN_COLOR = "WebIntent_scancolor";
    public static final String DOC_NAME = "WebIntent_docname";
    public static final String AES_KEY = "WebIntent_key";
    public static final String SHOW_QR = "WebIntent_showqr";
    public static final String CONTENT ="WebIntent_content" ;
    public static final String IDENTITY = "WebIntent_identity";
    public static final String USERNAME = "WebIntent_username";
    public static final String PWD = "WebIntent_pwd" ;
    public static final String SCAN_SIZE = "WebIntent_scansize";
    public static final String SCAN_QUALITY = "WebIntent_scanquality";
    public static final String SCAN_CROP = "WebIntent_scancrop" ;
}
