package com.crayonic.sigma.web;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.crayonic.sigma.MainActivity;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.archive.FileChooser;
import com.crayonic.sigma.crypto.craytoken.AdminCommand;
import com.crayonic.sigma.crypto.craytoken.BasicComm;

import org.xwalk.core.JavascriptInterface;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by kolarov on 5.9.2015.
 */
public class JS2JavaInterface {
    private static final String TAG = "JS2JavaInterface";
    public static final String REGEX = "regex";

    private final MainActivity activity;

    public JS2JavaInterface(MainActivity activity) {
        this.activity = activity;
    }

    @JavascriptInterface
    public void openPDF(String downloadURL, String uploadURL, String signatoryJSON, String callbackURL, String key) {
        // append URLs with current mBaseURL
        openPDF(downloadURL, uploadURL, signatoryJSON, callbackURL, key, activity, activity.mBaseURL);
    }

    // This can be called outside of browser context
    public static void openPDF(String downloadURL, String uploadURL, String signatoryJSON, String callbackURL, String key, Activity activity, String baseURL) {

        Intent intent = new Intent(activity, MainActivity.class);
        // figure out the decryption key options
        if (key == null || key.equalsIgnoreCase("undefined") || key.length() < 1) {
            if (activity.getIntent().hasExtra(WebIntent.AES_KEY)) {
                String originalKey = activity.getIntent().getExtras().getString(WebIntent.AES_KEY);// if no key is passed from JS then look for one passed to web activity
                if (originalKey != null && originalKey.length() > 0) {
                    downloadURL = downloadURL + "\n" + originalKey; // key has to be passed to download intent via URL!!!!
                    intent.putExtra(WebIntent.AES_KEY, originalKey); // for correctness also pass by intent
                }
            }
        } else {
            downloadURL = downloadURL + "\n" + key;
            intent.putExtra(WebIntent.AES_KEY, key);
        }

        intent.setAction(Sigma.DOWNLOAD_INTENT_ACTION);

        try {
            if ((new URI(downloadURL)).isAbsolute()) {
                // add base url only for relative links
                intent.setData(Uri.parse(downloadURL));

            } else {
                intent.setData(Uri.parse(baseURL + downloadURL));

            }
        } catch (URISyntaxException e) {
            Log.wtf(TAG, "Bad download URL in Javascript call: " + downloadURL);
        }


        try {
            if (uploadURL != null) {
                if ((new URI(uploadURL)).isAbsolute()) {
                    // add base url only for relative links
                    intent.putExtra(WebIntent.URL_UPLOAD, uploadURL);
                } else {
                    intent.putExtra(WebIntent.URL_UPLOAD, baseURL + uploadURL);

                }
            }
        } catch (URISyntaxException e) {
            Log.wtf(TAG, "Bad URL upload link in Javascript call: " + uploadURL);
        }

        try {
            if (callbackURL != null) {
                if ((new URI(callbackURL)).isAbsolute()) {
                    // add base url only for relative links
                    intent.putExtra(WebIntent.CALLBACK_URL, callbackURL);
                } else {
                    intent.putExtra(WebIntent.CALLBACK_URL, baseURL + callbackURL);
                }
            }
        } catch (URISyntaxException e) {
            Log.wtf(TAG, "Bad URL callback link in Javascript call: " + callbackURL);
        }

        if (signatoryJSON != null)
            intent.putExtra(WebIntent.SIGNATORY_JSON, signatoryJSON);

        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        activity.startActivity(intent);
    }

    @JavascriptInterface
    public void openScanner(String documentName, String docFormat, int pagesToScan, String scanColorSpace, String uploadURL, String callbackURL, String key) {
        // append URLs with current mBaseURL
     /*   try {
            URL currentURL = new URL(mXwalkView.getUrl());
            uploadURL = currentURL.getProtocol() + currentURL.getHost() + currentURL.getPort() + "/" + uploadURL;
            callbackURL = currentURL.getProtocol() + currentURL.getHost() + currentURL.getPort() + "/" + callbackURL;
        } catch (MalformedURLException e) {
            Log.wtf(TAG, "Invalid URL passed by XwalkView!");
        }
*/
        Intent intent = new Intent(activity, MainActivity.class);

        // figure out the encryption key options
        if (key == null || key.equalsIgnoreCase("undefined") || key.length() < 1) {
            if (activity.getIntent().hasExtra(WebIntent.AES_KEY)) {
                String originalKey = activity.getIntent().getExtras().getString(WebIntent.AES_KEY);// if no key is passed from JS then look for one passed to web activity
                if (originalKey != null && originalKey.length() > 0) {
                    intent.putExtra(WebIntent.AES_KEY, originalKey); // for correctness also pass by intent
                }
            }
        } else {
            intent.putExtra(WebIntent.AES_KEY, key);
        }

        intent.setAction(Sigma.SCAN_INTENT_ACTION);
        if (documentName != null && !documentName.isEmpty())
            intent.putExtra(WebIntent.DOC_NAME, documentName);

        if (uploadURL != null && uploadURL.length() > 5) {
            try {
                if ((new URI(uploadURL)).isAbsolute()) {
                    // add base url only for relative links
                    intent.putExtra(WebIntent.URL_UPLOAD, uploadURL);
                } else {
                    intent.putExtra(WebIntent.URL_UPLOAD, activity.mBaseURL + uploadURL);

                }
            } catch (URISyntaxException e) {
                Log.wtf(TAG, "Bad URL upload link in Javascript call: " + uploadURL);
            }
        }

        if (callbackURL != null && callbackURL.length() > 5) {
            try {
                if ((new URI(callbackURL)).isAbsolute()) {
                    // add base url only for relative links
                    intent.putExtra(WebIntent.CALLBACK_URL, callbackURL);
                } else {
                    intent.putExtra(WebIntent.CALLBACK_URL, activity.mBaseURL + callbackURL);
                }
            } catch (URISyntaxException e) {
                Log.wtf(TAG, "Bad URL callback link in Javascript call: " + callbackURL);
            }
        }
        intent.putExtra(WebIntent.DOC_FORMAT, docFormat);
        intent.putExtra(WebIntent.PAGES_TO_SCAN, pagesToScan);
        intent.putExtra(WebIntent.SCAN_COLOR, scanColorSpace);


        //  intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);  This flag does not work since cam is called from scanner ... TODO built in cam will fix this
        activity.startActivity(intent);
    }


    @JavascriptInterface
    public void openQR() {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setAction(Sigma.READQR_INTENT_ACTION);
        activity.startActivity(intent);
    }

    @JavascriptInterface
    public void openArchive(String uri) {

        if (uri == null || uri.isEmpty()) {
            // open default device doc archive
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
            new FileChooser(activity,dir).setFileListener(new FileChooser.FileSelectedListener() {
                @Override
                public void fileSelected(final File file) {
                    // do something with the file
                    activity.openSigma(file,file.getName(),null,null,null,null);
                }
            }).showDialog();
        }
    }


    @JavascriptInterface
    public void finish() {
        activity.finishAffinity();
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setAction(Intent.ACTION_DEFAULT);
        activity.startActivity(intent);
    }

    @JavascriptInterface
    public void createIdentity(String identityJSON, String user, String pwd) {
        // create CSR request with JSON string in the CN attribute and send over secure channel
        Intent intent = new Intent(activity, MainActivity.class);
        //Intent intent = new Intent();
        intent.setAction(Sigma.CREATE_IDENTITY_ACTION);
        intent.putExtra(WebIntent.IDENTITY, identityJSON);
        intent.putExtra(WebIntent.USERNAME, user); // web service authentication username
        intent.putExtra(WebIntent.PWD, pwd);
        intent.setFlags(intent.getFlags());
        //activity.setResult(Activity.RESULT_OK, intent);
        //activity.finish();
        activity.startActivity(intent);

    }

    @JavascriptInterface
    public void initiatePen(String pin){
        BasicComm bc = new BasicComm(activity);
        try {
            bc.sendPenCommand(AdminCommand.ADMIN_TRAIN, pin);
            bc.setLineResponseListener(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
