package com.crayonic.sigma.crypto.craytoken;

import android.util.Log;

import com.crayonic.smartcardio.CardException;
import com.crayonic.smartcardio.CardTerminal;
import com.crayonic.smartcardio.CommandAPDU;
import com.crayonic.smartcardio.ResponseAPDU;
import com.itextpdf.smartcard.SmartCard;
import com.itextpdf.smartcard.util.DigestAlgorithms;
import com.itextpdf.smartcard.util.IsoIec7816;
import com.itextpdf.smartcard.util.SmartCardIO;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;

/**
 * Created by peter on 08/02/16.
 */
public class CraytokenSmartCard extends SmartCard{

    private static final String TAG = "SmartCard";
    private final byte keyId;
    private final String encryptionAlgorithm;

    public CraytokenSmartCard(CardTerminal mCardTerminal,byte keyId, String encryptionAlgorithm) throws CardException {
        super(mCardTerminal);
        this.keyId = keyId;
        this.encryptionAlgorithm = encryptionAlgorithm;
    }


    public boolean hasPrivateKey() {
        if(encryptionAlgorithm.equalsIgnoreCase("RSA"))
            return true;
        else
            return false;
    }

    public Certificate[] getCertificateChain(byte[] fileID) throws CertificateException, IOException, CardException {

        // get certificate and add trusted certs to make chain
        Certificate cert = readCertificate(fileID);

        Certificate[] chain = new Certificate[1];
        chain[0] = cert;

        return chain;
    }



    public byte[] RSAEncrypt( byte[] msg) throws CardException, IOException {
        // First setup security env - select key for RSA algo
        byte[] data = new byte[] {
                0x04, // Length
                (byte)0x80, (byte)0x01, (byte) 0x84,
                keyId
        };

        Log.i(TAG, "Manage security environment");
        CommandAPDU commandAPDU = new CommandAPDU(
                IsoIec7816.CLA_00, IsoIec7816.INS_MANAGE_SECURITY_ENVIRONMENT,
                IsoIec7816.P1_COMPUTATION_SET, IsoIec7816.P2_CRT_DIGITAL_SIGNATURE,
                data);

        ResponseAPDU responseAPDU = SmartCardIO.transmit(channel, commandAPDU);

        if (responseAPDU.getSW() != IsoIec7816.SW_NO_FURTHER_QUALIFICATION) {
            throw new CardException("Incorrect response: " + Integer.valueOf(responseAPDU.getSW()));
        }

        //Log.i(TAG,"Verify PIN for signing");
        //int retries = verifyPin(verifyPinDirectCommand, verifyPinStartCommand, -1);

        Log.i(TAG, "Encrypt the bytes");
        commandAPDU = new CommandAPDU(
                IsoIec7816.CLA_00, IsoIec7816.INS_PERFORM_SECURITY_OPERATION,
                IsoIec7816.P1_DIGITAL_SIGNATURE, IsoIec7816.P2_INPUT_DATA,
                msg);
        responseAPDU = SmartCardIO.transmit(channel, commandAPDU);

        int sw = responseAPDU.getSW();
        // A pin is needed, and it isn't cached on the reader
        if (sw == IsoIec7816.SW_SECURITY_STATUS_NOT_SATISFIED) {
            Log.i(TAG, "Pin code couldn't be verified");
//            retries = verifyPin(verifyPinDirectCommand, verifyPinStartCommand, retries);
            responseAPDU = SmartCardIO.transmit(channel, commandAPDU);
            sw = responseAPDU.getSW();
        }
        if (sw == IsoIec7816.SW_NO_FURTHER_QUALIFICATION) {
            Log.i(TAG, "Signing done");
            return responseAPDU.getData();
        }
        else {
            throw new IOException("Digest could not be signed: " + Integer.toHexString(sw));
        }
    }


    /**
     * Returns the encryption algorithm used for the private key.
     * @return	an encryption algorithm (e.g. "RSA")
     */
    public String getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }


    /**
     * Reads an X509 Certificate from the card.
     * @param fileID	the fileID for the certificate
     * @return	an X506Certificate object.
     * @throws CertificateException
     * @throws CardException
     * @throws IOException
     */
    public X509Certificate readCertificate(byte[] fileID) throws CertificateException, CardException, IOException{
        byte[] certificateFile = readFile(fileID);
        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        return (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(certificateFile));
    }

    /**
     * Returns a pattern that is specific for the smart card implementation.
     *  specific pattern or null for the generic SmartCard object.
     */
    @Override
    public byte[] getPattern() {
        return null;
    }

    /**
     * Returns a mask for the pattern that is specific for the smart card implementation.
     * a specific pattern or null if the pattern needs to be an exact match.
     */
    @Override
    public byte[] getMask() {
        return null;
    }


}
