/**
 * Created by peter kolarov on 26.9.2015.
 */
package com.crayonic.sigma.crypto.providers;

import android.content.Context;
import android.os.Environment;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;
import android.util.Log;

import com.crayonic.sigma.SSL.SSLKeyManager;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.crypto.CryptoException;
import com.crayonic.sigma.crypto.CryptoProvider;
import com.crayonic.sigma.crypto.PKIUtils;
import com.crayonic.sigma.crypto.craytoken.BLEStack;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.crayonic.sigma.tools.ByteStreams;
import com.crayonic.smartcardio.CardTerminal;
import com.itextpdf.smartcard.crayonicid.CrayonicIDCard;
import com.itextpdf.smartcard.crayonicid.CrayonicIDCertificates;

import org.json.JSONObject;
import org.spongycastle.asn1.oiw.OIWObjectIdentifiers;
import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x509.AlgorithmIdentifier;
import org.spongycastle.asn1.x509.AuthorityKeyIdentifier;
import org.spongycastle.asn1.x509.BasicConstraints;
import org.spongycastle.asn1.x509.GeneralName;
import org.spongycastle.asn1.x509.GeneralNames;
import org.spongycastle.asn1.x509.X509Extension;
import org.spongycastle.cert.X509CertificateHolder;
import org.spongycastle.cert.X509ExtensionUtils;
import org.spongycastle.cert.X509v3CertificateBuilder;
import org.spongycastle.cert.jcajce.JcaX509CertificateConverter;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.DigestCalculator;
import org.spongycastle.operator.bc.BcDigestCalculatorProvider;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;
import org.spongycastle.pkcs.PKCS10CertificationRequest;
import org.spongycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.spongycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.math.BigInteger;
import java.net.URI;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;

import javax.crypto.Cipher;
import javax.security.auth.x500.X500Principal;

/**
 * Non-repudiation digital signatures  and certificates are provided by external Crayonic cryptoken a.k.a. "Craytoken" while authentication (e.g. SSL context) is provided by
 * native Android Key Store Crypto and Certificate provider
 */
public class CrayonicPenWithAndroidKeyStoreCryptoProvider implements CryptoProvider {

    private static final String TAG = "CrayonicPenCP";


    //private static final byte CERT_KEY_ID = 0x01;  //FIXME - Place real ID of cert/key stored in craytoken
    //private static final byte[] CERT_ID = {0x00, 0x02}; //FIXME

    static {
        Security.addProvider(new org.spongycastle.jce.provider.BouncyCastleProvider());
    }


    protected KeyStore mKeyStore;
    protected Context mContext;
    protected CardTerminal mCardTerminal;
    protected CrayonicIDCard mCraytokenSmartCard;

    /**
     * @param c Context of the calling service
     * @throws Exception
     */
    // Note: Constructor is called only via reflection by CryptoServices service
    public CrayonicPenWithAndroidKeyStoreCryptoProvider(Context c) throws Exception {
        mContext = c;
        mKeyStore = KeyStore.getInstance("AndroidKeyStore");
        mKeyStore.load(null); // initialize app key store
        Sigma.applicationKeyStore = mKeyStore;

        //  Trust Store (with pinned certificates) is also the Android Native Keystore
        // just make sure its initialized with our current certs
        loadTrustedCerts(mKeyStore);  // only load root cert if none is present yet
        Sigma.applicationTrustStore = mKeyStore;

        // create Card Terminal and Craytoken Smart Card with BLE stack for communication link
        BLEStack blestack = BLEStack.getInstance(c);
        mCardTerminal = new CardTerminal(blestack);

    }


    // load up keystore with trust chain from app assets in order to pin CA and sub CA certs
    protected void loadTrustedCerts(KeyStore ks) throws CryptoException{
        try {
            if(Sigma.applicationProperties.getProperty("ssl_pinned_cert_chain")!=null) {

                File f = new File(Sigma.getCertPath(), Sigma.applicationProperties.getProperty("ssl_pinned_cert_chain"));
                Log.w(TAG, "Loading Pinned CA certificate from: " + f.getAbsolutePath());
                FileInputStream fis = new FileInputStream(f);
                Certificate[] chain = PKIUtils.loadCertificateChainFromPEM(new InputStreamReader((fis)));
                String rootCertAlias = ((X509Certificate) chain[0]).getSubjectDN().getName(); // make the alias for trusted root its serial number
                if (mKeyStore.getEntry(rootCertAlias, null) == null) {
                    if (chain.length > 0) {
                        // test to replace certificate if any present
                        //KeyStore.TrustedCertificateEntry existingEntry = (KeyStore.TrustedCertificateEntry) ks.getEntry(TRUSTED_ROOT_CA_ALIAS, null);
                        ks.setCertificateEntry(rootCertAlias, chain[0]);//load only the ROOT CA (not any sub CAs)
                        Log.w(TAG, "!!!! From now app will only trust this CA for all SSL connections:" + rootCertAlias);
                    } else {
                        Log.wtf(TAG, "No trusted certificates found in assets!!!!");
                        throw new CryptoException("Error loading trusted certs!");
                    }
                }
            }else{
                Log.w(TAG, "No pinned certificates found in properties for trust anchor!");
            }
        } catch (Exception e) {
            Log.wtf(TAG, "Error loading trusted certs!");
            e.printStackTrace();
            throw new CryptoException("Error loading trusted certs!");
        }
    }

    /**
     * @param uriPath Ignored when dealing with native key store
     * @return list of aliases (could be empty)
     * @throws CryptoException
     */
    @Override
    public ArrayList<String> listCerts(final String uriPath) throws CryptoException {

        try {
            if (mKeyStore.aliases() == null) {
                return new ArrayList<>(0);
            }

            // filter out server certs and match unsigned with signed certificate (return only matching)


            ArrayList<String> aliases = Collections.list(mKeyStore.aliases());
            ArrayList<String> signedAliases = new ArrayList<>(aliases.size());
            for (String alias : aliases) {

                if (mKeyStore.getEntry(alias,null) instanceof KeyStore.PrivateKeyEntry  /*alias.endsWith(SSLKeyManager.SIGNED_POSTFIX*/) {
                    // private Key alias in keystore is the one with same name but no SIGNED_POSTFIX
                   // String privateKeyAlias = alias.substring(0, alias.indexOf(SSLKeyManager.SIGNED_POSTFIX));
                   // if (aliases.contains(privateKeyAlias)) {
                        signedAliases.add(alias); // add to final list if both aliases exist in keystore
                   // }
                }
            }
            return signedAliases;

        } catch (Exception e) {
            throw new CryptoException(e + "");
        }
    }

    @Override
    public Certificate[] getCertChain(final String certAlias) throws CryptoException {
        try {
            Certificate[] chain = null;
            // TODO try to get certificate from craytoken first

            try {
                if (mCraytokenSmartCard == null)
                    mCraytokenSmartCard = new CrayonicIDCard(mCardTerminal);  // this already connects to BLE device

                chain = CrayonicIDCertificates.getSignCertificateChain(mCraytokenSmartCard);
                if(chain != null)
                    return chain;

            }catch (Exception e){
                // fallback to internal key store
                Log.e(TAG, "Exception: " + e.getMessage());
                Log.w(TAG, "No certificate found in craytoken key store. falling back to internal key store ");
            }

            chain = mKeyStore.getCertificateChain(certAlias + SSLKeyManager.SIGNED_POSTFIX); // first try if there is signed cert available
            if(chain == null) {
                chain = mKeyStore.getCertificateChain(certAlias);
            }

            for(int i = 0; i < chain.length; i++){
                 X509Certificate x509Certificate = (X509Certificate) chain[i];
                 Log.w(TAG, "Certificate chain item "  + i + " is: " + x509Certificate.toString());
            }

            if(chain != null){
                return chain;
            }
            throw new CryptoException("No certificate available anywhere for cert alias: " + certAlias);
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    // for PKCS12 file format only!
    public String loadCertToKS(final String uri, final String pwd) throws CryptoException {
        try {
            FileInputStream fis = new FileInputStream(URI.create(uri).getPath());
            KeyStore ks = KeyStore.getInstance("pkcs12");
            ks.load(fis, pwd.toCharArray()); // load PFX file into temp PKCS12 keystore
            String alias = ks.aliases().nextElement();
            if (mKeyStore.containsAlias(alias)) {  // store private key and its password to perm app keystore
                Log.w(TAG, "Overwriting Android keystore contains cert alias: " + alias);
            }
            PrivateKey pKey = (PrivateKey) ks.getKey(alias, pwd.toCharArray());
            Certificate[] certChain = ks.getCertificateChain(alias);
            // Log.e("TAG", "Key format:" + pKey.getFormat());
            mKeyStore.setEntry(alias, ks.getEntry(alias, null), null);
            mKeyStore.setKeyEntry(alias, pKey.getEncoded(), certChain); // store PFX in global app keystore


            //FIXME hack to set the global app key store when signing cert is loaded (e.g. authenticated)
            Sigma.applicationKeyStore = mKeyStore;
            //TODO This hack will be fixed when we will implement full Java Crypto Provider SPI engines!!!

            return alias;

        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public void replaceCSRWithNewCertificate(final String alias, final Certificate[] chain) throws CryptoException {
        try {
            KeyStore.PrivateKeyEntry existingPrivateKeyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry(alias, null);
            if(existingPrivateKeyEntry != null) {
                KeyStore.PrivateKeyEntry newEntry = new KeyStore.PrivateKeyEntry(existingPrivateKeyEntry.getPrivateKey(), chain);
                mKeyStore.setEntry(alias, newEntry, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new CryptoException(e.getMessage());
        }

    }


    @Override
    public KeyPair createNewKeyPair(String alias, String principal) throws CryptoException {
        try {
            // Create new key if needed
            if (!mKeyStore.containsAlias(alias)) {
            }
            Calendar start = Calendar.getInstance();
            start.add(Calendar.HOUR_OF_DAY, -1);   //valid one hour before
            Calendar end = Calendar.getInstance();
            end.add(Calendar.HOUR_OF_DAY, 1); // valid for one hour after
            // create temp certificate for the key pair (can be replaced  with real cert after its CSR has been signed!)
            KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(mContext)
                    // KeyGenParameterSpec spec = new KeyGenParameterSpec.Builder(alias,KeyProperties.PURPOSE_SIGN)
                    .setAlias(alias)
                    .setEncryptionRequired() // FIXME Enable for production so lock screen is required!
                    .setKeySize(2048)
                    .setKeyType(KeyProperties.KEY_ALGORITHM_RSA)
                    .setSubject(new X500Principal(principal))
                    .setSerialNumber(BigInteger.ONE)
                    .setStartDate(start.getTime())
                    .setEndDate(end.getTime())
                            //        .setDigests(KeyProperties.DIGEST_SHA256)
                            //        .setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PKCS1)
                    .build();
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
            generator.initialize(spec);
            KeyPair keyPair = generator.generateKeyPair();

            return keyPair;
            //  }
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
        // throw new CryptoException(e.getMessage());
        //return null;
    }

    @Override
    public byte[] createPKCS10CSR(String certAlias, String principal) throws CryptoException {
        try {
            // create CSR if no pem found
            KeyPair keyPair = createNewKeyPair(certAlias, principal); //Note: Android keystore creates certificate with the Public Key

            // create and save PKCS10 CSR
            JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA256withRSA");
            ContentSigner signer = csBuilder.build(keyPair.getPrivate());

            PKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(
                    new X500Principal(principal), mKeyStore.getCertificate(certAlias).getPublicKey());

            PKCS10CertificationRequest csr = p10Builder.build(signer);
            // FOR TESTING ONLY --------------
            // String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/testAlias.csr";
            // FileUtilsPlus.saveBinaryFile(filePath + ".csr", csr.getEncoded());
            // FOR TESTING ONLY END ----------

            return csr.getEncoded();
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public String getPrivateKeyAlgorithm(String certAlias) throws CryptoException {
        try {
            String algo = mKeyStore.getCertificate(certAlias).getPublicKey().getAlgorithm();
            if(algo == null){
                algo = "RSA"; // token only supports RSA for now
            }
            return algo;
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }


    @Override
    public void deleteCertificateFromKeystore(final String certAlias) throws CryptoException {
        try {
            //SystemClock.sleep(1000);
            mKeyStore.deleteEntry(certAlias);
            Log.w(TAG, "Deleted keystore entry: " + certAlias);
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public byte[] decryptWithPrivKey(String certAlias, byte[] msgToDecrypt) throws CryptoException {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry(certAlias, null);
            PrivateKey privKey = privateKeyEntry.getPrivateKey();
            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            inCipher.init(Cipher.DECRYPT_MODE, privKey);
            byte[] decBytes = inCipher.doFinal(msgToDecrypt);
            return decBytes;
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override

    public byte[] encryptWithPrivKey(String certAlias, byte[] msgToEncrypt) throws CryptoException {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry(certAlias, null);
            PrivateKey privKey = privateKeyEntry.getPrivateKey();
            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            inCipher.init(Cipher.DECRYPT_MODE, privKey);
            byte[] encBytes = inCipher.doFinal(msgToEncrypt);
            return encBytes;
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public byte[] sign(String certAlias, String algo, byte[] msgToSign) throws CryptoException {
        try {
            // try to get it from craytoken first
            try {
                if (mCraytokenSmartCard == null)
                    mCraytokenSmartCard = new CrayonicIDCard(mCardTerminal);  // this already connects to BLE device

                if (mCraytokenSmartCard.getEncryptionAlgorithm().equalsIgnoreCase("RSA")) {
                    // do the PKCS1 signature externaly
                    MessageDigest digest = MessageDigest.getInstance("SHA256");
                    byte[] hash = digest.digest();
                    // encrypt with RSA in the cryptoki (not really sign)
                    byte[] signature = mCraytokenSmartCard.sign(hash, "SHA256");  //d.getEncoded()
                    // after signing we close
                    mCraytokenSmartCard.disconnect();
                    mCraytokenSmartCard = null;
                    return signature;
                }
            }catch (Exception e){
                // fallback to internal key store
                KeyStore.Entry entry = mKeyStore.getEntry(certAlias, null);
                if(entry != null && entry instanceof KeyStore.PrivateKeyEntry) {
                    Signature s = Signature.getInstance(algo); //algo - > "SHA256withRSA"
                    s.initSign(((KeyStore.PrivateKeyEntry) entry).getPrivateKey());
                    s.update(msgToSign);
                    byte[] signature = s.sign();
                    return signature;
                }else{
                    throw new CryptoException("Did not find signing private key for alias: " + certAlias);
                }
            }
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
        throw new CryptoException("Could not sign with cert alias: " + certAlias);
    }

    @Override
    public boolean verifySignature(String certAlias, String algo, byte[] msg, byte[] externalSignature) throws CryptoException {
        try {
            KeyStore.Entry entry = mKeyStore.getEntry(certAlias, null);
            if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
                Log.w(TAG, "Not an instance of a PrivateKeyEntry");
                return false;
            }
            Signature s = Signature.getInstance(algo);
            s.initVerify(((KeyStore.PrivateKeyEntry) entry).getCertificate());
            s.update(msg);
            boolean valid = s.verify(externalSignature);
            return valid;
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public byte[] encryptWithPubKey(String certAlias, byte[] msgToEncrypt) throws CryptoException {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry(certAlias, null);
            PublicKey publicKey = privateKeyEntry.getCertificate().getPublicKey();
            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            inCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] encBytes = inCipher.doFinal(msgToEncrypt);

// No need for stream cipher .....           ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//            CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inCipher);
//            cipherOutputStream.write(msgToEncrypt);
//            cipherOutputStream.close();
//            byte[] encBytes = outputStream.toByteArray();
            return encBytes;
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public byte[] decryptWithPubKey(String certAlias, byte[] msgToDecrypt) throws CryptoException {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry(certAlias, null);
            PublicKey publicKey = privateKeyEntry.getCertificate().getPublicKey();
            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            inCipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] decBytes = inCipher.doFinal(msgToDecrypt);
            return decBytes;
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }

    }

    @Override
    public boolean isAvailable()  {
        // some unit test of crypto engine that will create key pair and sign test message and verify signature
        if(mKeyStore.getProvider() != null)     // quick test (below are slow tests...)
            return true;
        else{
            Log.wtf(TAG, "cannot get keystore provider: "  + mKeyStore.getType());

        }

        // try deleting test key first
        try {
            mKeyStore.deleteEntry("testAlias");

        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        byte[] testmsg = "test message".getBytes();
        boolean valid = false;
        ContentSigner signer = null;
        String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/testAlias";

        try {
            Certificate[] chain = PKIUtils.loadCertificateChainFromPEM(new FileReader(filePath + ".pem"));
            if (chain != null) {
                // test to replace certificate if any present
                KeyStore.PrivateKeyEntry existingPrivateKeyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry("testAlias", null);
                KeyStore.PrivateKeyEntry newEntry = new KeyStore.PrivateKeyEntry(existingPrivateKeyEntry.getPrivateKey(), chain);
                mKeyStore.setEntry("testAlias", newEntry, null);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }


        try {
            // create new key and PKCS10 CSR
            byte[] csr = createPKCS10CSR("testAlias", "CN=testUser");
            FileUtilsPlus.saveBinaryFile(filePath + ".csr", csr);


        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            //  KeyPair kp = createNewKeyPair("testAlias", "CN=test");
            //kp.getPublic().getEncoded();

            //  KeyStore.PrivateKeyEntry existingPrivateKeyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry("testAlias", null);
            PrivateKey privateKey = (PrivateKey) mKeyStore.getKey("testAlias", null);
            //   KeyStore.PrivateKeyEntry existingPrivateKeyEntry2 = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry("testAlias2", null);
            //   PrivateKey privateKey2 = (PrivateKey) mKeyStore.getKey("testAlias2", null);

            JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA256withRSA");
            signer = csBuilder.build(privateKey);
//            AlgorithmIdentifier algo = signer.getAlgorithmIdentifier();
            OutputStream os = signer.getOutputStream();
            os.write(testmsg);
            byte[] signature = signer.getSignature();
            valid = verifySignature("testAlias", "SHA256withRSA", testmsg, signature); // we should get algorithm string somehow from algo above
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (signer != null) {
//            // delete test key
//            try {
//                mKeyStore.deleteEntry("testAlias");
//            } catch (KeyStoreException e) {
//                e.printStackTrace();
//            }
//        }

        // try deleting test key again since its created above
        try {
            mKeyStore.deleteEntry("testAlias");

        } catch (KeyStoreException e) {
            e.printStackTrace();
        }


        // also test loading private key from .P12 file
    /*    try {
            KeyStore ks = KeyStore.getInstance("pkcs12");
            ks.load(mContext.getAssets().open("cert/certificate.pfx"), "test".toCharArray()); // load PFX file into temp PKCS12 keystore
            String alias = ks.aliases().nextElement();

            PrivateKey pKey = (PrivateKey) ks.getKey(alias, "test".toCharArray());
            Certificate[] certChain = ks.getCertificateChain(alias);
            // Log.e("TAG", "Key format:" + pKey.getFormat());
            //mKeyStore.setEntry(alias, ks.getEntry(alias, null), null);
            mKeyStore.setEntry("testAlias",
                    new KeyStore.PrivateKeyEntry(pKey, certChain),
                    new KeyProtection.Builder(KeyProperties.PURPOSE_SIGN)
                            .setDigests(KeyProperties.DIGEST_SHA256)
                            .setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PKCS1)
                                    // Only permit this key to be used if the user
                                    // authenticated within the last ten minutes.
                            .setUserAuthenticationRequired(true)
                            .setUserAuthenticationValidityDurationSeconds(10 * 60)
                            .build());
            //sign(alias,"SHA256withRSA", hashToEncrypt);

            JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA256withRSA");
            // csBuilder.setProvider("AndroidOpenSSL");
            signer = csBuilder.build((PrivateKey) mKeyStore.getKey("testAlias", null));  //.getPrivate());

            AlgorithmIdentifier algo = signer.getAlgorithmIdentifier();
            OutputStream os = signer.getOutputStream();
            os.write(testmsg);
            byte[] signature = signer.getSignature();
            Log.e(TAG, algo.toString());
            valid = verifySignature("testAlias", "SHA256withRSA", testmsg, signature); // we should get algorithm string somehow from algo above

            if (signer != null) {
                // delete test key
                try {
                    mKeyStore.deleteEntry("testAlias");
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
*/


        return valid;
    }

    @Override
    public Certificate signCSR(String certAlias, PKCS10CertificationRequest csr, String certSigningAlias) throws CryptoException {
        try {
            Certificate[] certs = mKeyStore.getCertificateChain(certSigningAlias);
            X509Certificate[] chain = Arrays.copyOf(certs, certs.length, X509Certificate[].class);

            // X509Certificate[] chain = (X509Certificate[])certs;
            X509Certificate cacert = chain[0];

            X500Name issuer = new X500Name(cacert.getSubjectX500Principal().getName());
            BigInteger serial = new BigInteger(32, new SecureRandom());
            Calendar start = Calendar.getInstance();
            start.add(Calendar.HOUR_OF_DAY, -1);   //valid one hour before
            Calendar end = Calendar.getInstance();
            end.add(Calendar.YEAR, 1); // valid for one year


            X509v3CertificateBuilder certgen = new X509v3CertificateBuilder(issuer, serial, start.getTime(), end.getTime(), csr.getSubject(), csr.getSubjectPublicKeyInfo());
            certgen.addExtension(X509Extension.basicConstraints, false, new BasicConstraints(false));

            DigestCalculator digCalc = new BcDigestCalculatorProvider().get(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1));
            X509ExtensionUtils x509ExtensionUtils = new X509ExtensionUtils(digCalc);
            certgen.addExtension(X509Extension.subjectKeyIdentifier, false, x509ExtensionUtils.createSubjectKeyIdentifier(csr.getSubjectPublicKeyInfo()));
            certgen.addExtension(X509Extension.authorityKeyIdentifier, false, new AuthorityKeyIdentifier(new GeneralNames(new GeneralName(new X500Name(cacert.getSubjectX500Principal().getName()))), cacert.getSerialNumber()));

            // no need here for custom content signer if its part of android crypto provider - use standard content signer
            // ContentSigne  signer = new CustomContentSigner(this, certSigningAlias, "SHA1withRSA"); //BcRSAContentSignerBuilder(sigAlgId, digAlgId).build(PrivateKeyFactory.createKey(cakey.getEncoded()));
            JcaContentSignerBuilder signerBuilder = new JcaContentSignerBuilder("SHA1withRSA");
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry(certSigningAlias, null);
            PrivateKey privKey = privateKeyEntry.getPrivateKey();
            ContentSigner signer = signerBuilder.build(privKey);
            X509CertificateHolder holder = certgen.build(signer);


            // create PKCS7 message for sending message e.g. via email
            /*
            byte[] certencoded = holder.toASN1Structure().getEncoded(); // create certificate according to CSR and sign it
            CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
            signer = new AndroidKeystoreContentSigner();; //new JcaContentSignerBuilder("SHA2withRSA").build(cakey);
            generator.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().build()).build(signer, cacert));
            Store certs = new JcaCertStore(Arrays.asList(chain));
            generator.addCertificates(certs);
            CMSTypedData content = new CMSProcessableByteArray(certencoded);
            CMSSignedData signeddata = generator.generate(content, true);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            out.write("-----BEGIN PKCS #7 SIGNED DATA-----\n".getBytes("ISO-8859-1"));
            out.write(Base64.encode(signeddata.getEncoded(), Base64.DEFAULT));
            out.write("\n-----END PKCS #7 SIGNED DATA-----\n".getBytes("ISO-8859-1"));
            out.close();
            return new String(out.toByteArray(), "ISO-8859-1");
            */

            // replace CSR (temp cert) in key store with newly singed Cert
            X509Certificate signedCert = new JcaX509CertificateConverter().getCertificate(holder);
            chain[0] = signedCert; // update certificate
            KeyStore.PrivateKeyEntry keyEntry = (KeyStore.PrivateKeyEntry) mKeyStore.getEntry(certAlias, null);  //FIXME due to keystore bug this will not work to replace cert with new one
            mKeyStore.setKeyEntry(certAlias, keyEntry.getPrivateKey(), null, chain);
            //KeyStore.Entry entry = mKeyStore.getEntry(certAlias, null);


            return signedCert;
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }

    }

    //FIXME - NOT USED YET (still HTTPServices method is used instead)
    @Override
    public void writeCertificate(String certAlias,  byte[] crt) throws CryptoException {
        try {
            KeyStore.PrivateKeyEntry existingPrivateKeyEntry = (KeyStore.PrivateKeyEntry) Sigma.applicationKeyStore.getEntry(certAlias, null);
            if (existingPrivateKeyEntry != null) { // confirm existance of private key for given alias
                File certificatesFile = new File(Sigma.getCertPath(), Sigma.applicationProperties.getProperty("signing_pinned_cert_chain"));
                InputStream is = new FileInputStream(certificatesFile);
                String pemChain = ByteStreams.convertStreamToString(is);
                StringReader sr = new StringReader(pemChain);
                Certificate[] chain = PKIUtils.loadCertificateChainFromPEM(sr);
                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
                InputStream in = new ByteArrayInputStream(crt);
                X509Certificate newCertificate = (X509Certificate)certFactory.generateCertificate(in);
                Certificate[] fullchain = new Certificate[chain.length + 1];
                fullchain[0] = newCertificate;
                for (int i = 0; i < chain.length; i++) {
                    fullchain[i+1] = chain[i];
                }
                Sigma.applicationKeyStore.setKeyEntry(certAlias, existingPrivateKeyEntry.getPrivateKey(), null, fullchain);
            }
        }catch (Exception e){
            throw new CryptoException(e.getMessage());
        }
    }

    //old method for storing
    private boolean storeCertificate(String jsonResponse, String csrAlias) {
        // assuming PEM  file in JSON response with correct alias
        try {
            JSONObject jObj = new JSONObject(jsonResponse);
            if (!jObj.has("csralias")) {
                Log.e(TAG, "No 'csralias' name in JSON response to CSR request");
                return false;
            }

            if (!jObj.has("certificate")) {
                Log.e(TAG, "No 'certificate' name in JSON response to CSR request");
                return false;
            }

            String certAlias = jObj.getString("csralias");
            if(!certAlias.equalsIgnoreCase(csrAlias)){
                Log.e(TAG, "No 'csralias' does not match alias in JSON response to CSR request");
                return false;

            }
            String pemcert = jObj.getString("certificate"); //this should be the whole chain, but proxy can give only one cert

            File certificatesFile = new File(Sigma.getCertPath(), Sigma.applicationProperties.getProperty("signing_pinned_cert_chain"));
            String pemChain = prependPemCertToPemChain(pemcert, certificatesFile); //...so we append the rest with assets pem

            StringReader sr = new StringReader(pemChain);
            Certificate[] chain = PKIUtils.loadCertificateChainFromPEM(sr);
            // parse PKCS7 message to get chain - does not work with EJBCA - dunno why! //FIXME ideally this should work better then PEM chain....
/*                       byte[] pkcs7SignatureMessage = Base64.decode(PKCS7Message.getBytes());
                        Security.addProvider(new BouncyCastleProvider());
                        CMSSignedData s = new CMSSignedData(pkcs7SignatureMessage);
                        Store                   certStore = s.getCertificates();
                        SignerInformationStore signers = s.getSignerInfos();
                        Collection              c = signers.getSigners();
                        Iterator it = c.iterator();
                        while (it.hasNext())
                        {
                            SignerInformation   signer = (SignerInformation)it.next();
                            Collection          certCollection = certStore.getMatches(signer.getSID());
                            Iterator              certIt = certCollection.iterator();
                            X509CertificateHolder cert = (X509CertificateHolder)certIt.next();
                            if (signer.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(cert))){
                                Log.e(TAG, "Verified " + cert.getIssuer().toString());
                            }
                        }
*/
            // ... and store it in key store.. replacing old selfsigned cert
            KeyStore.PrivateKeyEntry existingPrivateKeyEntry = (KeyStore.PrivateKeyEntry) Sigma.applicationKeyStore.getEntry(certAlias, null);
            if (existingPrivateKeyEntry != null) { // confirm existance of private key for given alias
                Certificate[] chain2 = Sigma.applicationKeyStore.getCertificateChain(certAlias); // first try if there is signed cert available
                for(int i = 0; i < chain2.length; i++){
                    X509Certificate x509Certificate = (X509Certificate) chain2[i];
                    Log.i(TAG, "Key store entry before signed CSR:\n" +
                            "Certificate chain item "  + i + " is: " + x509Certificate.getSubjectDN().getName());
                }
                Sigma.applicationKeyStore.setKeyEntry(certAlias , existingPrivateKeyEntry.getPrivateKey() , null, chain);
                chain2 = Sigma.applicationKeyStore.getCertificateChain(certAlias);
                if(chain2.length != chain.length){
                    // sanity check (e.g. setEntry does not work)
                    Log.wtf(TAG, "Could not replace old certificate with the new one!!!!");
                    return false;
                }
                for(int i = 0; i < chain2.length; i++){
                    X509Certificate x509Certificate = (X509Certificate) chain2[i];
                    Log.i(TAG, "Key store entry with signed cert. and full chain\n" +
                            "Certificate chain item "  + i + " is: " + x509Certificate.getSubjectDN().getName());
                }
            } else {
                Log.wtf(TAG, "Lost private key for certificate " + certAlias);
                return false;
            }
        } catch (Exception e) {
            Log.e(TAG, "Bad response from csr upload url: " + jsonResponse);
            //Toast.makeText(mContext, "Failed to obtain identity from server.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    private String prependPemCertToPemChain(String pemcert, File certsFile) throws IOException {
        InputStream is = new FileInputStream(certsFile);
        String pemChain = ByteStreams.convertStreamToString(is);
        return pemcert  + "\n" + pemChain;
    }

}
