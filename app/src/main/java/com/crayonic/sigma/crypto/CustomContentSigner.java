package com.crayonic.sigma.crypto;


import org.spongycastle.asn1.x509.AlgorithmIdentifier;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * Created by kolarov on 2.10.2015.
 * <p/>
 * This is a proxy class to bouncy castle content signing that allows us to use bouncy castle algorithms for signing CSR or such but with
 * actual implementation of signing being one of our Crypto Providers
 */
public class CustomContentSigner implements ContentSigner {

    // private static final String ALGORITHM = "SHA1withRSA"; // signing algorithm for CSR (consider change to SHA256withRSA)
    private String algorithm;
    private ByteArrayOutputStream baos;
    private CryptoProvider cp;
    private String mPrivateKeyCertAlias;


    public CustomContentSigner(CryptoProvider cp, String certAlias, String algorithm) {
        this.cp = cp;
        this.mPrivateKeyCertAlias = certAlias;
        this.baos = new ByteArrayOutputStream();
        this.algorithm = algorithm;
    }

    @Override
    public AlgorithmIdentifier getAlgorithmIdentifier() {
        return new DefaultSignatureAlgorithmIdentifierFinder().find(algorithm);
    }

    @Override
    public OutputStream getOutputStream() {
        return baos;
    }

    @Override
    public byte[] getSignature() {
        byte[] bytes = baos.toByteArray();
        byte[] signed = null;
        baos.reset(); // reset for the next getSignature operation
        try {
            signed = cp.sign(mPrivateKeyCertAlias, algorithm, bytes);
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        return signed;
    }
}