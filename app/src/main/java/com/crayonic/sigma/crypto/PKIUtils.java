package com.crayonic.sigma.crypto;

import android.util.Log;

import org.spongycastle.cert.X509CertificateHolder;
import org.spongycastle.cert.jcajce.JcaX509CertificateConverter;
import org.spongycastle.crypto.CryptoException;
import org.spongycastle.jce.provider.X509CertificateObject;
import org.spongycastle.openssl.PEMParser;// == from SC version1.50
//import org.spongycastle.openssl.PEMReader; // for 1.47

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.util.ArrayList;


public class PKIUtils {
    public static final int MinPassPhraseLength = 4;
    private static final String TAG = "PKIUtils";


    public static Certificate[] loadCertificateChainFromPEM(Reader reader) throws CryptoException {
        ArrayList<Certificate> chain = new ArrayList<>(1);
        PEMParser pemReader = new PEMParser(reader); //-- TODO cannot use this new version yet since we have old BC/SC libs due to iText
        //PEMReader pemReader = new PEMReader(reader);

        //JcaX509CertificateConverter certconv = new JcaX509CertificateConverter().setProvider("BC");

        try {
            Object obj;
            while ((obj = pemReader.readObject()) != null) {
                if (obj instanceof X509CertificateObject) {
                    X509Certificate cert = (X509Certificate)  obj; //parsePemObject(obj, certconv);
                    Log.i(TAG, "read cert: " + cert.toString());
                    chain.add(cert);
                }else
                if(obj instanceof X509CertificateHolder){
                    try {
                        X509Certificate cert = new JcaX509CertificateConverter().setProvider( "BC" ).getCertificate( (X509CertificateHolder)obj );
                        Log.i(TAG, "read cert: " + cert.toString());
                        chain.add(cert);
                    } catch (CertificateException e) {
                        Log.e(TAG, "Cannot convert BC cert to Java cert!");
                        e.printStackTrace();
                    }
                }
                else{
                    Log.w(TAG, "Pem reader found this object in reader: " + obj.getClass().getName());
                }

            }
            Certificate[] certs = new Certificate[chain.size()];
            return  chain.toArray(certs);
        } catch (IOException e) {
            throw new CryptoException(e.getMessage());
        }
    }


    /*public static List<?> readPemObjects(InputStream is, final String pphrase)
            throws IOException {
        List<Object> list = new LinkedList<Object>();
        //PEMParser pr2 = new PEMParser(new InputStreamReader(is));
        PEMReader pr2 = new PEMReader(new InputStreamReader(is));

        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        JcaX509CertificateConverter certconv = new JcaX509CertificateConverter().setProvider("BC");

        while (true) {
            Object o = pr2.readObject();
            if (null == o)
                break; // done

            list.add(parsePemObject(o, pphrase, converter, certconv));
        }
        return list;
    }
*/
    private static Object parsePemObject(Object o,  /*final String pphrase, JcaPEMKeyConverter converter,*/ JcaX509CertificateConverter certconv) {

          /*  if (o instanceof PEMEncryptedKeyPair) {
                o = ((PEMEncryptedKeyPair) o).decryptKeyPair(new JcePEMDecryptorProviderBuilder().build(pphrase.toCharArray()));
            } else if (o instanceof PKCS8EncryptedPrivateKeyInfo) {
                InputDecryptorProvider pkcs8decoder = new JceOpenSSLPKCS8DecryptorProviderBuilder().build(pphrase.toCharArray());
                o = converter.getPrivateKey(
                        ((PKCS8EncryptedPrivateKeyInfo) o).decryptPrivateKeyInfo(pkcs8decoder));
            }
        } catch (Throwable t) {
            throw new RuntimeException("Failed to decode private key", t);
        }
*/
       /* if (o instanceof PEMKeyPair) {
            try {
                return converter.getKeyPair((PEMKeyPair) o);
            } catch (PEMException e) {
                throw new RuntimeException("Failed to construct public/private key pair", e);
            }
        } else*/ if (o instanceof RSAPrivateCrtKey) {
            RSAPrivateCrtKey pk = (RSAPrivateCrtKey) o;
            return pk; //makeKeyPair(pk);
        } else if (o instanceof X509CertificateHolder) {
            try {
                return certconv.getCertificate((X509CertificateHolder) o);
            } catch (Exception e) {
                throw new RuntimeException("Failed to read X509 certificate", e);
            }
        } else {
            // catchsink, should check for certs and reject rest?
            System.out.println("generic case  type " + o.getClass().getName());
            return o;
        }
    }

    /**
     * Writes certificate or private key in PEM format into given stream Private
     * key will be encrypted using 3des cipher with given password(password must
     * be 4+ long).
     *
     * @param w        {@link Writer}
     * @param obj      instance of {@link PrivateKey}
     * @param password password
     * @throws IOException
     */
   /* public static void writePem(Writer w, Object obj, String password)
            throws IOException {
        writePem(w, obj, password, true);
    }

    static void writePem(Writer w, Object obj, String password, boolean closeStream)
            throws IOException {
        PEMWriter pw = null;
        try {
            pw = new PEMWriter(w);
            if (obj instanceof PrivateKey) {
                if (null == password || password.length() < MinPassPhraseLength)
                    throw new IllegalArgumentException("Private key encryption password must be no less than " +
                            MinPassPhraseLength + " characters");
                PEMEncryptor encryptor = new JcePEMEncryptorBuilder("DES-EDE3-CBC").setProvider("BC").build(password.toCharArray());
                pw.writeObject(obj, encryptor);
            } else
                pw.writeObject(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (null != pw && closeStream)
                pw.close();
        }
    }*/
}