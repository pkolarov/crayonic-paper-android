package com.crayonic.sigma.crypto.craytoken;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import com.crayonic.sigma.Sigma;
import com.crayonic.smartcardio.Card;
import com.crayonic.smartcardio.CardChannel;
import com.crayonic.smartcardio.CardException;
import com.crayonic.smartcardio.CardTerminal;
import com.crayonic.smartcardio.CommandAPDU;
import com.crayonic.smartcardio.ResponseAPDU;

import java.nio.ByteBuffer;
import java.nio.ReadOnlyBufferException;
import java.nio.charset.Charset;

/**
 * Created by peter on 07/02/16.
 */
public class CraytokenChannel extends CardChannel implements ResponseListener {


    private static final String TAG = "CraytokenChannel";

    private static final long CHECK_IF_CONNECTED_INTERVAL = 200;

    private final Craytoken mCraytoken;
    private ByteBuffer mRXBuffer; //  buffer for recieving response from the card channel



    private final BLEStack mBLE;

    public CraytokenChannel(Craytoken craytoken) {
        this.mCraytoken = craytoken;
        mBLE = mCraytoken.getBLEStack();

    }

    /**
     * Returns the Card this channel is associated with.
     *
     * @return the Card this channel is associated with
     */
    @Override
    public Card getCard() {
        return mCraytoken;
    }

    /**
     * Returns the channel number of this CardChannel. A channel number of
     * 0 indicates the basic logical channel.
     *
     * @return the channel number of this CardChannel.
     * @throws IllegalStateException if this channel has been
     *                               {@linkplain #close closed} or if the corresponding Card has been
     *                               {@linkplain Card#disconnect disconnected}.
     */
    @Override
    public int getChannelNumber() {
        return 0;
    }

    /**
     * Transmits the specified command APDU to the Smart Card and returns the
     * response APDU.
     * <p/>
     * <p>The CLA byte of the command APDU is automatically adjusted to
     * match the channel number of this CardChannel.
     * <p/>
     * <p>Note that this method cannot be used to transmit
     * <code>MANAGE CHANNEL</code> APDUs. Logical channels should be managed
     * using the {@linkplain Card#openLogicalChannel} and {@linkplain
     * CardChannel#close CardChannel.close()} methods.
     * <p/>
     * <p>Implementations should transparently handle artifacts
     * of the transmission protocol.
     * For example, when using the T=0 protocol, the following processing
     * should occur as described in ISO/IEC 7816-4:
     * <p/>
     * <ul>
     * <li><p>if the response APDU has an SW1 of <code>61</code>, the
     * implementation should issue a <code>GET RESPONSE</code> command
     * using <code>SW2</code> as the <code>Le</code>field.
     * This process is repeated as long as an SW1 of <code>61</code> is
     * received. The response body of these exchanges is concatenated
     * to form the final response body.
     * <p/>
     * <li><p>if the response APDU is <code>6C XX</code>, the implementation
     * should reissue the command using <code>XX</code> as the
     * <code>Le</code> field.
     * </ul>
     * <p/>
     * <p>The ResponseAPDU returned by this method is the result
     * after this processing has been performed.
     *
     * @param command the command APDU
     * @return the response APDU received from the card
     * @throws IllegalStateException    if this channel has been
     *                                  {@linkplain #close closed} or if the corresponding Card has been
     *                                  {@linkplain Card#disconnect disconnected}.
     * @throws IllegalArgumentException if the APDU encodes a
     *                                  <code>MANAGE CHANNEL</code> command
     * @throws NullPointerException     if command is null
     * @throws CardException            if the card operation failed
     */
    @Override
    public ResponseAPDU transmit(CommandAPDU command) throws CardException {
        if (command == null)
            throw new NullPointerException("APDU Command cannot be null!");

//        if (!mCraytoken.isConnected()) {
//            if (mCraytoken.connect()) {  // connect now
//                try {
//                    Thread.sleep(50);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
////                long timeout = System.currentTimeMillis() + CardTerminal.CONNECT_TIMEOUT;
////                while (!mCraytoken.isConnected() || timeout > System.currentTimeMillis()) {
////                    try {
////                        Thread.sleep(10);
////                    } catch (InterruptedException e) {
////                        e.printStackTrace();
////                    }
////                }
////                if (!mCraytoken.isConnected())
////                    throw new IllegalStateException("Card not connected!");
//            } else
//                throw new IllegalStateException("Cannot reconnect card!");
//        }
        mRXBuffer = ByteBuffer.allocate(command.getNe() + 2); // create buffer according to expected return response

//        BLEStack bleStack = mCraytoken.getBLEStack();
//        bleStack.setResponseListener(this);
        mBLE.setResponseListener(this);
        mBLE.transmitBytes(command.getBytes());

        byte[] response = getReceivedBytes();
        while (response == null && !mBLE.hasTimeout() ) { //
            try {
                Thread.sleep(30); // block until we have back result or timeout occurs
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            response = getReceivedBytes();
        }

        if (response == null) {
            Log.e(TAG, "No APDU status response within time limit - returning null");
            return null;
        }

        ResponseAPDU rapdu = new ResponseAPDU(response);
        return rapdu;
    }

    /**
     * Transmits the command APDU stored in the command ByteBuffer and receives
     * the response APDU in the response ByteBuffer.
     * <p/>
     * <p>The command buffer must contain valid command APDU data starting
     * at <code>command.position()</code> and the APDU must be
     * <code>command.remaining()</code> bytes long.
     * Upon return, the command buffer's position will be equal
     * to its limit; its limit will not have changed. The output buffer
     * will have received the response APDU bytes. Its position will have
     * advanced by the number of bytes received, which is also the return
     * value of this method.
     * <p/>
     * <p>The CLA byte of the command APDU is automatically adjusted to
     * match the channel number of this CardChannel.
     * <p/>
     * <p>Note that this method cannot be used to transmit
     * <code>MANAGE CHANNEL</code> APDUs. Logical channels should be managed
     * using the {@linkplain Card#openLogicalChannel} and {@linkplain
     * CardChannel#close CardChannel.close()} methods.
     * <p/>
     * <p>See {@linkplain #transmit transmit()} for a discussion of the handling
     * of response APDUs with the SW1 values <code>61</code> or <code>6C</code>.
     *
     * @param command  the buffer containing the command APDU
     * @param response the buffer that shall receive the response APDU from
     *                 the card
     * @return the length of the received response APDU
     * @throws IllegalStateException    if this channel has been
     *                                  {@linkplain #close closed} or if the corresponding Card has been
     *                                  {@linkplain Card#disconnect disconnected}.
     * @throws NullPointerException     if command or response is null
     * @throws ReadOnlyBufferException  if the response buffer is read-only
     * @throws IllegalArgumentException if command and response are the
     *                                  same object, if <code>response</code> may not have
     *                                  sufficient space to receive the response APDU
     *                                  or if the APDU encodes a <code>MANAGE CHANNEL</code> command
     * @throws CardException            if the card operation failed
     */
    @Override
    public int transmit(ByteBuffer command, ByteBuffer response) throws CardException {
        if (response.isReadOnly()) {
            throw new ReadOnlyBufferException();
        }

        if (!mCraytoken.isConnected()) {
            if (mCraytoken.connect()) {  // connect now
                long timeout = System.currentTimeMillis() + CardTerminal.CONNECT_TIMEOUT;
                while (!mCraytoken.isConnected() || timeout > System.currentTimeMillis()) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!mCraytoken.isConnected())
                    throw new IllegalStateException("Card not connected!");
            } else
                throw new IllegalStateException("Cannot reconnect card!");
        }

        BLEStack bleStack = mCraytoken.getBLEStack();
        bleStack.setResponseListener(this);
        mRXBuffer = response;
        bleStack.transmitBBuffer(command);

        // wait until response buffer fills to expected limit or timeouts
        while ((response.position() < response.limit()) && !bleStack.hasTimeout()) {
            try {
                Thread.sleep(10); // block until we have back result or timeout occurs
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (bleStack.hasTimeout()) {
            Log.e(TAG, "No APDU response according to expected response buffer size within time limit");
        }

        return response.limit();
    }

    /**
     * Closes this CardChannel. The logical channel is closed by issuing
     * a <code>MANAGE CHANNEL</code> command that should use the format
     * <code>[xx 70 80 0n]</code> where <code>n</code> is the channel number
     * of this channel and <code>xx</code> is the <code>CLA</code>
     * byte that encodes this logical channel and has all other bits set to 0.
     * After this method returns, calling other
     * methods in this class will raise an IllegalStateException.
     * <p/>
     * <p>Note that the basic logical channel cannot be closed using this
     * method. It can be closed by calling {@link Card#disconnect}.
     *
     * @throws CardException         if the card operation failed
     * @throws IllegalStateException if this CardChannel represents a
     *                               connection the basic logical channel
     */
    @Override
    public void close() throws CardException {
        mCraytoken.disconnect(false);
    }

    @Override
    public void writeDataToResponseBuffer(byte[] data) {
    //    if((mRXBuffer.position() + data.length) <= mRXBuffer.limit()) {
            mRXBuffer.put(data);
   //     }else {
   //         Log.e(TAG, "APDU RX buffer would overflow!!!!");
   //     }
        Log.i(TAG, "APDU RX buffer state: Cap: " + mRXBuffer.capacity() +
                " Pos:" + mRXBuffer.position());
    }


    // returns null unless ALL expected bytes arrived in response!
    public byte[] getReceivedBytes() {
        if (mRXBuffer.position() < mRXBuffer.limit())
            return null;  // did not fill buffer to expected size so return null until timeout

        mRXBuffer.flip(); // if it is full switch to READ mode
        byte[] out = new byte[mRXBuffer.limit()];
        mRXBuffer.get(out);
        return out;
    }





}
