package com.crayonic.sigma.crypto.craytoken;

/**
 * Created by peter on 15/02/2017.
 */
public interface LineResponseListener {
    void writeLine(String line);

}
