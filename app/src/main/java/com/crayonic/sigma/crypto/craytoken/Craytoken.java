package com.crayonic.sigma.crypto.craytoken;


import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanRecord;
import android.util.Log;

import com.crayonic.smartcardio.ATR;
import com.crayonic.smartcardio.Card;
import com.crayonic.smartcardio.CardChannel;
import com.crayonic.smartcardio.CardPermission;
import com.crayonic.smartcardio.CardException;

/**
 * Created by peter on 06/02/16.
 */
public class Craytoken extends Card {
    private static final String TAG = "Craytoken";

    private BLEStack bleStack;
    BluetoothDevice device;
    int rssi;
    ScanRecord scanRecord;



    public Craytoken(BluetoothDevice device, int rssi,  ScanRecord scanRecord) {
        this.device = device;
        this.rssi = rssi;
        this.scanRecord = scanRecord;
    }




    public void setBLEStack(BLEStack stack){
        bleStack = stack;
    }

    public BLEStack getBLEStack(){
        return bleStack;
    }

    public String getMAC() {
        return device.getAddress();
    }

    public boolean isConnected() {
        return bleStack.isConnected();
    }

    public boolean connect() {
        return bleStack.connectSelectedPen();
    }


/**********************CARD API****************************************************/
    /**
     * Returns the ATR of this card.
     *
     * @return the ATR of this card.
     */
    @Override
    public ATR getATR() {
        return new ATR("Hello".getBytes());  //FIXME
    }

    /**
     * Returns the protocol in use for this card.
     *
     * @return the protocol in use for this card, for example "T=0" or "T=1"
     */
    @Override
    public String getProtocol() {
        return "T=1";
    }

    /**
     * Returns the CardChannel for the basic logical channel. The basic
     * logical channel has a channel number of 0.
     *
     * @return the CardChannel for the basic logical channel
     * @throws SecurityException     if a SecurityManager exists and the
     *                               caller does not have the required
     *                               {@linkplain CardPermission permission}
     * @throws IllegalStateException if this card object has been disposed of
     *                               via the {@linkplain #disconnect disconnect()} method
     */
    @Override
    public CardChannel getBasicChannel() {
        return new CraytokenChannel(this);
    }

    /**
     * Opens a new logical channel to the card and returns it. The channel is
     * opened by issuing a <code>MANAGE CHANNEL</code> command that should use
     * the format <code>[00 70 00 00 01]</code>.
     *
     * @return the logical channel which has been opened
     * @throws SecurityException     if a SecurityManager exists and the
     *                               caller does not have the required
     *                               {@linkplain CardPermission permission}
     * @throws CardException         is a new logical channel could not be opened
     * @throws IllegalStateException if this card object has been disposed of
     *                               via the {@linkplain #disconnect disconnect()} method
     */
    @Override
    public CardChannel openLogicalChannel() throws CardException {
        throw new CardException("NA");
    }

    /**
     * Requests exclusive access to this card.
     * <p/>
     * <p>Once a thread has invoked <code>beginExclusive</code>, only this
     * thread is allowed to communicate with this card until it calls
     * <code>endExclusive</code>. Other threads attempting communication
     * will receive a CardException.
     * <p/>
     * <p>Applications have to ensure that exclusive access is correctly
     * released. This can be achieved by executing
     * the <code>beginExclusive()</code> and <code>endExclusive</code> calls
     * in a <code>try ... finally</code> block.
     *
     * @throws SecurityException     if a SecurityManager exists and the
     *                               caller does not have the required
     *                               {@linkplain CardPermission permission}
     * @throws CardException         if exclusive access has already been set
     *                               or if exclusive access could not be established
     * @throws IllegalStateException if this card object has been disposed of
     *                               via the {@linkplain #disconnect disconnect()} method
     */
    @Override
    public void beginExclusive() throws CardException {
        throw new CardException("Not implemented");
    }

    /**
     * Releases the exclusive access previously established using
     * <code>beginExclusive</code>.
     *
     * @throws SecurityException     if a SecurityManager exists and the
     *                               caller does not have the required
     *                               {@linkplain CardPermission permission}
     * @throws IllegalStateException if the active Thread does not currently have
     *                               exclusive access to this card or
     *                               if this card object has been disposed of
     *                               via the {@linkplain #disconnect disconnect()} method
     * @throws CardException         if the operation failed
     */
    @Override
    public void endExclusive() throws CardException {
        throw new CardException("Not implemented");
    }

    /**
     * Transmits a control command to the terminal device.
     * <p/>
     * <p>This can be used to, for example, control terminal functions like
     * a built-in PIN pad or biometrics.
     *
     * @param controlCode the control code of the command
     * @param command     the command data
     * @return the response from the terminal device
     * @throws SecurityException     if a SecurityManager exists and the
     *                               caller does not have the required
     *                               {@linkplain CardPermission permission}
     * @throws NullPointerException  if command is null
     * @throws CardException         if the card operation failed
     * @throws IllegalStateException if this card object has been disposed of
     *                               via the {@linkplain #disconnect disconnect()} method
     */
    @Override
    public byte[] transmitControlCommand(int controlCode, byte[] command) throws CardException {
        //throw new CardException("Not implemented");
        bleStack.transmitBytes(command);
        return command;
    }

    /**
     * Disconnects the connection with this card. After this method returns,
     * calling methods on this object or in CardChannels associated with this
     * object that require interaction with the card will raise an
     * IllegalStateException.
     *
     * @param reset whether to reset the card after disconnecting.
     * @throws CardException     if the card operation failed
     * @throws SecurityException if a SecurityManager exists and the
     *                           caller does not have the required
     *                           {@linkplain CardPermission permission}
     */
    @Override
    public void disconnect(boolean reset) throws CardException {
        bleStack.disconnect();
    }

}
