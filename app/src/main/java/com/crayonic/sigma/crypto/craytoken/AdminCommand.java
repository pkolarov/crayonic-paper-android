package com.crayonic.sigma.crypto.craytoken;

/**
 * Created by peter on 14/05/16.
 */
public enum AdminCommand {
    ADMIN_BAT, ADMIN_ACCEL, ADMIN_GYRO, ADMIN_PRESS, ADMIN_LED , ADMIN_TRAIN, ADMIN_OFF, ADMIN_HWRESET,
    ADMIN_CARDRESET, ADMIN_ATR, ADMIN_PIN, ADMIN_APDU, ADMIN_CSR;// BINARY, ENDBINARY;
}
