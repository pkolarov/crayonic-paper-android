package com.crayonic.sigma.crypto;

/**
 * Created by kolarov on 26.9.2015.
 */
public class CryptoException extends Exception {

    public CryptoException(String message){
        super(message);
    }
}
