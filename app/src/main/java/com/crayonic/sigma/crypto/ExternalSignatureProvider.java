package com.crayonic.sigma.crypto;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.crayonic.sigma.services.CryptoServices;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalSignature;

import org.spongycastle.pkcs.PKCS10CertificationRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.lang.ref.WeakReference;
import java.security.GeneralSecurityException;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.lang.Thread.sleep;


/**
 * Created by peter kolarov on 27.9.2015.
 * <p/>
 * This class implements iText External Signature interface and serves as a connector to external cryptographic service providers
 * for the purpose of signing digests
 * Note: Before binding to external service,
 * the signing identity in the form of Certificate Alias needs to be selected (if not principal needs to be provide for ad hoc digital signature which will be created only)
 * <p/>
 * The crypto provider classes are tried in the order from primary to tertiary (also tested for availability e.g. connected HW)
 */


public class ExternalSignatureProvider implements ExternalSignature {
    private static final String TAG = "ExtSignatureProvider";
    private static final int CRYPTO_SERVICE_TIMEOUT = 10;  // after 10 seconds the crypto operation will be considered failed
    private final Context mContext;

    private final ArrayList<String> mPrincipals;       // all possible principals to select from
    private String mPrincipal;              // DN for the new self signed cert to use for signing
    private String mCertificateAlias;       // main signing certificate (signs doc or CSR if principal is passed in)
    private String mAdHocCertificateAlias;  // possibly new certificate created by CSR and signed by mCertificateAlias
    private String mHashAlgorithm;          // this will come from a lookup  table of accepted hash algos
    private String mEncryptionAlgorithm;     // this will be filled by remote service after call to getEncryptionAlgorithm()

    private byte[] mSignedHash; // this will be filled with signed data by msg handler after  'sign()' is called and successfull
    private Certificate[] mCertificateChain; // this will be filled with the cert chain
    private PKCS10CertificationRequest mCSR; // if no cert alias is passed to constructor - one will be created along with this CSR

    private volatile boolean waitingForRemoteService;

    //
    public ExternalSignatureProvider(Context c, String digestAlgo, String certAlias, ArrayList<String> principals) throws Exception {
        mAdHocCertificateAlias = null;
        mContext = c;
        mHashAlgorithm = DigestAlgorithms.getDigest(DigestAlgorithms.getAllowedDigests(digestAlgo));
        mEncryptionAlgorithm = "RSA";
        mCertificateAlias = certAlias;
        mPrincipals = principals;

        Intent intent = new Intent(c, CryptoServices.class);
        // Start crypto service
        if (!CryptoServices.isRunning()) {
            // first try pen crypto provider with fallback to Android key store for signing
            intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_PRIMARY, "com.crayonic.sigma.crypto.providers.CrayonicPenWithAndroidKeyStoreCryptoProvider");
            // fallback to internal HW key store if e.g. pen is not present
            intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_SECONDARY, "com.crayonic.sigma.crypto.providers.CrayonicPenP12KeyStoreCryptoProvider");
            // fallback to SW (BouncyCastle) key store
            intent.putExtra(CryptoServices.CS_CRYPTO_PROVIDER_TERTIARY, "com.crayonic.sigma.crypto.providers.BCKeyStoreCryptoProvider");
            c.startService(intent); // start the Crypto services given preferred providers in order of importance
        }
        // ...and bind to it
        doBindService(intent);
    }

    public boolean signWithExistingCertificate(String certAlias){
        mCertificateChain = null; // reset signing chain now
        mAdHocCertificateAlias = null; // reset ad hoc alias
        if(certAlias != null){
            mCertificateAlias = certAlias; // last minute certificate selection if any
        }

        if(mCertificateAlias == null){
            Log.e(TAG, "Cannot sign with existing certificate - None is selected/created!");
            if(mContext instanceof Activity){
                new Runnable()
                {
                    public void run()
                    {
                        Toast.makeText(mContext, "No certificate exists for signing!", Toast.LENGTH_LONG)
                                .show();
                    }
                };
            }
            return false;
        }else {
            Log.w(TAG, "Signature will be created using existing certificate alias: " + mCertificateAlias);
            mPrincipal = null;

            return true;
        }
    }

    public boolean signWithNewSelfSignedCertificate( int principalID){
        mCertificateChain = null; // reset signing chain now
        mAdHocCertificateAlias = null;
        if(mPrincipals == null || mPrincipals.isEmpty() || mPrincipals.size() <= principalID){
            Log.e(TAG, "Cannot sign with adhoc certificate - No Principal exists!");
            if(mContext instanceof Activity){
                new Runnable()
                {
                    public void run()
                    {
                        Toast.makeText(mContext, "No principals exists for signing!", Toast.LENGTH_LONG)
                                .show();
                    }
                };
            }
            return false;
        }

        mPrincipal = mPrincipals.get(principalID);
        Log.w(TAG, "New ad hoc self signed certificate will be created for principal: " + mPrincipal );
        return true;
    }

    @Override
    public String getHashAlgorithm() {
        return mHashAlgorithm;
    }

    @Override
    public String getEncryptionAlgorithm() {
        //call remote service to get algorithm for the given private key
//        mEncryptionAlgorithm = null;
//
//        createAdHocCertIfNullAndPossible();
//
//        // setup message and parameters
//        Message msg = Message.obtain(null, CryptoServices.MSG_GET_ALGORITHM);
//        msg.replyTo = mMessenger;
//        Bundle b = new Bundle();
//        msg.setData(b);
//        b.putString(CryptoServices.CS_CERT_ALIAS, mAdHocCertificateAlias != null ? mAdHocCertificateAlias : mCertificateAlias); // use ad hoc certificate if one has been created
//        // call service with the message and wait for it to fill in correct results
//        callRemoteServiceBlocking(msg);
//
       return mEncryptionAlgorithm;

    }

    @Override
    public byte[] sign(byte[] bytes) throws GeneralSecurityException {
        mSignedHash = null; // reset result
        String signMode = this.mHashAlgorithm + "with" + this.mEncryptionAlgorithm;

        createAdHocCertIfNullAndPossible();

        if (mAdHocCertificateAlias == null && mCertificateAlias == null) {
            throw new GeneralSecurityException("ExternalSignatureProvider has neither existing certificate Alias or principal for  self signed signature!");
        }

        // setup message and parameters
        Message msg = Message.obtain(null, CryptoServices.MSG_SIGN);
        msg.replyTo = mMessenger;
        Bundle b = new Bundle();
        msg.setData(b);
        b.putString(CryptoServices.CS_CERT_ALIAS, mAdHocCertificateAlias != null ? mAdHocCertificateAlias : mCertificateAlias); // use ad hoc certificate if one has been created
        b.putByteArray(CryptoServices.CS_HASH, bytes);
        b.putString(CryptoServices.CS_SIGN_ALGO, signMode );
        // call service with the message and wait for it to fill in correct results
        callRemoteServiceBlocking(msg);

        // Delete  ad-hoc Certificate from keystore if signed already ASAP!
        if(mAdHocCertificateAlias != null){
            // setup message and parameters
            msg = Message.obtain(null, CryptoServices.MSG_REMOVE_CERT);
            msg.replyTo = mMessenger;
            b = new Bundle();
            msg.setData(b);
            b.putString(CryptoServices.CS_CERT_ALIAS, mAdHocCertificateAlias); // use ad hoc certificate if one has been created
            // call service with the message and wait for it to fill in correct results
            callRemoteServiceBlocking(msg);
        }


        return mSignedHash;
    }


    // get cert chain
    //FIXME possibly redundant operation (in ExternalPKCS12FileIdentity) - should be passed in intents
    public Certificate[] getCertificateChain() throws GeneralSecurityException {
        if(mCertificateChain != null)
            return mCertificateChain;

        createAdHocCertIfNullAndPossible();

        if (mAdHocCertificateAlias == null && mCertificateAlias == null) {
            throw new GeneralSecurityException("ExternalSignatureProvider has neither existing certificate Alias or principal for  self signed signature!");
        }
        // setup message and parameters
        Message msg = Message.obtain(null, CryptoServices.MSG_GET_CERT_CHAIN);
        msg.replyTo = mMessenger;
        Bundle b = new Bundle();
        msg.setData(b);
        b.putString(CryptoServices.CS_CERT_ALIAS, mAdHocCertificateAlias != null ? mAdHocCertificateAlias : mCertificateAlias); // use ad hoc certificate if one has been created
        // call service with the message and wait for it to fill in correct results
        callRemoteServiceBlocking(msg);

        return mCertificateChain;
    }




    private void createAdHocCertIfNullAndPossible() {
        // check if we need to create new signing certificate  based on X500 principal (self signed or signed by mCertificateAlias)
        if (mPrincipal != null && mAdHocCertificateAlias == null) {
            // setup message and parameters
            Message msg = Message.obtain(null, CryptoServices.MSG_CREATE_CSR);
            msg.replyTo = mMessenger;
            Bundle b = new Bundle();
            b.putString(CryptoServices.CS_CERT_ALIAS, createNewCertAlias());
            b.putString(CryptoServices.CS_CERT_PRINCIPAL, mPrincipal);
            msg.setData(b);
            // call service with the message and wait for it to fill in correct results
            callRemoteServiceBlocking(msg);

            if (mCSR != null && mAdHocCertificateAlias != null) {  // check if create CSR was a success
                Log.i(TAG, "Successfully created self signed cert for alias: " + mAdHocCertificateAlias);

                //TODO may be in the future we will be signing the self signed cert with current user cert
                /*if (mCertificateAlias != null) { // check if we can sign the certificate with another one
                    // lets sign the CSR with cert alias
                    msg = Message.obtain(null, CryptoServices.MSG_SIGN_CSR);
                    msg.replyTo = mMessenger;
                    b = new Bundle();
                    b.putString(CryptoServices.CS_CERT_ALIAS, mAdHocCertificateAlias);
                    //b.putString(CryptoServices.CS_CERT_PRINCIPAL, mPrincipal);
                    try {
                        b.putByteArray(CryptoServices.CS_CSR, mCSR.getEncoded());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    b.putString(CryptoServices.CS_CERT_SIGNING_ALIAS, mCertificateAlias);
                    msg.setData(b);
                    // call service with the message and wait for it to fill in correct results
                    callRemoteServiceBlocking(msg);
                }*/
            }

        }
    }

    private String createNewCertAlias() {

        return mPrincipal+ new Date().getTime();

    }

    /*
    * This binds to a service - sends a message and waits until result or timeout - unbinds from service
     */

    private void callRemoteServiceBlocking(Message msg) {
        try {
            mServiceMsg.send(msg);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even do anything with it
            Log.e(TAG, "Crypto service has crashed on connection from " + TAG);
        }

        // lock up thread and wait for response or timeout to get result
        long shouldEndBy = System.currentTimeMillis() + CRYPTO_SERVICE_TIMEOUT * 1000000;
        waitingForRemoteService = true;
        while (shouldEndBy > System.currentTimeMillis() && waitingForRemoteService) {
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //----------------------------SERVICE COMM CLIENT code----------------------------------------
    final Messenger mMessenger = new Messenger(new IncomingHandler(this));
    Messenger mServiceMsg = null;
    boolean mIsBound;

    static class IncomingHandler extends Handler {
        private final WeakReference<ExternalSignatureProvider> mService;

        IncomingHandler(ExternalSignatureProvider service) {
            mService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            ExternalSignatureProvider cps = mService.get();
            if (msg.getData() != null) {
                switch (msg.what) {
                    case CryptoServices.MSG_SIGN:  //check for crypto errors on the sign operation
                        String error = msg.getData().getString(CryptoServices.CS_ERROR);
                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error during signing: " + error);
                            cps.mSignedHash = null;
                        } else {
                            cps.mSignedHash = msg.getData().getByteArray(CryptoServices.CS_RET_MSG);
                        }
                        cps.waitingForRemoteService = false; // stop waiting for result
                        break;

                    case CryptoServices.MSG_GET_CERT_CHAIN:
                        error = msg.getData().getString(CryptoServices.CS_ERROR);
                        cps.mCertificateChain = null;
                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error getting cert chain: " + error);
                        } else {
                            try {
                                byte[] certs = msg.getData().getByteArray(CryptoServices.CS_RET_CERTS);
                                //deserialize
                                ByteArrayInputStream bis = new ByteArrayInputStream(certs);
                                ObjectInput in = null;
                                in = new ObjectInputStream(bis);
                                List<Certificate> certLits = (List<Certificate>) in.readObject();
                                bis.close();
                                cps.mCertificateChain = (Certificate[]) certLits.toArray();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        cps.waitingForRemoteService = false; // stop waiting for result
                        break;

                    case CryptoServices.MSG_GET_ALGORITHM:  //check for crypto errors on the sign operation
                        error = msg.getData().getString(CryptoServices.CS_ERROR);
                        cps.mEncryptionAlgorithm = null;
                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error getting encryption algorithm: " + error);
                        } else {
                            cps.mEncryptionAlgorithm = msg.getData().getString(CryptoServices.CS_RET_ALGO);
                        }
                        cps.waitingForRemoteService = false; // stop waiting for result
                        break;

                    case CryptoServices.MSG_CREATE_CSR:  //check for crypto errors on the sign operation
                        error = msg.getData().getString(CryptoServices.CS_ERROR);

                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error on creating CSR: " + error);
                        } else {
                            byte[] encodedCSR = msg.getData().getByteArray(CryptoServices.CS_RET_CSR);
                            try {
                                cps.mCSR = new PKCS10CertificationRequest(encodedCSR);
                                cps.mAdHocCertificateAlias = msg.getData().getString(CryptoServices.CS_CERT_ALIAS);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        cps.waitingForRemoteService = false; // stop waiting for result
                        break;

                    case CryptoServices.MSG_SIGN_CSR:  //check for crypto errors on the sign operation
                        error = msg.getData().getString(CryptoServices.CS_ERROR);

                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error on signing CSR: " + error);
                        } else {
                            Certificate signedCertificate = (Certificate) msg.getData().getSerializable(CryptoServices.CS_RET_CSR);
                            cps.mAdHocCertificateAlias = msg.getData().getString(CryptoServices.CS_CERT_ALIAS);
                            // at this point the signed ad hoc cert is in key store ready to go
                        }
                        cps.waitingForRemoteService = false; // stop waiting for result
                        break;

                    case CryptoServices.MSG_REMOVE_CERT:  //check for crypto errors on the sign operation
                        error = msg.getData().getString(CryptoServices.CS_ERROR);
                        if (error != null) {
                            Log.e(TAG, "Crypto service returned error on removing cert: " + error);
                        } else {
                            cps.mAdHocCertificateAlias = null;
                        }
                        cps.waitingForRemoteService = false; // stop waiting for result
                        break;

                    default:
                        super.handleMessage(msg);
                }
            }
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mServiceMsg = new Messenger(service);
//            try {
//                Message msg = Message.obtain(null, CryptoServices.MSG_REGISTER_CLIENT);
//                msg.replyTo = mMessenger;
//                mServiceMsg.send(msg);
//            } catch (RemoteException e) {
//                // In this case the service has crashed before we could even do anything with it
//                Log.e(TAG, "Crypto service has crashed on connection from " + TAG);
//            }
            Log.d(TAG, "Successfully connected to crypto service!");
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mServiceMsg = null;
        }
    };


    private void doBindService(Intent i) {
//        Intent intent = new Intent();
//        intent.setPackage("com.crayonic.sigma.pdf");
//        intent.setComponent(new ComponentName("com.crayonic.sigma.pdf", "com.crayonic.sigma.pdf.services.CryptoServices"));
        mContext.bindService(i, mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    public void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
//            if (mServiceMsg != null) {
//                try {
//                    Message msg = Message.obtain(null, CryptoServices.MSG_UNREGISTER_CLIENT);
//                    msg.replyTo = mMessenger;
//                    mServiceMsg.send(msg);
//                } catch (RemoteException e) {
//                    // There is nothing special we need to do if the service has crashed.
//                }
//            }
            // Detach our existing connection.
            mContext.unbindService(mConnection);
            mIsBound = false;
        }
    }
}
