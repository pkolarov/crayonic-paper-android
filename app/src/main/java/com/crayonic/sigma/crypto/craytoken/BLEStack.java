package com.crayonic.sigma.crypto.craytoken;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.ParcelUuid;
import android.os.SystemClock;
import android.util.Log;

import com.crayonic.sigma.pdf.Utils;
import com.crayonic.sigma.tools.Crypto;
import com.crayonic.sigma.tools.StreamOrientedKnuthMorrisPratt;
import com.crayonic.smartcardio.CardException;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by peter on 07/02/16.
 */
public class BLEStack {
    private static final String TAG = "BLEStack";

    public static final int REQUEST_ENABLE_BT = 1010;

    //TODO make new UUIDS
    private static final UUID SIGN_SERVICE_UUID = UUID.fromString("0bd51666-e7cb-469b-8e4d-2742f1ba77cc"); //FIXME
    private static final UUID SERIAL_LINK_CHARACTERISTIC = UUID.fromString("e7add780-b042-4876-aae1-112855353cc1"); //FIXME
    private static final UUID CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805F9B34FB"); //FIXME

    // BT states
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_SCANNING = 3;
    private static final int STATE_SCAN_FOUND = 4;
    private static final int STATE_SCAN_NOT_FOUND = 5;

    private static final int MAX_COM_BLOCK_SIZE = 4096;  // Max 4kb for transfer block size (should be plenty for our use cases)
    private static final int MAX_BLE_CHUNK_SIZE = 20;  // number of byte in one BLE message transfer

    private static final long RECEIVE_TIMEOUT = 250;  // max time (ms) to wait for next received data from BLE device
    private static final long TIMEOUT_TO_RESPONSE = 5000; // max time (ms) to wait from command isssued to BLE device to the start of first data received
    private static final long CONNECTION_TIMEOUT = 5000; // max time from starting to connect to actual connection

    // MARKERS
    // private static final String BINARY_DATA_MARKER = "BINARY";
    private static final String ADMIN_DATA_MARKER = "ADMIN_";

    private static final int BINARY_STREAM_PACKET_SIZE = 4096; // 4k bin buffer should be enough for stream buffer
    //private static final String MAC_FILTER_DEBUG = "00:07:80:B5:ED:D2";  //FIXME set to null for production!!!!
    //private static final String MAC_FILTER_DEBUG = "00:07:80:B5:ED:E2";  //FIXME black pen  E4 - silver pen
    private static final String MAC_FILTER_DEBUG = null; // finds nearest pen
    private static final String CONNECTED_STRING = "CONNECTED\n";
    private static final String DISCONNECT_DATA_MARKER = "DISCONNECT";


    //private static StreamOrientedKnuthMorrisPratt mBinMarker;
    private static StreamOrientedKnuthMorrisPratt mAdminMarker;
    private static StreamOrientedKnuthMorrisPratt mDisconnectMarker;


    private boolean mBinaryMode;  // true if binary buffer is being filled

    //buffer thread safety
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    public final Lock readRXBufferLock = readWriteLock.readLock();
    //public final Lock writeRXBufferLock = readWriteLock.writeLock();


    // device and BT
    private Craytoken mSelectedPen;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothGattCharacteristic mSerialLinkCharacteristic; // comm link is in this characteristic for R/w/Indicate
    private int mConnectionState;

    private ByteBuffer mTXBuffer; // transmit buffer (receive buffer needs to be implemented by user of BLEStack!)

    private Context mContext;
    private long mTimeout;

    private ByteBuffer mBinaryDataBuffer;  // BINARY buffer
    private long mBinaryDataBufferTime; // time in millis of first binary packet reception for timing events
    private int mTotalBinaryDataReceived; // count of binary bytes received
    private int mTotalASCIIDataReceived; // count of ASCII or APDU bytes received
    private static final int BT_LATENCY = 50; // default BT packet latency FIXME this should be measured later using touch screen event


    //private String mMACFilter;
    private Hashtable<String, Craytoken> mScanTable = new Hashtable<>(10);

    static volatile private BLEStack INSTANCE; // stack is singleton

    private int mOnScanResultCounter;
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            mOnScanResultCounter += 1;
            BluetoothDevice device = result.getDevice();
            mScanTable.put(device.getAddress(), new Craytoken(device, result.getRssi(), result.getScanRecord()));
            Log.i(TAG, "Found Crayonic Pen with MAC: " + device.getAddress());

        }
    };

    private ResponseListener responseListner;

    public int getScanResultCount(){
        return mOnScanResultCounter;
    }


    public static BLEStack getInstance(Context c) {
        if (INSTANCE == null)
            INSTANCE = new BLEStack(c);

        mDisconnectMarker = new StreamOrientedKnuthMorrisPratt(DISCONNECT_DATA_MARKER);
        mAdminMarker = new StreamOrientedKnuthMorrisPratt(ADMIN_DATA_MARKER);

        return INSTANCE;
    }

    private BLEStack(Context c) {
        this.mContext = c;
        // Initializes Bluetooth adapter.
        mBluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        mConnectionState = STATE_DISCONNECTED;
        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            if (mContext instanceof Activity) {
                ((Activity) mContext).startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

    }


    // Callback from GATT server
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if(status == 133){
                Log.e(TAG, "Messed up BTLE from previous crash?");
                if(gatt !=null ) {

                    gatt.close();
                }
                mConnectionState = STATE_DISCONNECTED;
            }

            if(gatt == null){
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Pen disconnected from GATT server.");
                if(mBluetoothGatt !=null)
                    mBluetoothGatt.close();
                mSelectedPen = null;
                //mBluetoothGatt = null;
                return;
            }
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "Pen connected to GATT server.");
                // for out biz logic we are not connected until service is discovered
                Log.i(TAG, "Attempting to start service discovery:" + mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Pen disconnected from GATT server.");
                if(mBluetoothGatt !=null)
                    mBluetoothGatt.close();
                mSelectedPen = null;
               // mBluetoothGatt = null;
            }
        }

        @Override
        // New services discovered
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                List<BluetoothGattService> services = gatt.getServices();
                for (BluetoothGattService service : services) {
                    List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                    for (BluetoothGattCharacteristic characteristic : characteristics) {
                        if (characteristic.getUuid().compareTo(SERIAL_LINK_CHARACTERISTIC) == 0) {
                            // start notification on data receive
                            mBluetoothGatt.setCharacteristicNotification(characteristic, true);
                            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID);
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
                            gatt.writeDescriptor(descriptor);
                            mSerialLinkCharacteristic = characteristic;
                            mConnectionState = STATE_CONNECTED;
                            // create new buffer ready to receive binary stream right after connect (e.g. pen streaming sensor data)
                            if (mBinaryDataBuffer == null || mBinaryDataBuffer.capacity() < BINARY_STREAM_PACKET_SIZE) {
                                mBinaryDataBuffer = ByteBuffer.allocate(BINARY_STREAM_PACKET_SIZE);   // make sure some binary buffer is setup
                            } else {
                                mBinaryDataBuffer.clear(); // otherwise just clear it
                            }
                            mBinaryMode = true; // always start in bin mode and wait for admin marker
                            mDisconnectMarker.begin();  //reset buffer search markers
                            mAdminMarker.begin();
                            mBinaryDataBufferTime = 0; // reset time
                            mBinaryDataBuffer.clear(); // reset bin buffer
                            mTotalBinaryDataReceived = 0;
                            mTotalASCIIDataReceived = 0;

                            // send connected flag
                       //     mSerialLinkCharacteristic.setValue(CONNECTED_STRING); //FIXME current BT FW does not require this
                            mBluetoothGatt.writeCharacteristic(mSerialLinkCharacteristic);
                        }
                    }
                }
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        // Result of a characteristic read operation
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.i(TAG, "onCharacteristicRead");
            if (status == BluetoothGatt.GATT_SUCCESS && characteristic.getUuid().compareTo(SERIAL_LINK_CHARACTERISTIC) == 0) {
                Log.i(TAG, "...characteristic data:" + characteristic.getValue());
            }
        }


        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if(status == BluetoothGatt.GATT_SUCCESS) {
                Log.i(TAG, "onCharacteristicWrite");
                Log.i(TAG, "Remote device received: "+  bytesToHex(characteristic.getValue()));
                if (characteristic.getUuid().compareTo(SERIAL_LINK_CHARACTERISTIC) == 0 && mTXBuffer != null) {
                    // we must be trasmitting now so send next 20 byte chunk
                    int remaining = mTXBuffer.remaining() > MAX_BLE_CHUNK_SIZE ? MAX_BLE_CHUNK_SIZE : mTXBuffer.remaining();
                    if (remaining < 1) {
                        Log.i(TAG, "No more data to send... at:" + System.currentTimeMillis());
                        return;
                    }
                    byte[] nextChunk = new byte[remaining];
                    mTXBuffer.get(nextChunk);
                    // do next write
                    mSerialLinkCharacteristic.setValue(nextChunk);
                    mBluetoothGatt.writeCharacteristic(mSerialLinkCharacteristic);
                    Log.i(TAG, "Sending bytes: " + bytesToHex(nextChunk));
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            mTimeout = SystemClock.elapsedRealtime() + RECEIVE_TIMEOUT;  // reset receive data  timeout
            // run buffer copying on separate thread so we can lock it
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // writeRXBufferLock.lock();
                    //try
                    synchronized (INSTANCE) {
                        //read the serial link  data we just got notified about being available
                        Log.i(TAG, "onCharacteristicChange");
                        if (characteristic.getUuid().compareTo(SERIAL_LINK_CHARACTERISTIC) == 0) {  //&& mRXBuffer != null
                            byte[] data = characteristic.getValue();
                            String dataS = null;
                            dataS = new String(data);

                            mTotalASCIIDataReceived += dataS.length();
                            int markerEnd = mDisconnectMarker.partialSearch(dataS);

                            if (markerEnd > -1) {
                                // get any leftover binary data prior to marker
                                int indexOfDataEnd =   mTotalASCIIDataReceived - markerEnd; // make it relative to packet

                                if(indexOfDataEnd < 0 )
                                    indexOfDataEnd = 0; // FIXME HACK
                                byte[] binSlice = Arrays.copyOfRange(data, 0,indexOfDataEnd);

                                if(responseListner != null)
                                    responseListner.writeDataToResponseBuffer(binSlice);
                                Log.i(TAG, "DISCONNECT marker found at" + markerEnd);
                                disconnect(); // disconnect BT now!
                                return;
                            }


                            if (!mBinaryMode) {
                                // ADMIN or APDU Modes - lossless
                                Log.i(TAG, "Received ASCII data chunk: " + new String(data));
                                if(responseListner != null) {
                                    responseListner.writeDataToResponseBuffer(data);
                                    Log.i(TAG, "Wrote to buffer: " + bytesToHex(data) );
                                }
                            } else {
                                // Lossy BINARY stream MODE (e.g. biometrics signaling)

                                // fill binary buffer and keep watching for admin data marker
                                if(mBinaryDataBufferTime == 0 ) {
                                    mBinaryDataBufferTime = SystemClock.uptimeMillis(); // if first bin data received record its time
                                    Log.i(TAG, "First binary data rcv at: " + mBinaryDataBufferTime );
                                }
                                // search for ADMIN marker in bin data
                                markerEnd = mAdminMarker.partialSearch(dataS);
                                if (markerEnd > -1) {
                                    Log.i(TAG, "ADMIN marker found at" + markerEnd);
                                    int indexOfASCIIDataStart = markerEnd - mTotalBinaryDataReceived ; // make it relative to packet
                                    // get any leftover binary data prior to marker
                                    if(indexOfASCIIDataStart < 0 )
                                        indexOfASCIIDataStart = 0; // FIXME HACK
                                    byte[] binSlice = Arrays.copyOfRange(data, 0, indexOfASCIIDataStart);
                                    mBinaryDataBuffer.put(binSlice);
                                    // ...and some ascii data
                                    byte[] asciiSlice = Arrays.copyOfRange(data, indexOfASCIIDataStart, data.length);
                                    dataS = new String(asciiSlice);
                                    mTotalASCIIDataReceived += dataS.length();
                                    mDisconnectMarker.partialSearch(dataS);
                                    if(responseListner != null)
                                        responseListner.writeDataToResponseBuffer(asciiSlice);
                                    mBinaryMode = false;
                                    mTotalBinaryDataReceived += binSlice.length;
                                } else {
                                    //just bin data rcvd
                                    mBinaryDataBuffer.put(data);
                                    mTotalBinaryDataReceived += data.length;
                                    Log.i(TAG, "BINCount: " + mTotalBinaryDataReceived);
                                    Log.i(TAG, "Received BIN data chunk: " + Crypto.bytesToHex(data));
                                }
                            }
                        }
                    }
//                    finally {
//                        writeRXBufferLock.unlock();
//                    }
                }
            }).start();
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            Log.i(TAG, "Remote RSSI: " + rssi);
        }
    };

    public void disconnect() {
        mConnectionState = STATE_DISCONNECTED;
        mSerialLinkCharacteristic = null;
        if(mBluetoothGatt != null) {
            mBluetoothGatt.disconnect();
           // mBluetoothGatt.close();  This must be called in Callbacks only!!!!
        }

    }


    // APDU low level transmit
    public void transmitBytes(byte[] bytes) throws CardException {

        ByteBuffer txBuffer = ByteBuffer.allocate(bytes.length);
        txBuffer.put(bytes);
        txBuffer.flip();  // go to READ mode
        transmitBBuffer(txBuffer);
        Log.i(TAG, "Started sending data... at:" + System.currentTimeMillis());
    }

    // generic low level transmit and receive
    public void transmitBBuffer(ByteBuffer tx) throws CardException {

        mTXBuffer = tx;
        byte[] firstChunk = new byte[MAX_BLE_CHUNK_SIZE];
        int firstChunkSize = tx.remaining() > MAX_BLE_CHUNK_SIZE ? MAX_BLE_CHUNK_SIZE : tx.remaining();
        tx.get(firstChunk, 0, firstChunkSize);

        if (!isConnected()) {
            throw new CardException("Connect card first before sending bytes to it");
        }
        // starts write to BLE with first 20 byte chunk
        mSerialLinkCharacteristic.setValue(firstChunk);
        Log.i(TAG, "Sent 1st RX byte: " + bytesToHex(firstChunk));
        mBluetoothGatt.writeCharacteristic(mSerialLinkCharacteristic);
        mTimeout = SystemClock.elapsedRealtime() + TIMEOUT_TO_RESPONSE; // set timeout to response
        mBinaryMode = false; // always start RX in admin mode
    }



    public ByteBuffer getBinaryRXBuffer() {
        return mBinaryDataBuffer;
    }


    // this is safe to call when expected length of recieved data is known and set, ONLY!
//    public byte[] getReceivedBytes() {
//
//        if (mRXBuffer.position() < mRXBuffer.limit() && !hasTimeout())
//            return null;  // did not fill buffer to expected size so return null until timeout
//
//        if (mRXBuffer.position() < mRXBuffer.limit()) {
//            return new byte[0]; // did not fill in time so return empty buffer
//        }
//        byte[] out = null;
//        readRXBufferLock.lock();  // probably not needed here but just in case
//        try {
//            mRXBuffer.flip(); // if it is full switch to READ mode
//            out = new byte[mRXBuffer.limit()];
//            mRXBuffer.get(out);
//        } finally {
//            readRXBufferLock.unlock();
//        }
//        return out;
//    }

    // return true if timeout occured waiting for BLE read
    public boolean hasTimeout() {
        if (mTimeout < SystemClock.elapsedRealtime())
            return true;
        else
            return false;
    }


    public void startSelectPen(String mac) {
        // only scan when none has been found before - makes scanning more thread safe
        Log.i(TAG, "startSelectPen - connectionState:" + mConnectionState);
        if(mSelectedPen == null || mConnectionState == STATE_DISCONNECTED || mConnectionState == STATE_SCAN_NOT_FOUND) {
            mConnectionState = STATE_SCANNING;
//        if (mBluetoothGatt != null && mConnectionState != STATE_DISCONNECTED) {
//            disconnect();
//        }
            // start scan
            if (MAC_FILTER_DEBUG != null)
                mac = MAC_FILTER_DEBUG;  // use for debuging only // FIXME


            mScanTable = new Hashtable<>(10);
            mSelectedPen = null;
            mOnScanResultCounter = 0; // reset counter of scan callbacks
            //mBluetoothAdapter.startLeScan(new UUID[]{SIGN_SERVICE_UUID}, mScanCallback);  // old API < 21

            if (mBluetoothAdapter == null) {
                Log.wtf(TAG, "BT no adpater! - FIXME");
                return;
            }

            if (mBluetoothAdapter.isDiscovering())  // just make sure there is no BT3 scanning going on
                mBluetoothAdapter.cancelDiscovery();

            BluetoothLeScanner scanner = mBluetoothAdapter.getBluetoothLeScanner();

            if (scanner == null) {
                Log.wtf(TAG, "BT should be on at this point!!!!! - FIXME");
                return;
            }

            ScanSettings settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    //.setMatchMode(ScanSettings.MATCH_MODE_STICKY) API > 23
                    .build();
            List<ScanFilter> filters = new ArrayList<>();
            ScanFilter.Builder builder = new ScanFilter.Builder();
            builder.setServiceUuid(new ParcelUuid(SIGN_SERVICE_UUID));
            if (mac != null) {
                builder.setDeviceAddress(mac);
            }
            ScanFilter scanFilter = builder.build();
            filters.add(scanFilter);

            scanner.startScan(filters, settings, mScanCallback);
        }
    }

    public void stopSelectPen() {
        Log.i(TAG, "stopSelectPen - connectionState:" + mConnectionState);
        if(mConnectionState == STATE_SCANNING) {

            // mBluetoothAdapter.stopLeScan(mScanCallback);  // stop scan first!!!
            if (mBluetoothAdapter == null) {
                Log.wtf(TAG, "BT no adpater! - FIXME");
                return;
            }

            BluetoothLeScanner scanner = mBluetoothAdapter.getBluetoothLeScanner();
            if (scanner == null) {
                Log.wtf(TAG, "BT should be on at this point!!!!! - FIXME");
                return;
            }

            scanner.stopScan(mScanCallback);
            // sort em by signal strength
            List<Craytoken> scanList = new ArrayList<>(mScanTable.values());
            Collections.sort(scanList, new RSSIComparator());
            mSelectedPen = scanList.isEmpty() ? null : scanList.get(0);
            if (mSelectedPen != null) {
                mSelectedPen.setBLEStack(this);
                mConnectionState = STATE_SCAN_FOUND;
            }else{
                mConnectionState = STATE_SCAN_NOT_FOUND;
            }


        }
    }

    public Craytoken getSelectedPen() {
        return mSelectedPen;
    }

    public boolean connectSelectedPen() {
        Log.i(TAG, "connectSelectedPen - connectionState:" + mConnectionState);
        if (isConnected()) {
            Log.w(TAG, "Pen already connected");
            return true;
        }

        // start our APDU bidirectional Serial link emulation comm channel over BLE
        if (mSelectedPen != null) {
            mConnectionState = STATE_CONNECTING;
            mBluetoothGatt = mSelectedPen.device.connectGatt(mContext, false, mGattCallback);
            return true;
        } else {
            Log.e(TAG, "No pen in range to connect to!");
            return false;
        }
    }


    public boolean isConnected() {
        if (mSelectedPen!= null && mConnectionState == STATE_CONNECTED) {  // check if really connected to expected device
            BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
            List<BluetoothDevice> devices = bluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
            for (BluetoothDevice device : devices) {
                if (device.getType() == BluetoothDevice.DEVICE_TYPE_LE) {
                    Log.w(TAG, "Connected to GATT :" + device.getAddress());
                    if (device.getAddress().equalsIgnoreCase(mSelectedPen.getMAC())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


/*
    private void parseAdvertisementPacket(final byte[] scanRecord) {

        byte[] advertisedData = Arrays.copyOf(scanRecord, scanRecord.length);

        int offset = 0;
        while (offset < (advertisedData.length - 2)) {
            int len = advertisedData[offset++];
            if (len == 0)
                break;

            int type = advertisedData[offset++];
            switch (type) {
                case 0x02: // Partial list of 16-bit UUIDs
                case 0x03: // Complete list of 16-bit UUIDs
                    while (len > 1) {
                        int uuid16 = advertisedData[offset++] & 0xFF;
                        uuid16 |= (advertisedData[offset++] << 8);
                        len -= 2;
                        uuids.add(UUID.fromString(String.format(
                                "%08x-0000-1000-8000-00805f9b34fb", uuid16)));
                    }
                    break;
                case 0x06:// Partial list of 128-bit UUIDs
                case 0x07:// Complete list of 128-bit UUIDs
                    // Loop through the advertised 128-bit UUID's.
                    while (len >= 16) {
                        try {
                            // Wrap the advertised bits and order them.
                            ByteBuffer buffer = ByteBuffer.wrap(advertisedData,
                                    offset++, 16).order(ByteOrder.LITTLE_ENDIAN);
                            long mostSignificantBit = buffer.getLong();
                            long leastSignificantBit = buffer.getLong();
                            uuids.add(new UUID(leastSignificantBit,
                                    mostSignificantBit));
                        } catch (IndexOutOfBoundsException e) {
                            // Defensive programming.
                            Log.e("BlueToothDeviceFilter.parseUUID", e.toString());
                            continue;
                        } finally {
                            // Move the offset to read the next uuid.
                            offset += 15;
                            len -= 16;
                        }
                    }
                    break;
                case 0xFF:  // Manufacturer Specific Data
                    Log.d(TAG, "Manufacturer Specific Data size:" + len +" bytes" );
                    while (len > 1) {
                        if(i < 32) {
                            MfgData[i++] = advertisedData[offset++];
                        }
                        len -= 1;
                    }
                    Log.d(TAG, "Manufacturer Specific Data saved." + MfgData.toString());
                    break;
                default:
                    offset += (len - 1);
                    break;
            }
        }

*/

    public long getTimeofBinaryDataStreamStart() {
        return mBinaryDataBufferTime;
    }

    public void setResponseListener(ResponseListener responseListner) {
        this.responseListner = responseListner;
    }


    // helper class to sort scanned devices by rssi
    public class RSSIComparator implements Comparator<Craytoken> {
        @Override
        public int compare(Craytoken o1, Craytoken o2) {
            return o2.rssi - o1.rssi;
        }
    }


    public static boolean setBluetooth(boolean enable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();
        if (enable && !isEnabled) {
            return bluetoothAdapter.enable();
        }
        else if(!enable && isEnabled) {
            return bluetoothAdapter.disable();
        }
        // No need to change bluetooth state
        return true;
    }

    public static String bytesToHex(byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for(byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }
}
