/**
 * Created by peter kolarov on 26.9.2015.
 */
package com.crayonic.sigma.crypto.providers;

import android.content.Context;
import android.os.Environment;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;

import com.crayonic.sigma.SSL.SSLKeyManager;
import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.crypto.CryptoException;
import com.crayonic.sigma.crypto.CryptoProvider;
import com.crayonic.sigma.crypto.PKIUtils;
import com.crayonic.sigma.crypto.craytoken.ResponseListener;
import com.crayonic.sigma.pdf.file.FileUtilsPlus;
import com.crayonic.smartcardio.CardException;
import com.itextpdf.smartcard.crayonicid.CrayonicIDCard;
import com.itextpdf.smartcard.util.DigestAlgorithms;

import org.spongycastle.asn1.oiw.OIWObjectIdentifiers;
import org.spongycastle.asn1.x500.X500Name;
import org.spongycastle.asn1.x509.AlgorithmIdentifier;
import org.spongycastle.asn1.x509.AuthorityKeyIdentifier;
import org.spongycastle.asn1.x509.BasicConstraints;
import org.spongycastle.asn1.x509.GeneralName;
import org.spongycastle.asn1.x509.GeneralNames;
import org.spongycastle.asn1.x509.X509Extension;
import org.spongycastle.cert.X509CertificateHolder;
import org.spongycastle.cert.X509ExtensionUtils;
import org.spongycastle.cert.X509v3CertificateBuilder;
import org.spongycastle.cert.jcajce.JcaX509CertificateConverter;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.DigestCalculator;
import org.spongycastle.operator.bc.BcDigestCalculatorProvider;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;
import org.spongycastle.pkcs.PKCS10CertificationRequest;
import org.spongycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.spongycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;

import javax.crypto.Cipher;
import javax.security.auth.x500.X500Principal;

/**
 * Native Android Key Store Crypto and Certificate provider
 */
public class CrayonicPenP12KeyStoreCryptoProvider extends CrayonicPenWithAndroidKeyStoreCryptoProvider
        implements ResponseListener {

    private static final String TAG = "CrayonicPenProvider";
    private static final int MAX_RESPONSE_FROM_PEN = 4096;
    private ByteBuffer mRXBuffer; //  buffer for recieving response from the card channel


    private static final String TRUSTED_ROOT_ASSET_PATH = "cert/AmicoservicesCA-chain.pem";
    private String mResponseLine;

    /**
     * @param c Context of the calling service
     * @throws Exception
     */
    public CrayonicPenP12KeyStoreCryptoProvider(Context c) throws Exception {
        super(c);
        mRXBuffer = ByteBuffer.allocate(MAX_RESPONSE_FROM_PEN); // create buffer according to expected return response

    }


    @Override
    public void replaceCSRWithNewCertificate(final String alias, final Certificate[] chain) throws CryptoException {
        try {
            throw new Exception("need to implement");

        } catch (Exception e) {
            e.printStackTrace();
            throw new CryptoException(e.getMessage());
        }

    }


    @Override
    public KeyPair createNewKeyPair(String alias, String principal) throws CryptoException {
        try {
            throw new Exception("key pair created in token!");
            //  }
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
        // throw new CryptoException(e.getMessage());
        //return null;
    }

    @Override
    public byte[] createPKCS10CSR(String certAlias, String principal) throws CryptoException {
        try {
            if (mCraytokenSmartCard == null)
                mCraytokenSmartCard = new CrayonicIDCard(mCardTerminal);  // this already connects to BLE device

            mRXBuffer.clear(); // clear csr response buf before listening for response
            mResponseLine = null; // reset response line
            mCraytokenSmartCard.requestCSR(principal, this);

            while (mResponseLine == null) {
                try {
                    Thread.sleep(30);  //pause this thread until BTLE fills up line buffer with response
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            // mCraytokenSmartCard.disconnect();
            // mCraytokenSmartCard = null;
          //  if (mResponseLine.startsWith("----") && mResponseLine.endsWith("----")) {
                mResponseLine = removeCSRHeadersAndFooters(mResponseLine);
                mResponseLine = mResponseLine.trim();
            //}
            try {
                Thread.sleep(30);  //pause this thread until BTLE fills up line buffer with response
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mCraytokenSmartCard.switchToAPDU();
            try {
                Thread.sleep(30);  //pause this thread until BTLE fills up line buffer with response
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return Base64.decode(mResponseLine, Base64.DEFAULT);

        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public String getPrivateKeyAlgorithm(String certAlias) throws CryptoException {
        try {
            throw new Exception("need to implement");
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }


    @Override
    public void deleteCertificateFromKeystore(final String certAlias) throws CryptoException {
        try {
           //right now there is no way to delete specific cert from the pen !!!
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public byte[] decryptWithPrivKey(String certAlias, byte[] msgToDecrypt) throws CryptoException {
        try {
            throw new Exception("need to implement");
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override

    public byte[] encryptWithPrivKey(String certAlias, byte[] msgToEncrypt) throws CryptoException {
        try {
            throw new Exception("need to implement");
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public byte[] sign(String certAlias, String algo, byte[] msgToSign) throws CryptoException {
        try {
            // try to get it from craytoken first

            if (mCraytokenSmartCard == null)
                mCraytokenSmartCard = new CrayonicIDCard(mCardTerminal);  // this already connects to BLE device

            if (mCraytokenSmartCard.getEncryptionAlgorithm().equalsIgnoreCase("RSA")) {
                // do the PKCS1 signature externaly
                MessageDigest digest = MessageDigest.getInstance("SHA256");
                byte[] hash = digest.digest();
                // encrypt with RSA in the cryptoki (not really sign)
                byte[] signature = mCraytokenSmartCard.sign(hash, "SHA256");  //d.getEncoded()
                // after signing we close
                mCraytokenSmartCard.disconnect();
                mCraytokenSmartCard = null;
                return signature;
            }

        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
        throw new CryptoException("Could not sign with Pen" );
    }

    @Override
    public boolean verifySignature(String certAlias, String algo, byte[] msg, byte[] externalSignature) throws CryptoException {
        try {
            throw new Exception("need to implement");
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public void writeCertificate(String alias, byte[] crt) throws CryptoException {
        try {
            if (mCraytokenSmartCard == null)
                mCraytokenSmartCard = new CrayonicIDCard(mCardTerminal);  // this already connects to BLE device

//            mRXBuffer.clear(); // clear csr response buf before listening for response
//            mResponseLine = null; // reset response line

            byte[] fileID = new byte[]{0x50, 0x01}; // CERTIFICATE ID = 0x50, Position = 0x01 on the card
            mCraytokenSmartCard.writeFile(fileID, crt);

        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }



    @Override
    public byte[] encryptWithPubKey(String certAlias, byte[] msgToEncrypt) throws CryptoException {
        try {
//            if (mCraytokenSmartCard == null)
//                mCraytokenSmartCard = new CrayonicIDCard(mCardTerminal);  // this already connects to BLE device
//
//            byte[] enc = mCraytokenSmartCard.sign(msgToEncrypt, DigestAlgorithms.PLAIN_TEXT);


            throw new Exception("need to implement");
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }
    }

    @Override
    public byte[] decryptWithPubKey(String certAlias, byte[] msgToDecrypt) throws CryptoException {
        try {
            throw new Exception("need to implement");
        } catch (Exception e) {
            throw new CryptoException(e.getMessage());
        }

    }

    @Override
    public boolean isAvailable() {
        //test if pen is present and connectable
//FIXME DOES NOT WORK!!!!
//        try {
//            mCardTerminal.
//            return mCardTerminal.isCardPresent();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }


        return true;
    }


    @Override
    public void writeDataToResponseBuffer(byte[] data) {
        mRXBuffer.put(data);

        // if buffer finished with /n than we have a complete line of response
        if (mRXBuffer.get(mRXBuffer.position() - 1) == 0x0a) {
            mRXBuffer.flip();
            byte[] out = new byte[mRXBuffer.remaining()];
            mRXBuffer.get(out);
            mResponseLine = new String(out, Charset.forName("US-ASCII"));
            mRXBuffer.clear(); // and reset line buffer
        }
    }


    /**
     * Takes in a CSR/p10 as a string and removes the headers and footers of the request string.
     *
     * @param inString a CSR string
     * @return a CSR String stripped of the text headers and footers
     */
    public static String removeCSRHeadersAndFooters(String inString) {
        inString = inString.replace("-----BEGIN CERTIFICATE REQUEST-----", "");
        inString = inString.replace("-----END CERTIFICATE REQUEST-----", "");

        return inString;
    }


}
