package com.crayonic.sigma.crypto;



import org.spongycastle.operator.ContentSigner;
import org.spongycastle.pkcs.PKCS10CertificationRequest;

import java.security.KeyPair;
import java.security.cert.Certificate;
import java.util.ArrayList;

/**
 * Created by kolarov on 26.9.2015.
 *
 */
public interface CryptoProvider {

    //public static final String TRUSTED_ROOT_CA_ALIAS = "rootCA";

    // list all certificates as URIs available for crypto operations at the given URI path
    ArrayList<String> listCerts(String uriPath) throws CryptoException;
    // get cert aliases ordered from root to signing cert from the key store
    Certificate[] getCertChain(String certAlias) throws CryptoException;
    // load cert located at URI with private key protected by password (external private keys dont require password) and return its keystore Alias
    String loadCertToKS(String uri, String pwd) throws CryptoException;
    // remove given cert Alias from keystore e.g. logout
    void deleteCertificateFromKeystore(String certAlias) throws CryptoException;

    byte[] decryptWithPrivKey(String certAlias, byte[] msgToDecrypt) throws CryptoException;

    byte[] encryptWithPrivKey(String certAlias, byte[] msgToEncrypt) throws CryptoException;

    byte[] encryptWithPubKey(String certAlias, byte[] msgToEncrypt) throws CryptoException;

    byte[] decryptWithPubKey(String certAlias, byte[] msgToDecrypt) throws CryptoException;


    void replaceCSRWithNewCertificate(String alias, Certificate[] chain) throws CryptoException;

    KeyPair createNewKeyPair(String certAlias, String principal) throws CryptoException;

    byte[] createPKCS10CSR(String certAlias, String principal) throws CryptoException;

    String getPrivateKeyAlgorithm(String certAlias)  throws CryptoException;

    boolean isAvailable();

    byte[] sign(String certAlias, String algo, byte[] msg) throws CryptoException ;

    boolean verifySignature(String certAlias,String algo, byte[] msg, byte[] externalSignature) throws CryptoException ;

    Certificate signCSR(String certAlias, PKCS10CertificationRequest csr, String certSigningAlias) throws CryptoException;

    void writeCertificate(String certAlias,byte[] crt) throws CryptoException;
}

