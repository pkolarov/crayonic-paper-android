package com.crayonic.sigma.crypto.craytoken;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.crayonic.smartcardio.CardException;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Created by peter on 12/05/16.
 * Implements basic pen communication e.g. all ADMIN commands (APDU are in CraytokenChannel) and BINARY lossy
 *
 */
public class BasicComm implements ResponseListener {

    private static final long CONNECT_TIMEOUT = 1000;
    private static final long SCAN_FOR_PEN_TIMEOUT = 500;
    private static final int DATA_POINT_BYTE_SIZE = 2;  // size of the data structure in bytes arriving in BT BINARY stream mode - needed to recreate struct
    private static final int MAX_PRESSURE = 16384;
    private static final String TAG = "BasicComm";
    private static final long DATA_POINT_QUANTIZATION_TIME = 10; // data structure samples are measured every 10ms
    private static final int MAX_SCANS_WITH_RESULT = 0;  //how many times will pen need to be observed before considered found
    private static final long CHECK_SCAN_RESULT_INTERVAL = 500;
    private static final long CHECK_IF_CONNECTED_INTERVAL = 200;
    public static final int MAX_ADMIN_RESPONSE_LINE = 200; // max size of response to ADMIN commands

    private ByteBuffer mRXBuffer; //  buffer for recieving response from the card channel

    private final Handler mHandler = new Handler();

    private final Context mContext;

    private Craytoken currentPen;
    private final BLEStack mBLE;
    private long mFirstTouchTime;
    private long mLatency;
    private LineResponseListener mLineResponseListener;

    public BasicComm(Context c) {
        mContext = c;
        mBLE = BLEStack.getInstance(mContext);
        mFirstTouchTime = 0;
        mRXBuffer = ByteBuffer.allocate(MAX_ADMIN_RESPONSE_LINE); // create buffer according to expected return response

    }

    public boolean isConnected() {
        return mBLE.isConnected();
    }


    Runnable mConnectOnFound = new Runnable() {
        @Override
        public void run() {
            if (mBLE.getScanResultCount() > MAX_SCANS_WITH_RESULT) {
                mBLE.stopSelectPen();
                currentPen = mBLE.getSelectedPen();
                if (currentPen != null)
                    currentPen.connect();
            } else {
                mHandler.postDelayed(mConnectOnFound, CHECK_SCAN_RESULT_INTERVAL);
            }

        }
    };

    /*
    Send command when connected with sendRetry retries
     */
    public byte[] cmdToSend;
    public int sendRetryCount;

    Runnable mSendOnFound = new Runnable() {

        @Override
        public void run() {
            if (sendRetryCount < 0) {
                return;
            }

            sendRetryCount = sendRetryCount - 1;
            if (cmdToSend != null && cmdToSend.length > 0) {
                if (mBLE.isConnected()) {
                    try {
                        mRXBuffer.clear(); // clear response buf before listening for response
                        mBLE.transmitBytes(cmdToSend);
                        //receiveRetryCount = 50;
                        // mHandler.postDelayed(mReceiveOnSend, CHECK_IF_CONNECTED_INTERVAL);
                    } catch (CardException e) {
                        Log.e(TAG, "cannot transmit command!");
                        e.printStackTrace();
                    }
                } else {
                    mHandler.postDelayed(mSendOnFound, CHECK_IF_CONNECTED_INTERVAL);
                }
            }


        }
    };


    public void startScanForPen() {
        //start search and after timeout try to  connect
        mBLE.startSelectPen(null);
        mHandler.postDelayed(mConnectOnFound, CHECK_SCAN_RESULT_INTERVAL);
    }

    public void stopScanForPen() {
        mBLE.stopSelectPen();
        mHandler.removeCallbacks(mConnectOnFound);
    }


    public void disconnectPen() {
        try {
            mBLE.disconnect();
        } catch (Exception e) {
        } finally {
            currentPen = null;
        }

    }


    // connect to pen with some default timeout and send command and some parameters non-blocking
    public void sendPenCommand(final AdminCommand ac, final String params) throws Exception {

        startScanForPen();

        // set send command and retry count
        cmdToSend = (ac.name() + " " + params + "\n").getBytes();
        sendRetryCount = 50;

        mBLE.setResponseListener(this);              // responses to command will be received by this class
        mHandler.postDelayed(mSendOnFound, 1000);   // start about one second after scan
    }

    // returns pressure given time in millis from the binary buffer
    public float getPenPressure(final long time) {
        byte[] dataPoint = null;
        if (currentPen == null) {
            //Log.w(TAG, "No Crayonic Pen detected for pressure...");
            return -1f;
        }
        BLEStack ble = currentPen.getBLEStack();
        if (ble.getTimeofBinaryDataStreamStart() == 0) {
            Log.w(TAG, "Pen did not send any data yet..");
            return -1f; // no pressure data received yet so return -1
        }

        // sanity check
        if (mFirstTouchTime == 0 || mFirstTouchTime > time) {
            Log.wtf(TAG, "Asking for pressure before properly registering first screen touch???");
            return -1f;
        }

//        if(!ble.isConnected()) { // no time to connect now so it is commented out!!!
//            return 0f;
//        }
        ByteBuffer buffer = ble.getBinaryRXBuffer();
        if (buffer == null || (buffer.position() + 1) < DATA_POINT_BYTE_SIZE) {
            Log.e(TAG, "No binary buffer!!!");
            return -1f;
        }
        // calculate index to data struct based on time requested
        // long startTime = ble.getTimeofBinaryDataStreamStart() - getLatency();
        int index = Math.round((time - mFirstTouchTime) / DATA_POINT_QUANTIZATION_TIME);
        synchronized (ble) {
            if (index < 0) {
                Log.wtf(TAG, "This should not happen - getting data before touch event!");  //sanity check above should prevent this
                return -1f;
            }
            index = index * DATA_POINT_BYTE_SIZE; // make index a pointer to byte array
            int getIncompleteTrailingBytes = (buffer.position()) % DATA_POINT_BYTE_SIZE;

            if (index >= (buffer.position() - getIncompleteTrailingBytes)) {
                int aheadOffset = index - (buffer.position() - getIncompleteTrailingBytes);
                if (aheadOffset > 40) {
                    Log.w(TAG, "Too far ahead of buffer...");
                    return -1; // too far and wont catch up means we are maybe done sending data
                    // return 0 ; //FIXME this is probably more correct?
                }
                // trying to read beyond data we have received so just return one before
                Log.w(TAG, "Reading ahead of buffer by:" + aheadOffset);
                //move index to last record in buffer then...
                index = (buffer.position() - getIncompleteTrailingBytes) - DATA_POINT_BYTE_SIZE;
                if (index < 0) {
                    Log.w(TAG, "...and pen did not send data yet?");
                    return 0;
                }
            }

            dataPoint = new byte[DATA_POINT_BYTE_SIZE];
            dataPoint[0] = buffer.get(index);
            dataPoint[1] = buffer.get(index + 1);
            //Log.e(TAG, "Pressure 0: " + dataPoint[0]);
            //Log.e(TAG, "Pressure 1: " + dataPoint[1]);
        }

        // normalize pressure to 0 - 1 range
        short int16P = (short) (((dataPoint[0] & 0xFF) << 8) | (dataPoint[1] & 0xFF));
        if (int16P < 0) {
            int16P = 0; // no negative pressure!
        }
        float normalizePressure = ((float) int16P) / MAX_PRESSURE;
        String lp = String.format("%.05f", normalizePressure);
        Log.i(TAG, "Pen pressure: " + lp);
        return normalizePressure;
    }

    public float getPenAccel(final long time) {
        return 0f;
    }

    public void pointerDownNow(long t) {
        if (mFirstTouchTime > 0) {
            return;
        }

        if (currentPen == null) {
            //Log.w(TAG, "No Crayonic Pen detected for pressure");
            return;
        }

        Log.i(TAG, "First touch time event at: " + t);
        mFirstTouchTime = t;
    }

    public long getLatency() {
        if (mLatency != 0) {
            return mLatency;
        }
        // otherwise calculate it....
        BLEStack ble = currentPen.getBLEStack();

        long start = ble.getTimeofBinaryDataStreamStart(); // get time of first byte received
        if (start == 0) {
            // no data received yet
            return 0;
        }
        long diff = start - mFirstTouchTime;
        if (diff < 0) {
            Log.e(TAG, "Pen sending pressure before touch screen event!");
            diff = 1;
        }

        if (diff > 500) {
            Log.e(TAG, "Latency over 500ms - sanity check");
            diff = 100;
        }

        mLatency = diff;
        Log.i(TAG, "Pen touch latency set to:" + diff);
        return mLatency;
    }

    @Override
    public void writeDataToResponseBuffer(byte[] data) {
        if((data.length + mRXBuffer.position()) < mRXBuffer.capacity()) {
            mRXBuffer.put(data);
        }
        else {
            Log.wtf(TAG, "Expected response from pen is bigger then buffer");
            return;
        }

        // if buffer finished with /n than we have a complete line of response
        if(mRXBuffer.get(mRXBuffer.position()-1) == 0x0a) {
            mRXBuffer.flip();
            byte[] out = new byte[mRXBuffer.remaining()];
            mRXBuffer.get(out);
            mLineResponseListener.writeLine(new String(out, Charset.forName("US-ASCII")));
            mRXBuffer.clear(); // and reset line buffer
        }
    }

    public void setLineResponseListener(LineResponseListener l){
        mLineResponseListener = l;
        mBLE.setResponseListener(this);
    }
}
