package com.crayonic.sigma.crypto.craytoken;

/**
 * Created by peter on 15/02/2017.
 *
 * Listener for responses to requests from craytoken admin commands
 */
public interface ResponseListener {

     void writeDataToResponseBuffer(byte[] data);

}
