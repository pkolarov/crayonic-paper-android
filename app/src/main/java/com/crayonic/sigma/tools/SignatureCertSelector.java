package com.crayonic.sigma.tools;

import android.content.Context;
import android.util.Log;

import com.crayonic.sigma.Sigma;
import com.crayonic.sigma.pdf.SignatureField;

/**
 * Created by peter on 12/01/16.
 */
public class SignatureCertSelector {
    public static int selectSignatoryCert(SignatureField signatureField, Context context) {
        try {
            if (signatureField.fieldName.matches(Sigma.applicationProperties.getProperty("codebtor_fieldname_regex")))
                return 1;  // co signer is second

            if (signatureField.fieldName.matches(Sigma.applicationProperties.getProperty("debtor_fieldname_regex")))
                return 0;  // lead signer is always first

        }catch(RuntimeException ex){
            Log.w("SignatureCertSel", "Cannot pick certificate for signing selection....using default user cert. : " + ex.getMessage());

        }

        return -1; // signs with default user
    }
}
