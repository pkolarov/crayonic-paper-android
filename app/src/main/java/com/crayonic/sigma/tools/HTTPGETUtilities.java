package com.crayonic.sigma.tools;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.crayonic.sigma.SSL.SSLKeyManager;
import com.crayonic.sigma.SSL.SecureSSLSocketFactory;
import com.crayonic.sigma.Sigma;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.util.Enumeration;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;


/**
 * Created by kolarov on 27.9.2015.
 */
public class HTTPGETUtilities {

    public final static String FQR_SERVER = "https://www.fqr.io/l/";

    public static final String APPENGINE_DYNAMICGETURL_URL = "http://esign.ws/url";

    private static final String TAG = "HTTPGETUtilities";

    /*
    Download PDF file from uri to given path and return download size or
        return HTML if only HTML is located at URL
    */
    public static String downloadAndDecryptFile(Context context, final String uri, final String filepath, final String key, String keystoreAlias,
                                                KeyStore keyStore, KeyStore trustStore, String user, String pwd) throws Exception {

        boolean secureSSL = Sigma.applicationProperties.getProperty("secure_ssl", "false").equalsIgnoreCase("true");

        String fileName = null;
        URL url = new URL(uri);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestProperty("User-Agent", "CrayonicSigma");


        if(Sigma.applicationProperties.getProperty("url_connection_timeout") != null && TextUtils.isDigitsOnly (Sigma.applicationProperties.getProperty("url_connection_timeout")) )
            urlConnection.setConnectTimeout(Integer.valueOf(Sigma.applicationProperties.getProperty("url_connection_timeout")));

        if(Sigma.applicationProperties.getProperty("url_read_timeout") != null && TextUtils.isDigitsOnly (Sigma.applicationProperties.getProperty("url_read_timeout")) )
            urlConnection.setConnectTimeout(Integer.valueOf(Sigma.applicationProperties.getProperty("url_read_timeout")));

        if (user != null && pwd != null) {
            // use basic authentication if needed
            byte[] auth = (user + ":" + pwd).getBytes();
            String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
            urlConnection.setRequestProperty("Authorization", "Basic " + basic);
        }
        if(secureSSL) {  // secure SSL will use custom keystore and truststore for mutual authentication using the Pinned certificate
                         // ...optionally (ideally) keystore has teh client certificate for client authentication in another words client
                         // authentication requires pinned certificate (whole certificate chain specified in the PEM file)

            TrustManagerFactory tmf = null;
            KeyManager[] km = null;

            if (keyStore != null && keystoreAlias != null) {
                // client auth using specific certificate is requested
                km = new KeyManager[]{new SSLKeyManager(keyStore, keystoreAlias)};

                Certificate certificate = keyStore.getCertificate(keystoreAlias);
                Log.w(TAG, "SSL connection will use this client certificate: " );
                Log.w(TAG, certificate.toString());
            }

            if (trustStore != null) {
                // custom trust store is requested to be used instead of Android global trustore
                tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                tmf.init(trustStore);

                Log.w(TAG, "SSL connection will use only these trust anchors: " );
                Enumeration enumeration = trustStore.aliases();
                while(enumeration.hasMoreElements()) {
                    String alias = (String)enumeration.nextElement();
                    Log.w(TAG,"alias name: " + alias);
                    Certificate certificate = trustStore.getCertificate(alias);
                    Log.w(TAG,certificate.toString());
                }

            }

            // for HTTPS connections only
            if (urlConnection instanceof HttpsURLConnection) {
                if (km != null || tmf != null) {
                    SSLSocketFactory sslSocketFactory = null;
                    //  if (secureSSL) {
                    // create custom SSL context
                    SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
                    sslContext.init(km, tmf == null ? null : tmf.getTrustManagers(), null);
                    //sslContext.init(null, new TrustManager[] {new SSLTrustManager()}, new SecureRandom());  // debug trust manager
                    sslSocketFactory = new SecureSSLSocketFactory(sslContext.getSocketFactory());  // user our custom highly secure SSLSocketFactory forcing TLSv1.2
//                } else {
//                    //secureSSLSocketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();
//                    sslSocketFactory = SSLCertificateSocketFactory.getInsecure(0, null); // use insecure socket !!! TODO
//                }

                    //  HttpsURLConnection.setDefaultSSLSocketFactory(secureSSLSocketFactory);
                    if (((HttpsURLConnection) urlConnection).getSSLSocketFactory() != sslSocketFactory)
                        ((HttpsURLConnection) urlConnection).setSSLSocketFactory(sslSocketFactory);
                    // ...and set verifier for this connection if set in the props
                    //if(Sigma.applicationProperties.getProperty("ssl_hostname_verifier") != null) {
                        ((HttpsURLConnection) urlConnection).setHostnameVerifier(new HostnameVerifier() {
                            @Override
                            public boolean verify(String hostname, SSLSession session) {

                                Log.w(TAG, "Verifing host name: " + session.getPeerHost());
                                try {
                                    Log.w(TAG, "..against certificate: " + session.getPeerCertificateChain()[0].getSubjectDN().toString());
                                } catch (SSLPeerUnverifiedException e) {
                                }

                                if (Sigma.applicationProperties.getProperty("ssl_hostname_verifier", "*").equalsIgnoreCase("*")) {
                                    Log.w(TAG, "ssl_hostname_verifier = *    //ignoring hostname verification!");
                                    return true; // skip host name verification for *
                                } else {
                                    HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
                                    boolean ver = hv.verify(Sigma.applicationProperties.getProperty("ssl_hostname_verifier", "*"), session);
                                    return true;
                                }
                            }
                        });
                   // }

                }
            }
        }else {// end of secureSSL
            Log.w(TAG, "The SSL connection is using public Android trust store!!!");
        }

        InputStream in = null;
        try {
            in = urlConnection.getInputStream();

            if (urlConnection.getContentType().startsWith("text/html")) {
                // Mime type is HTML so just return HTML string
                return "HTML";
            } else {
                String raw = urlConnection.getHeaderField("Content-Disposition");
                if (raw != null && raw.indexOf("=") != -1) {
                    String[] args = raw.split(";");
                    for (String arg : args) {
                        if (arg.trim().startsWith("filename")) {
                            fileName = arg.split("=")[1];
                            fileName = fileName.replace("\"", "");
                            break;
                        }
                    }
                }
                if (fileName == null) {
                    fileName = "unknown";
                }
                File encF = null;
                FileOutputStream ous;
                if (key != null) {
                    encF = File.createTempFile("encrypted", null, context.getCacheDir());
                    ous = new FileOutputStream(encF);
                } else {
                    ous = new FileOutputStream(filepath);//context.openFileOutput(outf, Context.MODE_PRIVATE);//
                }
                int bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];
                int len, size = 0;
                while ((len = in.read(buffer)) != -1) {
                    size += len;
                    ous.write(buffer, 0, len);
                }
                ous.close();
                Log.i(TAG, "Downloaded " + (size / 1024) + "kB");
                // if encrypted file then decrypt it now and delete enc. file
                if (key != null && encF != null) {
                    Crypto.decryptBase64EncodedFile(encF.getAbsolutePath(), filepath, key);
                    encF.delete(); // try removing encrypted file ASAP
                }


                String ext = getExtensionFromContent(filepath);
                if (!fileName.toUpperCase().endsWith(ext.toUpperCase())) {
                    fileName = fileName + "." + ext;
                }

                return fileName;

            }
        }
        finally {
            if (in != null) {
                in.close();
            }
        }
    }

    private static String getExtensionFromContent(String filepath) throws Exception {
        byte[] buffer = new byte[4];
        InputStream is = new FileInputStream(filepath);
        if (is.read(buffer) > 0) {
            if (buffer[0] == 0x25 && buffer[1] == 0x50 && buffer[2] == 0x44 && buffer[3] == 0x46) {
                return "pdf";
            } else if (buffer[0] == 0x50 && buffer[1] == 0x4b && buffer[2] == 0x03 && buffer[3] == 0x04) {
                return "zip";
            }
        }
        is.close();
        return "";
    }

    /*
    Use GET to download web page using UTF-8 encoding
     */
    public static String doGET(String urlS) throws Exception {
        InputStream is = null;
        try {
            URL url = new URL(urlS);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("User-Agent", "CrayonicSigma");
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response code is: " + response);
            is = conn.getInputStream();
            // Convert the InputStream into a string
            String contentAsString = StringUtils.streamToString(is, "UTF-8"); // assume URL returns UTF-8 encoded text only
            return contentAsString;
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
}
