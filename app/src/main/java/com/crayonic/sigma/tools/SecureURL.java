package com.crayonic.sigma.tools;

/**
 * Created by kolarov on 28.9.2015.
 */
public class SecureURL {
    private String url;
    private String key;

    public SecureURL(String url) {
        this.url = url;
    }
    public String getUrl() {
        return url;
    }
    public String getKey() {
        return key;
    }
    public SecureURL parse() {

        // ...detect http or fqr protocol and extract key if any
        key = null;
        if (url.startsWith("http")) {
            // separate valid url and possible AES key separated by whitespace from it
            String[] urlkey = url.trim().split("\\s+");
            if (urlkey.length == 2) {
                key = urlkey[1];
            }
            url = urlkey[0];
        } else if (url.startsWith("fqr")) {
            // handle FQR protocol: fqr://fileindex/encryptionkey
            int slashslash = url.indexOf("//") + 2;
            String index = url.substring(slashslash, url.indexOf('/', slashslash));
            key = url.substring(url.indexOf('/', slashslash) + 1);
            url = HTTPGETUtilities.FQR_SERVER + index;
        }
        return this;
    }
}