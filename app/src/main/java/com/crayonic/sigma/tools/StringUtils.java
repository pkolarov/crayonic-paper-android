package com.crayonic.sigma.tools;

import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.Normalizer;

public class StringUtils {


	
	public static final String EMPTY = "";
	private static final String TAG = "StringUtils" ;

	public static String URLEnconde(String url) {
		try {
			return  URLEncoder.encode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			Log.e("urlencode", ""+e);
			return null;
		}
	}
	
	public static String formatString(String s) {
		String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		return temp.replaceAll("[^\\p{ASCII}]", "");
	}


	/** Write the object to a Base64 string. */
	public static String serializeToB64String( Serializable o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream( baos );
		oos.writeObject( o );
		oos.close();
		return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
	}

	public static String streamToString(InputStream is, String encoding) throws  UnsupportedEncodingException{
        /* To convert the InputStream to String we use the BufferedReader.readLine() method. We iterate until the
         * BufferedReader return null which means there's no more data to read. Each line will appended to a
         * StringBuilder and returned as String. */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is, encoding));
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			Log.e(TAG, "" + e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				Log.e(TAG, "" + e);
			}
		}
		return sb.toString();
	}

	public static boolean isEmpty (String s) {
		return s==null || s.trim().equals("");
	}
	
}
