package com.crayonic.sigma.tools;

import java.util.ArrayList;

/**
 * Created by peter on 19/05/16.
 */
public class StreamOrientedKnuthMorrisPratt {
    int m;
    int i;
    String ss;
    ArrayList<Integer> table;

    public StreamOrientedKnuthMorrisPratt(String ss) {
        this.ss = ss;
        buildTable(ss);
    }

    public void begin() {
        this.m = 0;
        this.i = 0;
    }

    public int partialSearch(String s) {
        int offset = this.m + this.i;

        while (this.m + this.i - offset < s.length()) {
            if (substr(ss, this.i, 1).equals(substr(s, this.m + this.i - offset, 1))) {
                if (this.i == this.ss.length() - 1) {
                    return this.m;
                }
                this.i += 1;
            } else {
                this.m += this.i - this.table.get(this.i);
                if (this.table.get(this.i) > -1)
                    this.i = this.table.get(this.i);
                else
                    this.i = 0;
            }
        }

        return -1;
    }

    private void buildTable(String ss) {
        int pos = 2;
        int cnd = 0;

        this.table = new ArrayList<>(ss.length() + 1);
        for (int i = 0; i < ss.length() + 1; i++)
            this.table.add(0);

        if (ss.length() > 2)
            this.table.add(ss.length(), 0);
        else
            this.table.add(2, 0);

        this.table.add(0, -1);
        this.table.add(1, 0);

        while (pos < ss.length()) {
            if (substr(ss, pos - 1, 1).equals(substr(ss, cnd, 1))) {
                cnd += 1;
                this.table.add(pos, cnd);
                pos += 1;
            } else if (cnd > 0) {
                cnd = this.table.get(cnd);
            } else {
                this.table.add(pos, 0);
                pos += 1;
            }
        }
    }

    private String substr(String ss, int s, int e) {
        if (s < 0) {
            s = (ss.length() + s) < 0 ? 0 : ss.length() + s;
        }

        if (e > ss.length()) {
            e = ss.length();
        }


        return ss.substring(s, s+e);
    }
/*
    public static function main() {
		StreamOrientedKnuthMorrisPratt KMP = new StreamOrientedKnuthMorrisPratt("aa");
		KMP.begin();
		KMP.partialSearch("ccaabb");

		KMP.begin();
		trace(KMP.partialSearch("ccarbb"));
		trace(KMP.partialSearch("lksjaabof"));

	}
	*/
}
