package com.crayonic.sigma.tools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

public class ImageToPdf 
{
	public static final int PICTURE_COMPRESS =100;

	
	public static void createPdf (String pdfPath,File[] images) throws Exception
	{
		List<String> imagesPath = new ArrayList<String>();
		for (File file : images)
		{
			imagesPath.add(file.getPath());
		}
		createPdf(pdfPath, imagesPath);
	}
	

	/**
	 * Add all images to one PDF, reduce images size
	 * 
	 * @param pdfPath - output PDF path
	 * @param imagesPath - list of images
	 * @throws Exception
	 */
	public static void createPdf (String pdfPath,List<String> imagesPath) throws Exception
	{
		if (imagesPath == null || imagesPath.size()==0)
			return;
		
		Document document = new Document();
        
		PdfWriter.getInstance(document, new FileOutputStream(pdfPath));
        document.open();
        
        for (String img : imagesPath)
        {
//        	BitmapFactory.Options options = new BitmapFactory.Options();
//        	//options.inJustDecodeBounds = true;
//        	options.outWidth = 500;
//        	options.outHeight = 300;
        	
        	
        	Bitmap bitmap = getBitmapFromFile(img);
//        	 ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        	 bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
//      	    byte[] byteArray = stream.toByteArray();
//      	    stream.close();

//        	
        	
        	Rectangle r = document.getPageSize();
        	
        	
        	
        	byte[] byteArray = getResizedBitmap(bitmap); // SCALE
            Image image = Image.getInstance(byteArray);
            
            float x = r.getWidth()-85;
            float idx = (image.getWidth() / image.getHeight());
            
            image.scaleAbsolute(x, x/idx);
            document.add(image);
            
            // prevent out of memory
            bitmap.recycle();
            bitmap = null;
        }
        document.close();
	}
	
	private static Bitmap getBitmapFromFile(String bitmapFilePath) throws FileNotFoundException
	{
	    // calculating image size
	    BitmapFactory.Options options = new BitmapFactory.Options();
	    //options.inJustDecodeBounds = true;
	    //options.inSampleSize = 2;

	    return  BitmapFactory.decodeStream(new FileInputStream(new File(bitmapFilePath)), null, options);


//	    BitmapFactory.Options o2 = new BitmapFactory.Options();
//	    options.inSampleSize = LOAD_SCALE;
//
//	    return BitmapFactory.decodeStream(new FileInputStream(new File(bitmapFilePath)), null, o2);

	}
	
	private static byte[] getResizedBitmap(Bitmap bm) throws IOException //  float scale
	{
//	    int width = bm.getWidth();
//	    int height = bm.getHeight();
//
//	    // Create a matrix for the manipulation
//	    Matrix matrix = new Matrix();
//
//	    // Resize the bit map
//	    matrix.postScale(scale, scale);
//
//	    // Recreate the new Bitmap
//	    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
//	    
//	    ByteArrayOutputStream stream = new ByteArrayOutputStream();
//	    resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//	    byte[] byteArray = stream.toByteArray();
//	    stream.close();
//	    resizedBitmap.recycle();
//	    return byteArray;

		 ByteArrayOutputStream stream = new ByteArrayOutputStream();
		 bm.compress(Bitmap.CompressFormat.JPEG, PICTURE_COMPRESS, stream);
		 byte[] byteArray = stream.toByteArray();
		 stream.close();
		    bm.recycle();
		    return byteArray;
		
	}
	
}
