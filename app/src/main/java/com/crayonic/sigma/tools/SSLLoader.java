package com.crayonic.sigma.tools;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.crayonic.sigma.crypto.CryptoException;
import com.crayonic.sigma.crypto.PKIUtils;

import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

@SuppressWarnings("ALL")
@SuppressLint("DefaultLocale")
public class SSLLoader {

    private static final String TAG = "SSLLoader";
    //static public String FILEIO_URL = "https://file.io/";
    //static public String FILEIO_URL_1H = FILEIO_URL + "?expires=1d";

    //static public SSLSocketFactory socketFactory = null;
    // static public KeyStore trustStore = null; // Keystore with Server side
    // certificates loaded in Android
    // cert store
    //static public KeyStore keyStore = null; // Keystore with client side
    static public final int CONNECTION_TIMEOUT = 5000;
    static public final int SO_TIMEOUT = 30000;

    // certificate for strong
    // authenticating device (loaded
    // from SD card).

    private static File getClientCertificateFile(File dir) {
        //
        // File[] pfxFiles = new File("/mnt/extSdCard/") // TODO: this is
        // hardcoded
        // // - fix to detect
        // // different mounting
        // // points
        // .listFiles(new FileFilter() {
        // public boolean accept(File file) {
        // if (file.getName().toLowerCase().endsWith("pfx")
        // || file.getName().toLowerCase().endsWith("p12")) {
        // return true;
        // }
        // return false;
        // }
        // });
        //
        // if (pfxFiles != null && pfxFiles.length == 1)
        // return pfxFiles[0];

        if (null == dir)
            dir = Environment.getExternalStorageDirectory();


        File[] pfxFiles = dir.listFiles(new FileFilter() {
            public boolean accept(File file) {
                if (file.getName().toLowerCase().endsWith("pfx") || file.getName().toLowerCase().endsWith("p12")) {
                    return true;
                }
                return false;
            }
        });

        if (pfxFiles != null && pfxFiles.length == 1)
            return pfxFiles[0];
        else
            return null;
    }


    private static KeyStore loadKeyStore(File dir, String pass) {
        char[] password = null;

        if (pass != null)
            password = pass.toCharArray();


        InputStream certificateStream = null;
        KeyStore keyStore = null;

        File pfxFile = getClientCertificateFile(dir);
        try {
            // Load client cert if one exists into keystore
            if (pfxFile != null) {
                certificateStream = new FileInputStream(pfxFile);
                keyStore = KeyStore.getInstance("PKCS12");
                keyStore.load(certificateStream, password);
                KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                keyManagerFactory.init(keyStore, password);
            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot load keystore" + e);
        }

        return keyStore;
    }


    static public void loadKSWithTrustedPemCerts(KeyStore ks, String trustStoreFilePath) throws CryptoException {
        try {
            File f = new File( trustStoreFilePath);
            FileInputStream fis = new FileInputStream(f);
            Certificate[] chain = PKIUtils.loadCertificateChainFromPEM(new InputStreamReader((fis)));
            for(int i = 0; i < chain.length; i++){
                String rootCertAlias = ((X509Certificate)chain[i]).getSerialNumber().toString(); // make the alias for trusted root its serial number
                if(ks.getEntry(rootCertAlias, null) == null) {
                    //only replace if not there yet
                    ks.setCertificateEntry(rootCertAlias, chain[i]);
                }
            }
        } catch (Exception e) {
            Log.wtf(TAG, "Error loading trusted certs!");
            e.printStackTrace();
            throw new CryptoException("Error loading trusted certs!");
        }
    }

    public static KeyStore loadTrustStore() {
        @SuppressWarnings("unused")
        X509TrustManager manager = null;
        FileInputStream fs = null;
        KeyStore trustStore = null;
        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                // ICS adn up has managed trust store
                trustStore = KeyStore.getInstance("AndroidCAStore");
                trustStore.load(null, null);

                /* Show us trusted certs from OS */
                /* Enumeration<String> aliases = trustStore.aliases(); while (aliases.hasMoreElements()) { String
                 * alias = aliases.nextElement(); X509Certificate cert = (X509Certificate)
                 * trustStore.getCertificate(alias); Log.d( "Subject DN: " + cert.getSubjectDN().getName());
                 * Log.d( "Issuer DN: " + cert.getIssuerDN().getName()); } */
            } else {
                // before ICS it was hardcoded in system
                trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                fs = new FileInputStream("/system/etc/security/cacerts.bks");
                trustStore.load(fs, null);
            }

            // init trust manager - optional?
            trustManagerFactory.init(trustStore);
            TrustManager[] managers = trustManagerFactory.getTrustManagers();

            for (TrustManager tm : managers) {
                if (tm instanceof X509TrustManager) {
                    manager = (X509TrustManager) tm;
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot load truststore", e);
        }
        return trustStore;
    }


    public static SSLSocketFactory getSSLSocketFactory(KeyStore keyStore, String password, KeyStore trustStore) {
        try {
            if (keyStore == null) {
                return new SSLSocketFactory(trustStore);
            } else {
                // ... create ssl with client cert
                return new SSLSocketFactory(keyStore, password, trustStore);

            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot get SSL socket factory" + e);
            return null;
        }

    }


    private static String getDomain(String url) {
        String domain = url.substring(url.indexOf("//") + 2);
        if (domain.contains(":")) {
            domain = domain.substring(0, domain.indexOf(":"));
        } else {
            domain = domain.substring(0, domain.indexOf("/"));
        }
        return domain;
    }



}
