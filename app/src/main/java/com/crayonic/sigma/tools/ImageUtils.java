package com.crayonic.sigma.tools;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.util.Log;



public class ImageUtils 
{


	private static final String TAG = "ImageUtils" ;

	public static boolean storeImage (byte[] cameraData , String imagePath, Context ctx)
	{
		try 
		{
			FileOutputStream fos = new FileOutputStream(imagePath);
			fos.write(cameraData);
			fos.close();
			return true;
		} 
		catch (java.io.IOException e) 
		{
			Log.e(TAG, "Save image" + e);

			return false;
		}
	}
	
	public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
}
