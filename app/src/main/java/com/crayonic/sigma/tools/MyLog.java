package com.crayonic.sigma.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import android.util.Log;

public class MyLog {
	public static volatile int LOG_LEVEL = Log.INFO;
	
	private String tag;
	
	//FileUtils
	private static volatile Object logLock = new Object();
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault());
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
	private static volatile String logFileName;
	private static volatile OutputStream logFileOutputStream;
	
	private static volatile Integer numberOfLogFiles=null;
	public static volatile File HOME_DIR;
	private static Pattern LOG_FILE_PATTERN = Pattern.compile("[\\d]{4}-[\\d]{2}-[\\d]{2}\\.log");
	
	public MyLog(String tag) {
		this.tag = tag;
	}
	
	public void l(int level, String str) {
		Log.println(level, tag, str);
		if(level >= LOG_LEVEL) {
			StringBuffer sb = new StringBuffer(tag.length() + str.length() + 5);
			switch (level) {
				case Log.ERROR: 
					sb.append("E ");
					break;
				case Log.WARN: 
					sb.append("W ");
					break;
				case Log.INFO: 
					sb.append("I ");
					break;
				case Log.DEBUG: 
					sb.append("D ");
					break;
				default: 
					return;
			
			}
			sb.append(tag);
			sb.append(" - ");
			sb.append(str);
			writeToLogFile(sb.toString());
		}
	}
	
	public void e(String s) {
		l(Log.ERROR, s);
	}

	public void e(String text, Throwable ex) {
        StringWriter stringWriter = new StringWriter(1024);
        PrintWriter printWriter = new PrintWriter(stringWriter, true);
        printWriter.write(text);
        printWriter.write(" \n");
        ex.printStackTrace(printWriter);
        e(stringWriter.toString());
    }
	
	public void w(String s) {
		l(Log.WARN, s);
	}
	public void i(String s) {
		l(Log.INFO, s);
	}
	public void d(String s) {
		l(Log.DEBUG, s);
	}
	
	public static void setLogDebug(boolean isLogDebug) {
		if (isLogDebug) {
			LOG_LEVEL = Log.DEBUG;
		} else {
			LOG_LEVEL = Log.INFO;
		}
	}
		
	
	//FileUtils ---------------------------------------------------
	
	public static void setNumberOfLogFiles(int numberOfLog_Files) {
		numberOfLogFiles = numberOfLog_Files;
		deleteOldLogFiles();
	}
	
	public static void init(File homeDirectory) {
		homeDirectory.mkdirs();
		HOME_DIR = homeDirectory;
	}
	
	/**
	 * 
	 * @param line
	 * @return true if log has been successfully written
	 */
	public static boolean writeToLogFile(String line) {
		Date dateTime = new Date();
		synchronized (logLock) {
			StringBuffer sb = new StringBuffer(line.length() + 20);
			sb.append(timeFormat.format(dateTime));
			sb.append(" ");
			sb.append(line);
			sb.append("\n");
			String newLogFileName = dateFormat.format(dateTime) + ".log";
			try {
				if (logFileName == null || !logFileName.equals(newLogFileName)) {
					if (logFileName != null) {
						logFileName = null;
						logFileOutputStream.close();
						logFileOutputStream = null;

						// new day has started ... this means to delete the old
						// log files
						deleteOldLogFiles();
					}

					logFileName = newLogFileName;
					logFileOutputStream = new FileOutputStream(new File(
							HOME_DIR, logFileName), true);
				}

				if (logFileOutputStream == null) {
					logFileOutputStream = new FileOutputStream(new File(
							HOME_DIR, logFileName), true);
				}

				logFileOutputStream.write(sb.toString().getBytes());
				logFileOutputStream.flush();
			} catch (IOException ex) {
				return false;
			}
			return true;
		}
	}
	
	/**
	 * 
	 *  days
	 *            the number of relevant days (the number of log files to not
	 *            remove)
	 */
	private static void deleteOldLogFiles() {
		if (numberOfLogFiles != null) {
			File[] logFiles = HOME_DIR.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return LOG_FILE_PATTERN.matcher(name).matches()
							&& new File(dir, name).isFile();
				}
			});
			if (logFiles != null) {
				Arrays.sort(logFiles);
				if (logFiles.length > numberOfLogFiles) {
					for (int i = 0; i < logFiles.length - numberOfLogFiles; i++) {
						logFiles[i].delete();
					}
				}
			}
		}
	}
	
}
