package com.crayonic.sigma.tools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URLDecoder;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Formatter;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;


public class Crypto {
    private static final String ALGORITHM = "AES/CTR/NoPadding";
    private static final String CHARSET = "UTF-8";
    private static final String TAG = "Crypto";

    // generates random key with 192 bit key size
    public static byte[] keyGen() throws NoSuchAlgorithmException {
        return keyGen(192);
    }

    public static byte[] keyGen(int keySize) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
        keyGenerator.init(keySize);
        return keyGenerator.generateKey().getEncoded();
    }

    public static String passwordGen(int keySize) {
        byte[] key;

        try {
            key = keyGen(keySize);
        } catch (NoSuchAlgorithmException e) {
            //if keygen fails try secure random
            SecureRandom random = new SecureRandom();
            key = new byte[keySize/8];
            random.nextBytes(key);
        }
        return Base64.encodeToString(key,  Base64.NO_CLOSE);
    }

    public static byte[] passwordToAESKey(String password){
//        byte[] encodedKey     = Base64.decode(password, Base64.DEFAULT);
//        return new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");

        password = password.trim(); // remove possible whitespace
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(password.getBytes(CHARSET));
        } catch (Exception e) {
            e.printStackTrace();
            return  null;
        }

    }

//    public static String encryptAndBaseEncode64String(String valueToEnc, byte[] lockKey) throws Exception {
//
//        byte[] encValue = encrypt(valueToEnc.getBytes(), lockKey);
//        String encryptedValue = Base64.encodeToString(encValue, Base64.DEFAULT);
//        return URLEncoder.encode(encryptedValue, CHARSET);
//    }

    public static String decryptAndBase64DecodeString(String encryptedValue, byte[] lockKey) throws Exception {
        encryptedValue = URLDecoder.decode(encryptedValue, CHARSET);
        byte[] decodedValue = Base64.decode(encryptedValue.getBytes(), Base64.DEFAULT);
        byte[] decValue = decrypt(decodedValue, lockKey);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    public static byte[] base64DecodeDecrypt(String encryptedValue, byte[] lockKey) throws Exception {
        encryptedValue = URLDecoder.decode(encryptedValue, CHARSET);
        byte[] decodedValue = Base64.decode(encryptedValue.getBytes(), Base64.DEFAULT);
        return decrypt(decodedValue, lockKey);

    }

    //
//    public static byte[] encrypt(byte[] dataToEncrypt, byte[] key) throws NoSuchAlgorithmException,
//            NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
//
//        Cipher c = Cipher.getInstance(ALGORITHM);
//        SecretKeySpec k = new SecretKeySpec(key, ALGORITHM);
//        c.init(Cipher.ENCRYPT_MODE, k);
//        return c.doFinal(dataToEncrypt);
//    }
//
//    public static byte[] decrypt(byte[] encryptedData, byte[] key) throws NoSuchAlgorithmException,
//            NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
//        Cipher c = Cipher.getInstance(ALGORITHM);
//        SecretKeySpec k = new SecretKeySpec(key, ALGORITHM);
//        c.init(Cipher.DECRYPT_MODE, k);
//		return c.doFinal(encryptedData);
//    }
//




    public static void decryptBase64EncodedFile(String encryptedFilePath, String decryptedFilePath, String password) throws Exception {
        // get key from password by hashing password with SHA-256

        byte[] key = passwordToAESKey(password);
        //byte[] key = Base64.decode(URLDecoder.decode(key64, CHARSET).getBytes(), Base64.DEFAULT);

        FileInputStream fs = new FileInputStream(encryptedFilePath);
        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(decryptedFilePath));
        if (key != null) {
            // read all bytes into one array for decrypting if key is given
            byte[] fileBytes = ByteStreams.toByteArray(fs);
            byte[] decoded64FileBytes = Base64.decode(fileBytes, Base64.DEFAULT);
            fs.close();
            byte[] plaintext = decrypt(decoded64FileBytes, key);
            os.write(plaintext);
            os.flush();
            os.close();
        }
    }


    // returns double hash of password in HEX format e.g. hash256(hash256(password))
    // Encrypted file is in Base64 encoded format for better JS handling
    public static String encryptFile(String filePath, String encryptedFilePath, String password) throws Exception {

        FileInputStream fis = new FileInputStream(filePath);
        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(encryptedFilePath));
        Base64OutputStream b64os = new Base64OutputStream(fos, Base64.DEFAULT);
        return encryptStream(fis, b64os, password);
    }

    public static String encryptStream(InputStream is, OutputStream os, String password) throws Exception {

        byte[] plainBytes = ByteStreams.toByteArray(is);
        is.close();
        // get key from password by hashing password with SHA-256
       //MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] key = passwordToAESKey(password);
        // generate strong random IV
        SecureRandom random = new SecureRandom();
        byte ivBytes[] = new byte[16];
        random.nextBytes(ivBytes);

        byte[] encBytes = encrypt(plainBytes, key, ivBytes);

        os.write(ivBytes); // attach 16 IV bytes to start of the file
        os.write(encBytes);
        os.flush();
        os.close();

        // return double hash of the password for some possible UID usage
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return bytesToHex(digest.digest(bytesToHex(key).getBytes(CHARSET)));  // SHA( SHA(key)->bin2hex)->bin2hex
    }

    // returns double hash of password in HEX format e.g. hash256(hash256(password))


    public static byte[] encrypt(byte[] dataToEncrypt, byte[] keyBytes, byte[] ivBytes) {
        try {
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            SecretKeySpec newKey = new SecretKeySpec(keyBytes, "RAW");
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
            return cipher.doFinal(dataToEncrypt);
        } catch (Exception e) {
            Log.e(TAG, "Crypto error" + e);
        }
        return null;
    }

    public static byte[] decrypt(byte[] encryptedData, byte[] keyBytes) {
        final int AES_IV_SIZE = 16;
        try {
            // first get the IV bytes from the start of the encrypted data
            byte[] ivBytes = Arrays.copyOfRange(encryptedData, 0, AES_IV_SIZE);
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            SecretKeySpec newKey = new SecretKeySpec(keyBytes, "RAW");
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
            return cipher.update(encryptedData, AES_IV_SIZE, encryptedData.length - AES_IV_SIZE); // skip the IV vector at the start of encrypted data
            //return cipher.doFinal(encryptedData);
        } catch (Exception e) {
            Log.e(TAG, "Crypto error" + e);
        }
        return null;

    }

    final protected static char[] hexArray = "0123456789abcdef".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    /**
     * Write the object to a Base64 string.
     * This uses AES-256 (CTR) with randomly gen. password encrypted with RSA that is stored at the start of the byte array
     */
    public static String serializeToRSAEncryptedB64String(Serializable o, Key RSAKey) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();


        // create random base64 password string(32) for input into our AES encryption alg. (SAH256 will make AES key from it)
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
        secureRandom.setSeed(System.nanoTime());
        byte[] passBytes = new byte[32];
        secureRandom.nextBytes(passBytes);
        String password = Base64.encodeToString(passBytes, Base64.DEFAULT).substring(0, 31);
        // ...and encrypt with the RSA pub key
        Cipher rsa;
        rsa = Cipher.getInstance("RSA");
        rsa.init(Cipher.ENCRYPT_MODE, RSAKey);
        byte[] encAESPassword = rsa.doFinal(password.getBytes());


        // encrypt our object as an input stream with encrypted password at the start
        ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(encAESPassword); // write encrypted password to output first
        encryptStream(bis, bos, password); // then write encrypted content with the given password

        //lastly encode output into B64 encoded String
        return Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT);
    }


    public static byte[] sha256(Serializable o) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();

        // get key from password by hashing password with SHA-256
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hashOfBytes = digest.digest(baos.toByteArray());
        //lastly encode output into B64 encoded String
        return hashOfBytes;//Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
    }

    public static PrivateKey readPrivateKeyFromFile(InputStream fis) throws Exception {

        DataInputStream dis = new DataInputStream(fis);
        byte[] keyBytes = new byte[600];
        dis.readFully(keyBytes);
        dis.close();

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    public static PublicKey readPublicKey(InputStream fis) throws Exception {

        DataInputStream dis = new DataInputStream(fis);
        byte[] keyBytes = new byte[600];
        try {
            dis.readFully(keyBytes);
        } catch (EOFException e) {
        }
        dis.close();
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }



    public static String createSha1(File file) throws Exception  {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        InputStream fis = new FileInputStream(file);
        int n = 0;
        byte[] buffer = new byte[8192];
        while (n != -1) {
            n = fis.read(buffer);
            if (n > 0) {
                digest.update(buffer, 0, n);
            }
        }

        // Convert the byte to hex format
        try (Formatter formatter = new Formatter()) {
            for (final byte b : digest.digest()) {
                formatter.format("%02x", b);
            }
            return formatter.toString();
        }
    }

    public static String sha1(final File file) throws  IOException {
        final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

        try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
            final byte[] buffer = new byte[1024];
            for (int read = 0; (read = is.read(buffer)) != -1;) {
                messageDigest.update(buffer, 0, read);
            }
        }

        // Convert the byte to hex format
        try (Formatter formatter = new Formatter()) {
            for (final byte b : messageDigest.digest()) {
                formatter.format("%02x", b);
            }
            return formatter.toString();
        }
    }

}
