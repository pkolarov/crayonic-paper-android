package com.crayonic.sigma.tools;

/**
 * Created by kolarov on 26.9.2015.
 */

import android.text.TextUtils;
import android.util.Log;

import com.crayonic.sigma.SSL.SSLKeyManager;
import com.crayonic.sigma.SSL.SecureSSLSocketFactory;
import com.crayonic.sigma.Sigma;

import java.security.KeyStore;
import java.util.HashMap;
import java.util.List;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;


/**
 * This utility class provides an abstraction layer for sending multipart HTTP
 * POST requests to a web server.
 *
 * @author www.codejava.net
 */
public class MultipartUtility {

    private static final String TAG = "MultipartUtil";
    static private String USER_AGENT = "Sigma";

    private final String mBoundary;
    private static final String LINE_FEED = "\r\n";
    private HttpURLConnection httpConn;
    private String charset;
    private OutputStream outputStream;
    private PrintWriter writer;

    /**
     * This constructor initializes a new HTTP POST request with content type
     * is set to multipart/form-data
     *
     * @param requestURL
     * @param charset
     * @param sslcontext
     * @throws IOException
     */
    public MultipartUtility(String requestURL, String charset, String user, String pwd, SSLContext sslcontext)
            throws IOException {
        this.charset = charset;

        // creates a unique mBoundary based on time stamp
        //mBoundary = "===" + System.currentTimeMillis() + "===";
        mBoundary = "----------" + System.currentTimeMillis();

        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();

        if(Sigma.applicationProperties.getProperty("url_connection_timeout") != null && TextUtils.isDigitsOnly (Sigma.applicationProperties.getProperty("url_connection_timeout")) )
            httpConn.setConnectTimeout(Integer.valueOf(Sigma.applicationProperties.getProperty("url_connection_timeout")));

        if(Sigma.applicationProperties.getProperty("url_read_timeout") != null && TextUtils.isDigitsOnly (Sigma.applicationProperties.getProperty("url_read_timeout")) )
            httpConn.setConnectTimeout(Integer.valueOf(Sigma.applicationProperties.getProperty("url_read_timeout")));

        if (httpConn instanceof HttpsURLConnection && sslcontext != null) {
            // set custom SSL context if one is available
            SSLSocketFactory secureSSLSocketFactory = new SecureSSLSocketFactory(sslcontext.getSocketFactory());  // user our custom highly secure SSLSocketFactory forcing TLSv1.2
            ((HttpsURLConnection) httpConn).setSSLSocketFactory(secureSSLSocketFactory);

            // ...and set verifier for this connection
            ((HttpsURLConnection) httpConn).setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    Log.w(TAG, "Verifing host name: " + session.getPeerHost());
                    try {
                        Log.w(TAG, "..against certificate: " + session.getPeerCertificateChain()[0].getSubjectDN().toString());
                    } catch (SSLPeerUnverifiedException e) {
                    }

                    if (Sigma.applicationProperties.getProperty("ssl_hostname_verifier","*").equalsIgnoreCase("*")) {
                        Log.w(TAG, "ssl_hostname_verifier = *    //ignoring hostname verification!");
                        return true; // skip host name verification for *
                    } else {
                        Log.w(TAG, "ssl_hostname_verifier = " + Sigma.applicationProperties.getProperty("ssl_hostname_verifier","*") + " //using hostname verification!");
                        HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
                        boolean ver = hv.verify(Sigma.applicationProperties.getProperty("ssl_hostname_verifier","*"), session);
                        return ver;
                    }
                }
            });

        }
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true);    // indicates POST method
        httpConn.setDoInput(true);
        httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + mBoundary);
        httpConn.setRequestProperty("User-Agent", "CrayonicSigma");

      /*  FIXME TEMPORARY DISABLE BASIC AUTH
        if(user != null && pwd != null){
            // use basic authentication if needed
            byte[] auth = (user + ":" + pwd).getBytes();
            String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
            httpConn.setRequestProperty("Authorization", "Basic " + basic);
        }
        /**/
        outputStream = httpConn.getOutputStream();
        writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
    }

    /**
     * Adds a form field to the request
     *
     * @param name  field name
     * @param value field value
     */
    public void addFormField(String name, String value) {
        writer.append("--" + mBoundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"" + name + "\"")
                .append(LINE_FEED);
        writer.append("Content-Type: text/plain; charset=" + charset).append(
                LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Adds a upload file section to the request
     *
     * @param fieldName  name attribute in <input type="file" name="..." />
     * @param uploadFile a File to be uploaded
     * @throws IOException
     */
    public void addFilePart(String fieldName, File uploadFile)
            throws IOException {
        String fileName = uploadFile.getName();
        writer.append("--" + mBoundary).append(LINE_FEED);
        writer.append(
                "Content-Disposition: form-data; name=\"" + fieldName
                        + "\"; filename=\"" + fileName + "\"")
                .append(LINE_FEED);
        if (URLConnection.guessContentTypeFromName(fileName) == null) {
            writer.append(
                    "Content-Type: application/octet-stream")
                    .append(LINE_FEED);
        } else {
            writer.append(
                    "Content-Type: " + URLConnection.guessContentTypeFromName(fileName))
                    .append(LINE_FEED);
        }
        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.flush();
        inputStream.close();

        writer.append(LINE_FEED);
        writer.flush();
    }

    /**
     * Adds a header field to the request.
     *
     * @param name  - name of the header field
     * @param value - value of the header field
     */
    public void addHeaderField(String name, String value) {
        writer.append(name + ": " + value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Completes the request and receives response from the server.
     *
     * @return a list of Strings as response in case the server returned
     * status OK, otherwise an exception is thrown.
     * @throws IOException
     */
    public List<String> finish() throws IOException {
        List<String> response = new ArrayList<String>();

        writer.append(LINE_FEED).flush();
        writer.append("--" + mBoundary + "--").append(LINE_FEED);
        writer.close();

        // checks server's status code first
        int status = httpConn.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpConn.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                response.add(line);
            }
            reader.close();
            httpConn.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        return response;
    }


/*
Do multipart post with option to pass in map of form fields, basic authentication parameters or mutual SSL with specific certificate aliase and trust store
 */


    public static String doMultipartPOSTwithFile(String requestURL, String fileFormKey, String filepath,
                                                 HashMap<String, String> valueMap, String keystoreAlias,
                                                 KeyStore keyStore, KeyStore trustStore, String user, String pwd)
                                                        throws Exception {
        TrustManagerFactory tmf = null;
        SSLContext sslContext = null;
        KeyManager[] km = null;

        boolean secureSSL = Sigma.applicationProperties.getProperty("secure_ssl", "false").equalsIgnoreCase("true");

        if(secureSSL) {
            if (keyStore != null && keystoreAlias != null) {
                // client auth using specific certificate is requested
                km = new KeyManager[]{new SSLKeyManager(keyStore, keystoreAlias)};
            }

            if (trustStore != null) {
                // custom trust store is requested to be used instead of Android global trustore
                // FIXME is it better to use pinned certificate via SSLTrustManager as opposed to custom truststore ?
                tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                tmf.init(trustStore);
            }

            if (km != null || tmf != null) {
                // create custom SSL context to accept only TLSv1.2 protocol
                sslContext = SSLContext.getInstance("TLSv1.2");
                sslContext.init(km, tmf == null ? null : tmf.getTrustManagers(), null);
                //SSLSocketFactory secureSSLSocketFactory = new SecureSSLSocketFactory(sslContext.getSocketFactory());  // user our custom highly secure SSLSocketFactory forcing TLSv1.2
                //HttpsURLConnection.setDefaultSSLSocketFactory(secureSSLSocketFactory);
            }
        }

        String charset = "UTF-8";
        File uploadFile = new File(filepath);
        MultipartUtility multipart = new MultipartUtility(requestURL, charset, user, pwd, sslContext);
        multipart.addHeaderField("User-Agent", USER_AGENT);
        //multipart.addHeaderField("Test-Header", "Header-Value");

        if (valueMap != null) {
            for (String key : valueMap.keySet()) {
                multipart.addFormField(key, valueMap.get(key));
            }
        }
        multipart.addFilePart(fileFormKey, uploadFile);
        List<String> responses = multipart.finish();
        StringBuilder response = new StringBuilder(responses.size() * 100);
        for (String line : responses) {
            response.append(line).append("\n");
        }
        return response.toString();
    }

}