package com.crayonic.branding;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;

/**
 * Created by peter on 09/06/16.
 */
public class Config {
    private static final String TAG = "BrandConfig";
    /*<color name="primary">#607D8B</color>
        <color name="primary_dark">#455A64</color>
        <color name="primary_light">#CFD8DC</color>
        <color name="accent">#536DFE</color>
        <color name="primary_text">#212121</color>
        <color name="secondary_text">#727272</color>
        <color name="icons">#FFFFFF</color>
        <color name="divider">#B6B6B6</color>
        */
    public final int primary,primaryDark, primaryLight, primaryText, secondaryText, accent, icons, divider;
    public final int test = 0x0000ff00B ;//ColorStateList.valueOf(0);
    public final String fontPath;

    public Config(int primary, int primaryDark, int primaryLight, int primaryText, int secondaryText, int accent, int icons, int divider, String fontPath) {
        this.primary = primary;
        this.primaryDark = primaryDark;
        this.primaryLight = primaryLight;
        this.primaryText = primaryText;
        this.secondaryText = secondaryText;
        this.accent = accent;
        this.icons = icons;
        this.divider = divider;
        this.fontPath = fontPath;

        if(fontPath != null){
            File test = new File(fontPath);
            if(test.exists()){
                Log.i(TAG, "Loading font: " + fontPath);
                setDefaultFont("SANS_SERIF", fontPath);  // replaces Sans Serif font with given TTF file
            }
        }
    }


    public void setDefaultFont(String staticTypefaceFieldName, String filePath) {
        final Typeface regular = Typeface.createFromFile(filePath);
        replaceFont(staticTypefaceFieldName, regular);
    }

    protected static void replaceFont(final String staticTypefaceFieldName, final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
